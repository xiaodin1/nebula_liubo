create function "pipeline"."sys_func_format_e_notation"(data decimal ,digit int ) returns varchar
    language plpythonu
as
$$

def format_e_notation(data, digit):
    if data == None:
        return None
    pattern = '{:.'+str(digit)+'e}'
    return pattern.format(data)

if __name__ == "__main__":
    ret = format_e_notation(data,digit)
    return ret
$$;
