CREATE OR REPLACE FUNCTION "pipeline"."isnumeric"("txtstr" varchar)
  RETURNS "pg_catalog"."bool" AS $BODY$BEGIN
	-- Routine body goes here...
	RETURN txtStr ~ '^([0-9]+[.]?[0-9]*|[.][0-9]+)$';
END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100