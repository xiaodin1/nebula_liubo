CREATE OR REPLACE FUNCTION "pipeline"."cols_to_vec_char"(VARIADIC "arr" _text)
  RETURNS "pg_catalog"."_text" AS $BODY$
    def begin_alg(arr):
        lists = []
        for item in arr:
            if item != None:
                lists.append(item)
        return lists

    if __name__ == '__main__':
        global result
        ret = begin_alg(arr)
        return ret
$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100