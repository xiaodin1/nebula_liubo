-- auto-generated definition
create function sys_func_format_time(data character varying, pattern character varying) returns text
    language plpythonu
as
$$
import time
import re
from dateutil.parser import parse

TIMESTAMP_10 = r"^\d{10}(\.\d+)?$"
TIMESTAMP_13 = r"^\d{13}$"

def format_time(data, pattern):
    if data == None:
        return None

    data = data.replace("年", "-").replace("月", "-").replace(",", ", ")
    data = re.compile(
        r"日|Mon\.|Tue\.|Wed\.|Thur\.|Fri\.|Sat\.|Sun\.|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday").sub(
        "", data)
    if pattern == "timestamp":
        if re.match(TIMESTAMP_10, data):
            ret = data
        elif re.match(TIMESTAMP_13, data):
            ret = int(data) / 1000
        else:
            pattern = "%Y-%m-%d %H:%M:%S"
            ret = time.mktime(time.strptime(parse(data).strftime(pattern), pattern))
        return int(ret)
    else:
        if re.match(TIMESTAMP_10, data):
            return time.strftime(pattern, time.localtime(int(data)))
        elif re.match(TIMESTAMP_13, data):
            return time.strftime(pattern, time.localtime(float(data)/1000))
        else:
            return parse(data).strftime(pattern)

if __name__ == "__main__":
    ret = format_time(data, pattern)
    return ret
$$;

alter function sys_func_format_time(varchar, varchar) owner to gpadmin;

