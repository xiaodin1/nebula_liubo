%pyspark
from utilTool import *

# 1.引号中键入dollar sign $ 以获取父节点数据集名称
sourceTable = "";

# 2.将数据读取为spark数据集
dataframe = load_df_spark(sourceTable)

# 3.对数据进行自定义操作（需要至少保留列"_record_id_")
dataframe = dataframe.select(['_record_id_'])