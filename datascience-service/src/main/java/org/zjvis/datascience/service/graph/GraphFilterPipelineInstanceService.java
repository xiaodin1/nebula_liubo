package org.zjvis.datascience.service.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.graph.GraphFilterPipelineInstanceDTO;
import org.zjvis.datascience.service.mapper.GraphFilterPipelineInstanceMapper;

/**
 * @description GraphFilterPipelineInstanceService
 * @date 2021-12-29
 */
@Service
public class GraphFilterPipelineInstanceService {
    private static final Logger logger = LoggerFactory.getLogger(GraphFilterPipelineInstanceService.class);

    @Autowired
    private GraphFilterPipelineInstanceMapper graphFilterPipelineInstanceMapper;

    public Long save(GraphFilterPipelineInstanceDTO dto) {
        graphFilterPipelineInstanceMapper.save(dto);
        return dto.getId();
    }

    public boolean update(GraphFilterPipelineInstanceDTO dto) {
        return graphFilterPipelineInstanceMapper.update(dto);
    }

    public GraphFilterPipelineInstanceDTO queryById(Long id) {
        return graphFilterPipelineInstanceMapper.queryById(id);
    }

    public GraphFilterPipelineInstanceDTO queryLatestInstanceByGraphId(Long graphId) {
        return graphFilterPipelineInstanceMapper.queryLatestInstanceByGraphId(graphId);
    }

    public void setStatusSuccess(GraphFilterPipelineInstanceDTO instance, Long duringTime) {
        instance.setStatus("SUCCESS");
        instance.setDuringTime(duringTime);
        this.update(instance);
    }

    public void setStatusFail(GraphFilterPipelineInstanceDTO instance) {
        instance.setStatus("FAIL");
        this.update(instance);
    }

    public void setStatusRunning(GraphFilterPipelineInstanceDTO instance) {
        instance.setStatus("RUNNING");
        this.update(instance);
    }

    public void deleteByPipelineId(Long graphFilterPipelineId) {
        graphFilterPipelineInstanceMapper.deleteByPipelineId(graphFilterPipelineId);
    }

    public void deleteByGraphId(Long graphId) {
        graphFilterPipelineInstanceMapper.deleteByGraphId(graphId);
    }
}
