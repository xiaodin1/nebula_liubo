package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.PipelineDTO;

import java.util.List;
import java.util.Map;

/**
 * @description 数据视图pipeline信息表，数据视图pipeline Mapper
 * @date 2021-10-20
 */
@Component
public interface PipelineMapper {

    boolean save(PipelineDTO pipeline);

    boolean update(PipelineDTO pipeline);

    PipelineDTO queryById(Long id);

    List<PipelineDTO> queryByProject(Long projectId);

    void delete(Map<String, Object> params);

    void deleteByProject(long projectId);

    List<PipelineDTO> queryByProjectAndUser(@Param("projectId") Long projectId, @Param("userId") Long userId);

    int countPipelineInSpecificProject(@Param("projectId") Long projectId, @Param("userId") Long userId);

    Long getDefaultPipelineId(@Param("projectId") Long projectId);
}
