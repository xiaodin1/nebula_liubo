package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.vo.user.PhoneBindVO;
import org.zjvis.datascience.common.vo.user.UserModifyPasswordVO;

import java.util.List;
import java.util.Set;

/**
 * @description User 数据表Mapper
 * @date 2021-11-23
 */
@Component
public interface UserMapper {

    int deleteByPrimaryKey(@Param("id") Long id);

    int insert(UserDTO record);

    int insertSelective(UserDTO record);

    UserDTO selectByPrimaryKey(Long id);

    //仅用做生成用户cookie，查到的字段不全
    List<UserDTO> select(UserDTO record);

    List<UserDTO> selectByName(@Param("name") String name);

    int updateByPrimaryKeySelective(UserDTO record);

    List<UserDTO> listByIds(Set<Long> userIds);

    UserDTO findByKey(String key);

    int updatePassword(UserModifyPasswordVO user);

    int updateGuideStatus(Long id);

    int updatePhone(UserDTO user);
}
