package org.zjvis.datascience.service.mapper;

import com.alibaba.fastjson.JSONObject;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.DatasetAndCategoryDTO;
import org.zjvis.datascience.common.dto.DatasetCategoryDTO;

import java.util.List;

/**
 * @description 数据集类型信息表，数据集类型Mapper
 * @date 2020-07-31
 */
@Mapper
public interface DatasetCategoryMapper {

    List<DatasetCategoryDTO> queryByUserId(Long userId);

    int save(DatasetCategoryDTO dcDTO);

    void update(DatasetCategoryDTO dcDTO);

    int delete(DatasetCategoryDTO dc);

    List<DatasetAndCategoryDTO> queryDatasetAndCategoryByUserId(Long userId);

    List<DatasetAndCategoryDTO> queryDatasetCategoryByProjectId(JSONObject data);

    int checkAuth(@Param("id") long id, @Param("userId") long userId);

    DatasetCategoryDTO queryById(Long id);

    DatasetCategoryDTO queryByUserIdAndName(@Param("userId") Long userId, @Param("name") String name);
}
