package org.zjvis.datascience.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.TaskSnapshotDTO;

/**
 * @description 任务节点Task快照信息表，任务节点快照Mapper
 * @date 2021-09-06
 */
@Mapper
public interface TaskSnapshotMapper {

    void batchAdd(List<TaskSnapshotDTO> taskSnapshots);

    List<TaskSnapshotDTO> queryByPipelineAndSnapshotId(@Param("pipelineId") Long pipelineId,
        @Param("pipelineSnapshotId") Long pipelineSnapshotId);

    int deleteByPipelineSnapshotId(List<Long> pipelineSnapshotId);
}
