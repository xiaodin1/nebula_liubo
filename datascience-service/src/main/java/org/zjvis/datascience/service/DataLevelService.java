package org.zjvis.datascience.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.DataLevelDTO;
import org.zjvis.datascience.service.mapper.DataLevelMapper;

/**
 * @description DataLevel 数据等级 Service
 * @date 2021-09-17
 */
@Service
public class DataLevelService {

    private final static Logger logger = LoggerFactory.getLogger("DataLevelService");

    @Autowired
    private DataLevelMapper dataLevelMapper;

    public List<DataLevelDTO> list() {
        return dataLevelMapper.list();
    }

}
