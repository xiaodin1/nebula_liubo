package org.zjvis.datascience.service.csv.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import lombok.experimental.SuperBuilder;

/**
 * @description 文件解析错误代码ErrorLineCodeInfo
 * @date 2021-12-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorLineCodeInfo {
    private List<String> lines;

    private int count;
}
