package org.zjvis.datascience.service.mapper;

import org.zjvis.datascience.common.dto.graph.GraphActionDTO;

import java.util.List;

/**
 * @description GraphAction 数据表Mapper
 * @date 2021-08-20
 */
public interface GraphActionMapper {
    boolean save(GraphActionDTO dto);

    boolean update(GraphActionDTO dto);

    GraphActionDTO queryById(Long id);

    List<GraphActionDTO> queryByGraphId(Long graphId);

    void delete(Long id);

    void deleteByGraphId(Long graphId);

    List<GraphActionDTO> queryLatestActionByGraphId(Long graphId, Long limitNum);
}
