package org.zjvis.datascience.service.dag;

import lombok.Data;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;

import java.util.concurrent.Future;

/**
 * @description 异步任务结果绑定对象
 * @date 2021-12-24
 */
@Data
public class TaskFuture {

    private TaskInstanceDTO instance;
    private Future<TaskRunnerResult> future;

    public TaskFuture(TaskInstanceDTO instance, Future<TaskRunnerResult> future) {
        this.instance = instance;
        this.future = future;
    }

}
