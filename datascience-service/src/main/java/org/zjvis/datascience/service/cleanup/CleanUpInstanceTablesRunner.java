package org.zjvis.datascience.service.cleanup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.service.TaskInstanceService;

/**
 * @description 清洗操作 历史记录清除线程
 * @date 2021-08-20
 */
public class CleanUpInstanceTablesRunner implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger("CleanUpInstanceTablesRunner");

    /**
     * 待删除的gp表名 ","分隔
     */
    private Long taskId;

    private TaskInstanceService taskInstanceService;

    public CleanUpInstanceTablesRunner(Long taskId, TaskInstanceService taskInstanceService) {
        this.taskId = taskId;
        this.taskInstanceService = taskInstanceService;
    }

    @Override
    public void run() {
//        taskInstanceService.drop
//        if (StringUtils.isNotEmpty(tables)) {
//            gpDataProvider.dropRedundantTables(tables, false);
//            logger.info(String.format("CleanUpInstanceTablesRunner cleanup historyRecords finish. taskId=%s", taskId));
//        }
    }
}
