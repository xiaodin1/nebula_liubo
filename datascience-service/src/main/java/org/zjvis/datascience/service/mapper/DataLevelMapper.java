package org.zjvis.datascience.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.DataLevelDTO;

/**
 * @description 数据等级信息表，数据等级Mapper
 * @date 2021-01-28
 */
@Mapper
public interface DataLevelMapper {

    List<DataLevelDTO> list();
}
