package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.user.UserDataInfoDTO;
import org.zjvis.datascience.common.dto.user.UserInfoDTO;
import org.zjvis.datascience.common.vo.UserSelectVO;

import java.util.List;

/**
 * @description UserInfo 数据表Mapper
 * @date 2021-08-22
 */
@Mapper
public interface UserInfoMapper {
    List<UserInfoDTO> selectAllUserInfo(UserSelectVO userSelectVO);

    List<UserDataInfoDTO> selectAllUserDatasetInfo(UserSelectVO userSelectVO);
}
