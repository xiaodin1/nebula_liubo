package org.zjvis.datascience.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.user.UserConfigDTO;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.vo.user.UserConfigVO;
import org.zjvis.datascience.service.mapper.UserConfigMapper;

/**
 * @description UserConfig 用户设置 Service
 * @date 2021-12-26
 */
@Service
public class UserConfigService {

    @Autowired
    private UserConfigMapper userConfigMapper;

    public UserConfigDTO query(long userId) {
        UserConfigDTO dto = userConfigMapper.queryByUser(userId);
        if (dto == null) {
            //如果没有配置，创建一个默认的配置
            userConfigMapper.insert(userId);
            dto = userConfigMapper.queryByUser(userId);
        }
        return dto;
    }

    public Boolean update(UserConfigVO vo) {
        vo.setUserId(JwtUtil.getCurrentUserId());
        return userConfigMapper.update(vo)>0?true:false;
    }
}
