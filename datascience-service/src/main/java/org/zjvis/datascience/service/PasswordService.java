package org.zjvis.datascience.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @description Password 密码 Service
 * @date 2021-07-21
 */
@Service
@Getter
public class PasswordService {

    @Value("${rsa.privateKey}")
    private String privateKey;

    @Value("${rsa.publicKey}")
    private String publicKey;

    public String decryptPassword(String password) {
        return password;
//        RSA rsa = new RSA(privateKey, null);
//        byte[] decryptPassword;
//        try {
//            decryptPassword = rsa.decrypt(password, KeyType.PrivateKey);
//        } catch (Exception e) {
//            throw new DataScienceException(BaseErrorCode.USER_PASSWORD_ERROR.getMsg());
//        }
//        return new String(decryptPassword);
    }

}
