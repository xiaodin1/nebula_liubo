package org.zjvis.datascience.service.mapper;

import java.util.List;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.PipelineInstanceDTO;

/**
 * @description 数据视图pipeline实例信息表，数据视图pipeline 实例Mapper
 * @date 2021-09-17
 */
@Component
public interface PipelineInstanceMapper {

    boolean save(PipelineInstanceDTO pipelineInstanceDTO);

    boolean update(PipelineInstanceDTO pipelineInstanceDTO);

    PipelineInstanceDTO queryById(Long id);

    List<PipelineInstanceDTO> queryByPipelineId(Long pipelineId);

    PipelineInstanceDTO getLatestInstance(Long piplineId);
}
