package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;
import org.zjvis.datascience.common.dto.TaskInstancePageQueryDTO;
import org.zjvis.datascience.common.dto.TaskInstanceQueryDTO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 任务节点实例 TaskInstance 信息表，任务节点实例Mapper
 * @date 2021-10-21
 */
@Component
public interface TaskInstanceMapper {

    boolean save(TaskInstanceDTO taskInstanceDTO);

    boolean update(TaskInstanceDTO taskInstanceDTO);

    List<TaskInstanceDTO> queryBySessionId(Long sessionId);

    List<TaskInstanceDTO> queryByTaskId(Long taskId);

    TaskInstanceDTO queryById(Long id);

    TaskInstanceDTO queryLatestInstanceForTask(Long taskId);

    TaskInstanceDTO queryBySpecTaskInstance(Map<String, Long> map);

    List<Long> queryTaskIdHaveRedundantTable(Map<String, Object> map);

    List<TaskInstanceDTO> queryRedundantTableByTaskId(Map<String, Object> map);

    List<TaskInstanceDTO> querySuccessInstanceByTaskId(Long taskId);

    List<Long> queryIdByTaskId(@Param("taskIds") List<Long> taskIds);

    boolean deleteActions(Map<String, Object> map);

    List<TaskInstanceDTO> queryByTaskIdAndOrder(Long taskId);

    void delete(Map<String, Object> params);

    List<TaskInstanceDTO> queryByTaskIdsAndOrder(@Param("taskIds") List<Long> taskIds);

    int batchSave(@Param("taskInstances") List<TaskInstanceDTO> taskInstances);

    List<String> queryAlgoRecentLogs(@Param("taskId") Long taskId, @Param("projectId") Long projectId);

    List<TaskInstanceQueryDTO> queryTaskInstanceInfo(TaskInstancePageQueryDTO dto);

    List<String> queryParentTaskInstanceId(Long id);

    List<String> queryTaskInstanceSourceDatasetName(@Param("taskInstanceIds") Set<Long> pids);

    List<Long> queryRedundantTableInCleanNode(@Param("limitDate") String limitDate);

    void deleteHistoryActions(@Param("taskId") Long taskId);

    void deleteByProjectId(Long projectId);

    void deleteByPipelineId(Long pipelineId);
}
