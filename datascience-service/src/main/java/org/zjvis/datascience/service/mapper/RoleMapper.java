package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.RoleDTO;

import java.util.List;

/**
 * @description 角色信息表，角色Mapper
 * @date 2021-10-21
 */
@Component
public interface RoleMapper {

    List<RoleDTO> list();

    RoleDTO getRoleById(Integer roleId);

    List<RoleDTO> listAll();
}
