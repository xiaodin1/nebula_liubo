package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.user.UserConfigDTO;
import org.zjvis.datascience.common.vo.user.UserConfigVO;

/**
 * @description UserConfig 数据表Mapper
 * @date 2021-09-03
 */
@Mapper
public interface UserConfigMapper {

    int update(UserConfigVO vo);

    UserConfigDTO queryByUser(long userId);

    void insert(long userId);

    void deleteByUser(long userId);
}
