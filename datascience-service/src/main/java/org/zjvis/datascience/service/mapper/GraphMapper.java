package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;
import org.zjvis.datascience.common.dto.graph.GraphDTO;

import java.util.List;
import java.util.Map;

/**
 * @description Graph 数据表Mapper
 * @date 2021-08-20
 */
@Component
public interface GraphMapper {
    boolean save(GraphDTO graph);

    boolean update(GraphDTO graph);

    GraphDTO queryByTaskId(Long id);

    GraphDTO queryById(Long id);

    void deleteByTaskId(Long taskId);

    void deleteById(Long id);

    List<TaskInstanceDTO> queryActionByParentIdAndOrder(String cid);

    TaskInstanceDTO queryLatestActionByParentId(String cid);

    //删除id>=#id的记录
    void deleteActionsByParentIdById(Map<String, Object> map);

    List<GraphDTO> queryByProjectId(Long id);

    List<GraphDTO> queryByProjectIdFull(Long id);

    List<String> queryNamesByProjectId(Long id);

    Long queryUserById(Long id);

    Long queryProjectById(Long id);


}
