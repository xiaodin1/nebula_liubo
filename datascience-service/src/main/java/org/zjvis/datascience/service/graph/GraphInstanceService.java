package org.zjvis.datascience.service.graph;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.graph.GraphInstanceDTO;
import org.zjvis.datascience.common.exception.BaseErrorCode;
import org.zjvis.datascience.common.exception.DataScienceException;
import org.zjvis.datascience.service.mapper.GraphInstanceMapper;

/**
 * @description GraphInstanceService
 * @date 2021-12-29
 */
@Service
public class GraphInstanceService {
    @Autowired
    private GraphInstanceMapper graphInstanceMapper;


    public Long save(GraphInstanceDTO graphInstance) {
        graphInstanceMapper.save(graphInstance);
        return graphInstance.getId();
    }

    public boolean update(GraphInstanceDTO graphInstance) {
        return graphInstanceMapper.update(graphInstance);
    }

    public GraphInstanceDTO queryById(Long id) {
        return graphInstanceMapper.queryById(id);
    }

    public GraphInstanceDTO queryLatestInstanceByGraphId(Long graphId) {
        return graphInstanceMapper.queryLatestInstanceByGraphId(graphId);
    }

    public String checkStatus(Long id) {
        GraphInstanceDTO graphInstance = this.queryById(id);
        if (graphInstance == null) {
            throw DataScienceException.of(BaseErrorCode.GRAPH_NO_BUILD_RECORD, " id: " + id);
        }
        return graphInstance.getStatus();
    }

    public String queryLatestStatus(Long graphId) {
        GraphInstanceDTO graphInstance = graphInstanceMapper.queryLatestInstanceByGraphId(graphId);
        if (graphInstance == null) {
            throw DataScienceException.of(BaseErrorCode.GRAPH_NO_BUILD_RECORD, " graphId: " + graphId);
        }
        return graphInstance.getStatus();
    }

    public void setStatusRunning(Long id) {
        GraphInstanceDTO graphInstance = this.queryById(id);
        graphInstance.setStatus("RUNNING");
        this.update(graphInstance);
    }

    public void setStatusRunning(GraphInstanceDTO graphInstance) {
        graphInstance.setStatus("RUNNING");
        this.update(graphInstance);
    }

    public void setStatusSuccess(Long id, Long duringTime, String graphName) {
        GraphInstanceDTO graphInstance = this.queryById(id);
        graphInstance.setStatus("SUCCESS");
        graphInstance.setDuringTime(duringTime);
        JSONObject data = new JSONObject();
        data.put("janusGraphName", graphName);
        graphInstance.setLogInfo(data.toJSONString());
        this.update(graphInstance);
    }

    public void setStatusSuccess(GraphInstanceDTO graphInstance, Long duringTime, String graphName) {
        graphInstance.setStatus("SUCCESS");
        graphInstance.setDuringTime(duringTime);
        JSONObject data = new JSONObject();
        data.put("janusGraphName", graphName);
        graphInstance.setLogInfo(data.toJSONString());
        this.update(graphInstance);
    }

    public void setStatusFail(Long id, String graphName, String errorMsg) {
        GraphInstanceDTO graphInstance = this.queryById(id);
        graphInstance.setStatus("FAIL");
        JSONObject data = new JSONObject();
        data.put("janusGraphName", graphName);
        data.put("errorMsg", errorMsg);
        graphInstance.setLogInfo(data.toJSONString());
        this.update(graphInstance);
    }

    public void setStatusFail(GraphInstanceDTO graphInstance, String graphName, String errorMsg) {
        graphInstance.setStatus("FAIL");
        JSONObject data = new JSONObject();
        data.put("janusGraphName", graphName);
        data.put("errorMsg", errorMsg);
        graphInstance.setLogInfo(data.toJSONString());
        this.update(graphInstance);
    }
}
