package org.zjvis.datascience.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.enums.PythonDateTypeFormatEnum;
import org.zjvis.datascience.service.dataprovider.GPDataProvider;

/**
 * @description Date 日期 Service
 * @date 2021-10-18
 */
@Service
public class DateService {

    @Autowired
    private GPDataProvider gpDataProvider;

    public JSONArray getDateOrder(String table, String col) {
        JSONArray order = new JSONArray();
        String sql = String
            .format("select distinct %s from %s order by pipeline.sys_func_format_time(\"%s\"::varchar, '%s')", col,
                table, col, PythonDateTypeFormatEnum.FORMAT_0.getVal());
        JSONArray pairs = gpDataProvider.executeQuery(sql);
        for (Object pair : pairs) {
            order.add(((JSONObject) pair).getString(col));
        }
        return order;
    }
}
