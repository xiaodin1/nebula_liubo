package org.zjvis.datascience.service.csv;

import lombok.Data;
import org.zjvis.datascience.service.csv.semiotic.Escape;
import org.zjvis.datascience.service.csv.semiotic.Quote;
import org.zjvis.datascience.service.csv.semiotic.Separate;

/**
 * @description CSV 文件符号处理
 * @date 2021-12-29
 */
@Data
public class CsvSemiotic {
    private Separate separate = Separate.NULL;  //
    private Quote quote = Quote.NULL;
    private Escape escape = Escape.NULL;
    private String otherSeparate = "";
    private String otherQuote = "";
    private String otherEscape = "";

    public CsvSemiotic(String separate, String quote, String escape) {
        if (!"".equals(separate)) {
            if (separate != null) {
                for (Separate value : Separate.values()) {
                    if (value.getSymbol().equals(separate)) {
                        this.separate = value;
                        break;
                    }
                }
                if (this.separate == Separate.NULL) otherSeparate = separate;
            } else {
                this.separate = Separate.NULL;
            }
        }
        if (!"".equals(quote)) {
            if (quote != null) {
                for (Quote value : Quote.values()) {
                    if (value.getSymbol().equals(quote)) {
                        this.quote = value;
                        break;
                    }
                }
                if (this.quote == Quote.NULL) otherQuote = quote;
            } else {
                this.quote = Quote.NULL;
            }
        }
        if (!"".equals(escape)) {
            if (escape != null) {
                for (Escape value : Escape.values()) {
                    if (value.getSymbol().equals(escape)) {
                        this.escape = value;
                        break;
                    }
                }
                if (this.escape == Escape.NULL) otherEscape = escape;
            } else {
                this.escape = Escape.NULL;
            }
        }
    }
}
