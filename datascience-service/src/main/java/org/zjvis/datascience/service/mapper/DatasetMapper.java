package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.DatasetDTO;
import org.zjvis.datascience.common.dto.dataset.DatasetWithCategoryDTO;

import java.util.List;

/**
 * @description 数据集信息表，数据集Mapper
 * @date 2021-10-19
 */
@Component
public interface DatasetMapper {

    int delete(DatasetDTO datasetDTO);

    List<DatasetDTO> queryByCategoryId(Long categoryId);

    DatasetDTO queryById(Long datasetId);

    int update(DatasetDTO datasetDTO);

    List<DatasetDTO> queryByProjectId(Long projectId);

    void save(DatasetDTO dataset);

    int checkAuth(@Param("id") long id, @Param("userId") long userId);

    DatasetDTO selectOne(DatasetDTO datasetDTO);

    List<DatasetDTO> queryByUserId(Long userId);

    List<DatasetWithCategoryDTO> queryDetailByUserId(Long userId);

    List<DatasetDTO> getAllTableDataSourceNeedScheduled();

    List<DatasetDTO> getAllUrlStatsOn();

    int updateIncrementalConfig(DatasetDTO datasetDTO);

    int countByCategoryIdAndDatasetName(@Param("categoryId") Long categoryId,
                                        @Param("datasetName") String datasetName);

    DatasetDTO queryByCategoryIdAndDatasetName(@Param("categoryId") Long categoryId,
                                               @Param("datasetName") String datasetName);

    List<String> selectLikeDataName(@Param("datasetName") String datasetName);
}
