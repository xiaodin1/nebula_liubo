package org.zjvis.datascience.service.mapper;

import org.zjvis.datascience.common.dto.graph.GraphFilterPipelineInstanceDTO;

/**
 * @description GraphFilterPipelineInstance 数据表Mapper
 * @date 2021-08-20
 */
public interface GraphFilterPipelineInstanceMapper {

    boolean save(GraphFilterPipelineInstanceDTO dto);

    boolean update(GraphFilterPipelineInstanceDTO dto);

    GraphFilterPipelineInstanceDTO queryById(Long id);

    GraphFilterPipelineInstanceDTO queryLatestInstanceByGraphId(Long graphId);

    boolean deleteByPipelineId(Long graphFilterPipelineId);

    boolean deleteByGraphId(Long graphId);


}
