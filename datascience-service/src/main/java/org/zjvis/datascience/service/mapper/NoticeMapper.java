package org.zjvis.datascience.service.mapper;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.notice.NoticeDTO;
import org.zjvis.datascience.common.dto.notice.NoticeQueryDTO;

/**
 * @description Notice 数据表Mapper
 * @date 2021-08-20
 */
@Mapper
public interface NoticeMapper {

    void save(NoticeDTO dto);

    List<NoticeQueryDTO> query(NoticeDTO dto);

    int delete(@Param("userId") Long userId, @Param("ids") Set<Long> ids);

    int deleteAll(@Param("userId") long currentUserId, @Param("status") Integer status);

    int batchUpdateStatus(@Param("userId") long currentUserId,@Param("ids") Set<Long> ids,@Param("status") int istatus);
}
