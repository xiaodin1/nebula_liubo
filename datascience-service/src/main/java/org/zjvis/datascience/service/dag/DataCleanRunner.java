package org.zjvis.datascience.service.dag;

import org.zjvis.datascience.common.dto.TaskInstanceDTO;
import org.zjvis.datascience.service.TColumnService;

import java.util.concurrent.Callable;

import static org.zjvis.datascience.common.constant.Constant.errorTpl;

/**
 * @description 清洗任务调度器
 * @date 2021-12-24
 */
public class DataCleanRunner implements Callable<TaskRunnerResult> {

    private TColumnService tColumnService;

    private TaskInstanceDTO instance;

    private TaskManager taskManager;

    public DataCleanRunner(TColumnService tColumnService, TaskInstanceDTO instance, TaskManager taskManager) {
        this.tColumnService = tColumnService;
        this.instance = instance;
        this.taskManager = taskManager;
    }

    @Override
    public TaskRunnerResult call() {
        if (instance.hasPrecautionaryError()) {
            return new TaskRunnerResult(500, String.format(errorTpl, "error happens when init stage."));
        }
        TaskRunnerResult result = null;
        try {
            result = tColumnService.execDataClean(instance, taskManager);
        } catch (Exception e) {
            result = new TaskRunnerResult(500, e.getMessage());
        }
        return result;
    }
}
