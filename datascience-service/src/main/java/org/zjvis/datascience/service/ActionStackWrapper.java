package org.zjvis.datascience.service;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description Action操作栈包装器
 * @date 2021-12-23
 */
@Data
@Component
public class ActionStackWrapper {

    private Map<Long, ActionDeStack> stackWrapper = new ConcurrentHashMap<>();

    public ActionDeStack getActionDeStackByProjectId(Long projectId) {
        return stackWrapper.getOrDefault(projectId, null);
    }

    public ActionDeStack getActionDeStackByPipelineId(Long pipelineId) {
        return stackWrapper.getOrDefault(pipelineId, null);
    }

    public boolean initStackForProjectId(Long projectId, ActionService actionService) {
        if (!stackWrapper.containsKey(projectId)) {
            ActionDeStack actionDeStack = new ActionDeStack(projectId);
            actionDeStack.initUndoStack(actionService);
            stackWrapper.put(projectId, actionDeStack);
        }
        return true;
    }

    public boolean initStackForPipelineId(Long pipelineId, ActionService actionService) {
        if (!stackWrapper.containsKey(pipelineId)) {
            ActionDeStack actionDeStack = new ActionDeStack(pipelineId);
            actionDeStack.initUndoStack(actionService);
            stackWrapper.put(pipelineId, actionDeStack);
        }
        return true;
    }
}