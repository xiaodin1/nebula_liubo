package org.zjvis.datascience.service.graph;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description GraphActionStackWrapper
 * @date 2021-12-29
 */
@Data
@Component
public class GraphActionStackWrapper {

    private Map<Long, GraphActionDeStack> stackWrapper = new ConcurrentHashMap<>();

    public GraphActionDeStack getActionDeStackByGraphId(Long graphId) {
        return stackWrapper.getOrDefault(graphId, null);
    }

    public boolean initStackForProjectId(Long graphId, GraphActionService graphActionService) {
        if (!stackWrapper.containsKey(graphId)) {
            GraphActionDeStack actionDeStack = new GraphActionDeStack(graphId);
            actionDeStack.initUndoStack(graphActionService);
            stackWrapper.put(graphId, actionDeStack);
        }
        return true;
    }
}
