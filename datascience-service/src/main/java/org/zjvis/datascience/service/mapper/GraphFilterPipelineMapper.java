package org.zjvis.datascience.service.mapper;

import org.zjvis.datascience.common.dto.graph.GraphFilterPipelineDTO;

import java.util.List;

/**
 * @description GraphFilterPipeline 数据表Mapper
 * @date 2021-08-20
 */
public interface GraphFilterPipelineMapper {
    boolean save(GraphFilterPipelineDTO dto);

    boolean update(GraphFilterPipelineDTO dto);

    boolean deleteById(Long id);

    GraphFilterPipelineDTO queryById(Long id);

    List<GraphFilterPipelineDTO> queryByGraphId(Long id);

    boolean deleteByGraphId(Long graphId);

    Long queryUserById(Long id);
}
