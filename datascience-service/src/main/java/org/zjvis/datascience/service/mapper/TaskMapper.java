package org.zjvis.datascience.service.mapper;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.TaskDTO;

import java.util.List;
import java.util.Map;

/**
 * @description 任务节点Task信息表，任务节点Mapper
 * @date 2021-12-17
 */
@Mapper
public interface TaskMapper {

    boolean save(TaskDTO task);

    boolean update(TaskDTO task);

    TaskDTO queryById(Long id);

    TaskDTO queryByParentId(Long parentId);

    List<TaskDTO> queryByPipeline(Long pipelineId);

    void delete(Map<String, Object> params);

    void deleteByProjectId(Long projectId);

    void deleteByPipelineId(Long pipelineId);

    List<Long> queryIdByProjectId(Long projectId);

    List<TaskDTO> queryByDatasetId(Map<String, Long> param);

    List<Long> queryIdByParentId(Long parentId);

    List<Long> queryIdByPipeline(Long pipelineId);

    List<Long> queryByTableNameAndPipelineId(@Param("tableName") String tableName, @Param("pipelineId") Long pipelineId);

    int batchSave(@Param("tasks") List<TaskDTO> tasks);

    List<Long> returnAllChildrenIds(Long taskId, Long projectId);

    JSONObject queryJsonObjectByIdAndKey(Long taskId, String keyPath);

    JSONArray queryJsonArrayByIdAndKey(Long taskId, String keyPath);

}
