package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.PipelineSnapshotDTO;

import java.util.List;

/**
 * @description 数据视图pipeline快照信息表，数据视图pipeline 快照Mapper
 * @date 2021-05-25
 */
@Mapper
public interface PipelineSnapshotMapper {

    void save(PipelineSnapshotDTO pipelineSnapshot);

    PipelineSnapshotDTO queryById(Long id);

    List<PipelineSnapshotDTO> queryByPipeline(Long pipelineId);

    int delete(List<Long> ids);

    int deleteByPipeline(Long pipelineId);

    int deleteByProject(Long projectId);

    int countByPipelineAndName(@Param("pipelineId") Long pipelineId,
                               @Param("name") String pipelineSnapshotName);

    int countByPipelineId(Long pipelineId);

    PipelineSnapshotDTO queryOldestPipelineSnapshot(Long pipelineId);

    List<PipelineSnapshotDTO> querySnapshotByPipeline(Long pipelineId);
}
