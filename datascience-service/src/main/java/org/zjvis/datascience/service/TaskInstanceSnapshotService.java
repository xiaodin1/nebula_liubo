package org.zjvis.datascience.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.TaskInstanceSnapshotDTO;
import org.zjvis.datascience.service.mapper.TaskInstanceSnapshotMapper;

/**
 * @description TaskInstanceSnapshot  任务节点实例快照服务 Service
 * @date 2021-12-29
 */
@Service
public class TaskInstanceSnapshotService {

    @Autowired
    TaskInstanceSnapshotMapper taskInstanceSnapshotMapper;

    public void batchAdd(List<TaskInstanceSnapshotDTO> taskInstanceSnapshots) {
        taskInstanceSnapshotMapper.batchAdd(taskInstanceSnapshots);
    }

    public List<TaskInstanceSnapshotDTO> queryByPipelineAndSnapshotId(Long pipelineId, Long pipelineSnapshotId) {
        return taskInstanceSnapshotMapper.queryByPipelineAndSnapshotId(pipelineId,pipelineSnapshotId);
    }

    public boolean deleteByPipelineSnapshotId(List<Long> asList) {
        int res = taskInstanceSnapshotMapper.deleteByPipelineSnapshotId(asList);
        return res>0?true:false;
    }
}
