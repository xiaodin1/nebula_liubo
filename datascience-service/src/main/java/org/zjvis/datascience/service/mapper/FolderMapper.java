package org.zjvis.datascience.service.mapper;


import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.FolderDTO;
import org.zjvis.datascience.common.dto.MLModelDTO;
import org.zjvis.datascience.common.vo.FolderVO;
import org.zjvis.datascience.common.vo.MLModelVO;

import java.util.List;

/**
 * @description Folder 数据表Mapper
 * @date 2021-08-20
 */
@Component
public interface FolderMapper {

    boolean save(FolderDTO dto);

    void updateFolder(FolderDTO dto);

    List<MLModelDTO> queryFolder(FolderVO vo);

    List<FolderDTO> getFolders(MLModelVO vo);

    FolderDTO getFolderById(MLModelVO vo);

    List<MLModelVO> queryModelByFolderId(Long folderId);

    void deleteFolder(FolderDTO dto);

    List<FolderDTO> queryFolderByUser(FolderDTO dto);

    FolderDTO queryById(Long folderId);
}
