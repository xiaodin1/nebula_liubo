package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.ProjectDTO;
import org.zjvis.datascience.common.dto.ProjectStatusDTO;

import java.util.List;
import java.util.Set;

/**
 * @description 项目信息表，项目 Mapper
 * @date 2021-09-23
 */
@Component
public interface ProjectMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ProjectDTO record);

    int insertSelective(ProjectDTO record);

    ProjectDTO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProjectDTO record);

    int updateByPrimaryKey(ProjectDTO record);

    List<ProjectDTO> select(ProjectDTO record);

    List<ProjectDTO> list(@Param("ids") List<Long> ids);

    /**
     * 查询用户拥有相应权限的项目
     *
     * @param userId
     * @param auth   读1 写2，不需要权限传null
     * @return List<ProjectDTO>
     */
    List<ProjectDTO> selectByUser(Long userId, Byte auth, String order);

    List<ProjectDTO> page(ProjectDTO pv);

    ProjectStatusDTO getProjectStatus(@Param(value = "id") Long id);

    List<ProjectDTO> queryByIds(@Param("ids") Set<Long> ids);

    int countPipelineInThisProject(long projectId, Long userId);
}
