package org.zjvis.datascience.service.cleanup;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.service.dataprovider.GPDataProvider;

/**
 * @description 清洗操作 推荐历史记录清除线程
 * @date 2021-10-15
 */
public class CleanUpRecommendTablesRunner implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger("CleanUpRecommendTablesRunner");

    /**
     * 待删除的gp表名 ","分隔
     */
    private String tables;

    private GPDataProvider gpDataProvider;

    public CleanUpRecommendTablesRunner(String tables, GPDataProvider gpDataProvider) {
        this.tables = tables;
        this.gpDataProvider = gpDataProvider;
    }

    @Override
    public void run() {
        if (StringUtils.isNotEmpty(tables)) {
            gpDataProvider.dropRedundantTables(tables, false);
            logger.info(String.format("CleanUpRecommendTablesRunner cleanup recommendTables task finish. tables=%s", tables));
        }
    }
}
