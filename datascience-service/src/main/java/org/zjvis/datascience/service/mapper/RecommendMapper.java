package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.RecommendDTO;

import java.util.List;
import java.util.Map;

/**
 * @description 语义推荐信息表，语义推荐Mapper
 * @date 2020-09-14
 */
@Mapper
public interface RecommendMapper {

    boolean save(RecommendDTO dto);

    boolean update(RecommendDTO dto);

    RecommendDTO queryById(Long id);

    List<RecommendDTO> queryByModify(Map map);
}
