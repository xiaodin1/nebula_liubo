package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.datasetsinfo.DatasetsInfoDTO;

import java.util.List;

/**
 * @description Dashboard 数据表Mapper 用于按条件查询当前用户所有的元数据信息
 * @date 2021-04-08
 */
@Mapper
public interface DatasetsInfoMapper {

    List<DatasetsInfoDTO> selectBy(long userId, DatasetsInfoDTO dataInfo);

}
