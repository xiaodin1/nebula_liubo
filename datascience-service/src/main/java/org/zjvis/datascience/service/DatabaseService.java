package org.zjvis.datascience.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.DatabaseDTO;
import org.zjvis.datascience.service.mapper.DatabaseMapper;

/**
 * @description Dashboard画布 Service
 * @date 2021-09-17
 */
@Service
public class DatabaseService {

    @Autowired
    private DatabaseMapper databaseMapper;

    public DatabaseDTO queryById(Long databaseId) {
        return databaseMapper.queryById(databaseId);
    }

}
