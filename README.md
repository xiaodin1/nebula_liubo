# 天枢可视分析平台 (Nebula)

## 介绍

* 天枢可视分析平台是致力于探寻数据价值的大数据可视分析平台，支持多种类型和来源的数据接入，可高效进行交互式数据清洗、分析与挖掘，并实现高性能的数据可视化
* 用户可以进[入天枢可视分析平台官网](https://jianwei.zjvis.net/intro)查看更多关于平台的介绍

## 核心功能

1. 智能数据整合
    * ![intelligent-data-integration](resource/intelligent-data-integration.png)
2. 交互数据分析
    * ![interactive-data-analysis](resource/interactive-data-analysis.png)
3. 敏捷模型构建
    * ![fast-model-building](resource/fast-model-building.png)
4. 海量数据支持
    * ![massive-data-supporting](resource/massive-data-supporting.png)
5. 图构建/图分析
    * ![massive-data-supporting](resource/graph-analysis.png)
6. 系统快速定制
    * ![massive-data-supporting](resource/bi-visualization.png)

## 安装

### 外部依赖组件

* 这些组件的容器化过程在开发中，后续我们将提供自动化搭建过程
* [搭建CDH-6.3.1离线](resource/installation/CDH-6.3.1-installation.md) 以提供
    + spark (配合安装 spark job server)
    + yarn resourcemanager 和 webapp
* [搭建 greenplum](resource/installation/greenplum-installation.md)
    + 搭建完毕后使用任意postgres 客户端，上传三张表到dataset下
    + [\_city_mapper\_](resource/greenplum/_city_mapper_.csv)
    + [\_country_mapper\_](resource/greenplum/_country_mapper_.csv)
    + [\_province_mapper\_](resource/greenplum/_province_mapper_.csv)
* [搭建 algo-py](algo-py/README.md)

### 开箱即用

1. 前置条件
    * jdk 8
    * docker engine (linux/amd64)
2. 克隆代码
    * ```shell
      git clone --single-branch --branch opensource $GIT_REPO_ADDRESS aiworks \
          && cd aiworks/deploy
      ```
3. 修改 [values/local/backend.values.template.yaml](deploy/values/local/backend.values.template.yaml)
    * `sparkJobServer` 相关配置项
    * `yarn.resourcemanager` 相关配置项
    * `yarn.webapp` 相关配置项
    * `flaskServer` 相关配置项
    * `jupyterlab` 相关配置项
    * `aliyun` 相关配置项，需要邮件推送权限
4. 创建k8s 环境
    * ```shell
      export KIND_BASE_URL=https://resource.static.zjvis.net/binary/kind \
          && export KUBECTL_BASE_URL=https://resource.static.zjvis.net/binary/kubectl \
          && export HELM_BASE_URL=https://resource.static.zjvis.net/binary/helm \
          && ./gradlew :createKubernetesEnv
      ```
5. 安装依赖组件
    * ```shell
      export PUBLISH_ENV=local && ./gradlew :createAllDependencies
      ```
    * 这条命令会在创建的k8s 环境内自动安装上
        + mariadb
        + redis-cluster
        + cassandra
        + minio
    * mariadb 的服务会被重定向到 `$YOUR_HOST_IP_ADDRESS:3006` 以供 [搭建 algo-py](algo-py/README.md)
        + 使用如下命令获取 `root` 用户的密码
            * ```shell
              ./build/runtime/bin/kubectl get secret --namespace middleware my-mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 --decode
              ```
    * minio 的服务会被重定向以供 [搭建 algo-py](algo-py/README.md)
        + web `$YOUR_HOST_IP_ADDRESS:9001`
        + console `$YOUR_HOST_IP_ADDRESS:9000`
        + 执行如下命令可以得到 `access-key`
            * ```shell
              ./build/runtime/bin/kubectl get secret --namespace middleware my-minio -o jsonpath="{.data.access-key}" \
                  | base64 --decode && echo
              ```
        + 执行如下命令可以得到 `secret-key`
            * ```shell
              ./build/runtime/bin/kubectl get secret --namespace middleware my-minio -o jsonpath="{.data.secret-key}" \
                  | base64 --decode && echo
              ```
6. 手动上传资源到 minio
    * 下一个版本会开放自动化功能
    * 使用浏览器访问 `http://$YOUR_HOST_IP_ADDRESS:9001`
        + 注意: 请替换 $YOUR_HOST_IP_ADDRESS 为你的服务部署地址
        + 使用`安装依赖组件`小节中得到的 `access-key` 和 `secret-key` 即可登录minio 进行操作
    * 下载资源
        + ```shell
          curl -o /tmp/GeoLite2-City.mmdb -L https://resource.static.zjvis.net/aiworks/minio/GeoLite2-City.mmdb
          curl -o /tmp/autojoin -L https://resource.static.zjvis.net/aiworks/minio/autojoin
          curl -o /tmp/mapping -L https://resource.static.zjvis.net/aiworks/minio/mapping
          ```
    * 创建 指定的`bucket` 并将资源上传到对应的 `bucket`
        + 将 `/tmp/autojoin` 和 `/tmp/mapping` 上传到 `fasttext-model` bucket
        + 将 `/tmp/GeoLite2-City.mmdb` 上传到 `vis-platform` bucket
        + 创建一个空的 bucket `graph-file`
7. 部署后端服务
    * ```shell
      export PUBLISH_ENV=local \
          && export DOCKER_REGISTRY=localhost:5000 \
          && export PULL_POLICY=Always \
          && export INITIALIZE=true \
          && ./gradlew :backendDeploy
      ```
    * 使用如下命令可以升级后端服务
        + ```shell
          export PUBLISH_ENV=local \
              && export DOCKER_REGISTRY=localhost:5000 \
              && export PULL_POLICY=Always \
              && export INITIALIZE=false \
              && ./gradlew :backendUpgrade
          ```
8. 部署前端服务
    * ```shell
      export PUBLISH_ENV=local \
          && export DOCKER_REGISTRY=localhost:5000 \
          && ./gradlew :frontendDeploy
      ```
    * 访问 `http://$YOUR_HOST_IP_ADDRESS:8080/`
        + 默认用户
            * username: `test-nebula`
            * password: `nebula@2021`
    * 使用如下命令可以升级前端服务
        + ```shell
          export PUBLISH_ENV=local \
              && export DOCKER_REGISTRY=localhost:5000 \
              && ./gradlew :frontendUpgrade
          ```
9. 删除整个k8s 集群
    * ```shell
      ./gradlew :deleteKubernetesEnv
      ```
    * 能够干净地清理掉前后端服务和依赖组件

### 集群化部署

* 功能我们将在后续公开，`开箱即用`所用的环境与真实k8s 集群差异很小，也可以仿照其搭建成分布式环境
* 我们的线上集群部署参数并不普适所有人，默认参数和普适参数正在讨论和测试中，敬请期待
