# Greenplum6.8.1单机版安装
## 下载地址
https://github.com/greenplum-db/gpdb/releases


## 安装要求
更详细信息请参考官方文档:
[greenplum 官方文档](https://docs.greenplum.org/6-8/install_guide/platform-requirements.html#topic_xbl_mkx_zgb)

### 系统支持
```
Red Hat Enterprise Linux 64-bit 7.x (See the following Note.)
Red Hat Enterprise Linux 64-bit 6.x
CentOS 64-bit 7.x
CentOS 64-bit 6.x
Ubuntu 18.04 LTS
Oracle Linux 64-bit 7, using the Red Hat Compatible Kernel (RHCK)
```
由于一些系统版本的内核有点问题会影响gp性能,官方推荐使用
```
RHEL 7.3 及其更高版本, CentOS 7.3及其更高版本 
```

### java版本支持

```
Open JDK 8 or Open JDK 11 

Oracle JDK 8 or Oracle JDK 11
```


## 系统配置

[greenplum 官方系统配置](https://docs.greenplum.org/6-8/install_guide/prep_os.html)

### 关闭SELINUX
``` bash
$ vim /etc/selinux/config
```
如果SELINUX不是disabled,修改如下:
``` 
SELINUX=disabled
```

### 关闭防火墙
关掉防火墙及相关服务
以centos7.x为例:
``` bash
#查看防火墙
$ systemctl status firewalld.service
#关掉防火墙
$ systemctl stop firewalld.service
$ systemctl disable firewalld.service
```

### 修改hostname设置hosts(方便后续初始化)
这里设置为gp-1,后续都用这个
``` bash
#查看hostname
$ hostname
#修改为gp-1
$ hostnamectl set-hostname gp-1
#修改hosts把改动的hostname gp-1添加进去
$ vim /etc/hosts
```
/etc/hosts添加如下内容:
```bash
# xxx.xxx.xxx.xxx为服务器ip
xxx.xxx.xxx.xxx gp-1
```

### 设置limit.conf文件
``` bash
$ vim /etc/security/limits.conf
```
设置如下内容:
```
* soft nofile 524288
* hard nofile 524288
* soft nproc 131072
* hard nproc 131072
```

### 设置/etc/sysctl.conf 
下面内容由官方提供,具体数值根据自己机器实际配置改动,[greenplum 官方系统配置](https://docs.greenplum.org/6-8/install_guide/prep_os.html)
```
# kernel.shmall = _PHYS_PAGES / 2 # See Shared Memory Pages
kernel.shmall = 197951838
# kernel.shmmax = kernel.shmall * PAGE_SIZE 
kernel.shmmax = 810810728448
kernel.shmmni = 4096
vm.overcommit_memory = 2 # See Segment Host Memory
vm.overcommit_ratio = 95 # See Segment Host Memory

net.ipv4.ip_local_port_range = 10000 65535 # See Port Settings
kernel.sem = 500 2048000 200 4096
kernel.sysrq = 1
kernel.core_uses_pid = 1
kernel.msgmnb = 65536
kernel.msgmax = 65536
kernel.msgmni = 2048
net.ipv4.tcp_syncookies = 1
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.tcp_max_syn_backlog = 4096
net.ipv4.conf.all.arp_filter = 1
net.core.netdev_max_backlog = 10000
net.core.rmem_max = 2097152
net.core.wmem_max = 2097152
vm.swappiness = 10
vm.zone_reclaim_mode = 0
vm.dirty_expire_centisecs = 500
vm.dirty_writeback_centisecs = 100
vm.dirty_background_ratio = 0 # See System Memory
vm.dirty_ratio = 0
vm.dirty_background_bytes = 1610612736
vm.dirty_bytes = 4294967296
```
重新加载配置
``` bash
$ sysctl -p
```

### IPC Object Removal设置(针对RHEL 7.2 或 CentOS 7.2 或 Ubuntu,其他版本无需执行本步)
``` bash
$ echo "RemoveIPC=no" >> /etc/systemd/logind.conf
$ service systemd-logind restart
```

### 检查安装m4(后续安装madlib组件用到)
```bash
yum -y install m4
```

### 创建gpadmin用户及组  
``` bash
$ groupadd  gpadmin
$ useradd  gpadmin -g gpadmin -d /home/gpadmin
#修改密码
$ echo "xxx" | passwd --stdin gpadmin
```

### gpadmin账号ssh配置 

``` bash
# 转成gpadmin
$ su  gpadmin
# 生成公钥和私钥
$ ssh-keygen -t rsa -b 2048 -N '' -f /home/gpadmin/.ssh/id_rsa
# 公钥写入authorized_keys
$ cp /home/gpadmin/.ssh/id_rsa.pub /home/gpadmin/.ssh/authorized_keys
```

## 安装greenplum
使用 ***root*** 账号在机器上安装
``` bash
# 退回root(如果已经是root略过此步)
$ exit
# 安装数据库
$ yum -y install  greenplum-db-6.8.1-rhel7-x86_64.rpm
```
默认安装路径在/user/local下

转给gpadmin
``` bash
$ chown -R gpadmin:gpadmin  /usr/local/greenplum*
```


## 初始化数据库
### 创建instance需要的目录
根据实际情况选择合适的目录,要保证目录的磁盘够用
``` bash
$ mkdir -p /opt/greenplum/data/master
$ mkdir -p /opt/greenplum/data/primary
#如果是用root创建的目录,记得修改owner
$ chown -R gpadmin:gpadmin /opt/greenplum
```

### 修改/home/gpadmin/.bash_profile和/home/gpadmin/.bashrc文件(内容一样)
``` bash
# 转到gpadmin账户
$ su gpadmin
``` 

文件尾添加如下内容
``` 
source /usr/local/greenplum-db-6.8.1/greenplum_path.sh
export MASTER_DATA_DIRECTORY=/opt/greenplum/data/master/gpseg-1
export PGPORT=5432
export PGUSER=gpadmin
export PGDATABASE=gpdb
```
使配置生效
``` bash
$ source /home/gpadmin/.bash_profile
``` 

###  创建并设置gp初始化文件init_config
``` bash
$ vim init_config
```
添加以下内容(***不要直接复制粘贴，注意替换相关配置信息***):
``` 
SEG_PREFIX=gpseg
PORT_BASE=33000
declare -a DATA_DIRECTORY=(/opt/greenplum/data/primary /opt/greenplum/data/primary )
MASTER_HOSTNAME=gp-1
MASTER_PORT=5432
MASTER_DIRECTORY=/opt/greenplum/data/master
DATABASE_NAME=gpdb
``` 

###  创建并设置gp初始化文件init_config
``` bash
$ vim hosts_file
```
添加以下内容(前面修改的hostname):
``` 
gp-1
``` 

### 初始化gp
``` bash
$ gpinitsystem -c init_config -h hosts_file 
``` 

### 测试链接
初始化成功之后,默认的gpadmin账号没有密码
``` bash
#执行命令进入数据库
$ psql
```
修改数据库密码
``` bash
alter role gpadmin with password 'xxxxxxx';
```

### 修改远程连接配置
``` bash
$ vim /opt/greenplum/data/master/gpseg-1/pg_hba.conf 
```
添加：
```
host     all         gpadmin         0.0.0.0/0               md5
```
重新加载配置文件：
``` bash
$ gpstop -u 
```
这样外部就可以连接到数据库了，如果命令不生效，可以停掉服务并重新启动.

### 其他常用命令
``` bash
#正常启动
$ gpstart 
#正常关闭
$ gpstop
#快速关闭
$ gpstop -M fast 
#查看集群状态
$ gpstate

```

## 当前版本使用gpload功能bug修改(csv文件字段为中文或保留字等问题处理)
修改  /usr/local/greenplum-db/bin/gpload.py 文件

修改1,在  import sys  下面加上
```bash
reload(sys)
sys.setdefaultencoding('utf8')
```

修改2,字段有数字或保留字符等,创建external 表错误,所以按照greenplum语法需要给字段加上双引号.
```bash
# 根据 create external table 查找到创建external表的sql,位置在上修改1完成之后文档的2434行,在external左右加上'\"'
sql += "(%s)" % ','.join(map(lambda a:'%s %s' % ('\"'+a[0]+'\"', a[1]), from_cols))

```
修改完重启服务
``` bash
$ gpstop -M fast 
$ gpstart
```

## 数据库及表初始化
```bash
$ vim gp-aiworks-init.sql
```
添加如下内容:
```
-- 创建aiworks库
create database  aiworks;

-- 连接aiworks
\c aiworks;

-- 创建模式
create schema dataset;
create schema  pipeline;
create schema  graph;
create schema  ml_model;

--安装扩展
create extension pgcrypto;

create extension plpythonu;
```
执行sql进行初始化
```bash
$ psql -U gpadmin -a -f gp-aiworks-init.sql
```

## 创建文件上传目录(平台数据接入功能用到)
```bash
$ mkdir -p /home/gpadmin/csv_yml
```

## madlib组件安装
安装组件
```bash
# 下载后解压
$ tar -xzvf madlib-1.18.0+2-gp6-rhel7-x86_64.tar.gz
# 解压后跳转到目录
$ cd madlib-1.18.0+2-gp6-rhel7-x86_64
# gppkg 安装
$ gppkg -i madlib-1.18.0+2-gp6-rhel7-x86_64.gppkg
# 跳转到安装目录
$ /usr/local/greenplum-db/madlib/bin
# 执行命令将madlib安装到指定数据库,x.x.x.x 为服务器ip
./madpack install -s madlib -p greenplum -c gpadmin@x.x.x.x/aiworks
```

上方指令为gp中aiworks数据库安装了madlib环境

-s 指定数据库中的schema

-p 指定安装的平台，这里是greenplum

-c 指定连接方式，用户名@主机名：端口还/数据库名


## 项目使用greenplum配置说明

```bash
greenplum:
  # gp master节点ip
  host: xxx.xxx.xxx.xxx
  # gp数据库端口
  port: 5432
  # 数据库名
  database: aiworks
  # 账号
  username: gpadmin
  # 密码
  password: xxx
  # gpload master hostname
  master_host_name: gp-1
  # gpload local file hostname
  local_host_name: gp-1
  ssh:
    # 服务器端口
    port: 22
    # 服务器admin账号的密码
    password: xxx
  filepath: /home/gpadmin/csv_yml/
```