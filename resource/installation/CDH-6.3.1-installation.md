# CDH-6.3.1离线搭建

## 离线资源下载地址
curl -LO https://resource.static.zjvis.net/cdh/CDH-6.3.2-el7-parcel.tar.gz

## 系统配置
### 修改hostname设置hosts(***所有节点***)
这里设置为nebula-vis-023
``` bash
YOUR_HOST_IP_ADDRESS=10.101.16.86
hostnamectl set-hostname nebula-vis-023
echo nebula-vis-023 $YOUR_HOST_IP_ADDRESS >> /etc/hosts
```

### 关闭SELINUX
``` bash
$ vim /etc/selinux/config
```
如果SELINUX不是disabled,修改如下:
``` 
SELINUX=disabled
```

### 关闭防火墙
关掉防火墙及相关服务
以centos7.x为例:
``` bash
systemctl status firewalld.service
systemctl stop firewalld.service
systemctl disable firewalld.service
```
### 禁用透明大页面压缩
``` bash
$ echo never > /sys/kernel/mm/transparent_hugepage/defrag
$ echo never > /sys/kernel/mm/transparent_hugepage/enabled
```
修改启动脚本：
``` bash
$ vim /etc/rc.local
```
添加：
```
echo never > /sys/kernel/mm/transparent_hugepage/defrag
echo never > /sys/kernel/mm/transparent_hugepage/enabled
```

### jdk检查安装
卸载系统自带的openjdk,安装oracle jdk

```shell
curl -LO https://resource.static.zjvis.net/cdh/jdk-8u202-linux-x64.tar.gz
tar zxf jdk-8u202-linux-x64.tar.gz
# 注意：CDH默认使用/usr/java下的jdk，如果是通过解压安装的jdk，解压目录不在/usr/local/ 下，启动cloudera-agent会报错，但是直接解压到/usr/java/下的话安装CM的时候会报错。只能通过解压在其它目录，然后通过创建软连接的方式解决，例如：
mkdir -p /usr/java && ln -s /root/cdh/jdk1.8.0_202/bin/java /usr/java/default
```

### 使用chrony同步系统时间
``` bash
yum install -y chrony \
    && systemctl enable chronyd \
    && systemctl start chronyd \
    && chronyc sources \
    && chronyc tracking \
    && timedatectl set-timezone 'Asia/Shanghai'
```

## 安装CM
安装daemons（***所有节点***）
``` bash
rpm -ivh https://resource.static.zjvis.net/cdh/CDH-6.3.2-el7-rpm/cloudera-manager-daemons-6.3.1-1466458.el7.x86_64.rpm
```
安装agent（***所有节点***）
``` bash
#--nodeps --force是跳过依赖强制安装，不然会安装失败
rpm -ivh https://resource.static.zjvis.net/cdh/CDH-6.3.2-el7-rpm/cloudera-manager-agent-6.3.1-1466458.el7.x86_64.rpm --nodeps --force
```
安装server（***仅主节点***）
``` bash
rpm -ivh https://resource.static.zjvis.net/cdh/CDH-6.3.2-el7-rpm/cloudera-manager-server-6.3.1-1466458.el7.x86_64.rpm
```
安装好之后，所有修改所有节点agent配置，`/etc/cloudera-scm-agent/config.ini`
所有把server_host的localhost换成server的hostname，换了之后启动cm在后面安装parcel前就会跳过install agents 界面直接进入install parcels界面。

## 5.安装元数据库mariadb或mysql
安装过程此处略过

## 安装msql jdbc driver（所有节点）
下载好驱动之后
``` bash
#创建目录
$ mkdir -p /usr/share/java/ 
#将驱动包拷贝到/usr/share/java/下并换名为mysql-connector-java.jar
$ cp mysql-connector-java-5.1.46-bin.jar /usr/share/java/mysql-connector-java.jar
```
## 创建数据库及⽤户授权
根据所需要安装的服务参照下表创建对应的数据库以及数据库用户：
|服务名 | 数据库名  |用户名|
| --------   | -----:   | :----: |
|Cloudera Manager Server | scm | scm|
|Activity Monitor | amon | amon|
|Reports Manager | rman | rman|
|Hue | hue | hue
|Hive Metastore Server | metastore | hive|
|Sentry Server | sentry | sentry
|Cloudera Navigator Audit Server | nav | nav|
|Cloudera Navigator Metadata Server | navms | navms|
|Oozie | oozie | oozie|

## 使用httpd配置parcel源
安装
``` bash
$ yum -y install httpd
``` 
开启
``` bash
$ sudo systemctl start httpd
``` 
创建/var/www/html/cdh6_parcel目录，并把相关资源放在这个目录下，同时也放到
/opt/cloudera/parcel-repo目录下。  
通过http://ip/cdh6_parcel 就能访问了

## 启动CDH
开启server
``` bash
$ systemctl start cloudera-scm-server
```
开启agent(***所有节点***)
``` bash
$ systemctl start cloudera-scm-agent
``` 
启动成功之后，根据 ***7180*** 端口进入CM的web界面
浏览器登录CM的web界面，账号密码均为 ***admin***

## CDH页面安装相关配置
登录CDH管理页面,前面根据页面提示一步一步来.

到选择存储库这一环节,选择自定义存储库，内容填http://ip/cdh6_parcel,
方法选择 使用Parcel,然后点击更多选项，填加远程parcel存储库url，同时删除其他url（都不可用了）

后面操作根据提示来就行了.


## aiworks使用haoop配置说明
``` bash
yarn:
  # yarn配置目录
  conf:
    path: /etc/hadoop/conf
  # yarn-site.xml中yarn.resourcemanager.address
  resourcemanager:
    host: xxx
    port: 8032
  # yarn-site.xml中yarn.resourcemanager.webapp.address 
  webapp:
    schema: http
    host: xxx
    port: 8088
``` 