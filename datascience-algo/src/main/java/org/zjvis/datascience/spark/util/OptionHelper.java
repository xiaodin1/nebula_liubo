package org.zjvis.datascience.spark.util;

import org.apache.commons.cli.Option;

import java.util.HashMap;
import java.util.Map;

/**
 * @description Spark算子运行命令工具类
 * @date 2021-12-23
 */
public class OptionHelper {
    private static Map<String, Option> optionMap = new HashMap<>();

    static {
        Option sourceTableOption = new Option("s", "source", true, "source table name");
        sourceTableOption.setRequired(true);
        optionMap.put("source", sourceTableOption);
        Option target = new Option("t", "target", true, "output table");
        target.setRequired(true);
        optionMap.put("target", target);
        Option featureColsOptions = new Option("f", "featureCols", true, "feature column");
        featureColsOptions.setRequired(true);
        optionMap.put("featureCols", featureColsOptions);
        Option idColOpt = new Option("idcol", "idCol", true, "column id");
        optionMap.put("idCol", idColOpt);
        Option sample = new Option("sample", "sample", true, "sample flag");
        optionMap.put("sample", sample);
    }

    public static Option getSpecOption(String key) {
        return optionMap.get(key);
    }

    public static Option getSpecOption(String shortOpt, String longOpt, String desc) {
        return getSpecOption(shortOpt, longOpt, desc, false);
    }

    public static Option getSpecOption(String shortOpt, String longOpt, String desc, boolean isNeeded) {
        Option option = new Option(shortOpt, longOpt, true, desc);
        option.setRequired(isNeeded);
        return option;
    }
}
