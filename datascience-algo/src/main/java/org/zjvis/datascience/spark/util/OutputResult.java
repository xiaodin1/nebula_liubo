package org.zjvis.datascience.spark.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @description Spark算子结果包装类
 * @date 2021-12-23
 */
@Data
public class OutputResult implements Serializable {
    private int status = 0;

    private JSONObject result;

    private String success = "success";

    public OutputResult() {
        result = new JSONObject();
        result.put("input_params", new JSONObject());
        result.put("output_params", new JSONArray());
        result.put("model_params", new JSONObject());
    }

    public void setInputParams(JSONObject inputParams) {
        result.put("input_params", inputParams);
    }

    public void setOutputParams(JSONArray outputParams) {
        result.put("output_params", outputParams);
    }

    public void setModelParams(JSONObject modelParams) {
        result.put("model_params", modelParams);
    }
}
