package org.zjvis.datascience.spark.algorithm;

import cn.hutool.core.text.UnicodeUtil;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description Spark算子节点入口（Scala引用）
 * @date 2021-12-23
 */
public class AlgorithmEntryWrapper {

    private final static Logger logger = LoggerFactory.getLogger("AlgorithmEntryWrapper");

    public static String getAlgorithmName(String[] args) {
        if (args.length < 1) {
            return "";
        }
        return args[0];
    }

    public static String entry(String[] args, SparkSession spark) {
        for (int i = 0; i < args.length; i++) {
            args[i] = UnicodeUtil.toString(args[i]);
        }
        String algoName = getAlgorithmName(args);
        BaseAlgorithm baseAlgorithm = null;
        if (algoName.equals("k-Means")) {
            baseAlgorithm = new KmeansAlgorithm(spark);
        } else if (algoName.equals("dbscan")) {
            baseAlgorithm = new DBscanAlgorithmV2(spark);
        } else if (algoName.equals("pca")) {
            baseAlgorithm = new PcaAlgorithm(spark);
        } else if (algoName.equals("linear")) {
            baseAlgorithm = new LinearRegressionAlgorithm(spark);
        } else if (algoName.equals("logistic")) {
            baseAlgorithm = new LogisticRegressionAlgorithm(spark);
        } else if (algoName.equals("fp-growth")) {
            baseAlgorithm = new FPGrowthAlgorithm(spark);
        } else if (algoName.equals("prefix-span")) {
            baseAlgorithm = new PrefixSpanAlgorithm(spark);
        } else if (algoName.equals("statistics-anomaly")) {
            baseAlgorithm = new StatisticsAnomaly(spark);
        } else if (algoName.equals("isolation-forest")) {
            baseAlgorithm = new IsolationForestAlgorithm(spark);
        } else if (algoName.equals("tsne")) {
            baseAlgorithm = new TSNEAlgorithm(spark);
        }
        baseAlgorithm.parseParams(args);
        baseAlgorithm.beginAlgorithm();
        return baseAlgorithm.getRetResult();
    }

}
