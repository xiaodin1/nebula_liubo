package org.zjvis.datascience.spark

import com.typesafe.config.Config
import org.apache.spark.sql.SparkSession
import org.scalactic.{Bad, Every, Good, One, Or}
import spark.jobserver.SparkSessionJob
import spark.jobserver.api.{JobEnvironment, SingleProblem, ValidationProblem}

import scala.util.Try

object UnCacheTableTool extends SparkSessionJob  {
  type JobData = Seq[String]
  type JobOutput = String

  override def runJob(sparkSession: SparkSession, runtime: JobEnvironment, data: JobData): JobOutput = {
    var result = "success"
    sparkSession.catalog.uncacheTable(data.head)
    result
  }

  override def validate(sparkSession: SparkSession, runtime: JobEnvironment, config: Config): Or[JobData, Every[ValidationProblem]] = {
    Try(config.getString("input.string").split(" ").toSeq)
      .map(words => Good(words))
      .getOrElse(Bad(One(SingleProblem("No input.string param"))))
  }
}
