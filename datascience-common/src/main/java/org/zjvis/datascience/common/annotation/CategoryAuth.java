package org.zjvis.datascience.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @description 数据分类鉴权注解
 * @date 2021-12-24
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CategoryAuth {

    //分类id字段，默认识别id，如果参数不是id需要在注解中加入field=”字段“
    String field() default "id";
}
