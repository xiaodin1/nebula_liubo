package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.graph.GraphFilterPipelineDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.BaseVO;

@Data
public class GraphFilterPipelineVO extends BaseVO {
    private Long id;

    private String name;

    private Long projectId;

    private Long graphId;

    private Long userId;

    private JSONObject data;

    public GraphFilterPipelineDTO dto() {
        GraphFilterPipelineDTO dto = DozerUtil.mapper(this, GraphFilterPipelineDTO.class);
        dto.setDataJson(this.data==null ? null : this.data.toJSONString());
        return dto;
    }
}
