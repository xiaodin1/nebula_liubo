package org.zjvis.datascience.common.util;

import com.alibaba.druid.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description 文件六转换工具类
 * @date 2021-08-27
 */
public class FileUtil {

    private final static Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * inputStream转字符串
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static String inputStream2String(InputStream is) throws IOException {
        if (is == null) {
            return "";
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        while ((line = in.readLine()) != null) {
            buffer.append(line);
        }
        return buffer.toString();
    }

    /**
     * inputStream转base64
     *
     * @param in
     * @return
     * @throws Exception
     */
    public static String inputStream2Base64(InputStream in) throws Exception {
        // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;

        // 读取图片字节数组
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buff = new byte[100];
            int rc = 0;
            while ((rc = in.read(buff, 0, 100)) > 0) {
                out.write(buff, 0, rc);
            }
            data = out.toByteArray();
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new Exception("base64转换异常");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return Base64.byteArrayToBase64(data);
    }

    public static String longSize2Str(long size) {
        StringBuffer bytes = new StringBuffer();
        DecimalFormat format = new DecimalFormat("###.0");
        if (size >= 1024 * 1024 * 1024) {
            double i = (size / (1024.0 * 1024.0 * 1024.0));
            bytes.append(format.format(i)).append("GB");
        } else if (size >= 1024 * 1024) {
            double i = (size / (1024.0 * 1024.0));
            bytes.append(format.format(i)).append("MB");
        } else if (size >= 1024) {
            double i = (size / (1024.0));
            bytes.append(format.format(i)).append("KB");
        } else {
            if (size <= 0) {
                bytes.append("0B");
            } else {
                bytes.append((int) size).append("B");
            }
        }
        return bytes.toString();
    }

    public static Long strSize2Long(String sizeStr) {
        String lowerCaseSizeStr = sizeStr.toUpperCase();
        Matcher matcher = Pattern.compile("[A-Z_]+").matcher(lowerCaseSizeStr);
        if (matcher.find()) {
            int index = matcher.start();
            String unit = lowerCaseSizeStr.substring(index);
            double times = 0;
            switch (unit) {
                case "B":
                    times = 1;
                    break;
                case "KB":
                    times = 1024;
                    break;
                case "MB":
                    times = 1024 * 1024;
                    break;
                case "GB":
                    times = 1024 * 1024 * 1024;
                    break;
            }
            return new Double(Double.parseDouble(lowerCaseSizeStr.substring(0, index)) * times)
                    .longValue();
        } else {
            return 0L;
        }

    }

    public static void main(String[] args) {
        Long aLong = strSize2Long("20MB");
        String netFileSizeDescription = longSize2Str(aLong);
        System.out.println(netFileSizeDescription);

    }

}
