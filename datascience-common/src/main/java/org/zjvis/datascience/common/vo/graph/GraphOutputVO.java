package org.zjvis.datascience.common.vo.graph;


import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GraphOutputVO implements Serializable {
    //右边实例视图
    private Long id;

    private List<NodeVO> nodes;

    private List<LinkVO> links;

    private String janusGraphName;

}
