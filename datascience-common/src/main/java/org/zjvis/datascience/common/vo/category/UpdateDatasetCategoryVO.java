package org.zjvis.datascience.common.vo.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 数据管理相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDatasetCategoryVO {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类目录id不能为空", groups = DatasetCategoryVO.Id.class)
    @Min(value = 1, message = "请输入有效分类目录id", groups = DatasetCategoryVO.Id.class)
    private Long id;

    @NotBlank(message = "分类目录名不能为空")
    @Length(max = 100, message = "分类目录名长度不能超过100")
    @ApiModelProperty(value = "分类目录名", required = true)
    private String name;
}
