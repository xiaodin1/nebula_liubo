package org.zjvis.datascience.common.dto.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description HTTP数据上传状态DTO
 * @date 2021-12-24
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpStatusInfoDTO {

    private Boolean status;
}
