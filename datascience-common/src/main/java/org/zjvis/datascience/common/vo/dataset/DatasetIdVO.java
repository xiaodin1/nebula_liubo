package org.zjvis.datascience.common.vo.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.category.DatasetCategoryVO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 数据查询渲染相关VO (根据数据集id获取数据集)
 * @date 2021-12-23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetIdVO {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类目录id不能为空", groups = DatasetCategoryVO.Id.class)
    @Min(value = 1, message = "请输入有效分类目录id", groups = DatasetCategoryVO.Id.class)
    private Long id;

    private Long transferUserId;

    public interface Id {
    }
}
