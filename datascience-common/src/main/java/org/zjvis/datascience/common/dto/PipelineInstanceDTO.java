package org.zjvis.datascience.common.dto;

import lombok.Data;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.PipelineInstanceVO;

import java.time.LocalDateTime;

/**
 * @description 数据视图pipeline实例信息表，数据视图pipeline 实例DTO
 * @date 2020-08-17
 */
@Data
public class PipelineInstanceDTO extends BaseDTO {

    private Long id;

    private Long projectId;

    private Long pipelineId;

    private Long userId;

    private String status;

    private Long duringTime;

    private LocalDateTime gmtRunning;

    public PipelineInstanceVO view() {
        PipelineInstanceVO vo = DozerUtil.mapper(this, PipelineInstanceVO.class);
        return vo;
    }

}
