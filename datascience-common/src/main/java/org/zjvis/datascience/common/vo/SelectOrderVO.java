package org.zjvis.datascience.common.vo;

import lombok.Data;

/**
 * @description 管理员筛选查看结果相关VO
 * @date 2021-08-22
 */
@Data
public class SelectOrderVO {
    private String field;

    private String orderRule;
}
