package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @description 数据集SQL查询记录信息表，数据集SQL查询记录DTO
 * @date 2021-04-21
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class SqlCategoryDTO extends BaseDTO implements Serializable {
    private Long id;
    private Long userId;
    private String rowSql;


    public SqlCategoryDTO(Long userId, String rowSql) {
        this.userId = userId;
        this.rowSql = rowSql;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


}
