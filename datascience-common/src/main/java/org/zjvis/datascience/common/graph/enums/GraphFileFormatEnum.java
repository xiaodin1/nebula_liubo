package org.zjvis.datascience.common.graph.enums;

/**
 * @description GraphFileFormatEnum
 * @date 2021-12-29
 */
public enum GraphFileFormatEnum {
    JSON("json", 0),
    CSV("csv", 1),
    GML("gml", 2),
    GRAPH_BUILD("graph_build", 3)
    ;

    private final String desc;

    private final int val;

    GraphFileFormatEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }
}
