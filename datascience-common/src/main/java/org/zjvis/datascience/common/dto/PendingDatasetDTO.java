package org.zjvis.datascience.common.dto;

import lombok.*;
import org.zjvis.datascience.common.enums.ImportTaskStatusEnum;

import java.io.Serializable;

/**
 * @description PendingDatasetDTO
 * @date 2021-12-29
 */
@ToString
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PendingDatasetDTO implements Serializable {

    private String databaseType;

    private String tableName;

    private String targetTableName;

    private Integer count;

    private DatasetDTO dataset;

    private String errorInfo;

    private String url;

    private String user;

    private String password;

    private ImportTaskStatusEnum status;

    private Long userId;

    private Long startTime;

    private Long costTime;

    private static final long serialVersionUID = 1L;

}
