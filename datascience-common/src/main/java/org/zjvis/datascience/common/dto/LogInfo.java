package org.zjvis.datascience.common.dto;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @description 日志对象封装类
 * @date 2020-06-29
 */
@Data
@Accessors(chain = true)
public class LogInfo implements Serializable {

    @JSONField(ordinal = 1)
    private String traceId;

    @JSONField(ordinal = 2)
    private String type;

    @JSONField(ordinal = 3)
    private String level;

    @JSONField(ordinal = 4)
    private String cName;

    @JSONField(ordinal = 5)
    private String mName;

    @JSONField(ordinal = 6)
    private String line;

    @JSONField(ordinal = 7)
    private String time = DateUtil.now();

    @JSONField(ordinal = 8)
    private Object info;

    public void setInfo(Object info) {
        this.info = info;
    }
}
