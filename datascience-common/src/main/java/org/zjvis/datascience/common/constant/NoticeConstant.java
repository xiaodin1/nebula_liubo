package org.zjvis.datascience.common.constant;

/**
 * @description 消息通知常量
 * @date 2021-12-24
 */
public class NoticeConstant {
    public static final String SOCKET_EVENT_PROJECT_USER_ROLE = "project_user_role";

    public static final String PROJECT_USER_ROLE_ADD_TITLE = "项目邀请：%s ";

    public static final String PROJECT_USER_ROLE_ADD_CONTENT = "%s被 %s 以 %s 身份添加到 %s 项目。";

    public static final String PROJECT_USER_ROLE_DELETE_TITLE = "项目移除：%s ";

    public static final String PROJECT_USER_ROLE_DELETE_CONTENT = "%s 被 %s 移出 %s 项目。";

    public static final String PROJECT_USER_ROLE_CHANGE_TITLE = "项目角色变动：%s ";

    public static final String PROJECT_USER_ROLE_CHANGE_CONTENT = "%s 在项目 %s 中的角色由 %s 调整为 %s。";

    public static final String PROJECT_MESSAGE_CHANGE_TITLE = "项目信息改动通知";

    public static final String PROJECT_MESSAGE_CHANGE_CONTENT = "项目 %s 被用户 %s 改动。";

    public static final String PROJECT_LOCK_TITLE = "项目锁定通知";

    public static final String PROJECT_LOCK_CONTENT = "项目 %s 被用户 %s 锁定。";

    public static final String PROJECT_UNLOCK_TITLE = "项目解锁通知";

    public static final String PROJECT_UNLOCK_CONTENT = "项目 %s 被用户 %s 解除锁定。";

    public static final String PIPELINE_RUN_OVERTIME_TITLE = "pipeline执行超过1分钟提醒";

    public static final String PIPELINE_RUN_OVERTIME_CONTENT = "您在项目 %s 中pipeline的执行时间为：%s，超过1分钟。";

    public static final String PROJECT_DATA_SOURCE_ADD_TITLE = "数据源添加通知";

    public static final String PROJECT_DATA_SOURCE_ADD_CONTENT = "用户 %s 在项目 %s 中添加了新的数据源 %s。";

    public static final String PROJECT_DATA_SOURCE_DELETE_TITLE = "数据源删除通知";

    public static final String PROJECT_DATA_SOURCE_DELETE_CONTENT = "用户 %s 在项目 %s 中删除了数据源 %s。";

    public static final String PIPELINE_QUERY_STATUS = "queryStatus";

    public static final String DATASET_TRANSFER_TITLE = "数据集归属人转移通知";

    public static final String DATASET_TRANSFER_CONTENT = "用户 %s 把其名下的数据集 %s 归属权转移给你，您可在 %s 下查看，名字已替换为 %s。";
}
