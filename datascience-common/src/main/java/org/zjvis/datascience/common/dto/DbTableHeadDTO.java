package org.zjvis.datascience.common.dto;

import java.util.List;
import lombok.Data;
import org.zjvis.datascience.common.vo.dataset.HeadVO;

/**
 * @description DbTableHeadDTO
 * @date 2021-12-29
 */
@Data
public class DbTableHeadDTO {
    private String tableName;
    private List<HeadVO> head;
}
