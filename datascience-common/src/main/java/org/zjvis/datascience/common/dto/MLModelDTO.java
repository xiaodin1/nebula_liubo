package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.MLModelVO;

import java.time.LocalDateTime;

/**
 * @description 模型训练信息表，模型训练DTO
 * @date 2021-11-23
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class MLModelDTO extends BaseDTO {

    private static final long serialVersionUID = 545955098891016101L;
    private Long id;

    private String name;

    private Long projectId;

    private Long userId;

    private String modelInfo;

    private String algorithm;

    private String modelDesc;

    private String modelType;

    private String modelParam;

    private String modelPath;

    private String sourceTable;

    private String status;

    private Long folderId;

    private Long inPanel;

    private Float runTime;

    private Long sourceId;

    private String sourceName;

    private Long invisible;

    private Long num;

    private Long sparktime;

    private String modelSavedPath;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime trainTime;

    public MLModelVO view() {
        MLModelVO vo = DozerUtil.mapper(this, MLModelVO.class);
        vo.setParam(JSONObject.parseObject(this.getModelParam()));
        vo.setData(JSONObject.parseObject(this.getModelInfo()));
        return vo;
    }

}
