package org.zjvis.datascience.common.graph.enums;

/**
 * @description GraphFilterTypeEnum
 * @date 2021-12-29
 */
public enum GraphFilterTypeEnum {
    NODE("节点", 0),
    LINK("边", 1),
    ATTR("属性", 2),
    TOPOLOGY("拓扑", 3),
    OPERATION("操作", 4)
    ;


    private final String desc;

    private final int val;

    GraphFilterTypeEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }
}
