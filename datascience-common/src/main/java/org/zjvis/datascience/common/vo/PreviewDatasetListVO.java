package org.zjvis.datascience.common.vo;

import org.zjvis.datascience.common.vo.dataset.PreviewDatasetVO;

import java.util.List;

/**
 * @description 数据结果预览相关VO
 * @date 2021-12-08
 */
public class PreviewDatasetListVO {

    List<PreviewDatasetVO> previewDatasetVOS;
    String tips;

    public List<PreviewDatasetVO> getPreviewDatasetVOS() {
        return previewDatasetVOS;
    }

    public void setPreviewDatasetVOS(List<PreviewDatasetVO> previewDatasetVOS) {
        this.previewDatasetVOS = previewDatasetVOS;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public PreviewDatasetListVO(List<PreviewDatasetVO> previewDatasetVOS, String tips) {
        this.previewDatasetVOS = previewDatasetVOS;
        this.tips = tips;
    }

    public PreviewDatasetListVO() {
    }
}
