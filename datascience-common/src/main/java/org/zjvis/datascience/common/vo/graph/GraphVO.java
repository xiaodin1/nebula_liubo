package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.zjvis.datascience.common.dto.graph.GraphDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.BaseVO;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GraphVO extends BaseVO {
    //左边建模视图
    private Long id;

    private String name;

    private Long projectId;

    private Long pipelineId;

    private Long userId;

    private Long TaskId;

    private List<CategoryVO> categories;

    private List<EdgeVO> edges;

    //右边实例视图
    private List<NodeVO> nodes;

    private List<LinkVO> links;

    private JSONObject graphInfo;

    private JSONObject actionContext;

    private Long filterInstanceId;

    public GraphDTO dto() {
        GraphDTO dto = DozerUtil.mapper(this, GraphDTO.class);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("categories", categories);
        jsonObject.put("edges", edges);
        jsonObject.put("nodes", nodes);
        jsonObject.put("links", links);
        dto.setDataJson(jsonObject.toJSONString());
        return dto;
    }

    public GraphVO tab() {
        GraphVO tab = new GraphVO();
        tab.setId(id);
        tab.setName(name);
        return tab;
    }

}
