package org.zjvis.datascience.common.util.sqlParse;

import com.alibaba.druid.sql.ast.expr.SQLPropertyExpr;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.alibaba.druid.util.StringUtils;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.zjvis.datascience.common.util.SqlUtil;

/**
 * @description : Mysql SQL 解析器
 * @date 2021-10-25
 */
public class MySqlVisitor extends MySqlASTVisitorAdapter {

    private Map<String, String> tableName;
    private String schema;
    private boolean userTableHit = true;
    private Set<String> unknownTables = new HashSet<>();

    public MySqlVisitor(Map tableName, String schema) {
        this.tableName = tableName;
        this.schema = schema;
    }

    public Map<String, String> getTableName() {
        return tableName;
    }

    public void setTableName(Map<String, String> tableName) {
        this.tableName = tableName;
    }

    public String getUnknownTables() {
        String tables = unknownTables.toString();
        return tables.substring(1, tables.length() - 1);
    }

    public boolean isUserTableHit() {
        return userTableHit;
    }

    //修改表名
    @Override
    public boolean visit(SQLExprTableSource x) {
        String targetTableName = x.getExpr().toString()
                .replace(" ", "").replace("\"","");
        if (null != this.schema && !this.schema.equals("")){
            targetTableName = this.schema + "::" + targetTableName;
        }
        String TableAlias = "";
        if (null != x.getAlias()){
            TableAlias = x.getAlias().replace(" ", "");
        }
        String resultTableName = tableName.get(targetTableName);
        if (resultTableName == null) {
            String possibleName = targetTableName + TableAlias;
            resultTableName = tableName.get(possibleName);
            if (null == resultTableName) {
                unknownTables.add(x.getExpr().toString());
                userTableHit = false;
            }
        }
        //别名，如果有别名，别名保持不变
        String s =
                StringUtils.isEmpty(x.getAlias()) ? SqlUtil.formatPGSqlColName(x.getExpr().toString())
                        : SqlUtil.formatPGSqlColName(x.getAlias());
        // 修改表名，不包含点才加 select id from c left join d on c.id = d.id 中的c 和 d
//        if (!x.getExpr().toString().contains(".")) {
//            x.setExpr(resultTableName);
//        }
        x.setExpr(resultTableName);
        x.setAlias(s);
        return true;
    }

    //修改列名前缀的表名
    @Override
    public boolean visit(SQLPropertyExpr x) {
        String name = x.getOwnernName();
        x.setOwner(SqlUtil.formatPGSqlColName(name));
        return true;
    }
//    //增加limit语句
//    @Override
//    public boolean visit(SQLLimit x) {
//        try {
//            if (Integer.valueOf(x.getRowCount().toString()) > DatabaseConstant.GP_SQL_PREVIEW_COUNT) {
//                x.setRowCount(DatabaseConstant.GP_SQL_PREVIEW_COUNT);
//            }
//        }catch (Exception e){
//            x.setRowCount(DatabaseConstant.GP_SQL_PREVIEW_COUNT);
//        }
//        return true;
//    }
}
