package org.zjvis.datascience.common.sql;

/**
 * @description : SQL聚合操作帮助类
 * @date 2020-08-04
 */
public class AggregatorHelper {

    enum Type {
        SUM, AVG, MAX, MIN, DISTINCT, COUNT
    }

    public static AggregatorHelper.Type valueOf(String aggregatorFun) {
        if ("sum".equals(aggregatorFun)) {
            return AggregatorHelper.Type.SUM;
        } else if ("avg".equals(aggregatorFun)) {
            return AggregatorHelper.Type.AVG;
        } else if ("max".equals(aggregatorFun)) {
            return AggregatorHelper.Type.MAX;
        } else if ("min".equals(aggregatorFun)) {
            return AggregatorHelper.Type.MIN;
        } else if ("distinct".equals(aggregatorFun)) {
            return AggregatorHelper.Type.DISTINCT;
        } else if ("count".equals(aggregatorFun)) {
            return AggregatorHelper.Type.COUNT;
        } else {
            return null;
        }
    }

}
