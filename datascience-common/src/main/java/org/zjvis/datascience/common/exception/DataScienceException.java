package org.zjvis.datascience.common.exception;

import lombok.Getter;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;

/**
 * @description 平台通用异常类
 * @date 2021-12-08
 */
@Getter
public class DataScienceException extends RuntimeException {

    private static final long serialVersionUID = 4465566270019332160L;

    private ApiResult apiResult;

    public DataScienceException(String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public DataScienceException(String message, Throwable cause) {
        super(message, cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public DataScienceException(Throwable cause) {
        super(cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR);
    }

    public DataScienceException(Integer code, String msg, String info, Throwable cause) {
        super(msg, cause);
        if (info == null) {
            this.apiResult = new ApiResult(code, msg);
        } else {
            this.apiResult = new ApiResult(code, msg + ":" + info);
        }
    }

    public DataScienceException(ErrorCode errorCode, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), null, cause);
    }

    public DataScienceException(ApiResultCode apiResultCode, Throwable cause) {
        this(apiResultCode.getCode(), apiResultCode.getMessage(), null, cause);
    }

    public DataScienceException(ErrorCode errorCode, String info, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), info, cause);
    }

    public static DataScienceException of(ErrorCode errorCode) {
        return new DataScienceException(errorCode, errorCode.getMsg(), null);
    }

    public static DataScienceException of(ErrorCode errorCode, String info) {
        return new DataScienceException(errorCode, info, null);
    }

    public static DataScienceException of(ErrorCode errorCode, String info, String tips) {
        DataScienceException e = new DataScienceException(errorCode, info, null);
        e.getApiResult().setTips(tips);
        return e;
    }

    public DataScienceException(ErrorCode errorCode) {
        this(errorCode, null);
        this.getApiResult().setTips(errorCode.getMsg());
    }

    public DataScienceException(Integer code, String msg) {
        this.apiResult = new ApiResult(code, msg);
    }

    public DataScienceException(ApiResultCode code, String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(code, message);
    }

}
