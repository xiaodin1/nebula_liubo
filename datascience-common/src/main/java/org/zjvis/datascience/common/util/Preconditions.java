package org.zjvis.datascience.common.util;

import cn.hutool.core.util.StrUtil;
import org.zjvis.datascience.common.exception.DataScienceException;
import org.zjvis.datascience.common.model.ApiResultCode;

/**
 * @description Precondition工具类
 * @date 2020-07-23
 */
public class Preconditions {

    public static void checkNotBlankIfNotNull(String str, String errorMsg) {
        if (str == null) {
            return;
        }
        if (StrUtil.isBlank(str)) {
            throw new DataScienceException(ApiResultCode.PARAM_ERROR.getCode(), errorMsg);
        }
    }

    public static void checkSizeIfNotNull(String str, int maxSize, String errorMsg) {
        checkSizeIfNotNull(str, 1, maxSize, errorMsg);
    }

    public static void checkSizeIfNotNull(String str, int minSize, int maxSize, String errorMsg) {
        if (str == null) {
            return;
        }
        if (str.length() > maxSize || str.length() < minSize) {
            throw new DataScienceException(ApiResultCode.PARAM_ERROR.getCode(), errorMsg);
        }
    }

    public static void checkEqual(int i, int j, String errorMsg) {
        if (i == j) {
            return;
        }
        throw new DataScienceException(ApiResultCode.PARAM_ERROR.getCode(), errorMsg);
    }

    public static void checkLessAndEqual(int i, int j, String errorMsg) {
        if (i <= j) {
            return;
        }
        throw new DataScienceException(ApiResultCode.PARAM_ERROR.getCode(), errorMsg);
    }

}
