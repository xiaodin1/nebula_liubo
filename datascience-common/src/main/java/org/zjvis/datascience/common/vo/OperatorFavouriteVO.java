package org.zjvis.datascience.common.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @description 算子收藏数据表 Operator_Favourite 相关VO
 * @date 2021-04-27
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OperatorFavouriteVO {

    @NotNull(message = "任务类型不能为空")
    private Integer taskType;

    @NotNull(message = "算子类型不能为空")
    private Integer algType;

    @NotNull(message = "算子名称不能为空")
    private String algName;

    private boolean starred;

    private boolean isEdited;

    private String descUrl;

}
