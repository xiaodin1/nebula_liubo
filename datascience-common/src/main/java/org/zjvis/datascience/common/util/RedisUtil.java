package org.zjvis.datascience.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @description redis工具类
 * @date 2020-03-13
 */
@Component
@SuppressWarnings({"unchecked", "all"})
public class RedisUtil {
    private final static Logger logger = LoggerFactory.getLogger("RedisUtil");

    private RedisTemplate<Object, Object> redisTemplate;
    @Value("${jwt.online-key}")
    private String onlineKey;

    public RedisUtil(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // =============================common============================

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
        } catch (Exception e) {
            logger.error(String.format("RedisUtils expire key %s time %s error: %s", key, time, e.getMessage()), e);
            return false;
        }
        return true;
    }

    /**
     * 根据 key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(Object key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 查找匹配key
     *
     * @param pattern key
     * @return /
     */
    public List<String> scan(String pattern) {
        ScanOptions options = ScanOptions.scanOptions().match(pattern).build();
        RedisConnectionFactory factory = redisTemplate.getConnectionFactory();
        RedisConnection rc = Objects.requireNonNull(factory).getConnection();
        Cursor<byte[]> cursor = rc.scan(options);
        List<String> result = new ArrayList<>();
        while (cursor.hasNext()) {
            result.add(new String(cursor.next()));
        }
        try {
            RedisConnectionUtils.releaseConnection(rc, factory, true);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils scan pattern %s error: %s", pattern, e.getMessage()), e);
        }
        return result;
    }

    /**
     * 分页查询 key
     *
     * @param patternKey key
     * @param page       页码
     * @param size       每页数目
     * @return /
     */
    public List<String> findKeysForPage(String patternKey, int page, int size) {
        ScanOptions options = ScanOptions.scanOptions().match(patternKey).build();
        RedisConnectionFactory factory = redisTemplate.getConnectionFactory();
        RedisConnection rc = Objects.requireNonNull(factory).getConnection();
        Cursor<byte[]> cursor = rc.scan(options);
        List<String> result = new ArrayList<>(size);
        int tmpIndex = 0;
        int fromIndex = page * size;
        int toIndex = page * size + size;
        while (cursor.hasNext()) {
            if (tmpIndex >= fromIndex && tmpIndex < toIndex) {
                result.add(new String(cursor.next()));
                tmpIndex++;
                continue;
            }
            // 获取到满足条件的数据后,就可以退出了
            if (tmpIndex >= toIndex) {
                break;
            }
            tmpIndex++;
            cursor.next();
        }
        try {
            RedisConnectionUtils.releaseConnection(rc, factory, true);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils findKeysForPage patternKey %s page %s size %s error: %s", patternKey, page, size, e.getMessage()), e);
        }
        return result;
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils hasKey key %s error: %s", key, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }

    // ============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {

        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 批量获取
     *
     * @param keys
     * @return
     */
    public List<Object> multiGet(List<String> keys) {
        Object obj = redisTemplate.opsForValue().multiGet(Collections.singleton(keys));
        return null;
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils set key %s value %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils set key %s value %s time %s error: %s", key, value, time, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key      键
     * @param value    值
     * @param time     时间
     * @param timeUnit 类型
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time, TimeUnit timeUnit) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, timeUnit);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils set key %s value %s time %s timeUnit %s error:%s", key, value, time, timeUnit, e.getMessage()), e);
            return false;
        }
    }

    // ================================Map=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);

    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils hmset key %s map %s error: %s", key, map, e.getMessage()), e);
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils hmset key %s map %s time %s error: %s", key, map, time, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils hset key %s item %s value %s error: %s", key, item, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils hset key %s item %s value %s time %s error: %s", key, item, value, time, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 删除hash表中的所有的值
     *
     * @param key  键 不能为null
     */
    public void hdelAll(String key) {
        Map<Object, Object> allItems = hmget(key);
        Set<Map.Entry<Object, Object>> entries = allItems.entrySet();
        for (Map.Entry<Object, Object> entry : entries) {
            hdel(key,entry.getKey());
        };
    }


    /**
     * HashGetAllKeys
     *
     * @param key  键 不能为null
     * @return 值
     */
    public Set<Object> hKeys(String key) {
        return redisTemplate.opsForHash().keys(key);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    // ============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils sGet key %s error: %s", key, e.getMessage()), e);
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils sHasKey key %s value %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils sSet key %s values %s error: %s", key, values, e.getMessage()), e);
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils sSetAndTime key %s time %s values %s error: %s", key, time, values, e.getMessage()), e);
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils sGetSetSize key %s error: %s", key, e.getMessage()), e);
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils setRemove key %s values %s error: %s", key, values, e.getMessage()), e);
            return 0;
        }
    }

    // ===============================sorted set=================================

    /**
     * 将zSet数据放入缓存
     *
     * @param key
     * @param time
     * @param values
     * @return Boolean
     */
    public Boolean zSet(String key, long time, Object value) {
        try {
            Boolean success = redisTemplate.opsForZSet().add(key, value, System.currentTimeMillis());
            if (success) {
                expire(key, time);
            }
            return success;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils zSet key %s time %s value %s error:%s", key, time, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 返回有序集合所有成员，从大到小排序
     *
     * @param key
     * @return Set<Object>
     */
    public Set<Object> zGet(String key) {
        try {
            return redisTemplate.opsForZSet().reverseRange(key, Long.MIN_VALUE, Long.MAX_VALUE);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils zGet key %s error: %s", key, e.getMessage()), e);
            return null;
        }
    }


    // ===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public List<Object> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lGetIndex key %s start %s end %s error: %s", key, start, end, e.getMessage()), e);
            return null;
        }
    }

    public List<Object> lGet(String key) {
        try {
            return redisTemplate.opsForList().range(key, 0, -1);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lGetIndex key %s [all data] error: %s", key, e.getMessage()), e);
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lGetListSize key %s error: %s", key, e.getMessage()), e);
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lGetIndex key %s index %s error: %s", key, index, e.getMessage()), e);
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lSet key %s value %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lSet key %s value %s time %s error: %s", key, value, time, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lSet key %s value %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lSet key %s value %s time %s error: %s", key, value, time, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return /
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lUpdateIndex key %s index %s value %s error: %s", key, index, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            return redisTemplate.opsForList().remove(key, count, value);
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lRemove key %s count %s value %s error:%s", key, count, value, e.getMessage()), e);
            return 0;
        }
    }

    /**
     * 从头部加入值
     *
     * @param key 键
     * @param value 值
     *
     * @return
     * */

    public Boolean llPush(String key,String value){
        try {
            redisTemplate.opsForList().leftPush(key, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lSet key %s value %s time %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 从尾部加入值
     *
     * @param key 键
     * @param value 值
     *
     * @return
     * */

    public Boolean lrPush(String key,String value){
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils lSet key %s value %s time %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 从尾部移除count个值
     *
     * @param key 键
     * @param count 移除个数
     *
     * @return 列表
    * */

    public List<Object> lrPop(String key,long count){
        long l = lGetListSize(key);
        List<Object> res = new ArrayList<>();
        if(l<count){
            while(l>0){
                res.add(redisTemplate.opsForList().rightPop(key));
                l--;
            }
        }else {
            while(count>0){
                res.add(redisTemplate.opsForList().rightPop(key));
                count--;
            }
        }
        return res;
    }

    /**

     * 尝试获取分布式锁

     * @param jedis Redis客户端

     * @param lockKey 锁

     * @param requestId 请求标识

     * @param expireTime 超期时间

     * @return 是否获取成功

     */
    private static final Long SUCCESS = 1L;
    public boolean tryGetDistributedLock(String lockKey, String requestId,int expireTime) {

        String script="local v=redis.call('get', KEYS[1]); if(v) then return 0 else redis.call('set',KEYS[1],ARGV[1]) redis.call('expire',KEYS[1], ARGV[2]) return 1 end";
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>(script, Long.class);
        Long result = redisTemplate.execute(redisScript, Collections.singletonList(lockKey), requestId,expireTime);
        if (SUCCESS.equals(result)) {
            return true;
        }
        return false;
    }

    /**

     * 释放分布式锁

     * @param jedis Redis客户端

     * @param lockKey 锁

     * @param requestId 请求标识

     * @return 是否释放成功

     */
    public boolean releaseDistributedLock(String lockKey, String requestId) {

        String script="if redis.call('get',KEYS[1])==ARGV[1] then redis.call('del',KEYS[1]) else return 0 end";
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>(script, Long.class);
        Long result = redisTemplate.execute(redisScript, Collections.singletonList(lockKey), requestId);
        System.out.println("resutl: "+ result);
        if (SUCCESS.equals(result)) {
            return true;
        }
        return false;
    }
    public boolean test(String lockKey, String requestId) {

        String script1="return redis.call('get',KEYS[1])";
        String script2="return redis.call('set',KEYS[1],ARGV[1])";
        Object result = redisTemplate.execute(RedisScript.of(script1), Collections.singletonList(lockKey), requestId);
        result = redisTemplate.execute(RedisScript.of(script2), Collections.singletonList(lockKey), requestId);
        System.out.println("resutl: "+ result);
        if (SUCCESS.equals(result)) {
            return true;
        }
        return false;
    }

    public Boolean setnx(String key, String value, Long second) {
        return redisTemplate.opsForValue().setIfAbsent(key, value, second, TimeUnit.SECONDS);
    }

    /**
     * 获取锁
     * @param key
     * @param value
     * @param expireTime 过期时间（秒）
     * @return
     */
    public Boolean getLock(String key, String value, Long expireTime) {
        return redisTemplate.opsForValue().setIfAbsent(key, value, expireTime, TimeUnit.SECONDS);
    }

    /**
     * 释放锁
     * @param key
     * @return
     */
    public Boolean releaseLock(String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 阻塞队列
     * @param key
     * @return
     */
    public Object blpop(String key) {
        return redisTemplate.opsForList().leftPop(key, 5 * 60L, TimeUnit.SECONDS);
    }


    /**
     * 从头部加入值
     * @param key 键
     * @param value 值
     * @param time 过期时间
     * @return
     */
    public Boolean llPush(String key, Object value, long time){
        try {
            redisTemplate.opsForList().leftPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            logger.error(String.format("RedisUtils llPush key %s value %s error: %s", key, value, e.getMessage()), e);
            return false;
        }
    }

    /**
     * 全量删除缓存
     * @param key 可以传一个值 或多个
     */
    public void allDel(String key) {
        Set<Object> keys = redisTemplate.keys(key + "*");
        redisTemplate.delete(keys);
    }


    /**
     * 删除hash表中的所有指定前缀的值
     *
     */
    public void hdelAll(String key, String item) {
        Set<Object> keys = redisTemplate.opsForHash().keys(key);
        for (Object o : keys) {
            String itemValue = (String) o;
            if (itemValue.startsWith(item)) hdel(key, itemValue);
        }
    }
}
