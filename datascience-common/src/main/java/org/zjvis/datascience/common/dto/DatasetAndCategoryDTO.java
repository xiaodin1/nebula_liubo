package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 数据集类型信息表，数据集类型DTO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetAndCategoryDTO {

    private Long categoryId;

    private String categoryName;

    private Long datasetId;

    private String datasetName;

    private String dataJson;

    private Integer status;
}
