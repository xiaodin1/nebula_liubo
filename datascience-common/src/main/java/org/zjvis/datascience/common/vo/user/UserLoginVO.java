package org.zjvis.datascience.common.vo.user;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 用户登录相关VO
 * @date 2021-12-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginVO {

    private static final long serialVersionUID = -6347175492072993897L;

    @NotBlank(message = "用户名或手机号不能为空")
    @Length(max = 20, message = "用户名长度不能超过20")
    @ApiModelProperty(value = "登录用户名/手机号", required = true)
    private String name;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

//    @ApiModelProperty(value = "记住密码")
//    private Boolean rememberMe;

}
