package org.zjvis.datascience.common.dto.notice;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @description 用户消息提醒DTO
 * @date 2021-12-24
 */
@Data
@Builder
public class NoticeDTO{
    private Long id;
    private String title;
    private Long userId;
    private String content;
    private String process;
    private Integer type;
    private Integer status;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModify;
    private Long gmtCreator;
    private Long gmtModifier;
    private Integer filterType;
    private Long projectId;
    private List<Long> ids;
}
