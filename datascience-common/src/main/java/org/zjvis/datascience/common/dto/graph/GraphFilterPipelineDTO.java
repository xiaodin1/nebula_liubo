package org.zjvis.datascience.common.dto.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.BaseDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.graph.GraphFilterPipelineVO;

/**
 * @description Graph分析构建清洗PipelineDTO
 * @date 2021-12-24
 */
@Data
public class GraphFilterPipelineDTO extends BaseDTO {
    private Long id;

    private String name;

    private Long projectId;

    private Long graphId;

    private Long userId;

    private String dataJson;

    public GraphFilterPipelineVO view() {
        GraphFilterPipelineVO vo = DozerUtil.mapper(this, GraphFilterPipelineVO.class);
        vo.setData(JSONObject.parseObject(this.dataJson));
        return vo;
    }
}
