package org.zjvis.datascience.common.dto.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description HTTP上传数据集配置信息DTO
 * @date 2021-12-24
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatasetHttpInfoDTO {

    private String url;

    private boolean status;
}
