package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 项目用户类型枚举类
 * @date 2020-07-23
 */
@Getter
public enum ProjectUserTypeEnum {
    creator(1,"创建者"),
    partaker(2,"参与者");
    private int type;
    private String name;
    ProjectUserTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
}
