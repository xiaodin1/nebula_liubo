package org.zjvis.datascience.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description 正则表达式工具类
 * @date 2020-09-04
 */
public class RegexpUtil {

    public static final String GP_SPECIAL_CHARACTER_REGEXP = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";

    /**
     * 检查字符串是否包含greenplum特殊字符
     * @param str
     * @return
     */
    public static boolean checkGPSpecialCharacter (String str){
        Pattern p = Pattern.compile(GP_SPECIAL_CHARACTER_REGEXP);
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        } else {
            return false;
        }
    }
}