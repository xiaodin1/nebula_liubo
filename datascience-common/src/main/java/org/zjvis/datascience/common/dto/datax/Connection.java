package org.zjvis.datascience.common.dto.datax;

import lombok.Data;

import java.util.List;

@Data
public class Connection {
    private List<String> table;
}
