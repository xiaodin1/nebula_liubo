package org.zjvis.datascience.common.widget.request;

import lombok.Data;
import org.apache.commons.compress.utils.Lists;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.validator.NotNullNumber;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 用于关联图表渲染的请求POJO 用于图表联动和 筛选器控件
 * @date 2021-12-01
 */
@Data
public class RelateWidgetRequestVO {

    private Long widgetId;

    private String xAttrStr;

    @Min(value = 1, message = "please enter a valid projectId")
    private Long projectId;

    //允许没有dashboardId 但是没有dashboardId 不返回数据。
    @NotNull
    private Long dashboardId;

    private Boolean isFilterWidget = false;

    private Long dataId;

    private String dataType;


    public RelateWidgetResultVO returnEmpty(){
        RelateWidgetResultVO resultVO = DozerUtil.mapper(this, RelateWidgetResultVO.class);
        resultVO.setRelateWidgets(Lists.newArrayList());
        return resultVO;
    }
}
