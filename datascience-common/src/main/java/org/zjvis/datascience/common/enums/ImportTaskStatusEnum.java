package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 数据导入任务状态枚举类
 * @date 2020-08-31
 */
@Getter
public enum ImportTaskStatusEnum {

    PENDING("进行中", (byte) 1),
    SUCCESS("成功", (byte) 2),
    FAIL("失败", (byte) 3),
    CANCEL("取消", (byte) 4),
    ;

    private String desc;
    private Byte value;

    ImportTaskStatusEnum(String desc, Byte value) {
        this.desc = desc;
        this.value = value;
    }

}
