package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;
import org.zjvis.datascience.common.util.DozerUtil;

import java.time.LocalDateTime;

/**
 * @description 任务实例数据表 TaskInstance 相关VO
 * @date 2020-10-20
 */
@Data
public class TaskInstanceVO extends BaseVO {
    private Long id;

    private String parentId;

    private Long taskId;

    private Long pipelineId;

    private Long projectId;

    private Long userId;

    private String status;

    private JSONObject data;

    private String sqlText;

    private String output;

    private Long sessionId;

    private String logInfo;

    private int runTimes;

    private String taskName;

    private Integer type;

    private Integer progress;

    private String applicationId;

    private LocalDateTime gmtRunning;

    public TaskInstanceDTO toTask(TaskInstanceVO vo) {
        TaskInstanceDTO taskInstanceDTO = DozerUtil.mapper(this, TaskInstanceDTO.class);
        taskInstanceDTO.setDataJson(data.toJSONString());
        return taskInstanceDTO;
    }
}
