package org.zjvis.datascience.common.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @description 数据导入配置DTO
 * @date 2020-10-09
 */
@Data
@Builder
public class DataConfigDTO {

    private String databaseType;

    private String server;

    private Integer port;

    private String user;

    private String password;

    private String databaseName;

    private String tableName;

    private String url;
}
