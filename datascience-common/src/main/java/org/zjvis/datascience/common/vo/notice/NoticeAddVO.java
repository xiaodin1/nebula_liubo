package org.zjvis.datascience.common.vo.notice;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @description 用户反馈增加 相关VO
 * @date 2021-09-15
 */
@Data
public class NoticeAddVO {
    @NotNull(message = "userId不能为空")
    @Min(value = 1,message = "请输入有效的userId")
    private Long userId;

    @NotBlank(message = "title长度不能超过100")
    @Length(max = 100,message = "title长度不能超过100")
    private String title;

    @NotBlank(message = "content不能为空")
    @Length(max = 1000, message = "content长度不能超过1000")
    private String content;
}
