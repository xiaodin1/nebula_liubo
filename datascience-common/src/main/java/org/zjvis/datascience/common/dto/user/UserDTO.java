package org.zjvis.datascience.common.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;
import org.zjvis.datascience.common.dto.BaseDTO;

import java.util.Date;

/**
 * @description 用户DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class UserDTO extends BaseDTO {

    private static final long serialVersionUID = -6873153579745180633L;

    private Long id;

    private String name;

    private String email;

    private Byte status;

    private String password;

    private String phone;

    private String remark;

    private Integer role;

    private Byte guideStatus;

    /*
    注册后完善
     */
    private String nickName; //昵称

    private String realName; //真实姓名

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBirth; //出生日期

    private String gender; //性别

    private String profession; //职业

    private String workOrg; //工作单位

    private String address; //通信地址

    private String researchField; //研究领域

    private String picEncode; //用户头像编码

}
