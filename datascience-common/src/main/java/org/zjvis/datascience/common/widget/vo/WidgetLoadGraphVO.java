package org.zjvis.datascience.common.widget.vo;

import lombok.Data;
import org.zjvis.datascience.common.validator.NotNullNumber;

import javax.validation.constraints.NotEmpty;

/**
 * @description 用于图表控件渲染图数据文件的请求POJO （单文件名）
 * @date 2021-12-01
 */
@Data
public class WidgetLoadGraphVO {

    @NotEmpty(message = "fileName cannot be null")
    private String fileName;

    @NotNullNumber(message = "please enter a valid projectId")
    private Long projectId;
}
