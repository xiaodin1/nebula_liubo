package org.zjvis.datascience.common.widget.enums;

import org.zjvis.datascience.common.enums.AlgEnum;

/**
 * @description 可配置的图表控件类型 枚举类
 * @date 2021-12-01
 */
public enum WidgetConfigEnum {

    ALGO("algo", 1),
    CONFIG("config", 2);

    private String desc;

    private Integer val;

    WidgetConfigEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }


    public static Boolean isNeedAlgo(int type){
        return type == AlgEnum.KMEANS.getVal() ||
                type == AlgEnum.TSNE.getVal() ||
                type == AlgEnum.PCA_DENSE.getVal() ||
                type == AlgEnum.LLE.getVal();
    }
}
