package org.zjvis.datascience.common.model.stat;

/**
 * @description 字段操作常量类
 * @date 2021-09-15
 */
public class ColumnConstant {
    /**
     * 详细信息模式
     */
    public final static Integer MODE_DETAIL = 1;
    /**
     * 摘要模式
     */
    public final static Integer MODE_ABSTRACT = 2;

    /**
     * 可视化视图表格组件查询
     */
    public final static Integer MODE_TABLE = 50;
    /**
     * 详细信息模式概览分段数
     */
    public final static Integer STAT_DETAIL_SEGMENT = 60;
    /**
     * 摘要模式分段数
     */
    public final static Integer STAT_ABSTRACT_SEGMENT = 13;
    /**
     * 详细模式默认每页大小
     */
    public final static Integer STAT_DETAIL_PAGESIZE = 12;

    /**
     * 按字典序排序
     */
    public final static Integer DICTIONARY_SORT_TYPE = 1;
    /**
     * 按统计值排序
     */
    public final static Integer COUNT_SORT_TYPE = 2;
    /**
     * 降序
     */
    public final static String DESC_ORDER = "DESC";
    /**
     * 清洗节点历史记录的图表
     */
    public final static String TYPE_CLEAN = "tclean";
    /**
     * 一个临时的列名
     */
    public final static String ROW_NUM = "_row_num_";

    /**
     * 拆分出前n个
     */
    public final static int SPLIT_FIRST = 1;
    /**
     * 拆分出后n个
     */
    public final static int SPLIT_LAST = 2;
    /**
     * 拆分出全部
     */
    public final static int SPLIT_ALL = 3;
    /**
     * 清洗推荐的筛选类型
     */
    public final static String TRANSFORM_TYPE = "transformType";
    /**
     * 筛选类型-筛选器
     */
    public final static String FILTER = "FILTER";
    /**
     * 筛选类型-刷选内容
     */
    public final static String BRUSH_CONTENT = "BRUSH_CONTENT";
    /**
     * 筛选类型-以单词开头
     */
    public final static String STARTS_WITH = "STARTS_WITH";
    /**
     * 筛选类型-以单词结尾
     */
    public final static String ENDS_WITH = "ENDS_WITH";
    /**
     * 筛选类型-光标位置
     */
    public final static String POSITION = "POSITION";
    /**
     * 筛选类型-单词位置
     */
    public final static String WORD_POSITION = "WORD_POSITION";
    /**
     * 筛选类型-字符串前/后
     */
    public final static String AFTER_BEFORE = "AFTER_BEFORE";
    /**
     * 表中所有空值
     */
    public final static String SELECT_NULL = "SELECT_NULL";
    /**
     * 去除重复行
     */
    public final static String TRANSFORM_REMOVE = "TRANSFORM_REMOVE";
    /**
     * 多个单元格
     */
    public final static String MULTIPLE_CELL = "MULTIPLE_CELL";

    //Data Wrangling by Result type start
    /**
     * 整数重复
     */
    public final static String INT_REPEAT = "INT_REPEAT";
    /**
     * 首尾填充字符串
     */
    public final static String STRING_FILLING = "STRING_FILLING";
    /**
     * 多项式
     */
    public final static String POLYNOMIAL = "POLYNOMIAL";
    /**
     * 小数点移位
     */
    public static final String DECIMAL_POINT_MOVE = "DECIMAL_POINT_MOVE";
    /**
     * 四舍五入
     */
    public static final String ROUND = "ROUND";
    /**
     * 向上保留n位小数
     */
    public static final String CEIL = "CEIL";
    /**
     * 向下保留n位小数
     */
    public static final String FLOOR = "FLOOR";
    /**
     * 字符串重复
     */
    public static final String STRING_REPEAT = "STRING_REPEAT";
    /**
     * 单词去除
     */
    public static final String WORD_REMOVE = "WORD_REMOVE";
    /**
     * 单词保留
     */
    public static final String WORD_RETAIN = "WORD_RETAIN";
    /**
     * 中间填充字符串
     */
    public static final String STRING_INSERT = "STRING_INSERT";
    /**
     * 单词的大小写切换
     */
    public static final String WORD_CASE = "WORD_CASE";
    /**
     * 单词缩写
     */
    public static final String WORD_INITIAL = "WORD_INITIAL";
    /**
     * 特殊字符替换
     */
    public static final String SPECIAL_CHARACTER_REPLACE = "SPECIAL_CHARACTER_REPLACE";
    /**
     * 根据下标删除字符
     */
    public static final String CURSOR_REMOVE = "CURSOR_REMOVE";
    /**
     * 根据下标保留字符
     */
    public static final String CURSOR_RETAIN = "CURSOR_RETAIN";
    /**
     * 日期转成指定格式的字符串
     */
    public static final String DATE_FORMAT = "DATE_FORMAT";
    /**
     * 默认的处理
     */
    public static final String DEFAULT_EXTRACT = "DEFAULT_EXTRACT";
    //Data Wrangling by Result type end

    /**
     * 用户输入的赋值新值
     */
    public static final String SET_VALUE = "setValue";
    /**
     * 用户输入的赋值新值 使用的策略
     */
    public static final String SET_VALUE_POLICY = "setValuePolicy";
    /**
     * 用于去重的列
     */
    public static final String PARTITION_COLS = "partitionCols";
    /**
     * 用于排序的列名
     */
    public static final String SORT_COL = "sortCol";
    /**
     * 排序方式
     */
    public static final String SORT_VAL = "sortVal";
    /**
     * 列名
     */
    public static final String COL = "col";
    /**
     * 新列名
     */
    public static final String NEW_COL = "newCol";
    /**
     * 用户刷选的内容
     */
    public static final String BRUSH_VALUE = "brushValue";
    /**
     * 刷选的首部单词
     */
    public static final String STARTS_VALUE = "startsValue";
    /**
     * 刷选的尾部单词
     */
    public static final String ENDS_VALUE = "endsValue";
    /**
     * 需要替换的值
     */
    public static final String ORIGIN_VALUE = "originValue";
    /**
     * 替换的值
     */
    public static final String REPLACE_VALUE = "replaceValue";
    /**
     * 光标起始位置
     */
    public static final String CURSOR_START = "cursorStart";
    /**
     * 光标结束位置
     */
    public static final String CURSOR_END = "cursorEnd";
    /**
     * 拆分数量
     */
    public static final String SPLIT_NUM = "splitNum";
    /**
     * 单词位置
     */
    public static final String WORD_INDEX = "wordIndex";
    /**
     * 在某字符串之后
     */
    public static final String AFTER = "after";
    /**
     * 在某字符串之前
     */
    public static final String BEFORE = "before";
    /**
     * 重复多少次
     */
    public static final String REPEAT_NUM = "repeatNum";
    /**
     * 首部填充
     */
    public static final String PREFIX = "prefix";
    /**
     * 尾部填充
     */
    public static final String SUFFIX = "suffix";
    /**
     * 多项式的系数
     */
    public static final String COEFFICIENT = "coefficient";
    /**
     * 小数点移动的位数
     */
    public static final String MOVE_NUM = "moveNum";
    /**
     * 保留数
     */
    public static final String RETAIN_NUM = "retainNum";
    /**
     * 从第几个单词开始
     */
    public static final String STARTS = "starts";
    /**
     * 删除数量
     */
    public static final String REMOVE_NUM = "removeNum";
    /**
     * 填充的字符串
     */
    public static final String INSERT_VALUE = "insertValue";
    /**
     * 字符串大小写切换参数
     */
    public static final String WORD_CASE_PARAM = "wordCase";
    /**
     * 大小写切换类型
     */
    public static final String CASE_TYPE = "caseType";
    /**
     * 单词大写
     */
    public static final String UPPER = "UPPER";
    /**
     * 单词小写
     */
    public static final String LOWER = "LOWER";
    /**
     * 首字母大写
     */
    public static final String FIRST_UPPER = "FIRST_UPPER";
    /**
     * 首字母小写
     */
    public static final String FIRST_LOWER = "FIRST_LOWER";
    /**
     * 保留单词首字母的参数
     */
    public static final String WORDS = "words";
    /**
     * 传参
     */
    public static final String PARAMS = "params";
    /**
     * 时间单位
     */
    public static final String TIME_UNIT = "timeUnit";
    /**
     * 时间的描述
     */
    public final static String DESC = "desc";
}
