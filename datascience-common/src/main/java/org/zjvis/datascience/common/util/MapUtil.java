package org.zjvis.datascience.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @description 键值对工具类
 * @date 2021-12-10
 */
public class MapUtil {


    /**
     * 反转map  K, V -> V, K
     *
     * @param map
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<V, K> reverseMap(Map<K, V> map) {
        Map<V, K> reverseRelationMap = new HashMap<>();
        Set<? extends Map.Entry<K, V>> entrys = map.entrySet();
        for (Map.Entry<K, V> item : entrys) {
            reverseRelationMap.put(item.getValue(), item.getKey());
        }
        return reverseRelationMap;
    }

    /**
     * 改变map 中 key的类型
     * 后续需要可以自己加
     *
     * @param map
     * @param clazz
     * @param <T>
     * @param <K>
     * @param <V>
     * @return
     */
    public static <T, K, V> Map<T, V> reMap(Map<K, V> map, Class<T> clazz) {
        Map<T, V> resultMap = new HashMap<>();
        Set<? extends Map.Entry<K, V>> entrys = map.entrySet();
        for (Map.Entry<K, V> item : entrys) {
            if (clazz.isInstance("")) {//String.class
                resultMap.put(clazz.cast(String.valueOf(item.getKey())), item.getValue());
            }
        }
        return resultMap;
    }
}
