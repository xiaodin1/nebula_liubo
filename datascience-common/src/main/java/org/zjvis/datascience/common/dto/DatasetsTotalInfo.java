package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.datasetsinfo.DatasetsInfoDTO;

import java.util.List;

/**
 * @description 数据查完整信息
 * @date 2021-05-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetsTotalInfo {

    private List<DatasetsInfoDTO> datasetsInfoDTOList;

    private Long tableCnt;

    private String totalSize;
}
