package org.zjvis.datascience.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @description 查询请求操作
 * @date 2021-01-08
 */
@Data
public class AuditQueryDTO implements Serializable {

    private static final long serialVersionUID = 7580890960607559757L;

    private String username;

    private String requestType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 每页显示条数
     */
    private Integer pageSize;

    /**
     * 当前页号
     */
    private Integer curPage;

    /**
     * 分页偏置，由curPage和pageSize计算出
     */
    private Integer pageStart;

}
