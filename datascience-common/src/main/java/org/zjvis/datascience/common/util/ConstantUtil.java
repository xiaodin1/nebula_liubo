package org.zjvis.datascience.common.util;

/**
 * @description 数据库驱动枚举工具类
 * @date 2020-09-22
 */
public class ConstantUtil {

    public static final int DATASOURCE_TYPE_GREENPLUM = 0;
    public static final int DATASOURCE_TYPE_TIDB = 1;
    public static final int DATASOURCE_TYPE_MYSQL = 2;
    public static final int DATASOURCE_TYPE_HIVE = 3;
    public static final int DATASOURCE_TYPE_SQLSERVER = 4;
    public static final int DATASOURCE_TYPE_ORACLE = 5;
    public static final int DATASOURCE_TYPE_HANA = 6;
    public static final int DATASOURCE_TYPE_DRUID = 7;
    public static final int DATASOURCE_TYPE_CLICKHOUSE = 8;
    public static final int DATASOURCE_TYPE_ELASTICSEARCH = 9;
    public static final int DATASOURCE_TYPE_POSTGRES = 10;


    public static final int TASK_TYPE_ALGO = 1;
    public static final int TASK_TYPE_ETL = 2;
    public static final int TASK_TYPE_DATA = 3;


}
