package org.zjvis.datascience.common.vo.dataset;

import cn.hutool.db.Entity;
import lombok.Data;

import java.util.List;

/**
 * @description 数据结果相关VO
 * @date 2021-08-24
 */
@Data
public class DatasetAndHeadVO {
    private List<HeadVO> head;
    private List<Entity> data;
}
