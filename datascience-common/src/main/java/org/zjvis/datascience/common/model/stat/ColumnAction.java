package org.zjvis.datascience.common.model.stat;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.vo.TaskInstanceVO;

/**
 * @description 字段操作类
 * @date 2020-11-05
 */
@Data
public class ColumnAction {
    private Long projectId;
    private Long taskId;
    private String taskName;
    private JSONObject data;

    public ColumnAction(TaskInstanceVO vo) {
        this.projectId = vo.getProjectId();
        this.taskId = vo.getTaskId();
        this.taskName = vo.getTaskName();
        this.data = vo.getData();
    }
}
