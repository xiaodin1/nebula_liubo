package org.zjvis.datascience.common.vo.dataset;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 数据上传相关VO
 * @date 2021-12-08
 */
@Data
public class DatasetFileWriteVO extends BaseTableConfigVO{

    @NotBlank(message = "请选择数据分类")
    private String categoryId;

    @NotBlank(message = "数据集名称不能为空")
    private String name;

    @NotBlank(message = "请选择您想导入的数据类型 excel.表格文件excel\\cvs , table.外部数据库")
    private String importType;

    @NotBlank(message = "上传文件名不能为空")
    private String filename;

    /**
     * 第一行是否作为字段名
     */
    private Boolean firstLineAsFields;

    private String sheetName;

    private String separate;

    private String quote;

    private String escape;
    // 唯一标识
    private String identifier;

    private String charSet;
}
