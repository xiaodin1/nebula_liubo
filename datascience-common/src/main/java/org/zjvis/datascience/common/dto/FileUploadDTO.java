package org.zjvis.datascience.common.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.vo.dataset.PreviewDatasetVO;

import java.util.List;

/**
 * @description 文件上传DTO
 * @date 2020-12-08
 */
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadDTO {
    private int code;

    @ApiParam(value = "缺少的块号")
    private List<Integer> missChunks;

    private Integer completeChunk;

    private boolean isComplete;

    private List<PreviewDatasetVO> data;

    private String tips;
    // 唯一标识
    private String identifier;

    private List<String> headers;

    private Integer progress;
}
