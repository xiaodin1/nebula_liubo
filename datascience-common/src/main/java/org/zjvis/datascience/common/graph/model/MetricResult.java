package org.zjvis.datascience.common.graph.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @description MetricResult
 * @date 2021-12-29
 */
@Data
@NoArgsConstructor
public class  MetricResult {
    public String tag;

    public Map<String, Object> result;

    public Object value;

    public MetricResult(String tag, Map result, Object value) {
        this.tag = tag;
        if (result != null) {
            this.result = new HashMap();
            Iterator<Map.Entry<Object, Object>> it = result.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Object, Object> entry = it.next();
                this.result.put(entry.getKey().toString(), entry.getValue());
            }
        }
        this.value = value;

    }
}
