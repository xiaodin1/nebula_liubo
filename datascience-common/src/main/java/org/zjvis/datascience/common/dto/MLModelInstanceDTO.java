package org.zjvis.datascience.common.dto;


import lombok.Data;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.MLModelInstanceVO;

import java.time.LocalDateTime;

/**
 * @description 模型训练实例信息表，模型训练instance DTO
 * @date 2021-11-23
 */
@Data
public class MLModelInstanceDTO extends BaseDTO {

    private Long id;

    private Long projectId;

    private Long userId;

    private String status;

    private Long duringTime;

    private LocalDateTime gmtRunning;

    private String algorithm;

    private String modelDesc;

    private String modelType;

    private String name;


    public MLModelInstanceVO view() {
        MLModelInstanceVO vo = DozerUtil.mapper(this, MLModelInstanceVO.class);
        return vo;
    }
}
