package org.zjvis.datascience.common.etl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.enums.ETLEnum;
import org.zjvis.datascience.common.exception.SqlParserException;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.common.sql.SqlHelper;
import org.zjvis.datascience.common.util.ToolUtil;
import org.zjvis.datascience.common.vo.TaskVO;

import java.util.Iterator;
import java.util.List;

/**
 * @description ETL操作模板父类
 * @date 2021-12-27
 */
public class BaseETL {

    protected final static int SAMPLE_NUMBER = 5000;

    protected int maxParentNumber = -1;

    private String name;

    protected String engineName = "madlib";

    protected final static String ID_COL = "_record_id_";

    private int subType;

    private String subTypeName;

    public String getName() {
        return name;
    }

    public String getSubTypeName() {
        return subTypeName;
    }

    public int getSubType() {
        return subType;
    }


    public BaseETL(String name, int subType, String subTypeName) {
        this.name = name;
        this.subType = subType;
        this.subTypeName = subTypeName;
    }

    protected void baseInitTemplate(JSONObject data) {
        data.put("algName", this.getName());
        data.put("subType", this.getSubType());
        data.put("subTypeName", this.getSubTypeName());
        data.put("input", new JSONArray());
        data.put("output", new JSONArray());
        data.put("parentType", new JSONArray());
        data.put("maxParentNumber", maxParentNumber);
    }

    public void initTemplate(JSONObject data) {
    }

    public void parserConf(JSONObject conf) {
    }

    public int getMaxParentNumber() {
        return maxParentNumber;
    }

    public boolean verify(TaskVO vo, List<ApiResultCode> errorCode) {
        return true;
    }

    /**
     * 定义每个算子的输出表格式
     *
     * @param vo
     * @return
     */
    public void defineOutput(TaskVO vo) throws SqlParserException {
    }

    public static BaseETL instance(JSONObject conf) {
        int type = conf.getIntValue("algType");
        if (ETLEnum.FILTER.getVal() == type) {
            return new Filter();
        } else if (ETLEnum.UNION.getVal() == type) {
            return new Union();
        } else if (ETLEnum.JOIN.getVal() == type) {
            return new Join();
        } else if (ETLEnum.SAMPLE.getVal() == type) {
            return new Sample();
        } else if (ETLEnum.SQL.getVal() == type) {
            return new Sql();
        } else if (ETLEnum.PIVOT_TABLE.getVal() == type) {
            return new PivotTable();
        }
        return null;
    }

    protected void prepareOutputColumnTypes(JSONArray outputColumnType, List<String> outputColumnNames,
                                            List<String> columnNames, List<String> columnTypes) {
        if (outputColumnNames == null || columnNames == null || columnTypes == null) {
            return;
        }
        Iterator<String> iterator = outputColumnNames.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String type = ToolUtil.getSpecColumnType(columnNames, columnTypes, key);
            if (StringUtils.isEmpty(type)) {
                //用户自定义的字段， 默认类型为VARCHAR
                type = "VARCHAR";
            }
            outputColumnType.add(type);
        }
    }

    protected void setSubTypeForOutput(JSONObject json) {
        json.put("subType", this.subType);
    }

    public String initSql(JSONObject json, List<SqlHelper> helpers, long timeStamp) {
        return initSql(json, helpers, timeStamp, engineName);
    }


    public String initSql(JSONObject conf, List<SqlHelper> sqlHelpers, long timeStamp, String engineName) {
        return StringUtils.EMPTY;
    }

}
