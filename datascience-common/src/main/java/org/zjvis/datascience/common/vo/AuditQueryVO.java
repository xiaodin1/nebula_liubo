package org.zjvis.datascience.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 数据 接口审计相关VO
 * @date 2021-01-08
 */
@Data
public class AuditQueryVO extends BaseVO {

    private String username;

    private String requestType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 每页显示条数
     */
    private Integer pageSize;

    /**
     * 当前页号
     */
    private Integer curPage;

}
