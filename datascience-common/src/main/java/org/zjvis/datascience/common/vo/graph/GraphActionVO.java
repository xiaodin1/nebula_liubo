package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.graph.GraphActionDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.BaseVO;

@Data
public class GraphActionVO extends BaseVO {
    private Long id;
    private String actionType;
    private Long projectId;
    private Long pipelineId;
    private Long taskId;
    private Long graphId;
    private Long userId;
    private JSONObject contextObj;

    public GraphActionDTO toDTO() {
        GraphActionDTO dto = DozerUtil.mapper(this, GraphActionDTO.class);
        dto.setContext(JSONObject.toJSONString(contextObj));
        return dto;
    }
}
