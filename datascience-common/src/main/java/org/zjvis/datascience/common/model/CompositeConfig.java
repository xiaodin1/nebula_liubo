package org.zjvis.datascience.common.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @description SQL复杂条件包装类
 * @date 2020-07-27
 */
@Data
public class CompositeConfig extends ConfigComponent {

    private String type;

    private List<ConfigComponent> configComponents = new ArrayList<ConfigComponent>();

    @Override
    public Iterator<ConfigComponent> getIterator() {
        return configComponents.iterator();
    }

    public void add(ConfigComponent configComponent) {
        configComponents.add(configComponent);
    }

}
