package org.zjvis.datascience.common.vo;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 任务节点表 Task存储相关VO
 * @date 2021-11-26
 */
@Data
public class TaskSaveVO {
    @ApiRequestInclude(groups = {Dataset.class, DatasetNameCheck.class})
    @NotNull(message = "categoryId不能为空", groups = {Dataset.class, DatasetNameCheck.class})
    @Min(value = 1, message = "请输入有效的categoryId", groups = {Dataset.class, DatasetNameCheck.class})
    private Long categoryId;

    @ApiRequestInclude(groups = {Dataset.class, DatasetNameCheck.class})
    //@NotNull(message = "categoryName不能为空", groups = {Dataset.class, DatasetNameCheck.class})
    private String categoryName;

    @ApiRequestInclude(groups = {DataSource.class, Dataset.class, DatasetNameCheck.class})
    @NotNull(message = "数据集名不能为空", groups = {DataSource.class, Dataset.class, DatasetNameCheck.class})
    private String datasetName;

    @ApiRequestInclude(groups = {DataSource.class, Dataset.class})
    @NotNull(message = "projectId不能为空", groups = {DataSource.class, Dataset.class})
    @Min(value = 1, message = "请输入有效的projectId", groups = {DataSource.class, Dataset.class})
    private Long projectId;

    @ApiRequestInclude(groups = {DataSource.class, Dataset.class})
    @NotNull(message = "taskId不能为空", groups = {DataSource.class, Dataset.class})
    @Min(value = 1, message = "请输入有效的taskId", groups = {DataSource.class, Dataset.class})
    private Long taskId;

    private String tableNameML;

    public interface DataSource {
    }

    public interface Dataset {
    }

    public interface DatasetNameCheck {
    }

}
