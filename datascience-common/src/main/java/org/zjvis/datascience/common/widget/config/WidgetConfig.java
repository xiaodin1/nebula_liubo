package org.zjvis.datascience.common.widget.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @description widget图表组装设置POJO
 * @date 2021-12-14
 */
@Data
public class WidgetConfig {
    private Long dataSetId;
    private Integer top;
    private List<WidgetSelectedConfig> selected;
    private List<WidgetWhereConfig> where;
    private List<WidgetGroupConfig> group;

    public static WidgetConfig parse(JSONObject widgetJson) {
        WidgetConfig widgetConfig = new WidgetConfig();
        JSONArray rows = widgetJson.getJSONArray("rows");
        JSONArray columns = widgetJson.getJSONArray("columns");
        JSONArray filters = widgetJson.getJSONArray("filters");
        JSONArray values = widgetJson.getJSONArray("values");

        List<WidgetSelectedConfig> selectList = new ArrayList<>();
        List<WidgetWhereConfig> whereList = new ArrayList<>();
        List<WidgetGroupConfig> groupList = new ArrayList<>();

        if (rows != null) {
            handleColumnsOrRows(rows, selectList, whereList, groupList);
        }

        if (columns != null) {
            handleColumnsOrRows(columns, selectList, whereList, groupList);
        }

        if (values != null) {
            handleValues(values, selectList);
        }

        widgetConfig.setSelected(selectList);
        widgetConfig.setWhere(whereList);
        widgetConfig.setGroup(groupList);
        widgetConfig.setTop(widgetJson.getInteger("top"));
        return widgetConfig;
    }

    private static void handleColumnsOrRows(JSONArray ja, List<WidgetSelectedConfig> selectList,
                                            List<WidgetWhereConfig> whereList, List<WidgetGroupConfig> groupList) {
        if (ja != null) {
            for (int i = 0; i < ja.size(); i++) {
                JSONObject row = ja.getJSONObject(i);
                if (row != null) {
                    WidgetSelectedConfig selectConfig = new WidgetSelectedConfig();
                    WidgetGroupConfig groupConfig = new WidgetGroupConfig();

                    String colName = row.getString("name");
                    groupConfig.setName(colName);
                    selectConfig.setName(colName);

                    String alias = row.getString("alias");
                    selectConfig.setAlias(alias);

                    groupList.add(groupConfig);
                    selectList.add(selectConfig);

                    String type = row.getString("filterType");
                    JSONArray childValues = row.getJSONArray("values");
                    if (StringUtils.isNotBlank(type) && childValues != null && !childValues.isEmpty()) {
                        WidgetWhereConfig whereConfig = new WidgetWhereConfig();
                        whereConfig.setName(colName);
                        whereConfig.setFilterType(type);
                        whereConfig.setValues(childValues.toJavaList(String.class));

                        whereList.add(whereConfig);
                    }
                }
            }
        }
    }

    private static void handleValues(JSONArray ja, List<WidgetSelectedConfig> selectList) {
        if (ja != null) {
            for (int i = 0; i < ja.size(); i++) {
                JSONObject value = ja.getJSONObject(i);
                if (value != null) {
                    WidgetSelectedConfig selectConfig = new WidgetSelectedConfig();
                    selectConfig.setAggType(value.getString("aggType"));
                    selectConfig.setName(value.getString("name"));
                    selectConfig.setAlias(value.getString("alias"));
                    selectList.add(selectConfig);
                }
            }
        }
    }
}
