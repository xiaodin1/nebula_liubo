package org.zjvis.datascience.common.dto.datax;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description DataXDTO
 * @date 2021-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataXDTO {

    private String fileName;

    private String dataXJson;
}
