package org.zjvis.datascience.common.vo.user;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 用户角色相关VO
 * @date 2021-09-03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class UserRoleVO {

    private String name;

    private Integer role;

}
