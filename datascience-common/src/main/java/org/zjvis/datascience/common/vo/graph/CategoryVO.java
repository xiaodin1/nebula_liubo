package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.vo.BaseVO;

import java.io.Serializable;
import java.util.List;

@Data
public class CategoryVO implements Serializable {
    private String id;

    private CategoryAttrVO originLabel;

    private String label;

    private String value;

    private List<CategoryAttrVO> attrs;

    private CategoryAttrVO keyAttr;

    private JSONObject tableInfo;

    private Integer x;

    private Integer y;

    private Integer size;

    private JSONObject labelCfg;

    private JSONObject style;

    public String tableName() {
        return tableInfo.getString("tableName");
    }

}
