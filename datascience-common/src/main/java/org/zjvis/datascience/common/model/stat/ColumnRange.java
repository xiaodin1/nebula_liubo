package org.zjvis.datascience.common.model.stat;

import lombok.Data;

/**
 * @description 字段范围计数器，用于数据结果渲染
 * @date 2021-09-15
 */
@Data
public class ColumnRange<C extends Comparable> {

    private C low;
    private C up;
    private Integer count;
    private Integer highLight;
    private Integer anomaly;
    private Integer imputation;

    public ColumnRange(C low, C up, Integer count) {
        this.low = low;
        this.up = up;
        this.count = count;
    }

    public ColumnRange(C low, C up, Integer count, Integer value, String type) {
        this.low = low;
        this.up = up;
        this.count = count;
        if (type.equals("highLight")) {
            this.highLight = value;
        } else if (type.equals("anomaly")) {
            this.anomaly = value;
        } else if (type.equals("imputation")) {
            this.imputation = value;
        }
    }
}
