package org.zjvis.datascience.common.vo;


import cn.weiguangfu.swagger2.plus.annotation.ApiRequestExclude;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.MLModelDTO;
import org.zjvis.datascience.common.dto.PipelineDTO;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.util.JwtUtil;

/**
 * @description 机器学习算子模板数据表 Model 相关VO
 * @date 2021-04-27
 */
@Data
@NoArgsConstructor
public class MLModelVO extends BaseVO{
    private static final long serialVersionUID = 2864046932490158053L;
    private Long id;

    private String name;

    private Long projectId;

    private Long userId;

    private JSONObject data;

    private String algorithm;

    private String modelDesc;

    private String modelType;

    private JSONObject param;

    private String modelPath;

    private String sourceTable;

    private String status;

    private Long folderId;

    private Long inPanel;

    private Float runTime;

    private Long sourceId;

    private String sourceName;

    private Long invisible;

    private Long num;

    private Long sparktime;

    private String modelSavedPath;

    @ApiRequestExclude(groups = {Ignore.class})
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime trainTime;

    public MLModelVO(Long projectId){
        this.setInPanel(1L);
        this.setProjectId(projectId);
    }

    public MLModelDTO toMLModel() {
        MLModelDTO model = DozerUtil.mapper(this, MLModelDTO.class);
        model.setModelInfo(data.toJSONString());
        model.setModelParam(param.toJSONString());
        return model;
    }

    public TaskDTO toTask(){
        return null;
    }

}
