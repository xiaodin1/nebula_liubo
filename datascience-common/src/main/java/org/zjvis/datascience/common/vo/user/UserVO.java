package org.zjvis.datascience.common.vo.user;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.zjvis.datascience.common.vo.BaseVO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * @description 数据表User 用户相关VO
 * @date 2021-12-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户模型", description = "用户模型")
public class UserVO extends BaseVO {

    private static final long serialVersionUID = -6347175492072993897L;

    public static final String NAME_BLANK = "用户名不能为空";
    public static final String NAME_TOO_SHORT = "搜索用户名长度不能少于2个字符";
    public static final int NAME_SIZE = 50;
    public static final String NAME_SIZE_MSG = "用户名长度不能超过50";

    @ApiRequestInclude(groups = {Id.class})
    @NotNull(message = "用户id不能为空", groups = Id.class)
    @Min(value = 1, message = "请输入有效用户id", groups = Id.class)
    @ApiModelProperty(value = "id")
    private Long id;

    @ApiRequestInclude(groups = {Name.class})
    @NotBlank(message = NAME_BLANK)
    @Length(max = NAME_SIZE, message = NAME_SIZE_MSG)
    @ApiModelProperty(value = "登录用户名", required = true)
    private String name;

    @Pattern(regexp = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", message = "邮箱地址格式有误")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "状态", required = true)
    private Byte status;

    private Integer role;

    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1[3|4|5|7|8|9][0-9]{9}$", message = "手机号码格式有误")
    private String phone;

    private String remark;

    private String nickName; //昵称

    private String realName; //真实姓名

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBirth; //出生日期

    private String gender; //性别

    private String profession; //职业

    private String workOrg; //工作单位

    private String address; //通信地址

    private String researchField; //研究领域

    private String picEncode; //用户头像编码

    private Byte guideStatus; //用户引导状态

    public interface Id {
    }

    public interface Name {
    }

}
