package org.zjvis.datascience.common.model.stat;

import lombok.Data;

/**
 * @description 字段比较器，用于清洗操作的排序
 * @date 2021-09-15
 */
@Data
public class ColumnCounter<T extends Comparable> {

    private T column;

    private Integer count;

    private Integer highLight;

    private Boolean isMatching;

    private Integer anomaly;

    private Integer imputation;

    public ColumnCounter(T column, Integer count) {
        this.column = column;
        this.count = count;
    }

    public Integer getCount() {
        return count == null ? 0: count;
    }
}
