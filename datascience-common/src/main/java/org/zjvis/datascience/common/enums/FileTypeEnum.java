package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 上传文件类型枚举类
 * @date 2021-09-08
 */
@Getter
public enum FileTypeEnum {

    CSV("csv", "csv"),
    ;

    private String desc;
    /**
     * 文件后缀
     */
    private String value;

    FileTypeEnum(String desc, String value) {
        this.desc = desc;
        this.value = value;
    }

}
