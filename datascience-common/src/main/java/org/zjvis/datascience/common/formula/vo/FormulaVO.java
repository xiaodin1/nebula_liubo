package org.zjvis.datascience.common.formula.vo;

import lombok.*;
import org.zjvis.datascience.common.formula.dto.FormulaHistoryDTO;
import org.zjvis.datascience.common.util.DozerUtil;

import javax.validation.constraints.NotNull;

/**
 * @description 公式CRUD渲染VO类
 * @date 2021-12-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class FormulaVO {
    private Long formulaId;

    private Long taskId;

    private String tableName;

    private String name;

    @NotNull(message = "formula cannot be null.")
    private String formula;

    private String result;

    private String type;

    private Long projectId;

    private Integer precisionNum;

    public FormulaHistoryDTO toHistoryDTO() {
        FormulaHistoryDTO dto = DozerUtil.mapper(this, FormulaHistoryDTO.class);
        if (null != this.getFormulaId()) {
            dto.setId(this.getFormulaId());
        }
        return dto;
    }
}
