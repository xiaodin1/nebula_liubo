package org.zjvis.datascience.common.vo.user;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @description 用户设置相关VO
 * @date 2021-12-13
 */
@Data
public class UserConfigVO {
    private Long userId;

    @ApiRequestInclude(groups = Level.class)
    @NotNull(message = "消息接收级别不能为空", groups = Level.class)
    @Min(value = 1, message = "请输入有效level", groups = Level.class)
    private Integer noticeReceiveLevel;

    @ApiRequestInclude(groups = SecondaryConfirmation.class)
    @NotNull(message = "数据视图节点删除二次确认值不能为空", groups = SecondaryConfirmation.class)
    @Min(value = 0, message = "请输入有效二次确认值", groups = SecondaryConfirmation.class)
    @Max(value = 1, message = "请输入有效二次确认值", groups = SecondaryConfirmation.class)
    private Integer nodeDeletionSecondaryConfirmation;

    public interface Level {
    }

    public interface SecondaryConfirmation {
    }
}
