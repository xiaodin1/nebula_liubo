package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.JlabVO;

/**
 * @description 自定义算子 [Jupyter Notebook版本]信息表，JlabDTO
 * @date 2021-11-26
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class JlabDTO extends BaseDTO {

    private Long id;

    private Long userId;

    private Long port;

    private String token;

    private int active;

    public JlabVO view() {
        JlabVO vo = DozerUtil.mapper(this, JlabVO.class);
        return vo;
    }
}
