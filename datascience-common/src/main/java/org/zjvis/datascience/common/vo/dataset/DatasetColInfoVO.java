package org.zjvis.datascience.common.vo.dataset;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

/**
 * @description 数据字段结果相关VO
 * @date 2021-08-24
 */
@Data
public class DatasetColInfoVO {

    private BaseTableConfigVO baseTableConfigVO;

    private List<JSONObject> colData;
}
