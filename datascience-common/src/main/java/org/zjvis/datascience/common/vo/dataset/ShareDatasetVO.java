package org.zjvis.datascience.common.vo.dataset;

import javax.validation.constraints.Min;
import lombok.Data;

/**
 * @description 共享数据集结果相关VO
 * @date 2021-12-23
 */
@Data
public class ShareDatasetVO {
    @Min(value = 1, message = "请输入有效id")
    private Long datasetId;

    @Min(value = 1, message = "请输入有效用户id")
    private Long userId;
}
