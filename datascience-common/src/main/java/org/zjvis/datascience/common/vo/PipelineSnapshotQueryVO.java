package org.zjvis.datascience.common.vo;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 数据视图快照数据表 Pipeline——SnapShot 查询相关VO
 * @date 2021-06-11
 */
@Data
@Builder
public class PipelineSnapshotQueryVO {
    private Long pipelineSnapshotId;

    private String pipelineSnapshotName;

    private Long pipelineId;

    private Long projectId;

    private String picture;

    private LocalDateTime gmtCreate;
}
