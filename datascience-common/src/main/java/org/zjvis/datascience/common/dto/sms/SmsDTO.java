package org.zjvis.datascience.common.dto.sms;

import lombok.Data;

/**
 * @description 用于存储用户发送短信的信息，作为value存入Redis中
 * @date 2021-12-24
 */
@Data
public class SmsDTO {
    int alreadySendMsg;
    long lastMsgTime;
    String verifyCode;
    boolean isVerified;
}
