package org.zjvis.datascience.common.widget.config;

import lombok.Data;

/**
 * @description widget图表聚合查询组装设置POJO
 * @date 2021-12-14
 */
@Data
public class WidgetGroupConfig {

    private String name;
}
