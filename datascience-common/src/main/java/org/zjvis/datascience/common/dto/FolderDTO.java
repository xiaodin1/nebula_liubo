package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.MLModelInstanceVO;
import org.zjvis.datascience.common.vo.MLModelVO;
import org.zjvis.datascience.common.vo.FolderVO;

/**
 * @description 我的模型文件夹信息表，文件夹DTO
 * @date 2021-07-23
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class FolderDTO extends BaseDTO{
    private Long id;

    private String name;

    private Long projectId;

    private Long userId;

    private Long modelNum;

    private String modelInfo;

    private Long unfold;

    public FolderVO view() {
        FolderVO vo = DozerUtil.mapper(this, FolderVO.class);
        vo.setModelInfo(JSONObject.parseObject(this.getModelInfo()));
        return vo;
    }
}
