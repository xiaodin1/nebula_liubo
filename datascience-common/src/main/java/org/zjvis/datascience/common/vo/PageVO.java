package org.zjvis.datascience.common.vo;

import com.google.common.collect.Maps;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @description 分页结果通用VO
 * @date 2021-11-22
 */
@Data
public class PageVO implements Serializable {

    private static final long serialVersionUID = -8691651555183900659L;

    protected Integer curPage = 1;

    protected Integer pageSize = 10;

    private Integer startIndex;

    public Map<String, Object> params() {
        Map<String, Object> params = Maps.newHashMap();
        params.put("curPage", curPage);
        params.put("pageSize", pageSize);
        return params;
    }

    public Integer getStartIndex() {
        if (startIndex == null || startIndex < 0) {
            if (pageSize == null || curPage == null || curPage <= 0) {
                return 0;
            }
            return pageSize * (curPage - 1);
        } else {
            return startIndex;
        }
    }

}
