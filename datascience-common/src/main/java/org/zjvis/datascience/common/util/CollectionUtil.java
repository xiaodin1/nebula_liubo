package org.zjvis.datascience.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * @description 集合类型切割工具类
 * @date 2020-08-28
 */
public class CollectionUtil extends cn.hutool.core.collection.CollectionUtil {

    /**
     * 对集合按照指定长度分段，每一个段为单独的集合，返回这个集合的列表
     *
     * @param <T>        集合元素类型
     * @param collection 集合
     * @param size       每个段的长度
     * @param mapper     T转换成R
     * @return 分段列表
     */
    public static <T, R> List<List<R>> split(Collection<T> collection, int size, Function<? super T, ? extends R> mapper) {
        final List<List<R>> result = new ArrayList<>();

        ArrayList<R> subList = new ArrayList<>(size);
        for (T t : collection) {
            if (subList.size() >= size) {
                result.add(subList);
                subList = new ArrayList<>(size);
            }
            subList.add(mapper.apply(t));
        }
        result.add(subList);
        return result;
    }

}
