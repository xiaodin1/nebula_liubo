package org.zjvis.datascience.common.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 用户反馈相关VO
 * @date 2021-12-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class UserFeedbackVO {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "用户id不能为空")
    @Min(value = 1, message = "请输入有效用户id")
    @ApiModelProperty(value = "id", required = true)
    private Long id;

    private Integer status; //反馈已读/未读

    private String feedback; //用户反馈

}
