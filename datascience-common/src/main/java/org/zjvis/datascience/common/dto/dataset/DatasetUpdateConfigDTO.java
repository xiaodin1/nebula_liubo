package org.zjvis.datascience.common.dto.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description HTTP数据上传更新定时配置DTO
 * @date 2021-12-24
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetUpdateConfigDTO {
    private Long id;

    private Long userId;

    private DatasetScheduleDTO datasetScheduleConfig;

    private String databaseType;

    private String server;

    private Integer port;

    private String databaseName;

    private String user;

    private String password;

    private String tableName;

    private String connectType;

    private String connectValue;
}
