package org.zjvis.datascience.common.widget.vo;

import lombok.Data;
import org.zjvis.datascience.common.validator.NotNullNumber;
import org.zjvis.datascience.common.vo.PageVO;

/**
 * @description 用于可视化系统构建 按节点渲染 可用图表控件的分页请求类POJO
 * @date 2021-12-01
 */
@Data
public class WidgetPageVO extends PageVO {

    private Long taskId; //不能加

    @NotNullNumber(message = "pipelineId cannot be null")
    private Long pipelineId;

    @NotNullNumber(message = "projectId cannot be null")
    private Long projectId;
}
