package org.zjvis.datascience.common.dto.datax;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {
    private DataConfig reader;
    private DataConfig writer;
}
