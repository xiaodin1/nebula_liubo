package org.zjvis.datascience.common.widget.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @description 用于图表渲染的请求POJO 多用于需要鉴权的数据获取接口
 * @date 2021-12-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PidsComboVOInWidget {

    /**
     * 允许Null, 用于区分编辑态还是发布态的 数据验证阶段
     */
    private Long pipelineId;

    @NotNull(message = "projectId cannot be null")
    private Long projectId;
}
