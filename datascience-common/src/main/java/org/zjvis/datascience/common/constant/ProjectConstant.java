package org.zjvis.datascience.common.constant;

/**
 * @description 项目管理常量
 * @date 2021-12-24
 */
public class ProjectConstant {

    /**
     * 存放项目用户角色的redis key
     */
    public static final String PROJECT_USER_ROLE_KEY = "project_%s_user_%s_role";

    /**
     * 存放项目状态的redis key
     */
    public static final String PROJECT_STATUS_KEY = "project_%s_status";

    /**
     * 项目可操作用户数量，防止pipeline中tab过多
     */
    public static final int PROJECT_OPERATIONAL_USER = 20;

    /**
     * 项目用户添加 key
     */
    public static final String PROJECT_USER_ADD_KEY = "project_%s_add_user";

    /**
     * 项目用户添加 key 的 过期时间
     */
    public static final int PROJECT_USER_ADD_KEY_EXPIRE_TIME_SECOND = 10;


}
