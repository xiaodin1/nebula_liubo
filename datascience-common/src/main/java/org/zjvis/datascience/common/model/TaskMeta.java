package org.zjvis.datascience.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description 平台左侧可选任务节点元信息 POJO
 * @date 2021-07-26
 */
@Data
public class TaskMeta implements Serializable {
    private Long id;
    private String name;                // 算子名
    private int type;                   // 算子大类
    private int subType;                // 算子子类
    private int algType;                // 算子标识数字， type和algType 共同决定算子唯一性
    private boolean isFolder;            // 是否为文件夹
    private boolean isEdited;           // 是否可编辑
    private String descUrl;             // 帮助url

    private Integer unfold;             // 折叠文件夹
    private boolean highlight;          // 文件夹中的模型
    private List<TaskMeta> modelInFolder;

    public TaskMeta(String name, int type, int subType, int algType, boolean isFolder) {
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.algType = algType;
        this.isFolder = isFolder;
        this.isEdited = false;
        this.descUrl = "";
        this.highlight = false;
    }

    public TaskMeta(String name, int type, int subType, int algType, boolean isFolder, Long id, boolean isEdited, String descUrl) {
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.algType = algType;
        this.isFolder = isFolder;
        this.id = id;
        this.isEdited = isEdited;
        this.descUrl = descUrl;
        this.highlight = false;
    }

    public TaskMeta(String name, int type, int subType, int algType, boolean isFolder, Long id, boolean isEdited
            , String descUrl, Integer unfold, List<TaskMeta> modelInFolder) {
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.algType = algType;
        this.isFolder = isFolder;
        this.id = id;
        this.isEdited = isEdited;
        this.descUrl = descUrl;
        this.unfold = unfold;
        this.modelInFolder = modelInFolder;
        this.highlight = false;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getSubType() {
        return subType;
    }

    public int getAlgType() {
        return algType;
    }

    public boolean getIsFolder() {
        return isFolder;
    }

    public boolean getHighLight() {
        return highlight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public void setAlgType(int algType) {
        this.algType = algType;
    }

    public void setIsFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }

    public void setHighLight(boolean highLight) {
        this.highlight = highLight;
    }

}