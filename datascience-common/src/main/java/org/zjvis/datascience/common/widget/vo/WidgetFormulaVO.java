package org.zjvis.datascience.common.widget.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.enums.TaskInstanceStatus;

/**
 * @description 用于来源于数据视图的图表控件的公式结果渲染POJO
 * @date 2021-12-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WidgetFormulaVO {

    private long formulaId;

    private String result;

    private String status;

    private String logInfo;

    private Integer precisionNum;

    public static WidgetFormulaVO fail(String msg) {
        WidgetFormulaVO widgetFormulaVO = new WidgetFormulaVO();
        widgetFormulaVO.setStatus(TaskInstanceStatus.FAIL.toString());
        widgetFormulaVO.setLogInfo(msg);
        return widgetFormulaVO;
    }
}
