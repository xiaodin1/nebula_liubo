package org.zjvis.datascience.common.dto.dataset;

import lombok.Data;

/**
 * @description 项目中引用的数据集DTO
 * @date 2021-12-24
 */
@Data
public class DatasetUsedInProjectDTO {

    private Long projectId;

    private Long userId;

    private String userName;
}
