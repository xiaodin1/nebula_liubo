package org.zjvis.datascience.common.graph.model;

/**
 * @description Link
 * @date 2021-12-29
 */
public class Link extends Element{
    private Node source;
    private Node target;
    private String edgeId;

    public Link(String id, Node src, Node tar, String edgeId) {
        super(id);
        this.source = src;
        this.target = tar;
        this.edgeId = edgeId;
    }

    public Node getSource() {
        return source;
    }

    public Node getTarget() {
        return target;
    }

    public String getEdgeId() {
        return edgeId;
    }
}
