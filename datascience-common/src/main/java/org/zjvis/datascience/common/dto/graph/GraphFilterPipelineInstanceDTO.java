package org.zjvis.datascience.common.dto.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.BaseDTO;

import java.time.LocalDateTime;

/**
 * @description Graph分析构建清洗Pipeline instanceDTO
 * @date 2021-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GraphFilterPipelineInstanceDTO extends BaseDTO {
    private Long id;
    private Long projectId;
    private Long graphId;
    private Long graphFilterPipelineId;
    private Long userId;
    private String dataJson;
    private String status;
    private Long duringTime;
    private String logInfo;
    private Integer runTimes;
    private LocalDateTime gmtRunning;

    public GraphFilterPipelineInstanceDTO(GraphFilterPipelineDTO graphFilterPipeline) {
        this.projectId = graphFilterPipeline.getProjectId();
        this.graphId = graphFilterPipeline.getGraphId();
        this.graphFilterPipelineId = graphFilterPipeline.getId();
        this.userId = graphFilterPipeline.getUserId();
        JSONObject data = new JSONObject();
        this.dataJson = data.toJSONString();
        this.status = "CREATE";
    }
}
