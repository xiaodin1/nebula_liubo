package org.zjvis.datascience.common.vo.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @description 数据集结果 标题栏VO
 * @date 2021-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HeadVO implements Serializable {

    private static final long serialVersionUID = 7899845375375564253L;

    private String name;

    private String type;

    private String recommendType;

    private String semantic;

    private String recommendSemantic;

}


