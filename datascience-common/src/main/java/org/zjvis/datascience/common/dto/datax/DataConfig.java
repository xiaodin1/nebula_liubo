package org.zjvis.datascience.common.dto.datax;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class DataConfig {
    private String name;
    private Parameter parameter;
}
