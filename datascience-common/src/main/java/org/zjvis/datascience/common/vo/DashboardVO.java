package org.zjvis.datascience.common.vo;

import lombok.Data;
import lombok.Getter;
import org.zjvis.datascience.common.dto.DashboardDTO;
import org.zjvis.datascience.common.util.DozerUtil;

import java.time.LocalDateTime;

/**
 * @description 画布表 Dashboard相关VO
 * @date 2021-12-22
 */
@Data
@Getter
public class DashboardVO extends BaseVO {

    private Long id;

    private Long projectId;

    private String layout;

    private String publishLayout;

    private LocalDateTime publishTime;

    private String publishNo;

    private Integer status;

    private String name;

    private String type;

    private Boolean isComplex;

    private Boolean showWatermark;

    /**
     * 对于复杂可视化视图，会复制一次pipeline 然后在这里存储新老taskId的对应关系
     */
    private String mappingJson;

    private Long pipelineId;

    public DashboardDTO toDashboard() {
        DashboardDTO dashboard = DozerUtil.mapper(this, DashboardDTO.class);
        return dashboard;
    }

    /**
     * https://gitee.com/baomidou/mybatis-plus/issues/IINUI
     * 不能删除
     *
     * @return
     */
    public Boolean getIsComplex() {
        return this.isComplex;
    }

}
