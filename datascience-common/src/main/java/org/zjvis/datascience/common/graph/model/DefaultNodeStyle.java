package org.zjvis.datascience.common.graph.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.util.ToolUtil;

import java.util.List;

/**
 * @description DefaultNodeStyle
 * @date 2021-12-29
 */
@Data
public class DefaultNodeStyle {
    private Integer size;

    private JSONObject style ;

    //avoid json ref
    private String labelCfg;

    private List<String> colorLoop;

    private Integer colorLoopSize;

    private String defaultColor;

    private String defaultType = "circle";

    public DefaultNodeStyle() {
        String json = new ToolUtil().readJsonFile("template/graph_filter/default_style.json");
        JSONObject obj = JSONObject.parseObject(json);
        size = obj.getInteger("size");
        style = obj.getJSONObject("style");
        labelCfg = obj.getJSONObject("labelCfg").toJSONString();
        defaultColor = obj.getString("defaultFill");

        json = new ToolUtil().readJsonFile("template/graph_filter/color_loop.json");
        colorLoop = JSONObject.parseArray(json).toJavaList(String.class);
        colorLoopSize = colorLoop.size();
    }
}
