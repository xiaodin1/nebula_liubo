package org.zjvis.datascience.common.dto.datax;

import lombok.Data;

@Data
public class WriterConnection extends Connection {
    private String jdbcUrl;
}
