package org.zjvis.datascience.common.enums;

/**
 * @description ETL节点枚举类
 * @date 2021-11-11
 */
public enum ETLEnum {
  
    FILTER("行列过滤", 1),
    JOIN("关联", 2),
    SAMPLE("采样", 3),  //select * from sales_fact_sample_flat order by random() limit 10
    UNION("拼接", 4),
    REMAP("重映射", 5),
    SQL("执行sql", 6),
    PIVOT_TABLE("数据透视", 7);

    private String desc;
  
    private int val;
  
    
    ETLEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }
    
    public String getDesc() {
        return desc;  
    }

    public Integer getVal() {
        return val;
    }
    
    public ETLEnum valueOfs(int val) {
        switch(val) {
          case 1: 
            return FILTER;
          case 2:
            return JOIN;
          case 3:
            return SAMPLE;
          case 4:
            return UNION;
          case 5:
            return REMAP;
          case 6:
            return SQL;
          case 7:
            return PIVOT_TABLE;
          default:
            return null;
        }
    }
  
}
