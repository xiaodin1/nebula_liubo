package org.zjvis.datascience.common.graph.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * @description FilterTreeNode
 * @date 2021-12-29
 */
public class FilterTreeNode<T> {
    public T value;
    public List<FilterTreeNode<T>> children;

    public FilterTreeNode() {
        this.children = new ArrayList<>();
    }

    public FilterTreeNode(T value) {
        this.value = value;
        this.children = new ArrayList<>();
    }
}
