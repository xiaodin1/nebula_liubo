package org.zjvis.datascience.common.vo.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.dataset.DatasetNameStatusDTO;

import java.util.List;

/**
 * @description 数据管理相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetAndCategoryForProjectVO {
    private Long categoryId;

    private String categoryName;

    List<DatasetNameStatusDTO> dataset;
}
