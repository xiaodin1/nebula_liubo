package org.zjvis.datascience.common.vo.db;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 数据库相关VO （获取指定数据库下所以有表名）
 * @date 2020-10-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DBWithBDNameVO {

    @NotBlank(message = "数据库类型不能为空")
    @ApiModelProperty(value = "数据库类型", required = true)
    private String databaseType;

    @NotBlank(message = "server不能为空")
    @ApiModelProperty(value = "server", required = true)
    private String server;

    @NotNull(message = "端口不能为空")
    @ApiModelProperty(value = "端口", required = true)
    @Min(value = 0, message = "请输入有效端口")
    private Integer port;

    @NotBlank(message = "数据库名不能为空")
    @ApiModelProperty(value = "数据库名", required = true)
    private String databaseName;

    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名", required = true)
    private String user;

    @NotBlank(message = "密码不能为空")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @ApiModelProperty(value = "连接方式", required = true)
    private String connectType;

    @ApiModelProperty(value = "服务名或SID", required = true)
    private String connectValue;
}
