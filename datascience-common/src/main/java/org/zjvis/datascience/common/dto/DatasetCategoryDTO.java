package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @description 数据集类型信息表，数据集类型DTO
 * @date 2020-07-31
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetCategoryDTO extends BaseDTO implements Serializable {
    private Long id;

    private String name;

    private Long userId;

    private Long parentId;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DatasetCategoryDTO other = (DatasetCategoryDTO) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
                && (this.getParentId() == null ? other.getParentId() == null : this.getParentId().equals(other.getParentId()))
                && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
                && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
                && (this.getGmtModifier() == null ? other.getGmtModifier() == null : this.getGmtModifier().equals(other.getGmtModifier()))
                && (this.getGmtCreator() == null ? other.getGmtCreator() == null : this.getGmtCreator().equals(other.getGmtCreator()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getParentId() == null) ? 0 : getParentId().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getGmtModifier() == null) ? 0 : getGmtModifier().hashCode());
        result = prime * result + ((getGmtCreator() == null) ? 0 : getGmtCreator().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(" id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", userId=").append(userId);
        sb.append(", parentId=").append(parentId);
        sb.append("]");
        return sb.toString();
    }

}
