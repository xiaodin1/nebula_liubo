package org.zjvis.datascience.common.enums;

/**
 * @description 语义推断子类型枚举类
 * @date 2021-07-29
 */
public enum SemanticSubEnum {
    //空
    NULL("无", "null", 0),
    //地理
    LONGITUDE("经度", "longitude", 1),
    LATITUDE("纬度", "latitude", 1),
    COORDINATE("经纬度", "coordinate", 1),
    COUNTRY("国家", "country", 1),
    PROVINCE("省份", "province", 1),
    CITY("城市", "city", 1),
    POSTCODE("邮编", "postcode", 1),
    //网络
    IP("IP地址", "ip", 2),
    URL("URL", "url", 2),
    HTTP_STATUS("HTTP状态码", "httpstatus", 2),
    //信息
    EMAIL("邮箱", "email", 3),
    TELEPHONE("电话", "telephone", 3),
    ID("身份证", "id", 3),
    PASSPORT("护照", "passport", 3),
    //类别
    DISORDER("无序类别", "disorder", 4),
    ORDER("有序类别", "order", 4),
    ;

    private final String desc;

    private final String val;

    private final int type;

    SemanticSubEnum(String desc, String val, int type) {
        this.desc = desc;
        this.val = val;
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public String getVal() {
        return val;
    }

    public int getType() {
        return type;
    }

}
