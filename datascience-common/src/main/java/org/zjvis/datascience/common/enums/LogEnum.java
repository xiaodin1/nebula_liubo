package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 日志类型枚举类
 * @date 2020-08-28
 */
@Getter
public enum LogEnum {

    // 系统报错日志
    SYS_ERR,
    // 用户请求日志
    REST_REQ,
    // 用户模块
    USER,
    // 项目模块
    PROJECT,
    // 数据模块
    DATASET,
    THREAD_POOL,
    ;

    /**
     * 判断日志类型不能为空
     *
     * @param logType 日志类型
     * @return boolean 返回类型
     */
    public static boolean isLogType(LogEnum logType) {
        return logType != null;
    }

}
