package org.zjvis.datascience.common.widget.config;

import java.util.List;

import lombok.Data;

/**
 * @description widget图表筛选条件查询组装设置POJO
 * @date 2021-12-14
 */
@Data
public class WidgetWhereConfig {
    private String name;

    private String filterType;

    private List<String> values;
}
