package org.zjvis.datascience.common.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.zjvis.datascience.common.constant.DatasetConstant;
import org.zjvis.datascience.common.enums.DataTypeEnum;
import org.zjvis.datascience.common.model.AggregateColumn;
import org.zjvis.datascience.common.model.stat.ColumnConstant;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static org.zjvis.datascience.common.constant.DatasetConstant.DEFAULT_ID_FIELD;

/**
 * @description 数据结果字段头包装工具类
 * @date 2021-12-01
 */
public class HeadUtil {

    public static JSONArray wrapHead(ResultSetMetaData metaData, JSONObject columnTypes, JSONObject semanticInfo, JSONObject numberFormat) throws SQLException {
        JSONArray heads = new JSONArray();
        for (int i = 1; i < metaData.getColumnCount() + 1; i++) {
            if (DEFAULT_ID_FIELD.equals(metaData.getColumnName(i))) {
                continue;
            }
            if (metaData.getColumnName(i).contains(ColumnConstant.ROW_NUM)) {
                continue;
            }
            JSONObject head = new JSONObject();
            String name = metaData.getColumnName(i);
            head.put("name", name);
            if (null != columnTypes && columnTypes.containsKey(name)) {
                String typeFromJson = columnTypes.getString(name).toLowerCase();
                if (DataTypeEnum.DATE.getValue().equalsIgnoreCase(typeFromJson)) {
                    head.put("desc", typeFromJson);
                    head.put("type", Types.DATE);
                } else if (DataTypeEnum.JSON.getValue().equalsIgnoreCase(typeFromJson)) {
                    head.put("desc", typeFromJson);
                    head.put("type", Types.OTHER);
                } else if (DataTypeEnum.ARRAY.getValue().equalsIgnoreCase(typeFromJson)) {
                    head.put("desc", typeFromJson);
                    head.put("type", Types.ARRAY);
                } else {
                    head.put("type", SqlUtil.encodeType(metaData.getColumnTypeName(i)));
                    head.put("desc", SqlUtil.changeType(metaData.getColumnTypeName(i)));
                }
            } else {
                head.put("type", SqlUtil.encodeType(metaData.getColumnTypeName(i)));
                head.put("desc", SqlUtil.changeType(metaData.getColumnTypeName(i)));
            }
            if (null != semanticInfo) {
                head.put("semantic", semanticInfo.getString(name));
            }else {
                head.put("semantic", null);
            }
            if (numberFormat!=null) {
                head.put("numberFormat", numberFormat.getJSONObject(name));
            } else {
                head.put("numberFormat", null);
            }
            heads.add(head);
        }
        return heads;
    }

    public static JSONArray wrapBasicHead(ResultSetMetaData metaData) throws SQLException {
        JSONArray heads = new JSONArray();
        for (int i = 1; i < metaData.getColumnCount() + 1; i++) {
            if (DatasetConstant.DEFAULT_ID_FIELD.equals(metaData.getColumnName(i))) {
                continue;
            }
            if (metaData.getColumnName(i).contains(ColumnConstant.ROW_NUM)) {
                continue;
            }
            JSONObject head = new JSONObject();
            head.put("name", metaData.getColumnName(i));
            head.put("type", metaData.getColumnType(i));
            head.put("desc", SqlUtil.changeType(metaData.getColumnTypeName(i)));
            heads.add(head);
        }
        return heads;
    }

    public static List<AggregateColumn> wrapAggHead(ResultSetMetaData metaData) throws SQLException {
        List<AggregateColumn> heads = new ArrayList<>();
        for (int i = 1; i < metaData.getColumnCount() + 1; i++) {
            if (DatasetConstant.DEFAULT_ID_FIELD.equals(metaData.getColumnName(i))) {
                continue;
            }
            if (metaData.getColumnName(i).contains(ColumnConstant.ROW_NUM)) {
                continue;
            }
            AggregateColumn head = new AggregateColumn();
            head.setIndex(i);
            head.setName(metaData.getColumnName(i));
            head.setType(metaData.getColumnTypeName(i));
            heads.add(head);
        }

        return heads;
    }
}
