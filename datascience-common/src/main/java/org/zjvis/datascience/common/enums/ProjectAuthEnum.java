package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 项目读写权限枚举类
 * @date 2020-07-23
 */
@Getter
public enum ProjectAuthEnum {

    READ("读", (byte) 1),
    WRITE("读写", (byte) 2),
    /**
     * 除了读写的权限，还有对于项目人员权限管理的权限
     */
    ADMIN("管理员", (byte) 3),
    ;

    private String desc;
    private Byte value;

    ProjectAuthEnum(String desc, Byte value) {
        this.desc = desc;
        this.value = value;
    }

}
