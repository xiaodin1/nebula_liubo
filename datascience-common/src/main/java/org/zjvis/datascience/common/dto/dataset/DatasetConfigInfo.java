package org.zjvis.datascience.common.dto.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 数据集上传配置信息DTO
 * @date 2021-12-24
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatasetConfigInfo {

    private Boolean needSchedule;

    private String crontab;

    private String incrementColumn;

    private String lastValue;

    private String dataXJson;

    private DatasetScheduleDTO detail;
}
