package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 数据等级信息表，数据等级DTO
 * @date 2021-01-28
 */
@Data
public class DataLevelDTO {

    private Integer id;

    private String name;

    private String nameEn;

    private String level;

    private String content;
}
