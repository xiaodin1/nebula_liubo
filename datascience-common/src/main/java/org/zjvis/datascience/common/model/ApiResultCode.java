package org.zjvis.datascience.common.model;

/**
 * @description 通用结果CODE枚举类
 * @date 2021-12-08
 */
public enum ApiResultCode {

    SUCCESS(100, "成功!"),

    /**
     * 参数验证类错误
     */
    PARAM_ERROR(201, "参数错误!"),
    TABLE_EMPTY(202, "当前节点数据表为空!!!!"),
    PARENT_TABLE_EMPTY(203, "父节点输出数据为空!!!!"),
    JOB_HAVE_NOT_DONE_YET(204, "父节点正在运算执行,请稍后再试"),
    TASK_VALIDATE_FAIL(606, "feature_cols存在非数值类型校验失败!!!"),
    PIPELINE_STOP_ERROR(1106, "停止pipeline失败"),
    MISSING_APPLICATION_ID(1107, "缺少ApplicationId, 无需停止或初始化阶段还未结束"),
    LOSE_CONNECTION(1108, "失去数据库连接，请刷新重试。"),

    /**
     * 权限类错误
     */
    NO_AUTH(301, "未经授权访问!"),

    /**
     * 文件上传错误
     */
    UPLOAD_ERROR(413, "文件上传失败!"),
    DOWNLOAD(412, "文件下载失败!"),
    EXCEL_TYPE_ERROR(419, "该文件为不支持的StrictOpenXML格式，请另存为csv或普通excel格式后重试!"),
    CONTENT_ERROR(700, "上传文件内容为空，请重新选择文件!"),
    FILE_SUPPORT_ERROR(701, "当前仅支持csv和excel格式文件!"),
    JSON_FORMAT_ERROR(702, "JSON格式错误"),
    URL_ERROR(703, "url不存在"),
    DATA_ERROR(704, "数据行解析有误，请检查数据格式"),
    CONTAIN_SPECIAL_CHARACTER(801, "参数不能包含特殊字符!"),

    /**
     * 用户操作错误
     */
    FORBIDDEN_CONNECT(414, "兄弟节点禁止连线"),
    DATASET_NAME(411, "数据集名称重复，请重新命名!"),
    ALLOW_ONE_PARENT(608, "只允许唯一父节点"),
    ALLOW_TWO_PARENT(609, "只允许两个父节点"),
    ALLOW_THREE_PARENT(610, "只允许三个父节点"),
    ALLOW_PARENT_ERROR(611, "节点连线超过最大限制"),
    LINE_DUP_ERROR(612, "不允许重复连线"),
    TASK_FORBIDDEN_COPY(616, "禁用节点不允许复制!"),
    TASK_FORBIDDEN_DEL(617, "禁用节点不允许删除"),
    NUMERIC_VALIDATE_FAIL(618, "数值类型配置校验失败，请根据右侧问号提示输入!"),
    FEEDBACK_CONTENT_ERROR(1103, "请填写正确的反馈信息"),
    FEEDBACK_LENGTH_ERROR(1104, "反馈字数太少或超出最大范围"),
    EMPTY_FEEDBACK_ERROR(1105, "请填写有效反馈后再提交"),
    INVALID_DATA_CONDITION(1106, "字段渲染条件有误"),

    /**
     * 数据读取类错误
     */
    DATASET_INFO_NULL(415, "符合条件的数据集信息不存在"),
    DATASET_ACT_UNREAD_NULL(416, "不存在未读信息"),
    DATASET_ACT_NULL(417, "不存在数据增删操作"),
    DATA_NULL(418, "数据不存在"),

    /**
     * 自定义算子
     */
    DATA_NULL_PIPELINE(420, "数据表名无效，请导入当前pipeline中的数据表！"),
    DATA_UPDATING(421, "代码提交中，请稍后重试！"),
    REGISTER_UDF_ERROR(600, "注册UDF失败"),
    UPDATE_UDF_ERROR(601, "更新UDF失败"),
    DROP_UDF_ERROR(602, "删除UDF失败"),
    UDF_DEFINE_ERROR(603, "自定义算子输入参数和输出参数定义错误"),
    DEFINE_UDF_DUPLICATE(604, "自定义算子名或者函数名重复"),
    EXTRACT_FUNCTION_FAIL(605, "抽取函数名失败"),
    DANGER_PATTERN(607, "脚本包括危险模式!"),

    /**
     * ETL
     */
    SQL_NOT_SUPPORT(613, "SQL算子只允许select语句"),
    SQL_NOT_PARENT(614, "SQL算子必须有父节点"),
    SQL_TABLE_PERMISSION(615, "SQL关联表只允许来自父节点"),
    DIMENSION_REDUCTION_VALIDATE_FAIL(619, "降维维度必须是大于0的整数，且小于等于特征列数!"),
    ISO_FOREST_PARAM_SAMPLES_ILLEGAL(620, "样本比例(max_samples)取值范围(0,1]!"),
    ISO_FOREST_PARAM_CONTAMINATION_ILLEGAL(621, "异常点比例(contamination)取值范围(0,0.5)!"),
    SAMPLE_PARAM_COUNT_ILLEGAL(622, "SAMPLE数量配置范围[1,1000000]!"),
    SCRIPT_NO_ENTRY(623, "python脚本必须定义函数begin_alg!!"),
    PARENT_HAS_NO_TABLE(625, "父节点还未运行生成表，无法清洗"),
    UDF_NOT_PASS(626, "自定义算子未通过审核"),
    UDF_NO_ENTRY(627, "自定义算子未定义入口函数beginAlg"),
    UDF_PARAM_ERROR(628, "自定义算子输入输出参数定义不完整"),
    UNION_TYPE_UNMATCHED(629, "连接条件中存在字段类型不匹配"),
    AUTO_TRIGGER_FAILED(630, "自动执行失败，需要用户手动配置后，再执行"),


    /**
     * 图分析 && 图构建
     */
    GRAPH_DATA_NOT_DONE(900, "数据正在写入图数据库,请稍后再试"),
    GRAPH_EDGE_DUP(901, "相同节点间无法连接多条边"),
    GRAPH_NO_CHILD(902, "网络构建节点无法拥有子节点"),
    GRAPH_NODE_LIMIT(903, "图节点数超过最大限制，请减少数据"),
    GRAPH_LOAD_FORMAT_ERROR(904, "文件格式不支持"),
    UNDO_FAILED(906, "执行撤销失败，可能没有可用的操作"),
    REDO_FAILED(907, "执行重做失败，可能没有可用的操作"),
    GRAPH_CATEGORY_CLEAN_ACTION_ID_NOT_FOUND(910, "找不到该记录"),

    /**
     * 图片类型
     */
    IMAGE_FORMAT_ERROR(908, "当前仅支持jpg,png,bmp和jpeg格式文件!"),
    IMAGE_SIZE_ERROR(909, "请上传小于2MB的图片"),
    FOLDER_NAME_DUP(1001, "文件夹名称已被占用，请选取其他名称"),
    FEEDBACK_IMAGE_LENGTH_ERROR(1100, "最多上传4张图片"),
    IMAGE_RESOLUTION_ERROR(1101, "当前图片分辨率过大"),
    DUPLICATE_IMAGE_ERROR(1102, "请不要上传重复图片"),

    /**
     * 机器学习模型
     */
    NOT_YOUR_MODEL(1002, "请查询属于你的模型"),
    GET_OUT_FAIL(1003, "获取模型结果失败"),
    FOLDER_NUM_LIMIT(1004, "已有文件夹数量达到上限(10)"),
    MODEL_NAME_DUP(1005, "模型名称已被占用，请选取其他名称"),
    MODEL_RUNNING_LIMIT(1006, "运行中模型数量已达上限(5)"),
    MODEL_NUM_LIMIT(1007, "模型创建数量已达上限(30)"),
    MODEL_NO_SOURCE(1008, "请指定用于训练的数据集"),
    MODEL_IN_USE(1009, "模型已被加载到pipeline中，请先删除pipeline中模型节点再删除模型文件"),
    FOLDER_IN_USE(1010, "文件夹中有正在使用中的模型，请先删除pipeline中模型节点再删除此文件夹"),
    SMOOTH_DATA_PRE_REQ(1011, "请连接10行以上的数据集以进行平滑操作"),
    /**
     * 默认错误
     */
    SYS_ERROR(500, "服务端异常!"),

    /**
     * 可视化构建
     */
    DASHBOARD_PUBLISHED(1201, "该项目中存在已发布的可视化构建系统"),
    DASHBOARD_UNSTUCK_FAILED(1202, "下架仪表盘失败"),

    /**
     * 分片异常
     */
    SLICE_UPLOAD_REDIS_TIMEOUT(1301, "后端服务忙碌，请尝试重新执行操作");

    int code;
    String message;

    ApiResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
