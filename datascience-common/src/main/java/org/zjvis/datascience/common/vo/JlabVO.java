package org.zjvis.datascience.common.vo;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.JlabDTO;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.util.DozerUtil;

/**
 * @description JupyterNotebook数据表 JLab 相关VO
 * @date 2021-06-15
 */
@Data
@NoArgsConstructor
public class JlabVO extends BaseVO {

    private Long id;

    private Long userId;

    private Long port;

    private String token;

    private int active;

//    public JlabVOb(Long projectId){
//        this.setInPanel(1L);
//        this.setProjectId(projectId);
//    }

    public JlabDTO toJlab() {
        JlabDTO jlab = DozerUtil.mapper(this, JlabDTO.class);
        return jlab;
    }

    public TaskDTO toTask() {
        return null;
    }

}
