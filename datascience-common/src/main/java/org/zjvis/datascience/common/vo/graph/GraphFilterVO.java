package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.graph.GraphFilterDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.BaseVO;

@Data
public class GraphFilterVO extends BaseVO {
    private Long id;

    private String name;

    private Long projectId;

    private Long graphId;

    private Long graphFilterPipelineId;

    private Integer type;

    private Integer subType;

    private Long parentId;

    private Long userId;

    private JSONObject data;

    public GraphFilterDTO dto() {
        GraphFilterDTO dto = DozerUtil.mapper(this, GraphFilterDTO.class);
        dto.setDataJson(this.data.toJSONString());
        return dto;
    }
}
