package org.zjvis.datascience.common.strategy.operation;

import org.zjvis.datascience.common.strategy.SetValueStrategy;

/**
 * @description : 多重插补设值策略
 * @date 2021-09-15
 */
public class ImputationStrategy implements SetValueStrategy {

    @Override
    public int doOperation(int num1, int num2) {
        return num1 - num2;
    }
}
