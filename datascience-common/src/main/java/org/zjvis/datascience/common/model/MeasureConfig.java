package org.zjvis.datascience.common.model;

import lombok.Data;

/**
 * @description 聚合函数包装类
 * @date 2021-10-26
 */
@Data
public class MeasureConfig extends DimensionConfig {

    private String expr;

    private String aggType;

    private Integer top;

}
