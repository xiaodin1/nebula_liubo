package org.zjvis.datascience.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.util.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.config.JwtConfig;
import org.zjvis.datascience.common.dto.user.UserDTO;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @description JWT
 * @date 2020-03-14
 */
@Component
public class JwtUtil {

    /**
     * 请求头
     */
    public static final String AUTH_HEADER = "Authorization";
    /**
     * 过期时间20分钟
     */
    private static final long EXPIRE_TIME = 20 * 60 * 1000;
    private static final String FIELD_NAME = "USER_NAME";
    public static final String USER_ID = "USER_ID";
    public static final long DEFAULT_USER = 0L;
    private static final UserDTO NO_USER = UserDTO.builder().id(DEFAULT_USER).build();

    private static JwtConfig jwtConfig;
    private static RedisUtil redisUtil;

    @Autowired
    public void setJwcConfig(JwtConfig myJwtConfig) {
        jwtConfig = myJwtConfig;
    }

    @Autowired
    public void setRedisUtil(RedisUtil myRedisUtil) {
        redisUtil = myRedisUtil;
    }

    /**
     * 验证token是否正确
     */
    public static boolean verify(String token, String username) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtConfig.secret);
            JWTVerifier verifier = JWT.require(algorithm).withClaim(FIELD_NAME, username).build();
            verifier.verify(token);
            return true;
        } catch (UnsupportedEncodingException exception) {
            return false;
        }
    }

    /**
     * 获得token中的自定义信息，无需secret解密也能获得
     */
    public static Claim getClaimFiled(String token, String filed) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim(filed);
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 生成签名
     */
    public static String sign(String userName,long userId) {
        try {
            Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
            Algorithm algorithm = Algorithm.HMAC256(jwtConfig.secret);
            // 附带username和userId
            String token = JWT.create().withClaim(FIELD_NAME, userName)
                .withClaim(USER_ID, userId)
                .withExpiresAt(date).sign(algorithm);
            saveToken(userName, token);
            return token;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /**
     * 获取用户名
     */
    public static String getUserName(String token) {
        return getClaimFiled(token, FIELD_NAME)==null?null:getClaimFiled(token, FIELD_NAME).asString();
    }

    public static Long getUserId(String token) {
        return getClaimFiled(token, USER_ID)==null?null:getClaimFiled(token, USER_ID).asLong();
    }

    /**
     * 获取 token的签发时间
     */
    public static Date getIssuedAt(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getIssuedAt();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 验证 token是否过期
     */
    public static boolean isTokenExpired(String token) {
        Date now = Calendar.getInstance().getTime();
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getExpiresAt()==null?true:jwt.getExpiresAt().before(now);
    }

    /**
     * 刷新 token的过期时间
     */
    public static String refreshTokenExpired(String token) {
        DecodedJWT jwt = JWT.decode(token);
        Map<String, Claim> claims = jwt.getClaims();
        try {
            Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
            Algorithm algorithm = Algorithm.HMAC256(jwtConfig.secret);
            Builder builer = JWT.create().withExpiresAt(date);
            for (Entry<String, Claim> entry : claims.entrySet()) {
                builer.withClaim(entry.getKey(), entry.getValue().asString());
            }
            String newToken = builer.sign(algorithm);
            String userName = claims.get(FIELD_NAME).asString();
            String tokenKey = getUserTokenKey(userName);
            if (redisUtil.get(tokenKey) == null) {
                return null;
            }
            saveToken(userName, newToken);
            return newToken;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /**
     * 生成16位随机盐
     */
    public static String generateSalt() {
        SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
        return secureRandom.nextBytes(16).toHex();
    }

    /**
     * 获取当前用户
     */
    public static UserDTO getCurrentUserDTO() {
        Object principal = null;
        SecurityManager manager = ThreadContext.getSecurityManager();
        if (!Objects.isNull(manager)) {
            principal = SecurityUtils.getSubject().getPrincipal();
        }
        return Objects.isNull(principal) ? NO_USER : (UserDTO) principal;
    }

    public static long getCurrentUserId() {
        UserDTO userDTO = getCurrentUserDTO();
        return userDTO == null ? JwtUtil.DEFAULT_USER : userDTO.getId();
    }

    /**
     * 获取用户token key
     */
    private static String getUserTokenKey(String userName) {
        return jwtConfig.onlineKey + userName;
    }

    private static void saveToken(String userName, String token) {
        String tokenKey = getUserTokenKey(userName);
        redisUtil.set(tokenKey, token, jwtConfig.jwtExpiration, TimeUnit.MINUTES);
    }

    public static void logout() {
        UserDTO userDTO = getCurrentUserDTO();
        if (userDTO == null) {
            return;
        }
        removeToken(userDTO.getName());
    }

    private static void removeToken(String userName) {
        String tokenKey = getUserTokenKey(userName);
        redisUtil.del(tokenKey);
    }

    /**
     * 获取当前登录用户token
     * @return
     */
    public static String getCurrentUserToken() {
        UserDTO user = getCurrentUserDTO();
        if (user==null) {
            return null;
        }
        String tokenKey = getUserTokenKey(user.getName());
        return (String)redisUtil.get(tokenKey);
    }

    public static boolean tokenClaimExist(String token, String key) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getClaim(key)==null?false:true;
    }

}
