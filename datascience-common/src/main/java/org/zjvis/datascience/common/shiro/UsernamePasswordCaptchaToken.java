package org.zjvis.datascience.common.shiro;

import lombok.Getter;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @description : 账号密码令牌类
 * @date 2020-06-01
 */
@Getter
public class UsernamePasswordCaptchaToken extends UsernamePasswordToken {

    private static final long serialVersionUID = 1L;

    public UsernamePasswordCaptchaToken() {
        super();
    }

    public UsernamePasswordCaptchaToken(String username, String password) {
        super(username, password);
    }

}
