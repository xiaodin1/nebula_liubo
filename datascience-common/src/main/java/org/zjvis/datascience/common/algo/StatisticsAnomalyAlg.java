package org.zjvis.datascience.common.algo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.common.constant.SqlTemplate;
import org.zjvis.datascience.common.enums.AlgEnum;
import org.zjvis.datascience.common.enums.SubTypeEnum;
import org.zjvis.datascience.common.sql.SqlHelper;
import org.zjvis.datascience.common.util.ToolUtil;
import org.zjvis.datascience.common.vo.TaskVO;

import java.util.List;

/**
 * @description statistics anomaly 异常检测算子模板类
 * @date 2021-12-24
 */
public class StatisticsAnomalyAlg extends BaseAlg {

    private final static Logger logger = LoggerFactory.getLogger("StatisticsAnomalyAlg");

    private static String TPL_FILENAME = "template/algo/statistics_anomaly.json";

    private static String SQL_TPL_MADLIB = "SELECT * FROM \"%s\".\"statistics_anomaly\"('%s', '%s', '%s', '%s')";

    private static String SQL_TPL_MADLIB_SAMPLE = "SELECT * FROM \"%s\".\"statistics_anomaly\"('CREATE VIEW %s AS SELECT * from %s where \"%s\" <= %s', '%s', '%s', '%s')";

    private static String SQL_TPL_SPARK = "statistics-anomaly -s %s -f %s -t %s -uk %s -idcol %s";


    public void initTemplate(JSONObject data) {
        JSONArray jsonArray = getTemplateParamList(TPL_FILENAME);
        data.put("setParams", jsonArray);
        baseInitTemplate(data);
    }

    public StatisticsAnomalyAlg() {
        super(AlgEnum.STAT_ANOMALY.name(), SubTypeEnum.ANOMALY_DETECTION.getVal(),
                SubTypeEnum.ANOMALY_DETECTION.getDesc());
        this.maxParentNumber = 1;
    }

    private String getStatAnomalySql(String sourceTable, String outTable, String featureCol,
                                     long timeStamp, String sampleTable) {
        if (StringUtils.isEmpty(featureCol)) {
            return StringUtils.EMPTY;
        }
        if (StringUtils.isNotEmpty(sampleTable)) {
            // 采样
            return String.format(SQL_TPL_MADLIB_SAMPLE, SqlTemplate.SCHEMA,
                    sampleTable, sourceTable, ID_COL, SAMPLE_NUMBER, outTable, ID_COL, featureCol);
        } else {
            // 全量, 根据配置
            if (getEngine().isMadlib()) {
                return String
                        .format(SQL_TPL_MADLIB, SqlTemplate.SCHEMA, sourceTable, outTable, ID_COL,
                                featureCol);
            } else if (getEngine().isSpark()) {
                return String
                        .format(SQL_TPL_SPARK, sourceTable, featureCol, outTable, timeStamp, ID_COL);
            }
        }
        return StringUtils.EMPTY;
    }

    public String initSql(JSONObject json, List<SqlHelper> sqlHelpers, long timeStamp,
                          String engineName) {
        this.engineName = engineName;
        String sourceTable = json.getString("source_table");
        sourceTable = ToolUtil.alignTableName(sourceTable, timeStamp);
        String outTable = json.getString("out_table_rename");
        outTable = ToolUtil.alignTableName(outTable, timeStamp);
        String featureCol = json.getString("feature_col");
        String sampleTable = "";
        if (!json.containsKey("isSample") || json.getString("isSample").equals("SUCCESS") || json
                .getString("isSample").equals("FAIL")) {
            sampleTable = outTable.replace("solid_", "view_");
            json.put("isSample", "CREATE");
        }
        String sql = this
                .getStatAnomalySql(sourceTable, outTable, featureCol, timeStamp, sampleTable);
        logger.debug("sql={}", sql);
        return sql;
    }

    public void defineOutput(TaskVO vo) {
        JSONObject jsonObject = vo.getData();
        String outTablePrefix = jsonObject.getString("out_table");
        String tableName = String
                .format(SqlTemplate.OUT_TABLE_NAME, outTablePrefix, vo.getPipelineId(), vo.getId());
        jsonObject.put("out_table_rename", tableName);
        JSONArray input = jsonObject.getJSONArray("input");
        if (input == null || input.size() == 0) {
            logger.warn("input is empty");
            return;
        }
        this.supplementForSelector(jsonObject, TPL_FILENAME, 1, vo, "number");
        jsonObject.put("source_table", input.getJSONObject(0).getString("tableName"));
        JSONArray outputColTypes = new JSONArray();
        JSONArray outputCols = new JSONArray();
        if (input != null && input.size() > 0) {
            if (!jsonObject.containsKey("feature_col")) {
                logger.warn("feature_col not exists!!!");
                return;
            }

            List<String> inputCols = input.getJSONObject(0).getJSONArray("tableCols")
                    .toJavaList(String.class);
            List<String> inputColumnTypes = input.getJSONObject(0).getJSONArray("columnTypes")
                    .toJavaList(String.class);
            outputCols.addAll(inputCols);
            outputColTypes.addAll(inputColumnTypes);
            outputCols.add("label");
            outputColTypes.add("varchar");
        }
        JSONArray output = new JSONArray();
        JSONObject outItem = new JSONObject();
        outItem.put("tableName", tableName);
        outItem.put("tableCols", outputCols);
        outItem
                .put("nodeName", vo.getName() == null ? AlgEnum.STAT_ANOMALY.toString() : vo.getName());
        outItem.put("columnTypes", outputColTypes);
        this.setSubTypeForOutput(outItem);
        output.add(outItem);
        jsonObject.put("output", output);
        vo.setData(jsonObject);
    }
}
