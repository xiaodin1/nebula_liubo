
package org.zjvis.datascience.common.pool;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description 默认线程工厂
 * @date 2020-04-10
 */
public class DefaultThreadFactoryImpl implements ThreadFactory {

    private static final AtomicInteger POOL_NUMBER = new AtomicInteger(1);
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String namePrefix;
    private final boolean isDaemon;
    private final long stackSize;
    private final Thread.UncaughtExceptionHandler exceptionHandler = new ThreadPoolExceptionHandler();

    /**
     * @param poolName  自定义的线程池名称
     * @param isDaemon  是否是守护线程
     * @param stackSize 新线程所需的堆栈大小，或者为零以指示要忽略此参数
     */
    public DefaultThreadFactoryImpl(String poolName, boolean isDaemon, long stackSize) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        namePrefix = "pool-" + poolName + POOL_NUMBER.getAndIncrement() + "-thread-";
        this.isDaemon = isDaemon;
        this.stackSize = stackSize;

    }

    public DefaultThreadFactoryImpl() {
        this("default", false, 0);
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), stackSize);
        t.setDaemon(isDaemon);
        t.setUncaughtExceptionHandler(exceptionHandler);
        if (t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }
        return t;
    }

}
