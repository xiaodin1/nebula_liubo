package org.zjvis.datascience.common.vo.project;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @description 项目数据集相关VO
 * @date 2021-10-29
 */
@Data
@AllArgsConstructor
public class ProjectDatasetVO {
    @NotNull(message = "项目id不能为空")
    @Min(value = 1, message = "请输入有效项目id")
    private Long projectId;

    private Long pipelineId;

    private String datasetName;

}
