package org.zjvis.datascience.common.widget.config;

import lombok.Data;

/**
 * @description widget图表指定字段查询组装设置POJO
 * @date 2021-12-14
 */
@Data
public class WidgetSelectedConfig {

    private String name;

    private String aggType;

    private String alias;
}
