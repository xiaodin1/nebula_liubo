package org.zjvis.datascience.common.dto.dataset;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @description 数据集存储写入DTO
 * @date 2021-12-24
 */
@Data
public class DatasetFileWriteDTO {

    @NotBlank(message = "请选择数据分类")
    private String categoryId;

    @NotBlank(message = "请选择您想导入的数据类型 excel.表格文件excel\\cvs , table.外部数据库")
    private String importType;

    @NotBlank(message = "上传文件名不能为空")
    private String filename;

    @NotBlank(message = "数据集名称不能为空")
    private String name;

    @NotNull(message = "需要上传的数据字段不能为空")
    @Size(min = 1, message = "需要上传的数据字段不能为空")
    private List<@Valid DatasetColumnDTO> data;

    /**
     * 第一行是否作为字段名
     */
    private Boolean firstLineAsFields;
}
