package org.zjvis.datascience.common.vo;

import lombok.Data;

import java.util.List;

/**
 * @description 管理员筛选查询相关VO
 * @date 2021-08-22
 */
@Data
public class UserSelectVO {
    private String userName;

    private String datasetName;
    //    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime startTime;
//
//    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime endTime;
    private String startTime;

    private String endTime;

    /**
     * 每页显示条数
     */
    private Integer pageSize;

    /**
     * 当前页号
     */
    private Integer curPage;

    private List<SelectOrderVO> selectOrderVOList;
}
