package org.zjvis.datascience.common.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.BaseDTO;

/**
 * @description 用户信息DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoDTO extends BaseDTO {

    private Long id;

    private String name;

    private Long DatasetCnt;

    private String DatasetSize;

    private Long DatasetSizeLong;

    private Integer ProjectNum;
}
