package org.zjvis.datascience.common.model;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.util.Page;
import org.zjvis.datascience.common.vo.dataset.DatasetQueryVO;

import java.io.Serializable;
import java.util.List;

/**
 * @description 复杂聚合查询 结果包装类
 * @date 2021-12-22
 */
@Data
public class AggregateResult implements Serializable {

    private static final long serialVersionUID = 7228648816672738111L;

    private List<AggregateColumn> columns;

    private Object[][] data;

    private JSONObject extraData;

    private Page<JSONArray> page;

    private String errorMsg;

    public AggregateResult() {
    }

    public AggregateResult(List<AggregateColumn> columns, Object[][] data) {
        if (data != null && data.length > 0 && columns != null) {
            if (data[0].length != columns.size()) {
                return;
            }
        }
        this.columns = columns;
        this.data = data;
    }

    public AggregateResult init(List<AggregateColumn> columns, Object[][] data) {
        if (data != null && data.length > 0 && columns != null) {
            if (data[0].length != columns.size()) {
                return this;
            }
        }
        this.setColumns(columns);
        this.setData(data);
        return this;
    }

    public AggregateResult(Object data) {
        this.columns = null;
        this.extraData = JSONObject.parseObject(data.toString());
    }

    public void setPageInfo(DatasetQueryVO queryVO) {
        Page<JSONArray> page = new Page<>();
        page.setCurPage(queryVO.getCurPage());
        page.setPageSize(queryVO.getPageSize());
        if (ObjectUtil.isNotNull(queryVO.getTotalRows())) {
            page.setTotalElementsAndPage(queryVO.getTotalRows());
            this.setPage(page);
        }
    }
}
