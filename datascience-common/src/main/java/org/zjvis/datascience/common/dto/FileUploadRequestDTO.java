package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 文件上传请求DTO
 * @date 2020-12-08
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class FileUploadRequestDTO {
    //总分片数量
    private Integer totalChunks;
    //当前为第几块分片
    private Integer chunkNumber;
    //按多大的文件粒度进行分片
    private Long chunkSize;
    // 当前块的大小
    private Long currentChunkSize;
    // 文件总大小
    private Long totalSize;
    //分片对象或者全文件
    private MultipartFile file;
    // 唯一标识
    private String identifier;
    // 文件名
    private String filename;
    // 文件编码
    private String charSet;
    // 分隔符
    private String separate;
    // 引用符号
    private String quote;
    // 转义
    private String escape;
    // 第一行是否是header, 是的话就需要多一次检查，不是生成默认列名
    private String firstLineAsFields;
}
