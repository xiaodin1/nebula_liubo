package org.zjvis.datascience.common.enums;

/**
 * @description 自定义算子的自定义语言类型
 * @date 2021-04-30
 */
public enum LanguageType {
    PYTHON("python"),
    PYTHON2("python2"),
    PYTHON3("python3"),
    JAVA("java"),
    SCALA("scala");

    LanguageType(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

    private String label;

    public static Boolean isPython(String lang) {
        return lang.equals(PYTHON.label) || lang.equals(PYTHON2.label) || lang.equals(PYTHON3.label);
    }
}
