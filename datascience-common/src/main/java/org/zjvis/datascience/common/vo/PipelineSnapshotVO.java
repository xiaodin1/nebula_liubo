package org.zjvis.datascience.common.vo;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @description 数据视图快照数据表 Pipeline——SnapShot 相关VO
 * @date 2021-06-11
 */
@Data
public class PipelineSnapshotVO {
    @ApiRequestInclude(groups = {Save.class, Recover.class, Delete.class, Query.class, Check.class})
    @NotNull(message = "pipeline id不能为空", groups = {Save.class, Recover.class, Delete.class, Query.class})
    @Min(value = 1, message = "请输入有效pipeline id", groups = {Save.class, Recover.class, Delete.class, Query.class})
    private Long pipelineId;

    @ApiRequestInclude(groups = {Save.class, Check.class})
    @NotBlank(message = "pipeline快照名不能为空", groups = {Save.class, Check.class})
    private String pipelineSnapshotName;

    @ApiRequestInclude(groups = {Save.class, Recover.class, Delete.class, Query.class, Check.class})
    @NotNull(message = "project id不能为空", groups = {Save.class, Recover.class, Delete.class, Query.class, Check.class})
    @Min(value = 1, message = "请输入有效project id", groups = {Save.class, Recover.class, Delete.class, Query.class, Check.class})
    private Long projectId;

    @ApiRequestInclude(groups = {Recover.class})
    @NotNull(message = "pipeline快照id不能为空", groups = {Recover.class})
    @Min(value = 1, message = "请输入有效pipeline快照id", groups = {Recover.class})
    private Long pipelineSnapshotId;

    @ApiRequestInclude(groups = {Delete.class})
    @NotNull(message = "pipeline快照id列表不能为空", groups = {Delete.class})
    @NotEmpty(message = "pipeline快照id列表不能为空", groups = {Delete.class})
    private List<Long> pipelineSnapshotIds;

    @ApiRequestInclude(groups = {Save.class})
    @NotBlank(message = "pipeline快照截图不能为空", groups = {Save.class})
    private String picture;

    public interface Save {
    }

    public interface Recover {
    }

    public interface Delete {
    }

    public interface Query {
    }

    public interface Check {
    }
}
