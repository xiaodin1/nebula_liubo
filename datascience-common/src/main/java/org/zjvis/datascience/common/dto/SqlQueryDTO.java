package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 数据集SQL查询记录，数据集SQL查询记录DTO
 * @date 2021-04-25
 */
@Data
public class SqlQueryDTO {
    private int code;
    private String errMsg;
    private String sql;
    private DataDto data;

    public SqlQueryDTO(int code, String errMsg) {
        this.code = code;
        this.errMsg = errMsg;
    }

    public SqlQueryDTO(int code, DataDto data) {
        this.code = code;
        this.data = data;
    }

    public void setData(DataDto data) {
        this.data = data;
    }

    public SqlQueryDTO(String sql) {
        this.sql = sql;
    }

    public SqlQueryDTO() {

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
