package org.zjvis.datascience.common.vo.column;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.constant.DatasetConstant;
import org.zjvis.datascience.common.vo.PageVO;

import javax.validation.constraints.NotBlank;

/**
 * @description 查询数据统计查询请求VO  （单个col）
 * @date 2021-12-22
 */
@Data
public class ColumnQueryVO extends PageVO {

    private Long taskId;

    @NotBlank
    private String table;

    private String name;

    private Integer type;

    //1=字段序;2=统计序
    private Integer sortType;

    //ASC,DESC
    private String sortVal;

    private String search;

    private JSONArray filter;

    private JSONArray searchFilter;

    //统计模式 1=详情detail 2=摘要abstrac
    private Integer mode;
    //可扩展的传参
    private JSONObject data;

    public static ColumnQueryVO init(){
        ColumnQueryVO vo = new ColumnQueryVO();
        vo.setFilter(new JSONArray());
        vo.setName(DatasetConstant.DEFAULT_ID_FIELD);
        return vo;
    }
}