package org.zjvis.datascience.common.strategy;

import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.enums.OutlierStrategyEnum;
import org.zjvis.datascience.common.strategy.operation.AVGStrategy;
import org.zjvis.datascience.common.strategy.operation.ImputationStrategy;
import org.zjvis.datascience.common.strategy.operation.KNNStrategy;
import org.zjvis.datascience.common.strategy.operation.ManualStrategy;

/**
 * @description : 多种设值策略 [策略模式 + 单例]
 * @date 2021-09-15
 */
@Setter
public class ValueHandlingContext {

    private SetValueStrategy strategy;

    private ValueHandlingContext() {
    }

    public String executeStrategy(int num1, int num2) {
        return String.valueOf(strategy.doOperation(num1, num2));
    }

    /**
     * 判断是否是手动设值
     *
     * @return
     */
    public boolean isManual() {
        return strategy instanceof ManualStrategy;
    }

    private static class ValueHandlingContextHolder {

        private static ValueHandlingContext instance = new ValueHandlingContext();
    }

    public static ValueHandlingContext getInstance() {
        return ValueHandlingContextHolder.instance;
    }

    public void chooseStrategy(String strategyNameStr) {
        //前端集成后 删掉这个if
        if (null == strategyNameStr || strategyNameStr.isEmpty()) {
            this.setStrategy(new ManualStrategy());
            return;
        }
        OutlierStrategyEnum strategy = OutlierStrategyEnum
            .valueOf(StringUtils.upperCase(strategyNameStr));
        switch (strategy) {
            case KNN:
                this.setStrategy(new KNNStrategy());
                break;
            case AVG:
                this.setStrategy(new AVGStrategy());
                break;
            case MULTIPLE_IMPUTATION:
                this.setStrategy(new ImputationStrategy());
                break;
            default:
                this.setStrategy(new ManualStrategy());
        }
    }

}
