package org.zjvis.datascience.common.vo.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.category.DatasetCategoryVO.Id;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 数据管理相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeleteDatasetCategoryVO {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类目录id不能为空", groups = Id.class)
    @Min(value = 1, message = "请输入有效分类目录id", groups = Id.class)
    private Long id;
}
