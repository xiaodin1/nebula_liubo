package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.ActionDTO;
import org.zjvis.datascience.common.util.DozerUtil;

/**
 * @description redo undo相关VO
 * @date 2021-01-14
 */
@Data
public class ActionVO extends BaseVO {

    private Long id;

    private Long userId;

    private String actionType;

    private Long projectId;

    private Long pipelineId;

    private JSONObject contextObj;

    public ActionDTO toTask() {
        ActionDTO dto = DozerUtil.mapper(this, ActionDTO.class);
        dto.setContext(JSONObject.toJSONString(contextObj));
        return dto;
    }
}
