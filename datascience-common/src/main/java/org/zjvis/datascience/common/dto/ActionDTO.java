package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.ActionVO;

/**
 * @description 操作记录表，支持redo和undo操作
 * @date 2021-01-14
 */
@Data
public class ActionDTO extends BaseDTO {

    private Long id;

    private Long userId;

    private String actionType;

    private Long projectId;

    private Long pipelineId;

    private String context;

    public ActionVO view() {
        ActionVO vo = DozerUtil.mapper(this, ActionVO.class);
        vo.setContextObj(JSONObject.parseObject(context));
        return vo;
    }
}