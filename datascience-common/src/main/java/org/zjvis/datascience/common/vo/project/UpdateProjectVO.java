package org.zjvis.datascience.common.vo.project;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 项目数据表 Project相关VO
 * @date 2021-08-10
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProjectVO {

    @ApiRequestInclude(groups = {Update.class, ChangeLockStatus.class})
    @NotNull(message = "项目id不能为空", groups = {ChangeLockStatus.class})
    @Min(value = 1, message = "请输入有效项目id", groups = {ChangeLockStatus.class})
    private Long id;

    @ApiRequestInclude(groups = {Update.class})
    @ApiModelProperty(value = "项目名")
    private String name;

    @ApiRequestInclude(groups = {Update.class})
    @ApiModelProperty(value = "项目描述")
    private String description;

    @ApiRequestInclude(groups = {ChangeLockStatus.class})
    @NotNull(message = "项目锁定状态不能为空", groups = {ChangeLockStatus.class})
    @ApiModelProperty(value = "项目锁定，不可修改")
    private Integer lock;

    private static final long serialVersionUID = 1L;

    public interface ChangeLockStatus {
    }

    public interface Update {
    }

}
