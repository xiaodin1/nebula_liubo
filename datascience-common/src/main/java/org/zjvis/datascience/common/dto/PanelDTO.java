package org.zjvis.datascience.common.dto;

import lombok.Data;
import org.zjvis.datascience.common.model.TaskItems;
import org.zjvis.datascience.common.model.TaskMeta;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.PanelVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 数据视图左侧算子节点面板类
 * @date 2020-08-21
 */
@Data
public class PanelDTO extends BaseDTO {
    private List<TaskItems> panel = new ArrayList<>();

    private List<TaskMeta> searchList = new ArrayList<>();

    public void add(TaskItems taskItems) {
        panel.add(taskItems);
    }

    public void add(TaskMeta taskMeta) {
        searchList.add(taskMeta);
    }

    public PanelVO view() {
        PanelVO vo = DozerUtil.mapper(this, PanelVO.class);
        return vo;
    }

    public List<TaskItems> getPanel() {
        return panel;
    }

    public void setPanel(List<TaskItems> panel) {
        this.panel = panel;
    }


}
