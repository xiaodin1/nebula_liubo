package org.zjvis.datascience.common.widget.vo;

import lombok.Data;

import java.util.List;

/**
 * @description 用于可视化系统构建 按节点渲染 可用图表控件的结果类POJO
 * @date 2021-12-01
 */
@Data
public class WidgetPackageVO {
    private Long tid;

    private String taskName;

    private List<WidgetVO> widgets;

    public WidgetPackageVO(Long tid, String taskName, List<WidgetVO> widgets) {
        this.tid = tid;
        this.taskName = taskName;
        this.widgets = widgets;
    }

}
