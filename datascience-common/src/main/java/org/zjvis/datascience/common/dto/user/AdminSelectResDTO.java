package org.zjvis.datascience.common.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @description 管理员筛选渲染结果DTO
 * @date 2021-08-23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminSelectResDTO<T> {
    private Integer pageNum;

    List<T> resultList;
}
