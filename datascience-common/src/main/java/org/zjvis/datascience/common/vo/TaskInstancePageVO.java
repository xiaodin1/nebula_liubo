package org.zjvis.datascience.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @description 任务实例数据表 TaskInstance 分页结果相关VO
 * @date 2020-10-20
 */
@Data
public class TaskInstancePageVO {
    private String username;

    private Integer type;

    private String projectName;

    private String pipelineName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 每页显示条数
     */
    @NotNull(message = "pageSize不能为空")
    @Min(value = 1,message = "请输入有效的pageSize")
    private Integer pageSize;

    /**
     * 当前页号
     */
    @NotNull(message = "curPage不能为空")
    @Min(value = 1,message = "请输入有效的curPage")
    private Integer curPage;

    /**
     * 排序字段
     */
    private String  sortColumn;

    /**
     * 排序方式
     */
    private String  sortMethod;
}
