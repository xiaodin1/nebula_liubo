package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class LinkVO implements Serializable {
    private String id;

    private String source;

    private String target;

    private String type = "line";

    private String edgeId;

    private String label;

    private Double weight = 1.0;

    private Boolean directed;

    private List<AttrVO> attrs;

    private JSONObject labelCfg = new JSONObject();

    private JSONObject style = new JSONObject();

    private Integer orderId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private JSONArray controlPoints;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Number curveOffset;

    public JSONObject toJSONObject() {
        JSONObject linkObj = new JSONObject();
        linkObj.put("id", this.orderId);
        linkObj.put("label", this.label);
        linkObj.put("source", this.source);
        linkObj.put("target", this.target);
        linkObj.put("weight", this.weight);
        linkObj.put("directed", this.directed);

        JSONObject config = new JSONObject();
        config.put("style", this.style);
        config.put("labelCfg", this.labelCfg);
        config.put("type", this.type);
        config.put("curveOffset", this.curveOffset);
        config.put("controlPoints", this.controlPoints);

        JSONObject attributes = new JSONObject();
        this.attrs.forEach(attr->attributes.put(attr.getKey(), attr.getValue()));
        linkObj.put("config", config);
        linkObj.put("attributes", attributes);
        return linkObj;
    }

}
