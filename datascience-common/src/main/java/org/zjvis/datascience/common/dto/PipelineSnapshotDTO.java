package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 数据视图pipeline快照信息表，数据视图pipeline 快照DTO
 * @date 2021-05-25
 */
@Data
public class PipelineSnapshotDTO extends BaseDTO {

    private Long id;

    private String name;

    private Long pipelineId;

    private Long projectId;

    private String picturePath;

}
