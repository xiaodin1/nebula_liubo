package org.zjvis.datascience.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

/**
 * @description 任务节点实例 TaskInstance 信息表，任务节点实例信息DTO
 * @date 2021-09-18
 */
@Data
public class TaskInstanceInfoDTO {

    private Long id;

    private String username;

    private String status;

    private String taskName;

    private Integer type;

    private String typeName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    private String runningTime;

    private List<String> sourceDataset;
}
