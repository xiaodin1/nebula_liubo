package org.zjvis.datascience.common.enums;

/**
 * @description 清洗操作Action 枚举类
 * @date 2021-12-01
 */
public enum ActionEnum {
    ORIGINAL_DATA("原始数据"),
    FILTER("筛选器"),
    RENAME("字段重命名"),
    TYPE_TRANSFORM("字段类型转化"),
    SEMANTIC_TRANSFORM("语义类型转化"),
    FORMAT_TRANSFORM("转换格式"),
    EDIT_ORDER("编辑有序类别顺序"),
    REPLACE("字段值替换"),
    NUMBER_FORMAT_CONVERSION("数字格式转换"),


    GROUP("字段分组"),

    SPLIT("字段拆分"),
    JSON_PARSE("json解析"),
    ARRAY_PARSE("array解析"),
    JSON_MERGE("合并为json"),
    ARRAY_MERGE("合并为array"),
    REMOVE("移除字段"),
    COPY("复制字段"),
    RETAIN("仅保留字段"),
    MERGE("字段合并"),

    PROCESS_UPPERCASE("字段大写"),
    PROCESS_LOWERCASE("字段小写"),
    REMOVE_ALPHA("清理-移除字母"),
    REMOVE_DIGITAL("清理-移除数字"),
    REMOVE_PUNCT("清理-移除标点"),
    PROCESS_TRIM("清理-裁剪前后空格"),
    REMOVE_SPACE("清理-移除所有空格"),

    DATE_YEAR("转换日期-年数"),
    DATE_MONTH("转换日期-月数"),
    DATE_DAY("转换日期-天数"),
    DATE_WEEK("换日期-周数"),
    DATE_QUARTERLY("转换日期-季度数"),

    TRANSFORM_SET("单元格重新赋值"),
    TRANSFORM_REMOVE("去除重复行"),
    TRANSFORM_REPLACE("替换内容"),
    TRANSFORM_EXTRACT("抽取出新列"),
    TRANSFORM_SPLIT("拆分列"),
    TRANSFORM_DELETE("删除所选行"),
    TRANSFORM_KEEP("保留所选行"),

    GROUP_BY("分组聚合"),

    RESULT_EXTRACT("按示例新增列"),
    ADD_COLUMN_BASED_ON_FORMULA("根据公式新增列"),
    PARTITION_AND_ORDER("分组排序新增列"),

    //undo,redo的操作类型
    UPDATE("update"),
    ADD("add"),
    ADD_UPDATE("add_update"),
    DELETE("delete"),
    CLEAN_ADD("clean_add"),
    CLEAN_DELETE("clean_delete"),
    CLEAN_UPDATE("clean_update"),

    //graph
    EDGE_CONF("edge_conf"),
    GRAPH_FILTER("graph_filter");

    ActionEnum(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

    private String label;

}
