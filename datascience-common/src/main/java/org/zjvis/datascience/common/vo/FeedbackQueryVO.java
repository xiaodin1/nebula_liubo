package org.zjvis.datascience.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 用户反馈 查询相关VO
 * @date 2021-06-15
 */
@Data
public class FeedbackQueryVO extends BaseVO {

    private static final long serialVersionUID = -4668504133163818662L;

    private String userName;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 每页显示条数
     */
    private Integer pageSize;

    /**
     * 当前页号
     */
    private Integer curPage;

}
