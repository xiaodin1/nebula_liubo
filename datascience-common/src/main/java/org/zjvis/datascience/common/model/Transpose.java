package org.zjvis.datascience.common.model;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.List;

/**
 * @description 将多位数据进行转置操作
 * @date 2021-09-13
 */
public class Transpose<T> extends AbstractList<List<T>> implements List<List<T>> {

    private List<T>[] lists;

    public Transpose(List<T>... lists) {
        this.lists = lists;
    }

    public void append(List<T> item) {
        int size = this.lists.length + 1;
        List<T>[] extended = (List<T>[]) new List<?>[size];
        extended[lists.length] = item;
        System.arraycopy(lists, 0, extended, 0, lists.length);
        this.lists = extended;
    }

    private class Column extends AbstractList<T> implements List<T> {

        private final int column;

        public Column(int column) {
            this.column = column;
        }

        @Override
        public T get(int row) {
            return lists[row].get(column);
        }

        @Override
        public int size() {
            return lists.length;
        }
    }

    @Override
    public List<T> get(int column) {
        return new Column(column);
    }

    @Override
    public int size() {
        return Arrays.stream(lists).mapToInt(List::size).min().orElse(Integer.MAX_VALUE);
    }

//    private void test() {
//        Transpose transpose = new Transpose();
//        List<Double> list1 = Arrays.asList(1.0, 1.5);
//        transpose.append(list1);
//        List<Double> list2 = Arrays.asList(30.0, 25.0, 66.6, 2123.9);
//        transpose.append(list2);
//        List<Double> list3 = Arrays.asList(30.0, 25.0, 232.9);
//        transpose.append(list3);
//
//        System.out.println(transpose);
//    }

//    public static void main(String[] args) {
//        new Transpose().test();
//    }
}

