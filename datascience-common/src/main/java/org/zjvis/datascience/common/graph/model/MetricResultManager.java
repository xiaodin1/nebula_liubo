package org.zjvis.datascience.common.graph.model;

import lombok.Data;

import java.util.*;

/**
 * @description MetricResultManager
 * @date 2021-12-29
 */
@Data
public class MetricResultManager {
    public static final String DegreeTag = "Degree";
    public static final String WeightedDegreeTag = "WeightedDegree";
    public static final String EigenvectorCentralityTag = "EigenvectorCentrality";
    public static final String BetweennessCentralityTag = "BetweennessCentrality";
    public static final String ClosenessCentralityTag = "ClosenessCentrality";
    public static final String EccentricityTag = "Eccentricity";
    public static final String StronglyConnectedComponentsTag = "StronglyConnectedID";
    public static final String ConnectedComponentsTag = "ConnectedID";
    public static final String WeaklyConnectedComponentsTag = "WeaklyConnectedID";
    public static final String ClusteringCoefficientTag = "ClusteringCoefficient";
    public static final String TrianglesTag = "NumberOfTriangles";
    public static final String PageRankTag = "PageRankScore";
    public static final String ClusterTag = "ModularityClass";


    public static final String DistanceTag = "Distance";
    public static final String DensityTag = "Density";

    public static final List<String> tagList = Arrays.asList(
            DegreeTag,
            WeightedDegreeTag,
            EigenvectorCentralityTag,
            BetweennessCentralityTag,
            ClosenessCentralityTag,
            EccentricityTag,
            PageRankTag,
            ClusterTag,
            DistanceTag,
            DensityTag,
            StronglyConnectedComponentsTag,
            ConnectedComponentsTag,
            WeaklyConnectedComponentsTag,
            ClusteringCoefficientTag,
            TrianglesTag
            );

    private MetricResult density;
    private MetricResult distance;
    private MetricResult degree;
    private MetricResult weightedDegree;
    private MetricResult eigenvectorCentrality;
    private MetricResult betweennessCentrality;
    private MetricResult closenessCentrality;
    private MetricResult eccentricity;
    private MetricResult pageRank;
    private MetricResult cluster;
    private MetricResult stronglyconnectedcomponents;
    private MetricResult connectedcomponents;
    private MetricResult weaklyconnectedcomponents;
    private MetricResult clusteringCoefficient;
    private MetricResult triangles;

    private Map<String, Map<String, Object>> id2MetricMap;

    private Map<String, Boolean> activateMap;


    public MetricResultManager() {
        activateMap = new HashMap<>();
        id2MetricMap = new HashMap<>();
        for (String tag: tagList) {
            activateMap.put(tag, false);
        }
    }

    public List<MetricResult> retrieveFromTag(List<String> tags) {
        List<MetricResult> metricResults = new ArrayList<>();
        if (tags == null) {
            return metricResults;
        }
        for (String tag: tags) {
            switch (tag) {
                case MetricResultManager.DegreeTag:
                    metricResults.add(this.getDegree());
                    break;
                case MetricResultManager.WeightedDegreeTag:
                    metricResults.add(this.getWeightedDegree());
                    break;
                case MetricResultManager.BetweennessCentralityTag:
                    metricResults.add(this.getBetweennessCentrality());
                    break;
                case MetricResultManager.ClosenessCentralityTag:
                    metricResults.add(this.getClosenessCentrality());
                    break;
                case MetricResultManager.EccentricityTag:
                    metricResults.add(this.getEccentricity());
                    break;
                case MetricResultManager.EigenvectorCentralityTag:
                    metricResults.add(this.getEigenvectorCentrality());
                    break;
                case MetricResultManager.PageRankTag:
                    metricResults.add(this.getPageRank());
                    break;
                case MetricResultManager.ClusterTag:
                    metricResults.add(this.getCluster());
                    break;
                case MetricResultManager.ConnectedComponentsTag:
                    metricResults.add(this.getConnectedcomponents());
                    break;
                case MetricResultManager.WeaklyConnectedComponentsTag:
                    metricResults.add(this.getWeaklyconnectedcomponents());
                    break;
                case MetricResultManager.StronglyConnectedComponentsTag:
                    metricResults.add(this.getStronglyconnectedcomponents());
                    break;
                case MetricResultManager.ClusteringCoefficientTag:
                    metricResults.add(this.getClusteringCoefficient());
                    break;
                case MetricResultManager.TrianglesTag:
                    metricResults.add(this.getTriangles());
                    break;
                default:
            }
        }
        return metricResults;

    }

    public List<String> selectTags(String type) {
        List<String> tags = new ArrayList<>();
        switch (type) {
            case "topology":
                tags.add(DegreeTag);
                tags.add(WeightedDegreeTag);
                tags.add(ClosenessCentralityTag);
                tags.add(BetweennessCentralityTag);
                tags.add(EccentricityTag);
                tags.add(StronglyConnectedComponentsTag);
                tags.add(ConnectedComponentsTag);
                tags.add(WeaklyConnectedComponentsTag);
                break;
            case "node":
                tags.add(EigenvectorCentralityTag);
                tags.add(ClusteringCoefficientTag);
                tags.add(TrianglesTag);
                break;
            case "link":
                tags.add(ClosenessCentralityTag);
                tags.add(BetweennessCentralityTag);
                tags.add(EccentricityTag);
                break;
        }
        return tags;
    }
}
