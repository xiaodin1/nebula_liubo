package org.zjvis.datascience.common.enums;

/**
 * @description 脱敏类型
 * @date 2021-01-15
 */
public enum MaskingEnum {
    MD5("md5加密", "md5"),
    SHA1("sha1加密", "sha1"),
    MOSAIC("字符串打码", "mosaic");

    private String desc;
    private String value;

    MaskingEnum(String desc, String value) {
        this.desc = desc;
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public String getValue() {
        return value;
    }
}
