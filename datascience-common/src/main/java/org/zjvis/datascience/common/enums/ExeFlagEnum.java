package org.zjvis.datascience.common.enums;

/**
 * @description Jlab自定义算子执行状态枚举类
 * @date 2021-11-16
 */
public enum ExeFlagEnum {
    INIT_NEW_LAB("build new jlab", 1),
    GET_TOKEN("get latest token",2),
    LOAD_DATA("load parent data", 3),
    UPDATE_TASK("update task(_instance)",4);

    private String desc;

    private int val;

    ExeFlagEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }
}
