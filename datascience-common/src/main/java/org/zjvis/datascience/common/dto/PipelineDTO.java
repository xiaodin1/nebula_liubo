package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.PipelineVO;

/**
 * @description 数据视图pipeline信息表，数据视图pipeline DTO
 * @date 2021-10-20
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class PipelineDTO extends BaseDTO {

    private Long id;

    private String name;

    private Long projectId;

    private Long userId;

    private String dataJson;

    public PipelineVO view() {
        return view(null);
    }

    public PipelineVO view(String userName) {
        PipelineVO vo = DozerUtil.mapper(this, PipelineVO.class);
        vo.setData(JSONObject.parseObject(this.getDataJson()));
        if (StringUtils.isNotBlank(userName)) {
            vo.setUserName(userName);
        }
        return vo;
    }

}
