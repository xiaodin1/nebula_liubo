package org.zjvis.datascience.common.model;

import lombok.Data;

import java.util.List;

/**
 * @description 算子结果TABLE POJO
 * @date 2020-07-29
 */
@Data
public class TableSchema {

    private String tableName;

    private List<ColumnSchema> columns;
}
