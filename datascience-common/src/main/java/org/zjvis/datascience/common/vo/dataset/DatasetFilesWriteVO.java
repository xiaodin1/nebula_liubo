package org.zjvis.datascience.common.vo.dataset;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @description 数据上传相关VO
 * @date 2021-08-23
 */
@Data
public class DatasetFilesWriteVO {

    @NotBlank(message = "请选择数据分类")
    private String categoryId;

    @NotBlank(message = "数据集名称不能为空")
    private String name;

    @NotBlank(message = "请选择您想导入的数据类型 excel.表格文件excel\\cvs , table.外部数据库")
    private String importType;

    @NotNull(message = "需要上传的数据字段不能为空")
    @Size(min = 1, message = "需要上传的表不能为空")
    private List<@Valid DatasetFileWriteFileVO> files;

    private String charSet;

    private String separate;

    private String quote;

    private String escape;

    private Boolean firstImport;
}
