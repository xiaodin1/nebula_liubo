package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description GreenPlumDTO
 * @date 2021-12-29
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Component
public class GreenPlumDTO {
    @Value("${greenplum.port}")
    private String port;
    @Value("${greenplum.url}")
    private String url;
    @Value("${greenplum.username}")
    private String userName;
    @Value("${greenplum.password}")
    private String password;
}
