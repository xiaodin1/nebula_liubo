package org.zjvis.datascience.common.dto.datax;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Job {
    private Map<String, Map<String, Integer>> setting;
    private List<Content> content;
}
