package org.zjvis.datascience.common.dto.datax;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description DataX配置DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Component
public class DataXConfigDTO {

    @Value("${dataX.filePath}")
    private String filePath;

    @Value("${dataX.logPath}")
    private String logPath;

    @Value("${commandPath}")
    private String commandPath;
}
