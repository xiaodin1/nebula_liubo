package org.zjvis.datascience.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description Pipeline 数据视图 查询相关VO
 * @date 2021-06-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PipelineIdVO {

    private Long pipelineId;
}
