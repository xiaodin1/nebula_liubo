package org.zjvis.datascience.common.formula;

/**
 * @description 公式状态枚举类
 * @date 2021-12-01
 */
public enum FormulaStatus {
    DELETED("删除", "DELETED"),
    ORIGINAL("原始", "ORIGINAL"),
    UPDATED_SUCCESS("更新成功", "UPDATED_SUCCESS"),
    UPDATED_FAILED("更新失败", "UPDATED_FAILED");

    private String desc;
    private String value;

    FormulaStatus(String desc, String value) {
        this.desc = desc;
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public String getValue() {
        return value;
    }
}
