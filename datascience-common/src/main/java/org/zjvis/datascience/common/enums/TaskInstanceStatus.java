package org.zjvis.datascience.common.enums;


/**
 * @description 任务实例运行状态枚举类
 * @date 2021-12-01
 */
public enum TaskInstanceStatus {

    CREATE,
    RUNNING,
    SUCCESS,
    FAIL,
    STOP,
    KILLED;

    public static Boolean isFailed(String status) {
        return status.equals(TaskInstanceStatus.FAIL.toString()) ||
                status.equals(TaskInstanceStatus.KILLED.toString()) ||
                status.equals(TaskInstanceStatus.STOP.toString());
    }

    public static Boolean isRunning(String status) {
        return status.equals(TaskInstanceStatus.RUNNING.toString()) ||
                status.equals(TaskInstanceStatus.CREATE.toString());
    }
}
