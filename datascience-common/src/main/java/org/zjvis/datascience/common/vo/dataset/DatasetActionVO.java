package org.zjvis.datascience.common.vo.dataset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.category.DatasetCategoryVO;

import javax.validation.constraints.Min;

/**
 * @description 数据动作相关VO
 * @date 2020-07-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatasetActionVO extends DatasetIdVO {
    @Min(value = 0, message = "请输入有效天数", groups = DatasetCategoryVO.Id.class)
    private Long daydiff;

    public interface DayDiff {
    }
}
