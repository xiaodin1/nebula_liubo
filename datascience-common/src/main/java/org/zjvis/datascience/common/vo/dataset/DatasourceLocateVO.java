package org.zjvis.datascience.common.vo.dataset;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 数据查询渲染相关VO (定位取数据集)
 * @date 2021-12-23
 */
@Data
public class DatasourceLocateVO {
    @NotNull(message = "数据集id不能为空")
    @Min(value = 1, message = "请输入有效数据集id")
    private Long datasetId;

    @NotNull(message = "pipeline id不能为空")
    @Min(value = 1, message = "请输入有效pipeline id")
    private Long pipelineId;

    @NotNull(message = "项目id不能为空")
    @Min(value = 1, message = "请输入有效项目id")
    private Long projectId;
}
