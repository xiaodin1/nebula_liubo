package org.zjvis.datascience.common.vo.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description 数据上传相关VO （进度渲染）
 * @date 2020-09-18
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImportDatasetProgressVO implements Serializable {

    private static final long serialVersionUID = -8696143058365985476L;

    /**
     * progress=进度*100,即保留两位小数后*100
     */
    private Integer progress;
    private String table;
    private String err;
    private String status;
    /**
     * 总数据量
     */
    private Integer count;
    /**
     * 单位ms，未完成时为null
     */
    private Long costTime;
    /**
     * 导入完的数据集的id，失败则为null
     */
    private Long datasetId;

}
