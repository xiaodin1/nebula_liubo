package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.vo.BaseVO;

import java.io.Serializable;
import java.util.List;

@Data
public class EdgeVO implements Serializable {
    private String id;

    private String label;

    private String weight;

    private JSONObject weightCfg;

    private String source;

    private String target;

    private String type;

    private List<CategoryAttrVO> attrs;

    private List<String> attrIds;

    private JSONObject joinConfigure;

    private JSONObject labelCfg;

    private JSONObject style;
}
