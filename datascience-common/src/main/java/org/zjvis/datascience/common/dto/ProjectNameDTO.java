package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description ProjectNameDTO
 * @date 2021-12-29
 */
@Data
public class ProjectNameDTO {
    private Long id;
    private String name;
}
