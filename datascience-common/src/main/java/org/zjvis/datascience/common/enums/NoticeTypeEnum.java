package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 用户提醒级别枚举类
 * @date 2021-09-02
 */
@Getter
public enum NoticeTypeEnum {
    system(0,"系统消息"),
    project(1,"项目消息");
    private int type;
    private String name;
    NoticeTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
}
