package org.zjvis.datascience.common.vo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @description 字段数值百分比 查询渲染相关VO
 * @date 2021-10-29
 */
@Data
public class PercentageDataVO {
    @NotNull(message = "taskId不能为空")
    private Long taskId;

    @NotNull(message = "projectId不能为空")
    private Long projectId;

    @NotBlank(message = "table不能为空")
    private String table;

    @NotBlank(message = "col不能为空")
    private String col;

    @NotNull(message = "百分比数值不能为空")
    @Min(value = 0, message = "百分比数值不能低于0")
    @Max(value = 100, message = "百分比数值不能高于100")
    private Double percentage;

    private boolean includeNull;
}
