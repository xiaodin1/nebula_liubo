package org.zjvis.datascience.common.model;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.hash.Hashing;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @description 算子结果TABLE POJO
 * @date 2020-07-29
 */
@Data
public class Table {

    private Long dsId;

    private String name;


    public Table(Long dsId, String name) {
        this.dsId = dsId;
        this.name = name;
    }

    public String key() {
        return Hashing.md5().newHasher()
                .putString(Joiner.on("|").join(dsId, StringUtils.isEmpty(name) ? "" : name, System.currentTimeMillis()), Charsets.UTF_8)
                .hash().toString();
    }

}
