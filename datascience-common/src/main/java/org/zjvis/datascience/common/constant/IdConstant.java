package org.zjvis.datascience.common.constant;

/**
 * @description 各类ID常量
 * @date 2021-12-24
 */
public class IdConstant {

    public static final String TASK_ID = "taskId";

    public static final String DATA_ID = "dataId";

    public static final String TASK_INSTANCE_ID = "taskInstanceId";

    public static final String PIPELINE_ID = "pipelineId";

    public static final String PROJECT_ID = "projectId";

    public static final String WIDGET_ID = "widgetId";

    public static final String QUERY_RESULT_IDX = "result";

    public static final String ID = "id";

    public static final String CLUSTER_ID = "cluster_id";

    public static final Long PIPELINE_DUMMY_IDX = -1234567890L;

    // 仅用于 节点执行 初始化阶段的失败， 当存在这个标志位时， 节点运行结果 直接值为失败
    public static final String INSTANCE_PRECAUTION_ERROR = "precautionaryError";

    public static final String RECORD_ID = "_record_id_";
}
