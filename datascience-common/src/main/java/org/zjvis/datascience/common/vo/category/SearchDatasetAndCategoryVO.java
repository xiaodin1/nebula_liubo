package org.zjvis.datascience.common.vo.category;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.zjvis.datascience.common.vo.project.ProjectIdVO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 数据管理相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchDatasetAndCategoryVO {
    private static final long serialVersionUID = 1L;

    @ApiRequestInclude(groups = {ProjectIdVO.ProjectId.class})
    @NotNull(message = "项目id不能为空", groups = ProjectIdVO.ProjectId.class)
    @Min(value = 1, message = "请输入有效项目id", groups = ProjectIdVO.ProjectId.class)
    private Long projectId;

    @NotBlank(message = "检索内容不能为空")
    @Length(max = 100, message = "检索内容长度不能超过100")
    @ApiModelProperty(value = "检索内容", required = true)
    private String content;
}
