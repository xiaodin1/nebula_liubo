package org.zjvis.datascience.common.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.BaseDTO;

import java.time.LocalDateTime;

/**
 * @description 查询用户反馈DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class QueryFeedbackDTO extends BaseDTO {

    private static final long serialVersionUID = 1587058925458064320L;

    private String userName;

    private String realName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 每页显示条数
     */
    private Integer pageSize;

    /**
     * 当前页号
     */
    private Integer curPage;

    /**
     * 分页偏置，由curPage和pageSize计算出
     */
    private Integer pageStart;



}
