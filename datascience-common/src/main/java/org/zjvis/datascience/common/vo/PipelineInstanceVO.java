package org.zjvis.datascience.common.vo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @description 数据视图实例数据表 Pipeline——Istance 相关VO
 * @date 2021-06-11
 */
@Data
public class PipelineInstanceVO extends BaseVO {
    private Long id;

    private Long projectId;

    private Long pipelineId;

    private Long userId;

    private String status;

    private Long duringTime;

    private LocalDateTime gmtRunning;

    private Map<Long, TaskInstanceVO> taskMap;

    private List<String> totalLogs;

}
