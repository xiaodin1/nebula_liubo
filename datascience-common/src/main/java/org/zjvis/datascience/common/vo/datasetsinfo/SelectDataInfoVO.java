package org.zjvis.datascience.common.vo.datasetsinfo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 按条件查询当前用户所有的元数据信息 相关VO
 * @date 2021-12-23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SelectDataInfoVO {
    // 筛选条件

    private String name;    // 数据表名

    private String dataType;    // 数据类型

    private String dataSource; // 存储数据源
}
