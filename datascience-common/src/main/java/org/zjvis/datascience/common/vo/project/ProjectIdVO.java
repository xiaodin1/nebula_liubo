package org.zjvis.datascience.common.vo.project;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 项目数据集ID查询相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectIdVO {

    @ApiRequestInclude(groups = {ProjectIdVO.ProjectId.class})
    @NotNull(message = "项目id不能为空", groups = ProjectIdVO.ProjectId.class)
    @Min(value = 1, message = "请输入有效项目id", groups = ProjectIdVO.ProjectId.class)
    private Long projectId;

    public interface ProjectId {
    }
}
