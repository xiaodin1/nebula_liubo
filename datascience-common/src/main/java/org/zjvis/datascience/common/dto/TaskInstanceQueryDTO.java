package org.zjvis.datascience.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.enums.TaskTypeEnum;
import org.zjvis.datascience.common.util.DozerUtil;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @description 任务节点实例 TaskInstance 信息表，任务节点实例 查询DTO
 * @date 2021-08-30
 */
@Data
public class TaskInstanceQueryDTO {
    private Long id;
    private String parentId;
    private String username;
    private String status;
    private String taskName;
    private Integer type;
    private Integer subType;
    private String subTypeName;
    private Integer algType;
    private String algName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    public TaskInstanceInfoDTO toInfo(List<String> sourceDataset) {
        TaskInstanceInfoDTO info = DozerUtil.mapper(this, TaskInstanceInfoDTO.class);
        TaskTypeEnum[] values = TaskTypeEnum.values();
        for (TaskTypeEnum value : values) {
            if (value.getVal().equals(this.type)) {
                if (StringUtils.isNotBlank(this.algName) && !"DATA_CLEANING".equals(this.algName)) {
                    info.setTypeName(value.getDesc() + "(" + this.algName + ")");
                } else {
                    info.setTypeName(value.getDesc());
                }
            }
        }
        info.setSourceDataset(sourceDataset);

        //TODO 当前没有记录运行时间，返回默认值 -
        info.setRunningTime("-");
        return info;
    }
}
