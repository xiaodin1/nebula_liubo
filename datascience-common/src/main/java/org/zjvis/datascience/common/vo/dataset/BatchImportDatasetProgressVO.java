package org.zjvis.datascience.common.vo.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @description 数据上传相关基础VO
 * @date 2020-08-28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BatchImportDatasetProgressVO implements Serializable {

    private static final long serialVersionUID = -1132827194085419488L;

    private Boolean finalFlag;
    private List<ImportDatasetProgressVO> progresses;

}
