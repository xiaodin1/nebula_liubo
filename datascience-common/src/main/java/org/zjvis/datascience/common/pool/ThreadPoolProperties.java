package org.zjvis.datascience.common.pool;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description 线程池通用配置
 * @date 2021-10-14
 */
@Data
@Component
public class ThreadPoolProperties {

    @Value("${basepool.corePoolSize:20}")
    private Integer corePoolSize = 20;
    @Value("${basepool.maximumPoolSize:40}")
    private Integer maximumPoolSize = 40;
    @Value("${basepool.keepAliveTime:120}")
    private Integer keepAliveTime = 120;
    @Value("${basepool.blockQueueSize:10000}")
    private Integer blockQueueSize = 10000;

}
