package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.DashboardVO;

import java.time.LocalDateTime;

/**
 * @description 画布信息表，画布DTO
 * @date 2021-12-21
 */
@Data
public class DashboardDTO extends BaseDTO {

    private Long id;

    private Long projectId;

    private String layout;

    private String publishLayout;

    private LocalDateTime publishTime;

    private String publishNo;

    private Integer status;

    private String name;

    private String type;

    private Boolean isComplex;

    private Boolean showWatermark;

    /**
     * 对于复杂可视化视图，会复制一次pipeline 然后在mappingJson存储新老taskId的对应关系
     */
    private String mappingJson;

    public DashboardVO view() {
        return DozerUtil.mapper(this, DashboardVO.class);
    }

    /**
     * https://gitee.com/baomidou/mybatis-plus/issues/IINUI
     * 不能删除
     *
     * @return
     */
    public Boolean getIsComplex() {
        return this.isComplex;
    }

    public Long getNewPipelineId() {
        JSONObject jsonObject = JSONObject.parseObject(this.getMappingJson());
        return jsonObject.getLong("newPipelineId");
    }

    public boolean isPublished() {
        return this.status.equals(1);
    }

}
