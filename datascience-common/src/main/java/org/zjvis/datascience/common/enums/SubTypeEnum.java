package org.zjvis.datascience.common.enums;

/**
 * @description Panel节点类型枚举类
 * @date 2021-11-05
 */
public enum SubTypeEnum {
    ETL_OPERATE("ETL/清洗", 0),
    GENERAL_ALGORITHM("算子库", 1),
    MLMODEL("我的模型",2),
    DIMENSION_REDUCTION("降维", 3),
    ANOMALY_DETECTION("异常检测", 4),
    PATTERN_DETECTION("频繁模式挖掘", 5),
    FAVOURITE("我的收藏", 6),
    SELF_DEFINE("自定义算子", 7),
    DATA_CLEANING("数据清洗", 8),
    GRAPH_NET("网络构建", 9),
    REGRESSION("回归", 10),
    CLUSTER("聚类",11),
    FEATURE_ENGINEERING("特征工程",12);

    private String desc;

    private int val;

    SubTypeEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }
}
