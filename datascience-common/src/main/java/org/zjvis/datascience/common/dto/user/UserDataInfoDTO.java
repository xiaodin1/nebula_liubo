package org.zjvis.datascience.common.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.BaseDTO;

/**
 * @description 用户详细信息DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserDataInfoDTO extends BaseDTO {
    private Long id;

    // 用户名
    private String userName;

    private Long datasetId;

    // 数据集名
    private String datasetName;

    // 数据集大小
    private Long datasetSize;

    private String datasetSizeString;

    private Boolean update;
}
