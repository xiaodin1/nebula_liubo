package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 日期时间类型枚举
 * @date 2021-09-14
 */
@Getter
public enum DateTimeFormatTypeEnum {

    DATE_TIME("日期时间", 0),
    DATE("日期", 1),
    TIME("时间", 2),
    ;

    private String desc;
    private Integer value;

    DateTimeFormatTypeEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }

}
