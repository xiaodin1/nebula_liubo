package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.OperatorTemplateDTO;
import org.zjvis.datascience.common.util.DozerUtil;

/**
 * @description 算子模板数据表 Operator_Template 相关VO
 * @date 2021-04-27
 */
@Data
public class OperatorTemplateVO extends BaseVO {
    private Long id;

    private String name;

    private Long userId;

    private Integer type;

    private Integer subType;

    private String subTypeName;

    private String description;

    private String functionName;

    private JSONObject inputParamsObj;

    private JSONObject outputParamsObj;

    private String languageType;

    private String scriptBody;

    private String sdk;

    private Integer status;

    private String suggestion;

    private String notebookId;

    private String interpreter;

    private String fullScript;

    private int isExport;

    public OperatorTemplateDTO toTask() {
        OperatorTemplateDTO dto = DozerUtil.mapper(this, OperatorTemplateDTO.class);
        if (inputParamsObj != null) {
            dto.setInputParams(inputParamsObj.toJSONString());
        }
        if (outputParamsObj != null) {
            dto.setOutputParams(outputParamsObj.toJSONString());
        }
        return dto;
    }
}
