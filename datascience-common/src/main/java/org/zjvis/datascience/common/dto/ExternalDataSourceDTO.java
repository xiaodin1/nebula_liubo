package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 外部数据源配置DTO
 * @date 2020-10-09
 */
@Data
public class ExternalDataSourceDTO {

    private String name;

    private String type;

    private String logo;
}
