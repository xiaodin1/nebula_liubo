package org.zjvis.datascience.common.dto.dataset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.DatasetDTO;

/**
 * @description 数据集分类组合DTO
 * @date 2021-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatasetWithCategoryDTO extends DatasetDTO {

    private String categoryName;

}
