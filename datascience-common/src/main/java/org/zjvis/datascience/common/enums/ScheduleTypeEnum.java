package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 定时任务类型枚举类
 * @date 2021-04-08
 */
@Getter
public enum ScheduleTypeEnum {

    DAY("day", 0),
    WEEK("week", 1),
    HOUR("hour", 2);


    private String desc;
    private int value;

    ScheduleTypeEnum(String desc, int value) {
        this.desc = desc;
        this.value = value;
    }
}
