package org.zjvis.datascience.common.vo.notice;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 用户反馈相关VO
 * @date 2021-09-15
 */
@Data
public class FeedbackNoticeAddVO extends NoticeAddVO {

    @NotNull(message = "id不能为空")
    @Min(value = 1, message = "请输入有效的id")
    private Long id;
}
