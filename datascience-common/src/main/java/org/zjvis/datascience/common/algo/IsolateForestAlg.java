package org.zjvis.datascience.common.algo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.common.constant.SqlTemplate;
import org.zjvis.datascience.common.enums.AlgEnum;
import org.zjvis.datascience.common.enums.SubTypeEnum;
import org.zjvis.datascience.common.sql.SqlHelper;
import org.zjvis.datascience.common.util.ToolUtil;
import org.zjvis.datascience.common.vo.TaskVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @description isolation forest 异常检测模板类
 * @date 2021-12-24
 */
public class IsolateForestAlg extends BaseAlg {

    private final static Logger logger = LoggerFactory.getLogger("IsolateForestAlg");

    private static String TPL_FILENAME = "template/algo/isolate_forest.json";

    private static String SQL_TPL_MADLIB = "SELECT * FROM \"%s\".\"isolation_forest\"('%s', '%s', '%s', '%s', %s, %s, %s)";

    private static String SQL_TPL_MADLIB_SAMPLE = "SELECT * FROM \"%s\".\"isolation_forest_sample\"('CREATE VIEW %s AS SELECT %s from %s where \"%s\" <= %s', '%s', '%s', '%s', %s, %s, %s)";

    private static String SQL_TPL_SPARK = "isolation-forest -s %s -f %s -t %s -tn %d -ms %s -c %s -md %d -uk %s -idcol %s";


    public void initTemplate(JSONObject data) {
        JSONArray jsonArray = getTemplateParamList(TPL_FILENAME);
        data.put("setParams", jsonArray);
        baseInitTemplate(data);
        JSONArray validate = new JSONArray();
        validate.add("feature_cols,number");
        data.put("validate", validate);
    }

    public IsolateForestAlg() {
        super(AlgEnum.ISO_FOREST.name(), SubTypeEnum.ANOMALY_DETECTION.getVal(),
                SubTypeEnum.ANOMALY_DETECTION.getDesc());
        this.maxParentNumber = 1;
    }

    private String getIsolateForestSql(String sourceTable, String outTable, String featureCols,
                                       int nEstimators, float maxSamples, float contamination, int maxDepth, long timeStamp,
                                       String sampleTable) {
        if (StringUtils.isEmpty(featureCols)) {
            return StringUtils.EMPTY;
        }
        if (StringUtils.isNotEmpty(sampleTable)) {
            // 采样
            List<String> fields = new ArrayList<>();
            fields.add(String.format("\"%s\"", ID_COL));
            String[] tmps = featureCols.split(",");
            for (String item : tmps) {
                fields.add(String.format("\"%s\"", item));
            }
            return String.format(SQL_TPL_MADLIB_SAMPLE, SqlTemplate.SCHEMA, sampleTable,
                    Joiner.on(",").join(fields), sourceTable, ID_COL, SAMPLE_NUMBER, outTable, ID_COL,
                    featureCols, nEstimators, maxSamples, contamination);
        } else {
            // 全量, 根据配置
            if (getEngine().isMadlib()) {
                return String
                        .format(SQL_TPL_MADLIB, SqlTemplate.SCHEMA, sourceTable, outTable, ID_COL,
                                featureCols, nEstimators, maxSamples, contamination);
            } else if (getEngine().isSpark()) {
                return String.format(SQL_TPL_SPARK, sourceTable, featureCols, outTable, nEstimators,
                        maxSamples, contamination, maxDepth, timeStamp, ID_COL);
            }
        }
        return StringUtils.EMPTY;
    }

    public String initSql(JSONObject json, List<SqlHelper> sqlHelpers, long timeStamp,
                          String engineName) {
        this.engineName = engineName;
        String sourceTable = json.getString("source_table");
        sourceTable = ToolUtil.alignTableName(sourceTable, timeStamp);
        String outTable = json.getString("out_table_rename");
        outTable = ToolUtil.alignTableName(outTable, timeStamp);
        // String idCol = json.getString("id_col");
        JSONArray features = json.getJSONArray("feature_cols");
        String featureCols = this.getFeatureColsStr(features);
        int nEstimators = json.getInteger("n_estimators");
        float maxSamples = json.getFloat("max_samples");
        float contamination = json.getFloat("contamination");
        int maxDepth = json.getInteger("max_depth");
        String sampleTable = "";
        if (!json.containsKey("isSample") || json.getString("isSample").equals("SUCCESS") || json
                .getString("isSample").equals("FAIL")) {
            sampleTable = outTable.replace("solid_", "view_");
            json.put("isSample", "CREATE");
        }
        String sql = this
                .getIsolateForestSql(sourceTable, outTable, featureCols, nEstimators,
                        maxSamples, contamination, maxDepth, timeStamp, sampleTable);
        logger.debug("sql={}", sql);
        return sql;
    }

    public void defineOutput(TaskVO vo) {
        JSONObject jsonObject = vo.getData();
        String outTablePrefix = jsonObject.getString("out_table");
        String tableName = String
                .format(SqlTemplate.OUT_TABLE_NAME, outTablePrefix, vo.getPipelineId(), vo.getId());
        jsonObject.put("out_table_rename", tableName);
        JSONArray input = jsonObject.getJSONArray("input");
        if (input == null || input.size() == 0) {
            logger.warn("input is empty");
            return;
        }
        jsonObject.put("source_table", input.getJSONObject(0).getString("tableName"));
        this.checkBoxSelectFilter(jsonObject, "number", FEATURE_COLS);
        this.supplementForCheckbox(jsonObject, TPL_FILENAME, 1, vo);
        JSONArray outputColTypes = new JSONArray();
        JSONArray outputCols = new JSONArray();
        outputCols.add(ID_COL);
        outputColTypes.add(ID_TYPE);
        if (input != null && input.size() > 0) {
            if (!jsonObject.containsKey("feature_cols")) {
                logger.warn("feature_cols not exists!!!");
                return;
            }
            List<String> featuresTmp = jsonObject.getJSONArray("feature_cols")
                    .toJavaList(String.class);
            List<String> features = new ArrayList<>();
            for (String f : featuresTmp) {
                if (f.contains(".")) {
                    String[] tmps = f.split("\\.");
                    features.add(tmps[tmps.length - 1]);
                }
            }
            List<String> inputCols = input.getJSONObject(0).getJSONArray("tableCols")
                    .toJavaList(String.class);
            List<String> inputColumnTypes = input.getJSONObject(0).getJSONArray("columnTypes")
                    .toJavaList(String.class);
            this.prepareOutputColumnTypes(outputColTypes, features, inputCols, inputColumnTypes);
            outputCols.addAll(features);
            outputCols.add("label");
            outputColTypes.add("varchar");
        }
        JSONArray output = new JSONArray();
        JSONObject outItem = new JSONObject();
        outItem.put("tableName", tableName);
        outItem.put("tableCols", outputCols);
        outItem
                .put("nodeName", vo.getName() == null ? AlgEnum.ISO_FOREST.toString() : vo.getName());
        outItem.put("columnTypes", outputColTypes);
        this.setSubTypeForOutput(outItem);
        output.add(outItem);
        jsonObject.put("output", output);
        vo.setData(jsonObject);
    }
}