package org.zjvis.datascience.common.dto.dataset;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 数据集JSON信息
 * @date 2021-12-24
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatasetJsonInfo {

    private String schema;

    private String table;

    private String type;

    private String source;

    private List<DatasetColumnDTO> columnMessage;
}
