# <center> algo-py
<center>服务于天枢可视分析平台主体工程aiworks的python机器学习算法与服务</center>

-----
## 1. 如何部署algo-py的web服务

### 1.1 本地环境要求:

- python3+


### 1.2 下载aiworks工程中的<u>algo-py</u>到本地


### 1.3 安装python依赖

到aiworks-py文件夹执行如下终端命令
```commandline 
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
```

### 1.4 修改配置文件

- 将数据库MySQL、GreenPlum与MinIO的配置项按格式写在<a href="common/config/config">config</a>中

- 在<a href="common/config/config.py">config.py</a>中定义config文件在服务器上的绝对路径

### 1.5 启动服务

进入到 "aiworks-py" 的文件夹运行下列代码

```bash
python3 app.py
```
(看到 <Server running at http://0.0.0.0:5001> 则表示服务启动成功)



### 1.6 停止服务

在服务器中执行脚本<a href="stop.sh">stop.sh</a>
```commandline
sh stop.sh
```

-----
## 2. 如何配置利用JupyterLab实现的自定义算子

### 2.0 环境要求
- 执行 **1.1**, **1.2**, **1.3** （如已满足条件请忽略）
- docker服务

### 2.1 搭建Docker镜像

- 进入到<a href="common/Dockerfile">Dockerfile</a>所在文件夹
     ```commandline
     cd aiworks-py/common/
     ```
- 搭建Docker镜像
     ```commandline
     docker build -t jlab_r:v8 .
     ```

### 2.2 更改配置文件
- 在<a href="common/config/config">config</a>中填写：
  - NOTEBOOKDIR：存储notebooks的文件夹服务器绝对路径
  - HOMEDIR："algo-py/common"文件夹在服务器中的绝对路径
  - PYDIR：python在服务器中的绝对路径
  - DOCKER_IMG_VERSION：docker容器镜像的名称与版本(默认=jlab_r:v8)
- （可选项）：    
  - DOCKER_MEM: docker容器内的最大存储（默认=1500M）
  - DOCKER_CPU: docker容器可被分配的cpu资源数量（默认=2）
 
### 2.3* 修改aiworks的配置项

------
## 3. 如何注册UDFs到Greenplum
平台上的部分功能调用了Greenplum上注册的UDFs(User Defined Functions)，想要使用这些功能就需要执行以下步骤：
- 修改<a href="../datascience-service/deploy.sh">deploy.sh</a>中的数据库信息
- 执行deploy.sh，把平台官方提供的UDFs注册到Greenplum
```commandline
sh deploy.sh
```
*若需要编写自己的UDFs，请将代码管理在此<a href="../datascience-service/src/main/resources/madlib/">文件夹</a>中。
若依赖的Python包没有，需要在Greenplum的服务器上安装。安装python库之前首先需要安装pip，然后批量安装requirements_udf.txt
```
yum install epel-release
yum install python-pip
pip install -r requirements_udf.txt
```

## 4. Auto-sklearn的系统要求
Auto-sklearn的系统要求:
- Linux operating system (for example Ubuntu)
- Python (>=3.7),
- C++ compiler (with C++11 supports).
```
yum install gcc-c++
```
如果您试图在没有提供pyrfr软件包的控制盘文件的系统上安装Auto-sklearn，您还需要: SWIG.
```
yum install swig3
```