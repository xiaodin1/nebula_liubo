import os
# import flaskr
import unittest
import tempfile

class TestMethods(unittest.TestCase):

	def test1(self):
		self.assertEqual("foo".upper(),"FOO")


	def test_split(self):
		s = 'hello world'
		self.assertEqual(s.split(), ['hello', 'world'])
		# check that s.split fails when the separator is not a string
		with self.assertRaises(TypeError):
			s.split(2)


if __name__ == '__main__':
	suite = unittest.TestLoader().loadTestsFromTestCase(TestMethods)
	unittest.TextTestRunner(verbosity=2).run(suite)