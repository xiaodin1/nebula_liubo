pid=`netstat -nlp | grep "5000" | awk '{print $7}' | cut -d/ -f1`

if [ "" != "$pid" ];
then
   echo "### stop old service..."
   kill -9 $pid
fi