from flask import Flask
from urllib.parse import quote_plus as urlquote
from waitress import serve
import logging
from api.feature_api import feature
from api.mlmodel_api import mlmodel
from api.gams_api import gams
from api.others_api import others
from model.db import db
import pymysql
from common.config.config import *

pymysql.install_as_MySQLdb()

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# flask & SQLALCHEMY
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "{}://{}:{}@{}:{}/{}?charset=utf8" \
    .format('mysql', MYSQL_USER, urlquote(MYSQL_PASSWORD), MYSQL_HOST, MYSQL_PORT, MYSQL_DBNAME)
app.config['POOL_SIZE'] = 50
app.config['SQLALCHEMY_ECHO'] = False
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 800
app.config['MAX_OVERFLOW'] = 20
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_POOL_RECYCLE'] = 800

db.init_app(app)

# -----------------------------------------APIs-------------------------------------------
app.register_blueprint(feature)
app.register_blueprint(mlmodel)
app.register_blueprint(gams)
app.register_blueprint(others)

if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=5001)
# if __name__ == '__main__':
#     app.testing(debug=True)
