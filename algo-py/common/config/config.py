import os
from minio import Minio
import urllib3

env = "dev"

#TODO define root path and config path
ROOT_PATH = "/root/flask/aiworks-py"
CONFIG_PATH = ROOT_PATH + "/common/config/config"

if not os.path.exists(CONFIG_PATH):
    print("config_dev path not existed")

CP = {}
with open(CONFIG_PATH) as f:
    for line in f:
        try:
            k, v = line.strip('\n').split("=")
            CP[k] = v
        except:
            continue

GP_URL = CP["GP_URL"]
GP_USER = CP["GP_USER"]
GP_PASSWORD = CP["GP_PASSWORD"]
GP_DRIVER = CP["GP_DRIVER"]
GP_HOST = CP["GP_HOST"]
GP_PORT = CP["GP_PORT"]
GP_DBNAME = CP["GP_DBNAME"]
GP_MAX_CONN = 20
GP_MIN_CONN = 1

GP_PROP = {"user": GP_USER,
           "password": GP_PASSWORD,
           "driver": GP_DRIVER}

# MYSQL
MYSQL_USER = CP["MYSQL_USER"]
MYSQL_PASSWORD = CP["MYSQL_PASSWORD"]
MYSQL_DRIVER = CP["MYSQL_DRIVER"]
MYSQL_DBNAME = CP["MYSQL_DBNAME"]
MYSQL_URL = CP["MYSQL_URL"]
MYSQL_PORT = CP["MYSQL_PORT"]
MYSQL_HOST = CP["MYSQL_HOST"]

MYSQL_PROP = {"user": MYSQL_USER,
              "password": MYSQL_PASSWORD,
              "driver": MYSQL_DRIVER}

MINIO_ADDRESS = CP["MINIO_ADDRESS"]
MINIO_ACCESS_KEY = CP["ACCESS_KEY"]
MINIO_SECRET_KEY = CP["SECRET_KEY"]

MINIO_CONF = {
    "endpoint": MINIO_ADDRESS,
    "access_key": MINIO_ACCESS_KEY,
    "secret_key": MINIO_SECRET_KEY,
    "secure": False
}
MINIO_CLIENT = Minio(**MINIO_CONF)
