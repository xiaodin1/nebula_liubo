from flask import request, Blueprint
import logging
from algo import compute_stats

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

others = Blueprint('others', __name__)


@others.route('/statistics', methods=['POST'])
def statistics():
    params = request.form.to_dict()
    return compute_stats.begin_alg(params["source_table"], params["func"], params["feature_col"], params["method"],
                                   params["condition"], float(params["observed"]), float(params["alpha"]))
