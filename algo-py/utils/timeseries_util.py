from statsmodels.tsa.stattools import adfuller

# ADF test判断特征列是否平稳 （同期插补，均值）
def adf_tests(df, cols):
    non_steady_col = []
    for col in cols:
        steady = adf_test(df[col].dropna())
        if not steady:
            non_steady_col.append(col)
    return non_steady_col


def adf_test(df):
    dftest = adfuller(df, autolag='AIC')
    p_value = round(dftest[1], 3)
    if p_value > 0.1:
        return False
    else:
        return True