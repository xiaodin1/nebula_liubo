import numpy as np
from utils.format_util import dup_name_handler


def run(df, params):
    results = {}
    fails = []

    cols = eval(params.get("cols"))
    is_append = int(params.get("append"))
    method = params.get("method")
    all_cols = df.columns.tolist()

    if len(cols) == 0:
        results["messages"] = "请选择特征列"
        return df, results
    for col in cols:
        data = df[col].values
        valid_index = np.where(np.isnan(data) == 0)[0]
        valid_data = data[valid_index]

        flag = True
        if method == 'zscore':
            mean = np.mean(valid_data)
            std = np.std(valid_data)
            new_data = (valid_data - mean) / std
        elif method == 'minmax':
            min = np.min(valid_data)
            max = np.max(valid_data)
            new_data = (valid_data - min) / (max - min)
        elif method == 'log':
            if np.min(valid_data) < 1:
                flag = False
                fails.append(col)
            else:
                new_data = np.log(valid_data) / np.log(np.max(valid_data))
        else:
            return None

        if flag:
            if is_append:
                new_col = '_'.join([col, method])
                new_col = dup_name_handler(new_col, all_cols)

                index = all_cols.index(col) + 1
                all_cols.insert(index, new_col)
                df = df.reindex(columns=all_cols)
                df[new_col] = np.full(len(data), np.nan)
                df[new_col][valid_index] = new_data
            else:
                df[col][valid_index] = new_data

    if len(fails) > 0:
        results["message"] = "以下特征列存在数值<1: {}".format(", ".join(fails))
    return df, results
