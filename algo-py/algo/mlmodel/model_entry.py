import logging
import os
from utils import model_util
from utils import mlmodel_util
from algo.mlmodel import kmeans, linear_regression, pca, decision_tree, auto_sklearn_classification, auto_sklearn_regression, support_vector_regression

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")


def kill_process(pid):
    cmd = "kill " + str(pid)
    try:
        os.system(cmd)
    except Exception as e:
        logging.info(e)
    cur_pid = os.getpid()
    cmd = "kill " + str(cur_pid)
    try:
        os.system(cmd)
        logging.info(cur_pid, "killed")
    except Exception as e:
        logging.info(e)


def ml_entry(para_list, output, dataframe, record):
    model_saved_path = "s3a://ml-model/{}/{}/{}_{}".format(para_list["usr_id"], model_util.model_type(para_list["algo"]),
                                                           para_list["algo"], para_list["model_id"])

    # dataframe = read_gp(sql)
    execution = para_list["execution"]
    algo = para_list["algo"].upper()
    print("ALGO: " + algo)
    mlmodel = None
    df_pred = None
    y_pred = None

    try:
        # ---------------------------------Algorithm Task Entry---------------------------------------
        if algo == "DTC":
            if execution == "predict":
                df_pred = decision_tree.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = decision_tree.train(dataframe, para_list, record)
        elif algo == "AUTOC":
            if execution == "predict":
                df_pred = auto_sklearn_classification.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = auto_sklearn_classification.train(dataframe, para_list, record, output)
        elif algo == "LR":
            if execution == "predict":
                df_pred = linear_regression.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = linear_regression.train(dataframe, para_list, record)
        elif algo == "SVR":
            if execution == "predict":
                df_pred = support_vector_regression.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = support_vector_regression.train(dataframe, para_list, record)
        elif algo == "AUTOR":
            if execution == "predict":
                df_pred = auto_sklearn_regression.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = auto_sklearn_regression.train(dataframe, para_list, record, output)
        elif algo == "KM":
            if execution == "predict":
                df_pred = kmeans.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = kmeans.train(dataframe, para_list, record)
        elif algo == "PCA":
            if execution == "predict":
                df_pred = pca.predict(dataframe, para_list, output)
            else:
                mlmodel, y_pred = pca.train(dataframe, para_list, record)
        else:
            output = {}
            output["error"] = {"algo name not found"}
    except Exception as e:
        logging.error(e)
        logging.error("error occurs in model entry")
        output["error"] = mlmodel_util.py_to_java(str(e))
    finally:
        logging.info("model entry return")
        if execution == "predict":
            return record, output, df_pred, None, None
        else:
            return record, output, None, mlmodel, y_pred
