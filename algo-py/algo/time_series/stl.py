import statsmodels.api as sm
import logging

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")


def run_STL(dataframe, para_list, output):
    try:
        featureCols = para_list["featureCol"]
        seasonals = para_list["seasonals"]
        for feature, s in featureCols.items():
            rd = sm.tsa.seasonal_decompose(dataframe[feature].values, freq=s)

    except Exception as e:
        logging.error(e)
    return 1
