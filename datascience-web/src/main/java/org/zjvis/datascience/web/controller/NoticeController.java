package org.zjvis.datascience.web.controller;

import cn.weiguangfu.swagger2.plus.annotation.ApiGroup;
import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.enums.NoticeFilterTypeEnum;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.vo.notice.NoticeVO;
import org.zjvis.datascience.service.NoticeService;

/**
 * @description 消息通知接口 Controller
 * @date 2021-09-02
 */
@ApiPlus(value = true)
@RequestMapping("/notice")
@RestController
@Api(tags = "notice", description = "消息通知接口")
public class NoticeController {
    @Autowired
    NoticeService noticeService;

    @ApiGroup(groups = NoticeVO.Query.class)
    @PostMapping(value = "/query")
    public ApiResult<Object> query(@Validated(NoticeVO.Query.class) @RequestBody NoticeVO notice) {
        notice.setUserId(JwtUtil.getCurrentUserId());
        return ApiResult.valueOf(noticeService.query(notice));
    }

    @ApiGroup(groups = NoticeVO.Ids.class)
    @PostMapping(value = "/process")
    public ApiResult<Boolean> process(@Validated(NoticeVO.Ids.class) @RequestBody NoticeVO notice) {
        return ApiResult.valueOf(noticeService.process(notice.getIds()));
    }

    @PostMapping(value = "/processAll")
    public ApiResult<Boolean> processAll() {
        return ApiResult.valueOf(noticeService.processAll());
    }

    @ApiGroup(groups = NoticeVO.Ids.class)
    @PostMapping(value = "/delete")
    public ApiResult<Boolean> delete(@Validated(NoticeVO.Ids.class) @RequestBody NoticeVO notice) {
        return ApiResult.valueOf(noticeService.delete(notice.getIds()));
    }

    @ApiGroup(groups = NoticeVO.DeleteByStatus.class)
    @PostMapping(value = "/deleteAll")
    public ApiResult<Boolean> deleteAll(@Validated(NoticeVO.DeleteByStatus.class) @RequestBody NoticeVO notice) {
        return ApiResult.valueOf(noticeService.deleteAll(notice.getStatus()));
    }

    @PostMapping(value = "/queryFilterType")
    public ApiResult<Object> queryFilterType() {
        JSONArray ja = new JSONArray();
        NoticeFilterTypeEnum[] notices = NoticeFilterTypeEnum.values();
        for (NoticeFilterTypeEnum notice:notices) {
            JSONObject jo = new JSONObject();
            jo.put("type",notice.getType());
            jo.put("name",notice.getName());
            ja.add(jo);
        }
        return ApiResult.valueOf(ja);
    }
}
