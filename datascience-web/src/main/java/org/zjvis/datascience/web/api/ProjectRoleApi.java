package org.zjvis.datascience.web.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.exception.DataScienceException;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.service.UserProjectService;
import com.alibaba.fastjson.JSONObject;
import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @description 项目角色管理插件接口 Controller
 * @date 2021-09-23
 */
@ApiPlus(value = true)
@RestController
@RequestMapping("/projectsApi/")
@Api(tags = "projectRoleApi", description = "项目角色管理插件接口")
@Validated
public class ProjectRoleApi {

    @Autowired
    private UserProjectService userProjectService;


    @PostMapping(value = "/checkRoleAuth")
    @Transactional
    @ResponseBody
    @ApiOperation(value = "检查用户权限", notes = "检查用户权限")
    public ApiResult<Boolean> checkRoleAuth(HttpServletRequest request, @RequestBody JSONObject params) {
        Long projectId = params.getLong("projectId");
        Integer authRoleId = params.getInteger("authRoleId");
        String token = params.getString("token");
        Long userId = null; //TODO token transform to userId

        if (projectId == null || projectId <= 0L || authRoleId == null || authRoleId <= 0) {
            return ApiResult.valueOf(ApiResultCode.PARAM_ERROR);
        }
        try {
            userProjectService.checkRoleAuth(projectId, authRoleId, false, userId);
        } catch (DataScienceException e) {
            return ApiResult.valueOf(Boolean.FALSE);
        }
        return ApiResult.valueOf(Boolean.TRUE);
    }

}
