package org.zjvis.datascience.web.controller;

import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.zjvis.datascience.common.annotation.ProjectRoleAuth;
import org.zjvis.datascience.common.enums.ProjectRoleAuthEnum;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.vo.dataset.DatasetIdAndProjectIdVO;
import org.zjvis.datascience.common.vo.project.LoadDatasetVO;
import org.zjvis.datascience.common.vo.project.ProjectDatasetVO;
import org.zjvis.datascience.service.DatasetProjectService;
import org.zjvis.datascience.service.TaskService;
import org.zjvis.datascience.service.WidgetService;
import org.zjvis.datascience.service.dataset.DatasetService;

import javax.validation.Valid;
import java.util.Set;

/**
 * @description 项目数据集管理接口 Controller
 * @date 2021-12-28
 */
@ApiPlus(value = true)
@RestController
@RequestMapping("/projects/datasets")
@Api(tags = "项目数据集管理")
@Validated
public class DatasetProjectController {

    @Autowired
    private DatasetProjectService datasetProjectService;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private WidgetService widgetService;

    @Lazy
    @Autowired
    private TaskService taskService;
    

    @PostMapping(value = "/create")
    @ApiOperation(value = "加载数据集")
    public ApiResult<Boolean> create(@ProjectRoleAuth(role = ProjectRoleAuthEnum.DEVELOPER) @Valid @RequestBody LoadDatasetVO vo) {
        datasetProjectService.add(vo, false);
        return ApiResult.valueOf(true);
    }

    @PostMapping(value = "/delete")
    @ApiOperation(value = "在项目中移除数据集，删除关联关系")
    public ApiResult<Boolean> delete(@ProjectRoleAuth(role = ProjectRoleAuthEnum.DEVELOPER) @Valid @RequestBody LoadDatasetVO vo)
        throws Exception {

        Set<Long> datasetIds = vo.getDatasetIds();
        for (Long datasetId : datasetIds) {
            datasetProjectService.checkCurrentUserDatasetUsedInProject(vo.getProjectId(), datasetId);
            //判断是不是自己的数据集，不是自己的不能操作
            datasetService.checkAuth(datasetId, false);
        }

        datasetProjectService.delete(vo);

        return ApiResult.valueOf(true);
    }

    @PostMapping(value = "/query")
    @ResponseBody
    @ApiOperation(value = "获取指定项目中的数据集数据")
    public ApiResult<Object> queryDataByDatasetAndProjectId(
            @Validated @RequestBody DatasetIdAndProjectIdVO datasetAndProjectVO) {
        JSONObject jo = datasetService.queryDataById(datasetAndProjectVO);
        if (jo.getInteger("code") != 200) {
            return new ApiResult(jo.getInteger("code"), jo.getString("errMsg"));
        } else {
            return ApiResult.valueOf(jo.get("data"));
        }
    }

    /**
     *
     * @param vo
     * @return
     */
    @PostMapping(value = "/queryAllDataset")
    @ApiOperation(value = "加载或检索项目数据源 (可视化构建)")
    public ApiResult<Object> queryAllDataset(@ProjectRoleAuth @RequestBody ProjectDatasetVO vo) {
        Object obj = datasetProjectService.queryAllDataset(vo.getProjectId(), vo.getPipelineId(), vo.getDatasetName());
        return ApiResult.valueOf(obj);
    }

    /**
     * 这个还是保持原样 去掉2  api/projects/datasets/queryProjectDataset
     * @param vo
     * @return
     */
    @PostMapping(value = "/queryProjectDataset")
    @ApiOperation(value = "加载或检索项目数据源 (数据视图)")
    public ApiResult<Object> queryProjectDataset(@ProjectRoleAuth @Valid @RequestBody ProjectDatasetVO vo) {
        Object obj = datasetProjectService.queryProjectDataset(vo);
        return ApiResult.valueOf(obj);
    }

}
