package org.zjvis.datascience.web.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.service.dag.DAGScheduler;

/**
 * @description Spark任务回调接口 Controller
 * @date 2021-11-19
 */
@RequestMapping("/callback")
@RestController
@Api(tags = "callback", description = "回调接口")
public class CallbackController {

    @Autowired
    private DAGScheduler dagScheduler;

    @PostMapping(value = "/updateJobStatus")
    @ApiOperation(value = "更新task的运行结果", notes = "接收jobServer的回调更新状态")
    public void updateJobStatus(@RequestBody JSONObject jsonObject) {
        dagScheduler.updateJobStatus(jsonObject);
    }

}
