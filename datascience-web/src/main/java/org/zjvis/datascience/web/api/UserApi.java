package org.zjvis.datascience.web.api;

import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.minidev.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.model.ApiResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @description 用户插件接口 Controller
 * @date 2021-09-23
 */
@ApiPlus(value = true)
@RestController
@RequestMapping("/usersApi")
@Api(tags = "userApi", description = "用户插件接口")
@Validated
public class UserApi {

    @PostMapping(value = "/getCurrentUserDTO")
    @Transactional
    @ResponseBody
    @ApiOperation(value = "获取当前用户", notes = "获取当前用户")
    public ApiResult<UserDTO> getCurrentUserDTO(HttpServletRequest request, @RequestBody JSONObject params) {
        String token = params.getAsString("token");
        return null;
    }

}
