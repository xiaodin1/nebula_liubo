package org.zjvis.datascience.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;
import org.zjvis.datascience.common.util.JwtUtil;

/**
 * @description : Jwt 令牌管理类
 * @date 2020-06-01
 */
public class JwtToken implements AuthenticationToken {

  private static final long serialVersionUID = 1L;
  /**
   * 加密后的 JWT token串
   */
  private String token;

  private String userName;

  public JwtToken(String token) {
    this.token = token;
    this.userName = JwtUtil.getUserName(token);
  }

  @Override
  public Object getPrincipal() {
    return this.userName;
  }

  @Override
  public Object getCredentials() {
    return token;
  }

}
