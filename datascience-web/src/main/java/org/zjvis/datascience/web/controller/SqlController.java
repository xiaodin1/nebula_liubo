package org.zjvis.datascience.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.annotation.ProjectAuth;
import org.zjvis.datascience.common.dto.dataset.DatasetNameTypeDTO;
import org.zjvis.datascience.common.enums.ProjectAuthEnum;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.project.ProjectIdVO;

import java.util.List;

/**
 * @description 数据集管理 Controller
 * @date 2021-12-29
 */
@Api(tags = "数据集管理")
@RestController
@RequestMapping("/dataset")
public class SqlController {
    @PostMapping("/queryBySql")
    @ApiOperation(value = "根据sql语句获取数据")
    public ApiResult<List<DatasetNameTypeDTO>> queryBySql(
            @ProjectAuth(auth = ProjectAuthEnum.READ) @Validated(value = ProjectIdVO.ProjectId.class) @RequestBody ProjectIdVO projectIdVO) {
        List<DatasetNameTypeDTO> res = null;
        return ApiResult.valueOf(DozerUtil.mapper(res, DatasetNameTypeDTO.class));
    }
}
