package org.zjvis.datascience.web.controller;

import cn.weiguangfu.swagger2.plus.annotation.ApiGroup;
import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import cn.weiguangfu.swagger2.plus.enums.ApiExecutionEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.widget.dto.WidgetFavouriteDTO;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.widget.vo.WidgetFavouriteVO;
import org.zjvis.datascience.service.UserProjectService;

import java.util.List;

/**
 * @description 可视化收藏接口 Controller
 * @date 2021-12-01
 */
@ApiPlus(value = true)
@RestController
@RequestMapping("/widget/")
@Api(tags = "可视化收藏")
@Validated
public class WidgetFavouriteController {

    @Autowired
    private UserProjectService userProjectService;

    @ApiGroup(groups = WidgetFavouriteVO.Query.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/favourite/queryByPipelineId")
    public ApiResult<List<WidgetFavouriteVO>> query(@Validated(value = WidgetFavouriteVO.Query.class)
                                                    @RequestBody WidgetFavouriteVO vo) {
        return ApiResult.valueOf(DozerUtil.mapper(userProjectService.queryFavouriteWidget(vo.getPipelineId()), WidgetFavouriteVO.class));
    }

    @ApiGroup(groups = WidgetFavouriteVO.Create.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/favourite/create")
    @ApiOperation(value = "收藏")
    public ApiResult<Boolean> create(@Validated(value = WidgetFavouriteVO.Create.class) @RequestBody WidgetFavouriteVO vo) {
        WidgetFavouriteDTO dto = DozerUtil.mapper(vo, WidgetFavouriteDTO.class);
        userProjectService.favouriteWidget(dto);
        return ApiResult.valueOf(true);
    }

    @ApiGroup(groups = WidgetFavouriteVO.Create.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/favourite/delete")
    @ApiOperation(value = "取消收藏")
    public ApiResult<Boolean> delete(@Validated(value = WidgetFavouriteVO.Create.class) @RequestBody WidgetFavouriteVO vo) {
        WidgetFavouriteDTO dto = DozerUtil.mapper(vo, WidgetFavouriteDTO.class);
        userProjectService.favouriteWidgetCancel(dto);
        return ApiResult.valueOf(true);
    }

}
