package org.zjvis.datascience.web.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.annotation.CategoryAuth;
import org.zjvis.datascience.common.dto.DatasetDTO;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.util.RedisUtil;
import org.zjvis.datascience.common.vo.dataset.PreviewDatasetVO;
import org.zjvis.datascience.common.vo.http.HttpDataCreateTableVO;
import org.zjvis.datascience.common.vo.http.HttpStatusVO;
import org.zjvis.datascience.service.HttpDataService;
import org.zjvis.datascience.service.SemanticService;

/**
 * @description http数据接入接口 Controller
 * @date 2021-12-08
 */
@Component
@Api(tags="http数据接入")
@RestController
@ResponseBody
@RequestMapping("/dataset")
public class HttpController {
    @Autowired
    HttpDataService httpDataService;

    @Autowired
    SemanticService semanticService;

    @Autowired
    RedisUtil redisUtil;

    private final static String HTTP_REDIS_PREFIX = "urlForImportHttpData_";
    private final static long HTTP_REDIS_EXPIRE = 86400; //24小时候未被消费的数据过期

    @PostConstruct
    @Scheduled(fixedDelay = 360000L)
    public void importDataToGP(){
        httpDataService.importDataToGP();
    }

    @PostMapping("/jsonParsing")
    @ApiOperation(value = "Json样式解析")
    public ApiResult<PreviewDatasetVO> jsonParsing(@Valid @RequestBody String vo){
        JSONObject jo = httpDataService.jsonParsing(vo);
        int code = jo.getInteger("code");
        if(code==ApiResultCode.JSON_FORMAT_ERROR.getCode()){
           return ApiResult.valueOf(ApiResultCode.JSON_FORMAT_ERROR,null,jo.getString("tips"));
        }
        PreviewDatasetVO previewHttpDatasetVO =(PreviewDatasetVO) jo.get("data");

        semanticService.recommendSemantic(previewHttpDatasetVO.getHead(),previewHttpDatasetVO.getData());
        return ApiResult.valueOf(ApiResultCode.SUCCESS,previewHttpDatasetVO);

    }

    @PostMapping("/httpDataCreateTable")
    @ApiOperation(value = "在GP中建表")
    public ApiResult<Long> httpDataCreateTable(@CategoryAuth(field = "categoryId") @Valid @RequestBody HttpDataCreateTableVO vo){
        return ApiResult.valueOf(httpDataService.httpDataCreateTable(vo));
    }

    @PostMapping("/updateHttpDataStatus")
    @ApiOperation(value = "更新url状态")
    public ApiResult<Boolean> updateHttpDataStatus(@Valid @RequestBody HttpStatusVO datasetVO){
        UserDTO user = JwtUtil.getCurrentUserDTO();
        if (user.getId().equals(0L)) {
            return ApiResult.valueOf(ApiResultCode.NO_AUTH);
        }
        DatasetDTO datasetDTO = DozerUtil.mapper(datasetVO, DatasetDTO.class);
        datasetDTO.setUserId(user.getId());
        return ApiResult.valueOf(httpDataService.updateHttpDataStatus(datasetDTO,datasetVO));
    }

    @PostMapping("/httpDataImport/{url}")
    @ApiOperation(value = "将数据增量导入到GP")
    public ApiResult<Boolean> httpDataImport(@Valid @RequestBody String vo,@PathVariable("url") String url){
        if(redisUtil.hget("urlToDataJson",url)==null){
            return ApiResult.valueOf(ApiResultCode.URL_ERROR,false,"url不存在或url已关闭");
        }
        try {
            redisUtil.llPush(HTTP_REDIS_PREFIX + url, vo, HTTP_REDIS_EXPIRE);
        }catch (Exception e){
            return ApiResult.valueOf(false);
        }
        return ApiResult.valueOf(true);
    }
}
