package org.zjvis.datascience.web.controller;

import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.service.socket.SocketIOService;

/**
 * @description socket接口 Controller
 * @date 2021-12-29
 */
@ApiPlus(value = true)
@RequestMapping("/socket")
@RestController
@Api(tags = "socket", description = "socket接口")
public class SocketController {
    @Autowired
    SocketIOService socketIOService;

    @PostMapping("/send")
    public void sendMsg(@RequestBody JSONObject data) {
        long userId = JwtUtil.getCurrentUserId();
        String eventType=data.getString("eventType");
        if (userId==0) {
            System.err.println("获取不到当前用户");
        } else if (StringUtils.isBlank(eventType)) {
            System.err.println("eventType为空");
        } else {
            socketIOService.sendToUser(String.valueOf(userId),eventType,data);
        }
    }
}
