package org.zjvis.datascience.service;

import java.util.Base64;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.crypto.CipherService;
import org.apache.shiro.io.DefaultSerializer;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.junit.Test;

/**
 * 加密测试
 *
 * @date 2020-04-20
 */
public class EncryptTest {

    public static final byte[] key = Base64.getDecoder().decode("f/SY5TIve5WWzT4aQlABJA==");
    private PrincipalCollection pc = new SimplePrincipalCollection();
    private DefaultSerializer s = new DefaultSerializer<PrincipalCollection>();
    private byte[] v = s.serialize(pc);
    private CipherService cs = new AesCipherService();

    @Test
    public void encrypt() {
        long b = System.currentTimeMillis();
        cs.encrypt(v, key);
        System.out.println("cost:" + (System.currentTimeMillis() - b) + "ms");
    }

}
