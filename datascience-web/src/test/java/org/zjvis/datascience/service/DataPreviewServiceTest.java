package org.zjvis.datascience.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.vo.DataPreviewVO;

import java.util.List;

public class DataPreviewServiceTest extends BaseTest{

    @Autowired
    DataPreviewService dataPreviewService;

    @Test
    public void testPreview() {
        Long taskId = 602L;
        Long sessionId = 64L;
        Long pipelineId = 108L;
        List<DataPreviewVO> previewVOS = dataPreviewService.preview(taskId, pipelineId);
        System.out.println(previewVOS.toString());
    }

    @Test
    public void testGetRecordCount() {

    }
}
