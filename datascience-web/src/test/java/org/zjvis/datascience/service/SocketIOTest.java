//package org.zjvis.datascience.web.socket;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import com.alibaba.fastjson.JSONObject;
//import com.corundumstudio.socketio.SocketIOClient;
//import com.corundumstudio.socketio.SocketIOServer;
//import org.zjvis.datascience.common.util.JwtUtil;
//
//@Service
//public class SocketIOTest {
//
//  private final static Logger logger = LoggerFactory.getLogger(SocketIOTest.class);
//
//  // 用来存已连接的客户端
//  private static Map<String, SocketIOClient> clientMap = new ConcurrentHashMap<>();
//
//  private static Map<String, Map<String, List<SocketIOClient>>> clientsMap = new ConcurrentHashMap<>();
//
//  private static Map<String, List<SocketIOClient>> clientsMap2 = new ConcurrentHashMap<>();
//
//    @Autowired
//  private SocketIOServer socketIOServer;
//
//  /**
//   * Spring IoC容器创建之后，在加载SocketIOServiceImpl Bean之后启动
//   * @throws Exception
//   */
//  @PostConstruct
//  private void autoStartup() throws Exception {
//      start();
//  }
//
//  @PreDestroy
//  private void autoStop() throws Exception  {
//      stop();
//  }
//
//  public void start() {
//    // 监听客户端连接
//    socketIOServer.addConnectListener(client -> {
//        System.err.println("--------------1---------------");
//        String uid = getParamsByClient(client);
//        if (uid != null) {
//            clientMap.put(uid, client);
//            logger.info("有新客户端连接UID:{}",uid);
//        }
//        // 给客户端发送一条信息 发送ClientReceive事件 需要客户端绑定此事件即可接收到消息
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("name","goat");
//        jsonObject.put("message","hello client");
//        System.err.println("-------------sessionId---------------"+client.getSessionId());
//
//        String userId = "zh";
//        List<SocketIOClient> scs = clientsMap2.get(userId);
//        if (scs==null) {
//            System.err.println("为"+userId+"创建列表同时添加session："+client.getSessionId());
//            scs = new ArrayList<>();
//            scs.add(client);
//            clientsMap2.put(userId,scs);
//        } else {
////            Iterator<SocketIOClient> iterator = scs.iterator();
////            while (iterator.hasNext()) {
////                SocketIOClient sc = iterator.next();
////                if (!sc.isChannelOpen()) {
////                    System.err.println("session: "+sc.getSessionId()+" 连接关闭,移除缓存");
////                    iterator.remove();
////                }
////            }
//            System.err.println(""+userId+"列表添加新数据，session："+JwtUtil.getCurrentUserId());
//            scs.add(client);
//        }
//
//        client.sendEvent("ClientReceive",jsonObject);
//    });
//
//    // 监听客户端断开连接
//    socketIOServer.addDisconnectListener(client -> {
//        System.err.println("--------------2---------------");
//        System.err.println("session连接中断："+client.getSessionId());
//
//        client.disconnect();
//
//        Set<String> keys = clientsMap2.keySet();
//        List<Object> l = new ArrayList<>();
//        for (String key:keys) {
//            List<SocketIOClient> scs = clientsMap2.get(key);
//            if (scs==null) {
//                continue;
//            } else {
//                Iterator<SocketIOClient> iterator = scs.iterator();
//                while (iterator.hasNext()) {
//                    SocketIOClient sc = iterator.next();
//                    if (!sc.isChannelOpen()) {
//                        System.err.println("session-disconnect: "+sc.getSessionId()+" 连接关闭,移除缓存");
//                        iterator.remove();
//                    } else {
//                        l.add(sc.getSessionId());
//                    }
//                }
//            }
//        }
//        System.err.println("存活session："+l.toString());
//
//
//
//        logger.info("一条客户端连接中断");
//    });
//
//    // 处理自定义的事件，与连接监听类似
//    // 此示例中测试的json收发 所以接收参数为JSONObject 如果是字符类型可以用String.class或者Object.class
//    socketIOServer.addEventListener("ServerReceive",JSONObject.class, (client, data, ackSender) -> {
//        // TODO do something
//        System.out.println("-------------3---------------");
//        JSONObject jsonObject = data;
//        String uid = getParamsByClient(client);
//        if (uid != null) {
//            client.sendEvent("Servereply",jsonObject);
//            logger.info("接收到SID:{}发来的消息:{}",uid,jsonObject.toJSONString());
//            System.err.println("接收到SID:{}发来的消息:"+uid+jsonObject.toJSONString());
//
//        }
//    });
//
//      socketIOServer.addEventListener("push_event", JSONObject.class, (client, data, ackSender) -> {
//          // TODO do something
//          System.err.println("开启push_event");
//          JSONObject jsonObject = data;
//          System.err.println(jsonObject.toJSONString());
//          if (data==null||data.isEmpty()) {
//              System.err.println("data is empty");
//          } else {
//              if (data.containsKey("event")) {
//                  String event  = jsonObject.getString("event");
//                  System.err.println("event: "+event);
//                  if (StringUtils.isNotBlank(event)) {
//                      String sessionId  = client.getSessionId().toString();
//
//                      System.out.println("-------------sessionId---------------"+sessionId);
//                      System.out.println("-------------namespace---------------"+client.getNamespace().getName());
//
//
//                      client.sendEvent("push_event",jsonObject);
//
//                      System.out.println("-------------6---------------");
//                  }
//              } else {
//                  System.err.println("no event");
//              }
//
//          }
//      });
//
//    socketIOServer.start();
//    logger.info("socket.io初始化服务完成");
//  }
//
//  public void stop() {
//      System.out.println("--------------4---------------");
//    if (socketIOServer != null) {
//        socketIOServer.stop();
//        socketIOServer = null;
//    }
//    logger.info("socket.io服务已关闭");
//  }
//
//  private String getParamsByClient(SocketIOClient client) {
//      System.err.println("--------------5--------------");
//      // 从请求的连接中拿出参数（这里的sid必须是唯一标识）
//      Map<String, List<String>> params = client.getHandshakeData().getUrlParams();
//      System.err.println("user: "+JwtUtil.getCurrentUserId());
//      List<String> list = params.get("UID");
//      if (list != null && list.size() > 0) {
//          return list.get(0);
//      }
//      return null;
//  }
//
//    /**
//     * 通知所有在线客户端
//     */
//    public void sendAllUser(String key, String event, String message) {
//        List<SocketIOClient> socketIOClients = clientsMap2.get(key);
//        System.err.println("进入sendAllUser");
//        if (socketIOClients!=null) {
//            for (SocketIOClient socketIOClient:socketIOClients) {
//                System.err.println("isChannelOpen: "+socketIOClient.isChannelOpen());
//                if (socketIOClient.isChannelOpen()) {
//                    System.err.println("处理事件");
//                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("name","push_event");
//                    jsonObject.put("message",message);
//                    jsonObject.put("event",event);
//                    System.err.println("处理之前："+jsonObject.toJSONString());
//                    socketIOClient.sendEvent(event, jsonObject);
//                } else {
//                    System.err.println("channel closed");
//                }
//            }
//        } else {
//            System.err.println("获取不到client");
//        }
//    }
//
//}
