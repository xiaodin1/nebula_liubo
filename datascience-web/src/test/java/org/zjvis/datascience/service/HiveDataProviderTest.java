package org.zjvis.datascience.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.service.dataprovider.GPDataProvider;
import org.zjvis.datascience.service.dataprovider.HiveDataProvider;

import java.util.List;

public class HiveDataProviderTest  extends BaseTest {
    @Autowired
    HiveDataProvider hiveDataProvider;

    @Autowired
    GPDataProvider gpDataProvider;

    @Test
    public void testShowDatabases() {
        List<String> result = hiveDataProvider.showDatabases(5L);
        for (String e: result) {
            System.out.println(e);
        }
    }
}
