package org.zjvis.datascience.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class WidgetTest {

  public static void main(String[] args) {
    JSONObject json = new JSONObject();
    json.put("type", "dataset");
    json.put("id", 5);
    JSONObject dataJson = new JSONObject();
    JSONArray filters = new JSONArray();
    
    JSONArray groups = new JSONArray();
    JSONObject group1 = new JSONObject();
    group1.put("col", "gender");
    group1.put("filterType", null);
    group1.put("values", new JSONArray());
    groups.add(group1);
    
    JSONArray keys = new JSONArray();
    JSONObject key1 = new JSONObject();
    key1.put("col", "the_year");
    key1.put("filterType", "=");
    key1.put("values", "[2016]");
    keys.add(key1);
    
    JSONArray values = new JSONArray();
    JSONObject value1 = new JSONObject();
    value1.put("col", "store_sales");
    value1.put("aggregate_type", "sum");
    value1.put("sort", "desc");
    value1.put("topN", "5");
    values.add(value1);
    
    JSONObject config = new JSONObject();
    config.put("filters", filters);
    config.put("groups", groups);
    config.put("keys", keys);
    config.put("values", values);
    
    dataJson.put("config", config);
    json.put("widgetJson", dataJson);
    
    System.out.println(json.toJSONString());
  }

  
}
