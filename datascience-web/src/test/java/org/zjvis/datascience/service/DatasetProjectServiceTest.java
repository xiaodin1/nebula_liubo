package org.zjvis.datascience.service;

import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.DatasetProjectDTO;

public class DatasetProjectServiceTest extends BaseTest {

  @Autowired
  public DatasetProjectService service;

  @Test
  public void testQueryByDatasetId() {
     List<DatasetProjectDTO> datasetProjectList = service.queryByDatasetId(129L);
     System.out.println(datasetProjectList);
  }
  
}
