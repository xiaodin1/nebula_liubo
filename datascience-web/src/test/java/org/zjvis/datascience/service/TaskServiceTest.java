package org.zjvis.datascience.service;

import java.util.List;

import cn.hutool.core.lang.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.TaskDTO;
import com.alibaba.fastjson.JSONObject;

public class TaskServiceTest extends BaseTest {
    @Autowired
    TaskService taskService;

    @Test
    public void testWrapDto() {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(219L);
        JSONObject jsonObject = new JSONObject();
        JSONObject position = new JSONObject();
        position.put("x", 100);
        position.put("y", 200);
        jsonObject.put("position", position);
        jsonObject.put("isForbiden", 1);
        taskDTO.setDataJson(jsonObject.toJSONString());
        taskService.wrapTaskDTO(taskDTO);
        System.out.println(taskDTO.getDataJson());
    }

    @Test
    public void testDeleteLine() {
        TaskDTO taskDTO =  taskService.queryById(257L);
        // taskService.deleteLine(taskDTO.getId());
    }
    
    @Test
    public void testQueryByDatasetId() {
      Long datasetId = 106L;
      Long projectId = 95L;
      List<TaskDTO> tasks = taskService.queryByDatasetId(datasetId, projectId);
      System.out.println(tasks);
    }

    @Test
    public void testQueryById() {
        TaskDTO taskDTO = taskService.queryById(7448L);
        Assert.notNull(taskDTO);
    }
}
