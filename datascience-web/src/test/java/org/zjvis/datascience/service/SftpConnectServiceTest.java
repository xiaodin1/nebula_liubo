package org.zjvis.datascience.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class SftpConnectServiceTest extends BaseTest {
    @Autowired
    SftpConnectService sftpConnectService;

    @Test
    public void testDownloadFile() {
        String remoteFilePath = "/home/gpdata/wangyizhong/dat.csv";
        String localFilePath = "D:\\dat.csv";
        boolean flag = sftpConnectService.downloadFile(remoteFilePath, localFilePath);
        Assert.assertTrue(flag);
    }

    @Test
    public void testCopyGpTableToLocal() {
        String tableName = "dataset.v_53_1611890301071_m";
        String filePath = "/home/gpdata/wangyizhong/dat5.csv";
        boolean flag = sftpConnectService.copyGpTableToLocal(tableName, filePath);
        Assert.assertTrue(flag);
    }

}
