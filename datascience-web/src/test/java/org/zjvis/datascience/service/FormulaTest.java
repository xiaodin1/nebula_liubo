package org.zjvis.datascience.service;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormulaTest {

//    public static void main(String[] args) {
//        Integer precisionNum = 4;
//        String[] arr = new String[]{"6", "1.0", "1", "32342342.4433", "4.0"};
//        for (String result: arr) {
//            System.out.printf(result);
//
//            //填充0 的 最长长度是10
//            String res = trimmmm(result, precisionNum);
//
//            System.out.println(" -> " + res +  " \t precisionNum ->" + precisionNum);
//        }
//
//    }

    private static String trimmmm(String result, Integer precisionNum){
        if (result.split("\\.").length == 1) {
            result += ".";
        }
        result = String.format("%-" + (result.length() + 10) + "s", result).replace(" ", "0");
        if (precisionNum.equals(0)) {
            result = result.split("\\.")[0];
        } else {
            result = result.substring(0, result.indexOf('.') + precisionNum + 1);
        }
        return result;
    }

    private static final String[] chars = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".split(" ");

    public static void main(String[] args) {
//        TreeMap<Integer, String> treeMap = new TreeMap<>();
//        for (int i = 0; i < chars.length; i++) {
//            treeMap.put(i, chars[i]);
//        }
//        System.out.println(treeMap);
//        Integer low = treeMap.firstKey();
//        Integer high = treeMap.lastKey();
//        System.out.println(low);
//        System.out.println(high);
//        Iterator<Integer> it = treeMap.keySet().iterator();
//        for (int i = 0; i <= 6; i++) {
//            if (i == 3) { low = it.next(); }
//            if (i == 6) { high = it.next(); }
//            else { it.next(); }
//        }
//        System.out.println(low);
//        System.out.println(high);
//        System.out.println(treeMap.subMap(low, high));
//        System.out.println(treeMap.headMap(high));
//        System.out.println(treeMap.tailMap(low));
//
//         int COUNT_BITS = Integer.SIZE - 3;
//         int CAPACITY   = (1 << COUNT_BITS) - 1;
//// runSt high-order bits
//         int RUNNING    = -1 << COUNT_BITS;
//        System.out.println(COUNT_BITS);
//        System.out.println(RUNNING);
        String[] aggrStrings = new String[]{
                "%%",
                "%%%",
                "%%%%%%%",
        };
        for( String v : aggrStrings) {
            Matcher matcher = Pattern.compile("%%*").matcher(v);
            System.out.println(matcher.matches());
        }

    }
}
