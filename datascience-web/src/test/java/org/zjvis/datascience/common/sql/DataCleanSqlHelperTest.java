package org.zjvis.datascience.common.sql;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;

public class DataCleanSqlHelperTest {

  @Test
  public void testInitSql() {
    long start = System.currentTimeMillis();
    String mapJson = "{\"行_id\":-5,\"省_自治区\":-1,\"订单日期\":93,\"订单_id\":-1,\"邮寄方式\":-1,\"客户_id\":-1,\"客户名称\":-1,\"细分\":-1,\"城市\":-1,\"国家_地区\":-1,\"销售区域\":-1,\"产品_id\":-1,\"类别\":-1,\"子类别\":-1,\"产品名称\":-1,\"销售额\":2,\"数量\":-5,\"折扣\":2,\"利润\":2,\"发货日期\":93,\"_record_id_\":4}";
    Map map = JSONObject.parseObject(mapJson, LinkedHashMap.class);
    System.out.println(map);

    String data = "{\"action\":\"TYPE_TRANSFORM\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"toType\":\"VARCHAR\",\"description\":\"为 字符串 类型\",\"id\":3000}";
    printSql("====字段类型转换====", map, data);

    data = "{\"filter\":[[{\"col\":\"行_id\",\"filterType\":\"=\",\"values\":[\"26\"]},{\"col\":\"行_ID\",\"filterType\":\"=\",\"values\":[\"#NULL\"]}]],\"action\":\"FILTER\",\"table\":\"orders_emea_1603242244347\",\"id\":3000}";
    printSql("====定值筛选,包含null值====", map, data);

    data = "{\"filter\":[[{\"col\":\"行_id\",\"filterType\":\"<>\",\"values\":[\"#NULL\"]}]],\"action\":\"FILTER\",\"table\":\"orders_emea_1603242244347\",\"col\":\"行_ID\",\"id\":3000}";
    printSql("====不包含null值====", map, data);

    data = "{\"filter\":[[{\"col\":\"客户名称\",\"filterType\":\"like\",\"values\":[\"%a\"]}]],\"action\":\"FILTER\",\"table\":\"orders_emea_1603242244347\",\"id\":3000}";
    printSql("====通配符匹配====", map, data);

    data = "{\"filter\":[[{\"col\":\"行_id\",\"filterType\":\"[a,b]\",\"values\":[\"100\",\"200\"]}]],\"action\":\"FILTER\",\"table\":\"orders_emea_1603242244347\",\"id\":3000}";
    printSql("====数值范围筛选====", map, data);

    data = "{\"filter\":[[{\"col\":\"发货日期\",\"filterType\":\"[a,b]\",\"values\":[\"2015/01/01\",\"2016/01/01\"]}]],\"action\":\"FILTER\",\"table\":\"orders_emea_1603242244347\",\"id\":3000}";
    printSql("====日期范围筛选====", map, data);

    data = "{\"action\":\"PROCESS_UPPERCASE\",\"table\":\"orders_emea_1603242244347\",\"col\":\"客户名称\",\"description\":\"所有值已更改为大写\",\"id\":3000}";
    printSql("====字段大写====", map, data);

    data = "{\"action\":\"PROCESS_LOWERCASE\",\"table\":\"orders_emea_1603242244347\",\"col\":\"客户名称\",\"description\":\"\",\"id\":3000}";
    printSql("====字段小写====", map, data);

    data = "{\"action\":\"REMOVE_ALPHA\",\"table\":\"orders_emea_1603242244347\",\"col\":\"产品名称\",\"description\":\"\",\"id\":3000}";
    printSql("====移除字母====", map, data);

    data = "{\"action\":\"REMOVE_DIGITAL\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单_id\",\"description\":\"\",\"id\":3000}";
    printSql("====移除数字====", map, data);

    data = "{\"action\":\"REMOVE_PUNCT\",\"table\":\"orders_emea_1603242244347\",\"col\":\"产品名称\",\"description\":\"\",\"id\":3000}";
    printSql("====移除标点====", map, data);

    data = "{\"action\":\"PROCESS_TRIM\",\"table\":\"orders_emea_1603242244347\",\"col\":\"产品名称\",\"description\":\"\",\"id\":3000}";
    printSql("====裁剪空格====", map, data);

    data = "{\"action\":\"REMOVE_SPACE\",\"table\":\"orders_emea_1603242244347\",\"col\":\"产品名称\",\"description\":\"\",\"id\":3000}";
    printSql("====移除所有空格====", map, data);

    data = "{\"action\":\"GROUP\",\"table\":\"orders_emea_1603242244347\",\"col\":\"邮寄方式\",\"groupConfig\":[{\"source\":[\"一级\",\"二级\",\"当日\"],\"target\":\"标准级\",\"targetType\":\"origin\"},{\"source\":[\"特级\"],\"target\":\"特级\",\"targetType\":\"new\"}],\"id\":3000}";
    printSql("====分组操作====", map, data);

    data = "{\"action\":\"SPLIT\",\"table\":\"orders_emea_1603242244347\",\"col\":\"城市\",\"separator\":\"-\",\"splitType\":1,\"splitNum\":2,\"id\":3000}";
    printSql("====拆分前n个====", map, data);

    data = "{\"action\":\"SPLIT\",\"table\":\"orders_emea_1603242244347\",\"col\":\"城市\",\"separator\":\"-\",\"splitType\":2,\"splitNum\":2,\"id\":3000}";
    printSql("====拆分后n个====", map, data);

    data = "{\"action\":\"SPLIT\",\"table\":\"orders_emea_1603242244347\",\"col\":\"城市\",\"separator\":\"-\",\"splitType\":3,\"splitNum\":2,\"id\":3000,\"maxSplit\":3}";
    printSql("====全部拆分====", map, data);

    data = "{\"action\":\"DATE_YEAR\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"id\":3000}";
    printSql("====日期转换成年====", map, data);

    data = "{\"action\":\"DATE_MONTH\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"id\":3000}";
    printSql("====日期转换成月份====", map, data);

    data = "{\"action\":\"DATE_DAY\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"id\":3000}";
    printSql("====日期转换成日====", map, data);

    data = "{\"action\":\"DATE_WEEK\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"id\":3000}";
    printSql("====日期转换成周数====", map, data);

    data = "{\"action\":\"DATE_QUARTERLY\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"id\":3000}";
    printSql("====日期转换成季度====", map, data);

    data = "{\"action\":\"RENAME\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单日期\",\"newCol\":\"重命名订单日期\",\"id\":3000}";
    printSql("====重命名字段操作====", map, data);

    data = "{\"action\":\"COPY\",\"table\":\"orders_emea_1603242244347\",\"col\":\"行_id\",\"id\":3000}";
    printSql("====复制字段====", map, data);

    data = "{\"action\":\"RETAIN\",\"table\":\"orders_emea_1603242244347\",\"retainCols\":[\"订单_id\",\"订单日期\"],\"id\":3000}";
    printSql("====仅保留字段====", map, data);

    data = "{\"action\":\"REMOVE\",\"table\":\"orders_emea_1603242244347\",\"removeCols\":[\"订单_id\",\"订单日期\"],\"id\":3000}";
    printSql("====删除字段====", map, data);

    data = "{\"action\":\"REPLACE\",\"table\":\"orders_emea_1603242244347\",\"col\":\"邮寄方式\",\"groupConfig\":[{\"source\":[\"一级\",\"二级\",\"当日\"],\"target\":\"标准级\",\"targetType\":\"origin\"},{\"source\":[\"特级\"],\"target\":\"特级\",\"targetType\":\"new\"}],\"id\":3000}";
    printSql("====替换操作====", map, data);

    data = "{\"action\":\"MERGE\",\"table\":\"orders_emea_1603242244347\",\"mergeCols\":[\"订单日期\",\"邮寄方式\",\"利润\"],\"separator\":\",\",\"id\":3000}";
    printSql("====合并字段====", map, data);
    System.out.println(System.currentTimeMillis() - start);
  }

  public void printSql(String desc, Map map, String data) {
    JSONObject obj = JSONObject.parseObject(data);
    System.out.println(desc);
    System.out.println(DataCleanSqlHelper.initSql(Maps.newLinkedHashMap(map), obj, 3750l));
  }
}
