-- DDL


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for action
-- ----------------------------
DROP TABLE IF EXISTS `action`;
CREATE TABLE `action` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action_type` varchar(100) NOT NULL DEFAULT '' COMMENT '操作类型',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipelineId',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `context` mediumtext COMMENT '上下文内容json格式',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline` (`pipeline_id`),
  KEY `idx_project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dashboard
-- ----------------------------
DROP TABLE IF EXISTS `dashboard`;
CREATE TABLE `dashboard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `layout` longtext COMMENT '看板布局',
  `publish_layout` longtext COMMENT '发布配置',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '发布状态:0=未发布;1=已发布',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  `publish_no` varchar(100) DEFAULT NULL COMMENT '发布号',
  `name` varchar(255) DEFAULT '画布1' COMMENT '名称',
  `type` varchar(127) DEFAULT '系统' COMMENT '类型',
  `is_complex` tinyint(1) DEFAULT '0' COMMENT '是否包含复杂widget',
  `mapping_json` text COMMENT '新旧taskId对应关系',
  `show_watermark` tinyint(1) DEFAULT '1' COMMENT '发布后是否有水印',
  PRIMARY KEY (`id`),
  UNIQUE KEY `publish_no` (`publish_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for data_level
-- ----------------------------
DROP TABLE IF EXISTS `data_level`;
CREATE TABLE `data_level` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) NOT NULL COMMENT '数据中文名',
  `name_en` varchar(255) DEFAULT NULL COMMENT '数据英文名',
  `level` varchar(255) NOT NULL COMMENT '数据分级（L1~L4）',
  `content` varchar(1047) DEFAULT NULL COMMENT '数据说明',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `gmt_modifier` bigint(8) DEFAULT NULL COMMENT '创建者',
  `gmt_creator` bigint(8) DEFAULT NULL COMMENT '修改者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for database
-- ----------------------------
DROP TABLE IF EXISTS `database`;
CREATE TABLE `database` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '数据源名称',
  `type` varchar(20) NOT NULL COMMENT '数据源类型',
  `config_json` text COMMENT '数据源配置',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据源表';

-- ----------------------------
-- Table structure for dataset
-- ----------------------------
DROP TABLE IF EXISTS `dataset`;
CREATE TABLE `dataset` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '名称',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '所属用户id',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '所属类别id',
  `data_json` text COMMENT '数据集元信息',
  `data_config` text COMMENT '数据源的配置信息',
  `incremental_data_config` text COMMENT '增量数据的详细配置',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_user_category_name` (`user_id`,`category_id`,`name`),
  KEY `idx_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据集表';

-- ----------------------------
-- Table structure for dataset_action
-- ----------------------------
DROP TABLE IF EXISTS `dataset_action`;
CREATE TABLE `dataset_action` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action_type` varchar(100) NOT NULL DEFAULT '' COMMENT '操作类型',
  `dataset_id` bigint(20) unsigned NOT NULL COMMENT '数据集id',
  `row_affect` bigint(20) unsigned NOT NULL COMMENT '影响行数',
  `row_final` bigint(20) unsigned DEFAULT NULL COMMENT '最终行数',
  `context` mediumtext COMMENT '具体操作',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `status` varchar(100) NOT NULL DEFAULT '' COMMENT '用户是否已读',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `gmt_creator` bigint(20) NOT NULL COMMENT '创建者',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '修改者',
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`) USING BTREE,
  KEY `idx_dataset` (`dataset_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for dataset_category
-- ----------------------------
DROP TABLE IF EXISTS `dataset_category`;
CREATE TABLE `dataset_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '数据集目录名称',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `parent_id` bigint(20) unsigned DEFAULT NULL COMMENT '数据集目录父目录',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dataset_project
-- ----------------------------
DROP TABLE IF EXISTS `dataset_project`;
CREATE TABLE `dataset_project` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) unsigned DEFAULT NULL COMMENT '项目id',
  `dataset_id` bigint(20) unsigned NOT NULL COMMENT '数据集id',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0' COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_dataset_project` (`dataset_id`,`project_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据集项目关联表';

-- ----------------------------
-- Table structure for external_data_source
-- ----------------------------
DROP TABLE IF EXISTS `external_data_source`;
CREATE TABLE `external_data_source` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) NOT NULL COMMENT '外部数据名（统一风格：英文字母全部小写）',
  `content` varchar(1047) DEFAULT NULL COMMENT '说明',
  `type` varchar(255) DEFAULT NULL COMMENT '所属类别',
  `logo` text DEFAULT NULL COMMENT 'logo图片',
  `status` char(2) DEFAULT '1' COMMENT '状态值：1：可用，0：不可用',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `gmt_modifier` bigint(8) DEFAULT NULL COMMENT '创建者',
  `gmt_creator` bigint(8) DEFAULT NULL COMMENT '修改者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='外部数据源信息表';

-- ----------------------------
-- Table structure for folder
-- ----------------------------
DROP TABLE IF EXISTS `folder`;
CREATE TABLE `folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `model_info` varchar(255) DEFAULT NULL,
  `unfold` int(11) NOT NULL DEFAULT '0',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_creator` bigint(20) DEFAULT NULL,
  `gmt_modifier` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for formula_history
-- ----------------------------
DROP TABLE IF EXISTS `formula_history`;
CREATE TABLE `formula_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) NOT NULL COMMENT '实际表名',
  `name` varchar(255) NOT NULL COMMENT '展示的表名',
  `formula` varchar(255) DEFAULT NULL COMMENT '原始公式',
  `result` varchar(255) DEFAULT NULL COMMENT '公式的结果',
  `project_id` bigint(20) NOT NULL COMMENT '项目id',
  `type` varchar(255) DEFAULT NULL,
  `table_name` varchar(255) NOT NULL,
  `precision_num` tinyint(10) DEFAULT '1' COMMENT '数字类型结果精度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for graph
-- ----------------------------
DROP TABLE IF EXISTS `graph`;
CREATE TABLE `graph` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `project_id` bigint(20) unsigned NOT NULL,
  `pipeline_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `task_id` bigint(20) unsigned DEFAULT NULL,
  `data_json` longtext CHARACTER SET utf8mb4,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) NOT NULL,
  `gmt_creator` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pipeline_id` (`pipeline_id`),
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for graph_action
-- ----------------------------
DROP TABLE IF EXISTS `graph_action`;
CREATE TABLE `graph_action` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action_type` varchar(100) NOT NULL DEFAULT '' COMMENT '操作类型',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `pipeline_id` bigint(20) unsigned DEFAULT NULL COMMENT 'pipelineId',
  `task_id` bigint(20) unsigned DEFAULT NULL COMMENT '任务节点id',
  `graph_id` bigint(20) unsigned NOT NULL COMMENT '图构建id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `context` mediumtext CHARACTER SET utf8mb4 COMMENT '上下文内容json格式',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_pipeline` (`pipeline_id`) USING BTREE,
  KEY `idx_project` (`project_id`) USING BTREE,
  KEY `idx_graph` (`graph_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for graph_filter
-- ----------------------------
DROP TABLE IF EXISTS `graph_filter`;
CREATE TABLE `graph_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '过滤器id',
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '过滤器名称',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `graph_id` bigint(20) unsigned NOT NULL COMMENT '图视图id',
  `graph_filter_pipeline_id` bigint(20) unsigned NOT NULL COMMENT '过滤器流程图id',
  `type` tinyint(4) unsigned NOT NULL,
  `sub_type` tinyint(4) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL COMMENT '父过滤器id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `data_json` longtext CHARACTER SET utf8mb4,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) NOT NULL,
  `gmt_creator` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `graph_id` (`graph_id`) USING BTREE,
  KEY `graph_filter_pipeline_id` (`graph_filter_pipeline_id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图分析过滤器表';

-- ----------------------------
-- Table structure for graph_filter_pipeline
-- ----------------------------
DROP TABLE IF EXISTS `graph_filter_pipeline`;
CREATE TABLE `graph_filter_pipeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '过滤器流程id',
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '过滤器流程名称',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `graph_id` bigint(20) unsigned NOT NULL COMMENT '图视图id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `data_json` longtext CHARACTER SET utf8mb4,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) NOT NULL,
  `gmt_creator` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `graph_id` (`graph_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图分析过滤器流程图表';

-- ----------------------------
-- Table structure for graph_filter_pipeline_instance
-- ----------------------------
DROP TABLE IF EXISTS `graph_filter_pipeline_instance`;
CREATE TABLE `graph_filter_pipeline_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '过滤器流程id',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `graph_id` bigint(20) unsigned NOT NULL COMMENT '图视图id',
  `graph_filter_pipeline_id` bigint(20) unsigned NOT NULL COMMENT '过滤器流程图id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `data_json` longtext CHARACTER SET utf8mb4,
  `status` char(10) NOT NULL DEFAULT 'CREATE' COMMENT '任务状态',
  `during_time` bigint(20) DEFAULT '0' COMMENT '开始执行到结束耗时',
  `log_info` text CHARACTER SET utf8mb4 COMMENT '日志',
  `run_times` int(10) DEFAULT '0' COMMENT '执行次数',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_running` datetime DEFAULT NULL COMMENT '开始执行时间',
  `gmt_modifier` bigint(20) NOT NULL,
  `gmt_creator` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `graph_id` (`graph_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图分析过滤器流程运行实例';

-- ----------------------------
-- Table structure for graph_instance
-- ----------------------------
DROP TABLE IF EXISTS `graph_instance`;
CREATE TABLE `graph_instance` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `graph_id` bigint(20) unsigned NOT NULL COMMENT '图id',
  `task_id` bigint(20) unsigned DEFAULT NULL COMMENT '任务id',
  `pipeline_id` bigint(20) unsigned DEFAULT NULL COMMENT 'pipelineId',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `data_json` text CHARACTER SET utf8mb4 COMMENT 'json结果',
  `status` char(10) NOT NULL DEFAULT 'CREATE' COMMENT '任务状态',
  `during_time` bigint(20) DEFAULT '0' COMMENT '开始执行到结束耗时',
  `log_info` text CHARACTER SET utf8mb4 COMMENT '日志',
  `run_times` int(10) DEFAULT '0' COMMENT '执行次数',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_running` datetime DEFAULT NULL COMMENT '开始执行时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline` (`pipeline_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jlab
-- ----------------------------
DROP TABLE IF EXISTS `jlab`;
CREATE TABLE `jlab` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `port` bigint(20) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_name_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for model
-- ----------------------------
DROP TABLE IF EXISTS `model`;
CREATE TABLE `model` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `algorithm` varchar(100) DEFAULT NULL COMMENT 'algorithm name',
  `model_type` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT 'user define name',
  `model_desc` text COMMENT 'model description',
  `param` text,
  `source_table` varchar(100) DEFAULT NULL,
  `source_id` bigint(20) DEFAULT NULL,
  `source_name` varchar(255) DEFAULT NULL,
  `model_saved_path` text,
  `other_info` longtext COMMENT 'metrics & result',
  `status` varchar(100) DEFAULT NULL,
  `in_panel` bigint(1) unsigned zerofill DEFAULT '0' COMMENT '1 for models in panel(not in folder); 2 for models in folder',
  `folder_id` bigint(20) DEFAULT NULL,
  `algotime` float DEFAULT NULL,
  `sparktime` float DEFAULT NULL,
  `runtime` float DEFAULT NULL COMMENT 'algo runtime',
  `num` bigint(20) DEFAULT NULL COMMENT 'data source num row',
  `progress_id` varchar(100) DEFAULT NULL COMMENT 'PID/applicaiton ID to kill',
  `gmt_creator` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) DEFAULT NULL,
  `gmt_modify` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invisible` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for model in panel but not in the list',
  `train_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for model_instance
-- ----------------------------
DROP TABLE IF EXISTS `model_instance`;
CREATE TABLE `model_instance` (
  `id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `algorithm` varchar(100) DEFAULT NULL COMMENT 'algorithm name',
  `model_desc` text COMMENT 'model description',
  `source_table` varchar(100) DEFAULT NULL,
  `executor_cores` bigint(255) DEFAULT NULL,
  `executor_mem` varchar(255) DEFAULT NULL,
  `driver_mem` varchar(255) DEFAULT NULL,
  `num` bigint(20) DEFAULT NULL,
  `algotime` float DEFAULT NULL,
  `sparktime` float DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT 'user define name',
  `model_saved_path` text,
  `model_type` varchar(100) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `other_info` text,
  `param` text,
  `status` varchar(100) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) DEFAULT NULL,
  `gmt_creator` bigint(20) DEFAULT NULL,
  `progress_id` varchar(100) DEFAULT NULL,
  `runtime` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for model_local
-- ----------------------------
DROP TABLE IF EXISTS `model_local`;
CREATE TABLE `model_local` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  `workplace` varchar(100) DEFAULT NULL,
  `logo` int(11) DEFAULT '0',
  `open` int(11) DEFAULT '1',
  `public_data` int(11) DEFAULT '1',
  `field` text,
  `summary` text,
  `keyword` text,
  `model_desc` text,
  `star` bigint(20) DEFAULT '0',
  `watch` bigint(20) DEFAULT '0',
  `watch_stat` text,
  `download` bigint(20) DEFAULT '0',
  `download_stat` text,
  `version` varchar(100) DEFAULT NULL,
  `top_version` tinyint(4) DEFAULT '1',
  `param` text,
  `metric` text,
  `config` text,
  `code_path` varchar(100) DEFAULT NULL,
  `source_table` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `comment` text,
  `resource` text,
  `reference` text,
  `cite` text,
  `instruction` text,
  `trainable` int(11) DEFAULT NULL,
  `other_info` longtext,
  `source_id` bigint(20) DEFAULT NULL,
  `source_name` varchar(100) DEFAULT NULL,
  `model_saved_path` text,
  `in_panel` int(1) unsigned DEFAULT '0',
  `folder_id` bigint(20) DEFAULT NULL,
  `algo_time` float DEFAULT NULL,
  `run_time` float DEFAULT NULL,
  `num` bigint(20) DEFAULT NULL,
  `progress_id` varchar(100) DEFAULT NULL,
  `invisible` tinyint(1) DEFAULT '0',
  `train_time` datetime DEFAULT NULL,
  `gmt_creator` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modifier` bigint(20) DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_local_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for model_online
-- ----------------------------
DROP TABLE IF EXISTS `model_online`;
CREATE TABLE `model_online` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` bigint(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  `workplace` varchar(100) DEFAULT NULL,
  `logo` int(11) DEFAULT '0',
  `open` int(11) DEFAULT '1',
  `public_data` int(11) DEFAULT '1',
  `field` text,
  `summary` text,
  `keyword` text,
  `model_desc` text,
  `star` int(11) DEFAULT '0',
  `watch` bigint(20) DEFAULT '0',
  `watch_stat` text,
  `download` bigint(20) DEFAULT '0',
  `download_stat` text,
  `version` varchar(100) DEFAULT NULL,
  `top_version` tinyint(4) DEFAULT '1',
  `param` text,
  `metric` text,
  `config` text,
  `code_path` varchar(100) DEFAULT NULL,
  `source_table` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `comment` text,
  `resource` text,
  `reference` text,
  `cite` text,
  `instruction` text,
  `gmt_creator` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) DEFAULT NULL,
  `gmt_modify` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_online_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'notice id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT 'notice标题',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '消息所属用户id',
  `content` text NOT NULL COMMENT '通知内容',
  `process` text COMMENT '处理内容',
  `type` tinyint(4) unsigned NOT NULL COMMENT '消息类型：1.项目角色添加 2.普通通知',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态-0.未读 1.已读',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通知消息表';

-- ----------------------------
-- Table structure for operator_favourite
-- ----------------------------
DROP TABLE IF EXISTS `operator_favourite`;
CREATE TABLE `operator_favourite` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_type` int(11) NOT NULL COMMENT '任务类型',
  `alg_type` int(11) NOT NULL COMMENT '算法类型',
  `alg_name` varchar(20) NOT NULL DEFAULT '',
  `starred` tinyint(1) DEFAULT '0' COMMENT '是否收藏',
  `is_edited` tinyint(1) DEFAULT '0' COMMENT '是否可编辑',
  `desc_url` varchar(200) DEFAULT '' COMMENT '帮助链接',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_creator_task_alg` (`gmt_creator`,`task_type`,`alg_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='算子收藏表';

-- ----------------------------
-- Table structure for operator_template
-- ----------------------------
DROP TABLE IF EXISTS `operator_template`;
CREATE TABLE `operator_template` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '所属用户id',
  `type` tinyint(4) unsigned NOT NULL DEFAULT '4' COMMENT '类型',
  `sub_type` tinyint(4) unsigned NOT NULL DEFAULT '7' COMMENT '子分类类型',
  `sub_type_name` varchar(100) NOT NULL DEFAULT '自定义算子' COMMENT '子类型名',
  `description` text COMMENT '描述信息',
  `function_name` varchar(50) NOT NULL DEFAULT '' COMMENT '函数名',
  `input_params` text COMMENT '输入参数模板',
  `output_params` text COMMENT '输出参数模板',
  `language_type` varchar(10) DEFAULT 'python' COMMENT '语言',
  `script_body` text COMMENT '脚本主体',
  `sdk` varchar(255) DEFAULT '' COMMENT 'sdk地址',
  `status` tinyint(4) DEFAULT '0' COMMENT '状态-0.待审核 1.已通过 2.已驳回 3.已失效',
  `suggestion` varchar(1023) DEFAULT NULL COMMENT '审核意见',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  `notebook_id` varchar(20) DEFAULT '' COMMENT '脚本唯一id,zeppelin提供',
  `interpreter` varchar(20) DEFAULT 'spark' COMMENT '脚本执行引擎,可选spark|madlib',
  `full_script` text COMMENT '组装成完整可执行脚本',
  `is_export` tinyint(4) DEFAULT '0' COMMENT '是否导出为自定义算子',
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='算子模板表';

-- ----------------------------
-- Table structure for pipeline
-- ----------------------------
DROP TABLE IF EXISTS `pipeline`;
CREATE TABLE `pipeline` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `data_json` text COMMENT 'json配置',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_project` (`project_id`),
  KEY `idx_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pipeline_instance
-- ----------------------------
DROP TABLE IF EXISTS `pipeline_instance`;
CREATE TABLE `pipeline_instance` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipelineId',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `status` char(10) NOT NULL DEFAULT 'create' COMMENT 'pipeline状态',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `during_time` bigint(20) DEFAULT '0' COMMENT '开始执行到结束耗时',
  `gmt_running` datetime DEFAULT NULL COMMENT '开始执行时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pipeline_snapshot
-- ----------------------------
DROP TABLE IF EXISTS `pipeline_snapshot`;
CREATE TABLE `pipeline_snapshot` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'pipeline快照id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'pipeline快照名称',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipelineId',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `picture_path` varchar(255) DEFAULT NULL COMMENT '截图路径',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline` (`pipeline_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='pipeline快照表';

-- ----------------------------
-- Table structure for plugin
-- ----------------------------
DROP TABLE IF EXISTS `plugin`;
CREATE TABLE `plugin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `token` varchar(50) NOT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0-off  1-on',
  `url` varchar(50) DEFAULT '',
  `version` varchar(50) DEFAULT '',
  `icon_encode` mediumblob COMMENT '图标编码',
  `data_json` text,
  `description` varchar(50) DEFAULT '',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0' COMMENT '初始创建者',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='插件';

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '项目名',
  `description` varchar(100) NOT NULL DEFAULT '' COMMENT '描述',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0' COMMENT '初始创建者',
  `lock` tinyint(4) NOT NULL DEFAULT '0' COMMENT '锁定标记：0：正常，1：锁定',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '项目类型：0：普通项目， 1：示范项目',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_name_creator` (`gmt_creator`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目表';

-- ----------------------------
-- Table structure for rc_audit
-- ----------------------------
DROP TABLE IF EXISTS `rc_audit`;
CREATE TABLE `rc_audit` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `request_time` datetime DEFAULT NULL COMMENT '请求时间',
  `url` varchar(511) DEFAULT NULL COMMENT 'URL',
  `params` varchar(2047) DEFAULT NULL COMMENT '请求参数',
  `request_type` varchar(255) DEFAULT NULL COMMENT '请求类型',
  `request_data` varchar(511) DEFAULT NULL COMMENT '请求数据集/表名/视图名，可用逗号分隔',
  `username` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `ip` varchar(255) DEFAULT NULL COMMENT '请求ip地址',
  `env_info` text COMMENT '环境信息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录时间',
  PRIMARY KEY (`id`),
  KEY `request_time` (`request_time`),
  KEY `uid` (`username`),
  KEY `request_time_2` (`request_time`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for recommend
-- ----------------------------
DROP TABLE IF EXISTS `recommend`;
CREATE TABLE `recommend` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '所属用户id',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '所属类别id',
  `data_json` text COMMENT '数据集元信息',
  `table_names` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`),
  KEY `idx_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推荐数据表meta信息';

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '角色名',
  `description` text COMMENT '描述',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) DEFAULT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) DEFAULT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sql_category
-- ----------------------------
DROP TABLE IF EXISTS `sql_category`;
CREATE TABLE `sql_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `row_sql` varchar(200) NOT NULL DEFAULT '' COMMENT '原始SQL语句',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipelineId',
  `parent_id` varchar(200) NOT NULL DEFAULT '' COMMENT '父id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(4) unsigned NOT NULL COMMENT '类型',
  `data_json` text COMMENT 'json配置',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  `child_id` varchar(200) NOT NULL DEFAULT '' COMMENT '子id',
  `operator_id` bigint(20) DEFAULT '0' COMMENT 'operatorTemplate 表对应id',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline` (`pipeline_id`),
  KEY `idx_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for task_instance
-- ----------------------------
DROP TABLE IF EXISTS `task_instance`;
CREATE TABLE `task_instance` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(200) NOT NULL DEFAULT '' COMMENT '父id',
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipelineId',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `status` char(10) NOT NULL DEFAULT 'create' COMMENT '任务状态',
  `data_json` text COMMENT 'json结果',
  `sql_text` text COMMENT '可执行sql语句',
  `output` text COMMENT '执行结果输出',
  `session_id` bigint(20) DEFAULT NULL COMMENT '执行sessionid',
  `log_info` text COMMENT '日志',
  `run_times` int(10) DEFAULT '0' COMMENT '执行次数',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_running` datetime DEFAULT NULL COMMENT '开始执行时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  `task_name` varchar(100) NOT NULL DEFAULT '',
  `type` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT 'task类型',
  `progress` tinyint(3) unsigned DEFAULT '0' COMMENT '任务进度',
  `application_id` varchar(100) DEFAULT NULL COMMENT 'spark的任务id',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline` (`pipeline_id`),
  KEY `idx_task` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for task_instance_snapshot
-- ----------------------------
DROP TABLE IF EXISTS `task_instance_snapshot`;
CREATE TABLE `task_instance_snapshot` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'task instance快照id',
  `pipeline_snapshot_id` bigint(20) unsigned NOT NULL COMMENT 'pipeline快照id',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipeline id',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `task_instance_json` text COMMENT 'task instance信息以json字符串保存',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline_snapshot` (`pipeline_snapshot_id`),
  KEY `idx_pipeline` (`pipeline_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='task instance快照表';

-- ----------------------------
-- Table structure for task_snapshot
-- ----------------------------
DROP TABLE IF EXISTS `task_snapshot`;
CREATE TABLE `task_snapshot` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'task快照id',
  `pipeline_snapshot_id` bigint(20) unsigned NOT NULL COMMENT 'pipeline快照id',
  `pipeline_id` bigint(20) unsigned NOT NULL COMMENT 'pipeline id',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `task_json` text COMMENT 'task信息以json字符串保存',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  PRIMARY KEY (`id`),
  KEY `idx_pipeline_snapshot` (`pipeline_snapshot_id`),
  KEY `idx_pipeline` (`pipeline_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='task快照表';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(50) NOT NULL COMMENT '登录用户名',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '用户email',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户状态:1=正常;',
  `role` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户角色：0-普通用户，1-管理员',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_creator` bigint(20) unsigned zerofill NOT NULL DEFAULT '00000000000000000000' COMMENT '初始创建者',
  `gmt_modifier` bigint(20) unsigned zerofill NOT NULL DEFAULT '00000000000000000000' COMMENT '最后修改者',
  `password` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `nick_name` varchar(50) DEFAULT '' COMMENT '昵称',
  `real_name` varchar(50) DEFAULT '' COMMENT '真实姓名',
  `date_birth` date DEFAULT '2000-01-01' COMMENT '出生日期',
  `gender` varchar(4) DEFAULT '' COMMENT '性别',
  `profession` varchar(100) DEFAULT '' COMMENT '职业',
  `work_org` varchar(100) DEFAULT '' COMMENT '工作单位',
  `address` varchar(200) DEFAULT '' COMMENT '通信地址',
  `research_field` varchar(100) DEFAULT '' COMMENT '研究领域',
  `pic_encode` mediumblob COMMENT '用户头像编码',
  `guide_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户引导状态：0-未引导；1-已引导',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_config
-- ----------------------------
DROP TABLE IF EXISTS `user_config`;
CREATE TABLE `user_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `notice_receive_level` tinyint(4) NOT NULL DEFAULT '1' COMMENT '消息接收级别，1：所有消息，2：与自己相关，3：屏蔽消息',
  `node_deletion_secondary_confirmation` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据视图中删除节点二次确认：0：关闭，1：开启',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0',
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_feedbacks
-- ----------------------------
DROP TABLE IF EXISTS `user_feedbacks`;
CREATE TABLE `user_feedbacks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT ' 主键id',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=未回复，1=已回复',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0' COMMENT '初始创建者',
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改者',
  `feedback` text COMMENT '用户反馈问题描述',
  `feedback_pic` mediumblob COMMENT '相关图片编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for user_project
-- ----------------------------
DROP TABLE IF EXISTS `user_project`;
CREATE TABLE `user_project` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户',
  `project_id` bigint(20) unsigned NOT NULL COMMENT '项目id',
  `role_id` int(20) DEFAULT NULL COMMENT '角色id',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0' COMMENT '初始创建者',
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改者',
  `auth` tinyint(4) DEFAULT '1' COMMENT '读写权限：1-读，2-读写，3-管理员，有项目权限管理的权限',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_user_project` (`user_id`,`project_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户、项目权限表';

-- ----------------------------
-- Table structure for widget
-- ----------------------------
DROP TABLE IF EXISTS `widget`;
CREATE TABLE `widget` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tid` bigint(20) unsigned DEFAULT NULL COMMENT 'tid',
  `project_id` bigint(20) DEFAULT NULL COMMENT '项目Id',
  `type` varchar(20) NOT NULL COMMENT '类型:dataset,task',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `data_json` mediumtext CHARACTER SET utf8mb4 COMMENT '配置',
  `publish_name` varchar(100) DEFAULT NULL COMMENT '发布名称',
  `publish_data_json` mediumtext COMMENT '发布配置',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间',
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `gmt_modifier` bigint(20) NOT NULL COMMENT '最后修改者',
  `gmt_creator` bigint(20) NOT NULL COMMENT '初始创建者',
  `dashboard_id` bigint(20) DEFAULT '0' COMMENT '画布Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for widget_favourite
-- ----------------------------
DROP TABLE IF EXISTS `widget_favourite`;
CREATE TABLE `widget_favourite` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pipeline_id` bigint(20) unsigned NOT NULL,
  `widget_id` bigint(20) unsigned NOT NULL,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modify` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_modifier` bigint(20) NOT NULL DEFAULT '0',
  `gmt_creator` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_idx_widget_pipeline_creator` (`pipeline_id`,`gmt_creator`,`widget_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='可视化视图收藏表';

-- ----------------------------
-- Procedure structure for add_column
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_column`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_column`()
BEGIN
SET @table_name = "dataset";
SET @exeSql = CONCAT('ALTER TABLE ' , @table_name , " ADD COLUMN gmt_create datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '初始创建时间';");

PREPARE stmt FROM @exeSql;
EXECUTE stmt;

DEALLOCATE PREPARE stmt;


SET @exeSql = CONCAT('ALTER TABLE ' , @table_name , " ADD COLUMN gmt_modify datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间';");

PREPARE stmt FROM @exeSql;
EXECUTE stmt;

DEALLOCATE PREPARE stmt;

SET @exeSql = CONCAT('ALTER TABLE ' , @table_name , " ADD COLUMN  gmt_modifier BIGINT NOT NULL DEFAULT 0 COMMENT '最后修改者';");

PREPARE stmt FROM @exeSql;
EXECUTE stmt;

DEALLOCATE PREPARE stmt;
SET @exeSql = CONCAT('ALTER TABLE ' , @table_name , " ADD COLUMN  gmt_creator BIGINT NOT NULL DEFAULT 0 COMMENT '初始创建者';"
);

PREPARE stmt FROM @exeSql;
EXECUTE stmt;

DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;
