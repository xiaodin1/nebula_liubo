### requirements

1. jdk 8
2. docker engine (linux/amd64)

### usage

1. clone project
    * ```shell
      git clone --single-branch --branch opensource git@git.zjvis.org:bigdata/aiworks.git \
          && cd aiworks
      ```
2. create kubernetes environment
    * ```shell
      export KIND_BASE_URL=https://resource.static.zjvis.net/binary/kind \
          && export KUBECTL_BASE_URL=https://resource.static.zjvis.net/binary/kubectl \
          && export HELM_BASE_URL=https://resource.static.zjvis.net/binary/helm \
          && ./gradlew :createKubernetesEnv
      ```
3. install dependencies
    * ```shell
      export PUBLISH_ENV=local && ./gradlew :createAllDependencies
      ```
4. upload resources to minio
    * TODO automatically upload resources to minio
    * create forward tunnel for minio service
        + ```shell
          ./build/runtime/bin/kubectl port-forward --namespace middleware --address 0.0.0.0 svc/my-minio 9001:9001
          ```
    * visit `http://$YOUR_HOST_IP_ADDRESS:9001`
        + extract `access-key`
            * ```shell
              ./build/runtime/bin/kubectl get secret --namespace middleware my-minio -o jsonpath="{.data.access-key}" \
                  | base64 --decode && echo
              ```
        + extract `secret-key`
            * ```shell
              ./build/runtime/bin/kubectl get secret --namespace middleware my-minio -o jsonpath="{.data.secret-key}" \
                  | base64 --decode && echo
              ```
    * download resources
        + ```shell
          curl -o /tmp/GeoLite2-City.mmdb -L https://resource.static.zjvis.net/aiworks/minio/GeoLite2-City.mmdb
          curl -o /tmp/autojoin -L https://resource.static.zjvis.net/aiworks/minio/autojoin
          curl -o /tmp/mapping -L https://resource.static.zjvis.net/aiworks/minio/mapping
          ```
    * create buckets and upload resources
        + upload `/tmp/autojoin` and `/tmp/mapping` to the bucket named `fasttext-model`
        + upload `/tmp/GeoLite2-City.mmdb` to the bucket named `vis-platform`
        + create empty bucket named `graph-file`
5. deploy backend
    * ```shell
      export PUBLISH_ENV=local \
          && export DOCKER_REGISTRY=localhost:5000 \
          && export PULL_POLICY=Always \
          && export INITIALIZE=true \
          && ./gradlew :backendDeploy
      ```
    * upgrade
        + ```shell
          export PUBLISH_ENV=local \
              && export DOCKER_REGISTRY=localhost:5000 \
              && export PULL_POLICY=Always \
              && export INITIALIZE=false \
              && ./gradlew :backendUpgrade
          ```
6. deploy frontend
    * ```shell
      export PUBLISH_ENV=local \
          && export DOCKER_REGISTRY=localhost:5000 \
          && ./gradlew :frontendDeploy
      ./gradlew :frontendPortForward
      ```
    * upgrade
        + ```shell
          export PUBLISH_ENV=local \
              && export DOCKER_REGISTRY=localhost:5000 \
              && export FRONTEND_SOURCE_HOME=/root/git_projects/nebula \
              && ./gradlew :frontendUpgrade
          ```
7. destroy cluster
    * ```shell
      ./gradlew :deleteKubernetesEnv
      ```