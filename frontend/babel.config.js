
const isProduction = process.env.NODE_ENV === 'production'
const plugins = []
if (isProduction) {
  plugins.push(['transform-remove-console', { 'exclude': ['error', 'warn'] }])
}

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: plugins
}
