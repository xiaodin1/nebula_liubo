/**
 * graph analysis api
 * @author zhangjing
 */

import axios from '@/api/http'

// 查询所有标签页
export const queryGraphByProjectId = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryGraphByProjectId',
  })

// 新建标签
export const addGraph = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/addGraph',
  })

// 删除标签页
export const deleteGraphById = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/deleteGraphById',
  })

// 查询视图数据
export const queryGraphByTabId = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryById',
  })

// 重命名标签
export const renameTab = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/renameTab',
  })

// 加载图数据
export const loadData = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/loadData',
  })

// 加载轮询
export const loadDqueryLoadStatusata = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryLoadStatus',
  })

// 新建过滤流程
export const addFilterPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/addFilterPipeline',
  })

// 删除过滤节点
export const deleteFilterPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/deleteFilterPipeline',
  })

// 更新过滤节点
export const updateFilterPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/updateFilterPipeline',
  })

// 查询过滤流程
export const queryFilterPipelineByGraphId = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryFilterPipelineByGraphId',
  })

// 新建过滤器
export const addFilter = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/addFilter',
  })

// 删除过滤器
export const deleteFilter = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/deleteFilter',
  })

// 更新过滤器
export const updateFilter = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/updateFilter',
  })

// 查询过滤器
export const queryFilterByFilterPipelineId = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryFilterByFilterPipelineId',
  })

// 查询过滤器可用属性
export const queryFilterAvailableAttributes = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryFilterAvailableAttrs',
  })

// 运行过滤器
export const executeFilterPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/executeFilterPipeline',
  })

// 重置过滤器
export const resetFilterPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/resetFilterPipeline',
  })

// 加载图文件列表
export const loadListFiles = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/listFiles',
  })

// 上传图文件
export const uploadFile = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/uploadFile',
  })

// 样式 / 布局: 可用属性查询
export const queryAvailableAttributes = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryAvailableAttrs',
  })

// 根据属性着色， 或根据属性值排序设置
export const queryAttributeStyle = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryAttrStyle',
  })

// 查询图数据 节点和边的分类
export const queryAvailableCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryAvailableCategory',
  })

// 数据统计: 统计指标
export const queryMetrics = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryMetrics',
  })

// 执行统计计算
export const executeMetrics = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/executeMetrics',
  })

// footer 图数据详情
export const queryGraphDataDetail = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryDataDetail',
  })

// 统计运行 pageRank算法 queryPageRank
export const queryPageRank = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryPageRank',
  })

// 统计运行 社区发现算法 queryCluster
export const queryCluster = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryCluster',
  })

// 删除节点和边
export const deleteItems = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/batchDeleteNodesAndLinks',
  })

// 更新节点和边
export const updateItems = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/batchUpdate',
  })

// 最短路径
export const queryPath = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryPath',
  })

// 搜索
export const search = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/search',
  })

export const activateMetric = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/activateMetric',
  })

// 保存到可视化
export const saveWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/saveWidget',
  })

// 前进
export const redo = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/redo',
  })

// 后退
export const undo = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/undo',
  })

// 热区
export const queryHeatMap = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryHeatMap',
  })
