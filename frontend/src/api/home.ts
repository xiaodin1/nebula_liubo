/**
 * api
 * @author zhangfanfan
 */

import axios from '@/api/http'

export const queryAllProjects = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/query',
  })

export const copyProjectById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/copy',
  })

export const copyTemplateProjectById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/template/copy',
  })

export const deleteProjectById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/deleteById',
  })

export const changeStatus = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/changeStatus',
  })

export const createProject = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/create',
  })

export default {}
export const queryProjectById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/queryById',
  })

export const updateProjectInfoById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/update',
  })

// project role

export const queryRoleList = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/list',
  })

export const queryRoleByProjectId = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/queryByProjectId',
  })

export const addRole = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/add',
  })

export const deleteRoleById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/deleteById',
  })

export const updateProjectRole = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/update',
  })

export const userDatasetUsedCheck = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/userDatasetUsedCheck',
  })

// 废弃/projects/auth开头的接口
export const queryAuthByProjectId = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/auth/queryByProjectId',
  })

export const updateProjectAuth = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/auth/update',
  })

export const deleteAuthById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/auth/deleteById',
  })

export const createAuth = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/auth/create',
  })
