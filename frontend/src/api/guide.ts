/**
 * guide api 新用户引导
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * 关闭用户引导
 * @param options
 */
export const guideChange = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/guideChange',
  })

/**
 * 检查该用户引导状态
 * @param options
 */
export const guideCheck = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/guideCheck',
  })

export default {}
