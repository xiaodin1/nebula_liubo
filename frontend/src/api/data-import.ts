/* eslint-disable import/prefer-default-export */
import axios from '@/api/http'

/**
 * 验证json错误信息
 */
export const JsonValidate = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/jsonParsing',
  })

/** 创建http表格 */
export const HttpCreateTable = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/httpDataCreateTable',
  })
