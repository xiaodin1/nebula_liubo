import axios from '@/api/http'

/**
 * 保存dashboard信息
 */
export const saveDashboard = (options?: any) =>
  axios.request({
    ...options,
    url: '/dashboard/save',
  })

/**
 * 更新dashboard信息
 */
export const updateDashboard = (options?: any) =>
  axios.request({
    ...options,
    url: '/dashboard/update',
  })

export const getDashboard = (options?: any) =>
  axios.request({
    ...options,
    url: '/dashboard/queryById',
  })

export const getDashboardByProjectId = (options?: any) =>
  axios.request({
    ...options,
    url: '/dashboard/queryByProjectId',
  })

// 发布可视化
export const publishDashboard = (options?: any) =>
  axios.request({
    ...options,
    url: '/dashboard/publish',
  })
