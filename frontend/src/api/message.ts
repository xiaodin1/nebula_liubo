/**
 * notice api
 * @author Junjie Jin
 */
import axios from '@/api/http'

export const queryNotice = (options?: any) =>
  axios.request({
    ...options,
    url: '/notice/query',
  })

export const queryFilterType = (options?: any) =>
  axios.request({
    ...options,
    url: '/notice/queryFilterType',
  })

export const queryProjectList = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/queryProjectName',
  })

export const processNotice = (options?: any) =>
  axios.request({
    ...options,
    url: '/notice/process',
  })

export const deleteNotice = (options?: any) =>
  axios.request({
    ...options,
    url: '/notice/delete',
  })

export const processAllNotice = (options?: any) =>
  axios.request({
    ...options,
    url: '/notice/processAll',
  })

export const deleteAllNotice = (options?: any) =>
  axios.request({
    ...options,
    url: '/notice/deleteAll',
  })

export default {}
