/**
 * 项目算子 api
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * 所有算子列表  搜索算子
 * @param options
 */
export const allOperatorList = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/load_panel',
  })

/**
 * 收藏算子
 * @param options
 */
export const starredOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/favourite',
  })
