/**
 * 自定义算子 jupyter api
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * type === 9 点击编辑算子时，获取url
 * @param options
 */
export const queryJlabURL = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryJlabURL',
  })

/**
 * 用户点击关闭JLab网页窗口时，需记录csv文件到formData
 * @param options
 */
export const loadJlabls = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/loadJlabls',
  })
