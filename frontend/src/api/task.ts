/**
 * 节点相关 api
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * 数据预览
 * @param options
 */
export const infoData = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/query',
  })

/**
 * pipeline 节点列表
 * @param options
 */
export const allNodeList = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryByPipeline',
  })

/**
 * 添加 节点
 * @param options
 */
export const addNodeToPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/load',
  })

/**
 * 添加 节点 (new)
 * @param options
 */
export const taskComplexLoad = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/complexLoad',
  })

/**
 * 添加 节点 (new)
 * @param options
 */
export const taskAddUnionJoinNode = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/addUnionJoinNode',
  })

/**
 * 连线接口 (new)
 * @param options
 */
export const taskConnectLine = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/connectLine',
  })

/**
 *  复制 节点
 * @param options
 */
export const nodeCopy = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/copy',
  })
/**
 * 算子节点数据预览
 * @param options
 */
export const nodeDataPreview = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/preview',
  })

/**
 * 更新 节点
 * @param options
 */
export const updateNodeToPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/update',
  })

/**
 * simple 轻量 更新 节点 （节点位置 节点名称）
 * @param options
 */
export const simpleUpdate = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/simpleUpdate',
  })

/**
 * 删除节点
 * @param options
 */
export const deleteNodeById = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/deleteById',
  })

/**
 * 删除连线
 * @param options
 */
export const deleteLineById = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/deleteLine',
  })

/**
 * pipeline  节点执行状态查询
 * @param options
 */
export const pipelineInstanceStatus = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/queryStatus',
  })

/**
 * pipeline  单个节点的刷新
 * @param options
 */
export const taskQueryById = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryById',
  })

/**
 * 清空 所有节点
 * @param options
 */
export const clearProjectById = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/clearProject',
  })

/**
 * 批量禁复制 节点
 * @param options
 */
export const batchCopy = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/batchCopy',
  })

/**
 * 批量禁用 节点
 * @param options
 */
export const batchForbidden = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/batchForbidden',
  })

/**
 * 批量启用 节点
 * @param options
 */
export const batchUnForbidden = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/batchUnForbidden',
  })

/**
 * 批量删除 节点
 * @param options
 */
export const batchDelete = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/batchDelete',
  })

// auto join 结果概览
export const queryJoinResult = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryJoinResult',
  })

// join 编辑联接子句时根据已选列给出排序后的候选列
export const apiJoinRecommend = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/joinRecommend',
  })

// undo
export const undo = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/undo',
  })

// redo
export const redo = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/redo',
  })

/**
 * query linked table list
 * @param id //   task节点id
 */
export const queryInputTables = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryInputTables',
  })

// pipeline 经济算子节点可视化
export const taskQueryDataVis = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryDataVis',
  })

export default {}
