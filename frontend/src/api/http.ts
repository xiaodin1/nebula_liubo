/**
 * request base axios
 * @author zhangfanfan
 */

import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { message } from 'ant-design-vue'
import { baseURL } from '@/api/axios-config'

const publicPath: string = process.env.BASE_URL
const disableMessageErrorCode: Set<number> = new Set([416, 417]) // 不需要提示信息的errcode

message.config({
  top: '100px',
  duration: 2,
  maxCount: 1,
})

const instance = axios.create({
  baseURL,
  method: 'post',
  headers: {
    post: {
      'Content-Type': 'application/json;charset=utf-8',
    },
  },
})

// Add a request interceptor
instance.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // Do something before request is sent
    return config
  },
  (error) => {
    // Do something with request error
    message.error('请求超时:', error.toString())
    return Promise.reject(error)
  }
)

// Add a response interceptor
instance.interceptors.response.use(
  (response: AxiosResponse) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (disableMessageErrorCode.has(response.data.code)) {
      // 不需要提示信息的
      return response
    }
    switch (response.data.code) {
      case 100:
        return response
      case 20012: // 未登陆
        window.location.href = `${publicPath}${
          publicPath.endsWith('/') ? '' : '/'
        }login`
        return response
      case 401: // 无权限访问
        message.info('无权限访问')
        window.location.href = `${publicPath}${
          publicPath.endsWith('/') ? '' : '/'
        }management/projects`
        return response
      case undefined: // 下载
        return response
      case 416: // 屏蔽未读消息
        return response
      case 40013: // filewrite 数据类型转化失败 提示重复
        return response
      case 500: // 服务器异常
        message.error(response.data.message)
        return response
      default:
        return response
    }
  },
  (error) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.log('error:', error.response.data.code)
    console.log(error.response)
    message.error(error.response.data.message)

    // 未登录
    if (error.response.data.code === 301) {
      window.location.href = `${publicPath}${
        publicPath.endsWith('/') ? '' : '/'
      }login`
    }
    return Promise.reject(error)
  }
)

export default instance
