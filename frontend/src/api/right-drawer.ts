/**
 * api
 * @author futaotao
 */
import axios from '@/api/http'
import { hashWidgetQuery } from '@/util/chart-widget'

// 预览数据
export const infoData = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/query',
  })

// 预览数据
export const nodeDataPreview = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/preview',
  })

export const queryVisualById = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryByTaskId',
  })

// 查询可视化
export const widgetQueryById = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryById',
  })

// 添加收藏
export const addFavorite = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/favourite/create',
  })

// 取消收藏
export const deleteFavorite = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/favourite/delete',
  })

// 查看收藏的可视化
export const queryFavorites = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/favourite/queryByPipelineId',
  })

// 查看全局收藏的可视化
export const getGlobalWidgets = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/getGlobalWidgets',
  })

export const getWidgetData = (options?: any) =>
  axios.request({
    ...hashWidgetQuery(options),
    url: '/widget/getData',
  })

export const saveWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/save',
  })

export const queryDatasetById = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/queryDataById',
  })

export const deleteVisualById = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/deleteById',
  })

export const updateWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/update',
  })
