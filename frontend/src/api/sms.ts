import axios from '@/api/http'

export const getImageCaptchaCode = (options?: any) =>
  axios.request({
    ...options,
    url: '/sms/captcha',
  })

export const getPhoneCaptchaRegister = (options?: any) =>
  axios.request({
    ...options,
    url: '/sms/registerSendMsg',
  })

export const getPhoneCaptcha = (options?: any) =>
  axios.request({
    ...options,
    url: '/sms/modifySendMsg',
  })

export const verifyPhone = (options?: any) =>
  axios.request({
    ...options,
    url: '/sms/verify',
  })

export const phoneBind = (options?: any) =>
  axios.request({
    ...options,
    url: '/sms/phoneBind',
  })

export const phoneBindChange = (options?: any) =>
  axios.request({
    ...options,
    url: '/sms/phoneBindChange',
  })
