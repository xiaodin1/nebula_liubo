/**
 * api
 * @author zhangfanfan
 */

import axios from '@/api/http'
import { hashWidgetQuery } from '@/util/chart-widget'

/**
 * 项目数据列表
 * @param options
 */
export const queryDatalist = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/queryAllDataset',
  })

// 收藏可视化
export const favorite = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/favourite',
  })

export const getWidgetData = (options?: any) =>
  axios.request({
    ...hashWidgetQuery(options),
    url: '/widget/getData',
  })

export default {}
