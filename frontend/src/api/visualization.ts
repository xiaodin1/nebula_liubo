/**
 * visualization api
 * @author zhangfanfan
 */

import axios from '@/api/http'

export const queryVisualizationData = (options?: any) =>
  axios.request({
    ...options,
    url: '/visualization/query',
  })

export const tcolumnQueryValuesByPage = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryValuesByPage',
  })

export const tcolumnQueryMinMax = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryMinMax',
  })

export const tcolumnQueryDetail = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryDetail',
  })

/**
 * 获取字段统计数据
 */
export const tcolumnQueryStat = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryStat',
  })

export const graphQueryDataTable = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/queryGraphByTaskId',
  })

export const graphAddCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/addCategory',
  })

export const graphBatchDeleteCategoriesAndEdges = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/batchDeleteCategoriesAndEdges',
  })

export const graphDeleteCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/deleteCategory',
  })

export const graphAddEdge = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/addEdge',
  })

// 更改 edge 边
export const graphUpdateEdge = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/updateEdge',
  })

export const graphDeleteEdge = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/deleteEdge',
  })

export const graphCategoryAttributesOp = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/categoryAttrOP',
  })

export const graphRemoveCategoryAttribute = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/removeCategoryAttr',
  })

export const graphAddCategoryAttribute = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/addCategoryAttr',
  })

export const graphUpdateCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/updateCategory',
  })

export const graphLoadGraphData = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/loadGraphData',
  })

export const graphQueryById = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/queryById',
  })

export const graphQueryFullInputById = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryFullInputById',
  })

// analysis 两点所有路径 (最短路径)
export const graphQueryPath = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/queryPath',
  })

// analysis 用于构建完首次保存样式信息
export const graphBatchUpdate = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/batchUpdate',
  })

// analysis 删除节点
export const graphDeleteNode = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/deleteNode',
  })

// analysis 批量删除节点和边
export const graphBatchDeleteNodesAndLinks = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/batchDeleteNodesAndLinks',
  })

// analysis 保存视图
export const graphSaveWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/saveWidget',
  })

// analysis 搜索
export const graphSearch = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/search',
  })

// analysis 社团检测
export const graphQueryCluster = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/queryCluster',
  })

// edge configure 边配置推荐
export const graphQueryEdgeConfigure = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/queryEdgeConfigure',
  })

// edge configure 边配置推荐
export const graphSaveEdgeConfigure = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/saveEdgeConfigure',
  })

// analysis 更新节点
export const graphUpdateNode = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/updateNode',
  })

//  点击返回，触发关闭图构建session
export const graphCloseSession = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/closeGraphSession',
  })

// 下方表 重命名， 修改类型， 筛选
export const graphAddAction = (options?: any) =>
  axios.request({
    ...options,
    url: '/graph/addAction',
  })

// 查询修改历史
export const graphQueryAction = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/queryAction',
  })
// 修改语义
export const graphUpdateSemantic = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/updateSemantic',
  })

export const graphUndo = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/undo',
  })

export const graphRedo = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/redo',
  })

export const graphUpdateAction = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/updateAction',
  })

export const graphDeleteAction = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/deleteAction',
  })

// 加载到图分析
export const loadToAnalysis = (options?: any) =>
  axios.request({
    ...options,
    url: 'graph/loadToAnalysis',
  })
