interface IKV {
  [key: string]: any
}

/* eslint-disable */

export function isObj(object: any) {
  return (
    object &&
    typeof object === 'object' &&
    Object.prototype.toString.call(object).toLowerCase() === '[object object]'
  )
}

export function isArray(object: any) {
  return object && typeof object === 'object' && object.constructor === Array
}

export function getLength(object: any) {
  let count = 0
  for (const i in object) {
    count++
  }
  return count
}

export function CompareObj(objA: any, objB: any, flag: boolean): boolean {
  for (const key in objA) {
    if (!flag) {
      // 跳出整个循环

      break
    }
    if (!Object.prototype.hasOwnProperty.call(objB, key)) {
      flag = false
      break
    }
    if (!isArray(objA[key])) {
      // 子级不是数组时,比较属性值
      if (objB[key] != objA[key]) {
        flag = false
        break
      }
    } else {
      if (!isArray(objB[key])) {
        flag = false
        break
      }
      const oA = objA[key]
      const oB = objB[key]
      if (oA.length != oB.length) {
        flag = false
        break
      }
      for (const k in oA) {
        if (!flag) {
          // 这里跳出循环是为了不让递归继续
          break
        }
        flag = CompareObj(oA[k], oB[k], flag)
      }
    }
  }
  return flag
}

export function Compare(objA: IKV, objB: IKV): boolean {
  if (!isObj(objA) || !isObj(objB)) {
    return false
  } // 判断类型是否正确
  if (getLength(objA) != getLength(objB)) {
    return false
  } // 判断长度是否一致
  return CompareObj(objA, objB, true) // 默认为true
}
