/**
 * @description 选择操作指令枚举，用于数据视图 > DataTable刷选单元格或点击行、列，触发预览、操作指令枚举。
 * 注意这里的action需要与推荐面板你的action保持一致
 */
enum OptionPreViewAction {
  /**
   * 行操作，常规面板 > 删除所选行
   */
  rowCommonDelete = 'rowCommonDelete',

  /**
   * 行操作, 常规面板 > 仅保留所选行
   */
  rowCommonKeep = 'rowCommonKeep',

  /**
   * 行操作，筛选面板 > 选出存在缺失值的行
   */
  rowFilterSelectNullRows = 'rowFilterSelectNullRows',

  /**
   * 行操作，筛选面板 > 选出存在缺失值的行 > 删除所选行
   */
  rowFilterSelectNullRowsDelete = 'rowFilterSelectNullRowsDelete',

  /**
   * 行操作，筛选面板 > 选出存在缺失值的行 > 仅保留所选行
   */
  rowFilterSelectNullRowsKeep = 'rowFilterSelectNullRowsKeep',

  /**
   * 列操作，筛选面板 > 选出该列存在缺失值的单元格
   */
  columnFilterSelectNullCells = 'columnFilterSelectNullCells',

  /**
   * 列操作，筛选面板 > 选出该列存在缺失值的单元格 > 为所选单元格重新赋值
   */
  columnFilterSelectNullCellsReset = 'columnFilterSelectNullCellsReset',

  /**
   * 列操作，筛选面板 > 选出该列存在缺失值的单元格 > 删除所选单元格存在的行
   */
  columnFilterSelectNullCellsDeleteRows = 'columnFilterSelectNullCellsDeleteRows',

  /**
   * 列操作，筛选面板 > 选出该列存在缺失值的单元格 > 仅保留所选单元格存在的行
   */
  columnFilterSelectNullCellsKeepRows = 'columnFilterSelectNullCellsKeepRows',

  /**
   * 列操作，筛选面板 > 选出值为xxx的单元格
   */
  columnFilterContains = 'columnFilterContains',

  /**
   * 列操作，筛选面板 > 选出值为xxx的单元格 > 所选单元格重新赋值
   */
  columnFilterContainsReset = 'columnFilterContainsReset',
  /**
   * 列操作，筛选面板 > 选出值为xxx的单元格 > 删除所选单元格所在的行
   */
  columnFilterContainsDeleteRows = 'columnFilterContainsDeleteRows',

  /**
   * 列操作，筛选面板 > 选出值为xxx的单元格 > 仅保留所选单元格所在的行
   */
  columnFilterContainsKeepRows = 'columnFilterContainsKeepRows',

  /**
   * 列操作，筛选面板 > 比较运算符
   */
  columnFilterCompare = 'columnFilterCompare',

  /**
   * 列操作，筛选面板 > 比较运算符 > 重置单元格
   */
  columnFilterCompareReset = 'columnFilterCompareReset',

  /**
   * 列操作，筛选面板 > 比较运算符 > 删除单元格所在的行
   */
  columnFilterCompareDeleteRows = 'columnFilterCompareDeleteRows',

  /**
   * 列操作，筛选面板 > 比较运算符 > 仅保留单元格所在的行
   */
  columnFilterCompareKeepRows = 'columnFilterCompareKeepRows',

  /**
   * 列操作，筛选面板 > 范围筛选
   */
  columnFilterRange = 'columnFilterRange',

  /**
   * 列操作，筛选面板 > 范围筛选 > 重置单元格
   */
  columnFilterRangeReset = 'columnFilterRangeReset',

  /**
   * 列操作，筛选面板 > 范围筛选 > 删除单元格所在的行
   */
  columnFilterRangeDeleteRows = 'columnFilterRangeDeleteRows',

  /**
   * 列操作，筛选面板 > 范围筛选 > 仅保留单元格所在的行
   */
  columnFilterRangeKeepRows = 'columnFilterRangeKeepRows',

  /**
   * 列操作，筛选面板 > 列合并
   */
  columnMergeColumns = 'columnMergeColumns',
  /**
   * 列操作，常用面板 > 删除当前选中的列
   */
  columnCommonDeleteSelectColumns = 'columnCommonDeleteSelectColumns',
  /**
   * 列操作，常用面板 > 删除未被选中的列
   */
  columnCommonDeleteOtherColumns = 'columnCommonDeleteOtherColumns',

  /**
   * 列操作, 常规面板 > 去除所选列中的重复行
   */
  columnCommonDeleteRepeatCells = 'columnCommonDeleteRepeatCells',

  /**
   * 单元格,筛选面板 > 选出该列存在缺失值的单元格
   */
  cellFilterSelectNullCells = 'cellFilterSelectNullCells',

  /**
   * 单元格 > 选出该列存在缺失值的单元格 > 为所选单元格重新赋值
   */
  cellFilterSelectNullCellsReset = 'cellFilterSelectNullCellsReset',

  /**
   * 单元格 > 选出该列存在缺失值的单元格 > 删除所选单元格存在的行
   */
  cellFilterSelectNullCellsDeleteRows = 'cellFilterSelectNullCellsDeleteRows',

  /**
   * 单元格 > 选出该列存在缺失值的单元格 > 仅保留所选单元格存在的行
   */
  cellFilterSelectNullCellsKeepRows = 'cellFilterSelectNullCellsKeepRows',

  /**
   * 单元格 > 选出与选中单元格值相等的单元格
   */
  cellFilterSelectContainsCell = 'cellFilterSelectContainsCell',

  /**
   * 单元格 > 选出与选中单元格值相等的单元格 > 为所选单元格重新赋值
   */
  cellFilterSelectContainsCellReset = 'cellFilterSelectContainsCellReset',

  /**
   * 单元格 > 选出与选中单元格值相等的单元格 > 删除所选单元格存在的行
   */
  cellFilterSelectContainsCellDeleteRows = 'cellFilterSelectContainsCellDeleteRows',

  /**
   * 单元格 > 选出与选中单元格值相等的单元格 > 仅保留所选单元格存在的行
   */
  cellFilterSelectContainsCellKeepRows = 'cellFilterSelectContainsCellKeepRows',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 选出大于某个数值的单元格
   */
  cellFilterSelectCompareCell = 'cellFilterSelectCompareCell',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 为所选单元格重新赋值
   */
  cellFilterSelectCompareCellReset = 'cellFilterSelectCompareCellReset',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 删除所选单元格存在的行
   */
  cellFilterSelectCompareCellDeleteRows = 'cellFilterSelectCompareCellDeleteRows',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 仅保留所选单元格存在的行
   */
  cellFilterSelectCompareCellKeepRows = 'cellFilterSelectCompareCellKeepRows',

  /**
   * 单元格 > 选出与选中单元格值相等的单元格
   */
  cellFilterSelectRangeCell = 'cellFilterSelectRangeCell',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 为所选单元格重新赋值
   */
  cellFilterSelectRangeCellReset = 'cellFilterSelectRangeCellReset',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 删除单元格所在的行
   */
  cellFilterSelectRangeCellDeleteRows = 'cellFilterSelectRangeCellDeleteRows',

  /**
   * 单元格 > 选出与选中所选范围的单元格 > 仅保留单元格所在的行
   */
  cellFilterSelectRangeCellKeepRows = 'cellFilterSelectRangeCellKeepRows',

  /**
   * 单元格 > 重新赋值 > 重新赋值为null
   */
  cellResetSelectCellReset = 'cellResetSelectCellReset',

  /**
   * 单元格 > 常规 > 删除所选单元格所在的行
   */
  cellCommonDeleteSelectRows = 'cellCommonDeleteSelectRows',

  /**
   * 单元格 > 常规 > 仅保留所选单元格所在的行
   */
  cellCommonKeepSelectRows = 'cellCommonKeepSelectRows',

  // ----------------------------------------通用刷选 start-------------------------------------------
  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格
   */
  selectionFilterSelectCellsByKeyword = 'selectionFilterSelectCellsByKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格 > 替换当前所选单元格中刷选内容
   */
  selectionFilterSelectCellsByKeywordReplaceKeyword = 'selectionFilterSelectCellsByKeywordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格 > 为所选单元格重新赋值
   */
  selectionFilterSelectCellsByKeywordReplaceContent = 'selectionFilterSelectCellsByKeywordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格 > 删除单元格所在的行
   */
  selectionFilterSelectCellsByKeywordDeleteRows = 'selectionFilterSelectCellsByKeywordDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格 > 仅保留单元格所在的行
   */
  selectionFilterSelectCellsByKeywordKeepRows = 'selectionFilterSelectCellsByKeywordKeepRows',

  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格 > 抽取所选内容单独形成一列
   */
  selectionFilterSelectCellsByKeywordExtractColumn = 'selectionFilterSelectCellsByKeywordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出包含关键字的单元格 > 根据所选内容将当前列拆分多列
   */
  selectionFilterSelectCellsByKeywordSplitColumns = 'selectionFilterSelectCellsByKeywordSplitColumns',

  // ----------------------------------------通用刷选 end-------------------------------------------

  // ----------------------------------------刷选内容为首部 start-------------------------------------------
  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格
   */
  selectionFilterSelectCellsStartWithKeyword = 'selectionFilterSelectCellsStartWithKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格 > 替换选中的内容
   */
  selectionFilterSelectCellsStartWithKeywordReplaceKeyword = 'selectionFilterSelectCellsStartWithKeywordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格 > 替换刷选单元格的内容
   */
  selectionFilterSelectCellsStartWithKeywordReplaceContent = 'selectionFilterSelectCellsStartWithKeywordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格 > 抽取选中内容为一列
   */
  selectionFilterSelectCellsStartWithKeywordExtractColumn = 'selectionFilterSelectCellsStartWithKeywordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格 > 根据刷选内容将列拆分为多列
   */
  selectionFilterSelectCellsStartWithKeywordSplitColumns = 'selectionFilterSelectCellsStartWithKeywordSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格 > 删除单元格所在的行
   */
  selectionFilterSelectCellsStartWithKeywordDeleteRows = 'selectionFilterSelectCellsStartWithKeywordDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字开头的单元格 > 仅保留单元格所在的行
   */
  selectionFilterSelectCellsStartWithKeywordKeepRows = 'selectionFilterSelectCellsStartWithKeywordKeepRows',

  // ----------------------------------------刷选内容为首部 end-------------------------------------------

  // ----------------------------------------刷选内容为尾部部 start-------------------------------------------
  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格
   */
  selectionFilterSelectCellsEndWithKeyword = 'selectionFilterSelectCellsEndWithKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格 > 替换选中的内容
   */
  selectionFilterSelectCellsEndWithKeywordReplaceKeyword = 'selectionFilterSelectCellsEndWithKeywordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格 > 替换刷选单元格的内容
   */
  selectionFilterSelectCellsEndWithKeywordReplaceContent = 'selectionFilterSelectCellsEndWithKeywordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格 > 抽取选中内容为一列
   */
  selectionFilterSelectCellsEndWithKeywordExtractColumn = 'selectionFilterSelectCellsEndWithKeywordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格 > 根据刷选内容将列拆分为多列
   */
  selectionFilterSelectCellsEndWithKeywordSplitColumns = 'selectionFilterSelectCellsEndWithKeywordSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格 > 删除所选单元格
   */
  selectionFilterSelectCellsEndWithKeywordDeleteRows = 'selectionFilterSelectCellsEndWithKeywordDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出以关键字结尾的单元格 > 仅保留所选单元格
   */
  selectionFilterSelectCellsEndWithKeywordKeepRows = 'selectionFilterSelectCellsEndWithKeywordKeepRows',

  /**
   * 单元格文字选中 > 筛选 > 选出关键字位置相同的单元格
   */
  selectionFilterSelectCellsKeywordSamePosition = 'selectionFilterSelectCellsKeywordSamePosition',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容
   */
  selectionFilterSelectCellsSamePosSelection = 'selectionFilterSelectCellsSamePosSelection',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容 > 替换刷选内容
   */
  selectionFilterSelectCellsSamePosSelectionReplaceKeyword = 'selectionFilterSelectCellsSamePosSelectionReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容 > 替换所在单元格的内容
   */
  selectionFilterSelectCellsSamePosSelectionReplaceContent = 'selectionFilterSelectCellsSamePosSelectionReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容 > 刷选内容抽取为单独一列
   */
  selectionFilterSelectCellsSamePosSelectionExtractColumn = 'selectionFilterSelectCellsSamePosSelectionExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容 > 根据刷选内容拆分为多列
   */
  selectionFilterSelectCellsSamePosSelectionSplitColumns = 'selectionFilterSelectCellsSamePosSelectionSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容 > 删除所选行
   */
  selectionFilterSelectCellsSamePosSelectionDeleteRows = 'selectionFilterSelectCellsSamePosSelectionDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容位置一致的内容 > 仅保留所选行
   */
  selectionFilterSelectCellsSamePosSelectionKeepRows = 'selectionFilterSelectCellsSamePosSelectionKeepRows',

  /**
   * 单元格文字选中 > 替换 > 替换刷选内容
   */
  selectionReplaceSelectionKeyword = 'selectionReplaceSelectionKeyword',
  /**
   * 单元格文字选中 > 重新赋值 > 刷选单元格重新赋值
   */
  selectionCellsReset = 'selectionCellsReset',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容
   */
  selectionFilterSelectCellsSamePosWord = 'selectionFilterSelectCellsSamePosWord',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容 > 替换刷选内容
   */
  selectionFilterSelectCellsSamePosWordReplaceKeyword = 'selectionFilterSelectCellsSamePosWordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容 > 重置单元格内容
   */
  selectionFilterSelectCellsSamePosWordReplaceContent = 'selectionFilterSelectCellsSamePosWordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容 > 抽取刷选内容为一列
   */
  selectionFilterSelectCellsSamePosWordExtractColumn = 'selectionFilterSelectCellsSamePosWordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容 > 根据刷选内容拆分为多列
   */
  selectionFilterSelectCellsSamePosWordSplitColumns = 'selectionFilterSelectCellsSamePosWordSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容 > 删除所选内容所在单元格的行
   */
  selectionFilterSelectCellsSamePosWordDeleteRows = 'selectionFilterSelectCellsSamePosWordDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出与刷选内容单词位置一致的内容 > 仅保留所选内容所在单元格的行
   */
  selectionFilterSelectCellsSamePosWordKeepRows = 'selectionFilterSelectCellsSamePosWordKeepRows',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格
   */
  selectionFilterSelectCellsStartWithSameWord = 'selectionFilterSelectCellsStartWithSameWord',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格 > 替换刷选内容
   */
  selectionFilterSelectCellsStartWithSameWordReplaceKeyword = 'selectionFilterSelectCellsStartWithSameWordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格 > 设置单元格内容
   */
  selectionFilterSelectCellsStartWithSameWordReplaceContent = 'selectionFilterSelectCellsStartWithSameWordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格 > 抽取刷选内容为一列
   */
  selectionFilterSelectCellsStartWithSameWordExtractColumn = 'selectionFilterSelectCellsStartWithSameWordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格 > 根据刷选内容拆分为多列
   */
  selectionFilterSelectCellsStartWithSameWordSplitColumns = 'selectionFilterSelectCellsStartWithSameWordSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格 > 删除所选内容所在单元格的行
   */
  selectionFilterSelectCellsStartWithSameWordDeleteRows = 'selectionFilterSelectCellsStartWithSameWordDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为word的单元格 > 保留所选内容所在单元格的行
   */
  selectionFilterSelectCellsStartWithSameWordKeepRows = 'selectionFilterSelectCellsStartWithSameWordKeepRows',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词
   */
  selectionFilterSelectCellsEndWithSameWord = 'selectionFilterSelectCellsEndWithSameWord',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词 > 替换关键字
   */
  selectionFilterSelectCellsEndWithSameWordReplaceKeyword = 'selectionFilterSelectCellsEndWithSameWordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词 > 单元格重新赋值
   */
  selectionFilterSelectCellsEndWithSameWordReplaceContent = 'selectionFilterSelectCellsEndWithSameWordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词 > 抽取刷选内容为一列
   */
  selectionFilterSelectCellsEndWithSameWordExtractColumn = 'selectionFilterSelectCellsEndWithSameWordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词 > 拆分成多列
   */
  selectionFilterSelectCellsEndWithSameWordSplitColumns = 'selectionFilterSelectCellsEndWithSameWordSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词 > 仅保留所选行
   */
  selectionFilterSelectCellsEndWithSameWordKeepRows = 'selectionFilterSelectCellsEndWithSameWordKeepRows',

  /**
   * 单元格文字选中 > 筛选 > 包含xxx，且前面有单词的单元格刷选出该单词 > 删除所选行
   */
  selectionFilterSelectCellsEndWithSameWordDeleteRows = 'selectionFilterSelectCellsEndWithSameWordDeleteRows',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWord = 'selectionFilterSelectCellsStartWithAndEndWithSameWord',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格 > 替换关键字
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceKeyword = 'selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceKeyword',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格 > 替换单元格
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceContent = 'selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceContent',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格 > 抽取出一列
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWordExtractColumn = 'selectionFilterSelectCellsStartWithAndEndWithSameWordExtractColumn',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格 > 拆分成多列
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWordSplitColumns = 'selectionFilterSelectCellsStartWithAndEndWithSameWordSplitColumns',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格 > 仅保留所选行
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWordKeepRows = 'selectionFilterSelectCellsStartWithAndEndWithSameWordKeepRows',

  /**
   * 单元格文字选中 > 筛选 > 选出前面为xxx且后面为yyy的单元格 > 删除所选行
   */
  selectionFilterSelectCellsStartWithAndEndWithSameWordDeleteRows = 'selectionFilterSelectCellsStartWithAndEndWithSameWordDeleteRows',

  /**
   * 地图数据卡片bar选中 > 重新赋值 为特定值
   */
  mapDataBarSelectReset = 'mapDataBarSelectReset',

  /**
   * 按公式新增列
   */
  newColumnBaseOnFormula = 'ADD_COLUMN_BASED_ON_FORMULA',
  /**
   * 单元格文字选中 > 对象解析
   */
  selectionJsonParse = 'selectionJsonParse',

  /**
   * 单元格文字选中 > 数组解析
   */
  selectionArrayParse = 'selectionArrayParse',

  /**
   * 列操作，选择面板 > 列 嵌合成对象
   */
  columnsChimeraIntoJson = 'columnsChimeraIntoJson',

  /**
   * 列操作，选择面板 > 列 嵌合成数组
   */
  columnsChimeraIntoArray = 'columnsChimeraIntoArray',

  // TODO 后续的待添加
}

/**
 * 字段统计区域菜单中触发的action
 */
export enum StatisticsAction {
  copyColumn = 'copyColumn', // 复制字段
  keepOnlyColumn = 'keepOnlyColumn', // 仅保留字段
  deleteColumn = 'deleteColumn', // 删除字段
  trimSpace = 'trimSpace', // 剪裁空格
  typeTransform = 'typeTransform', // 类型转换
  dataFormatConversion = 'dataFormatConversion', // 格式转换
  upperCase = 'upperCase', // 设为大写
  lowerCase = 'lowerCase', // 设为小写
  removeLetter = 'removeLetter', // 移除字母
  removeNumber = 'removeNumber', // 移除数字
  removePunctuation = 'removePunctuation', // 移除标点符号
  removeAllSpace = 'removeAllSpace', // 移除所有空格
  cleanFromVis = 'cleanFromVis', // 可视化清洗
  renameColumn = 'renameColumn', // 重命名字段
  filterSelected = 'filterSelected', // 特定值筛选
  filterNumberRange = 'filterNumberRange', // 数值范围筛选
  filterDateRange = 'filterDateRange', // 日期范围筛选
  filterWildcard = 'filterWildcard', // 通配符筛选
  filterNull = 'filterNull', // null值筛选
  partitionBy = 'partitionBy', // 值分组
  splitColumn = 'splitColumn', // 拆分字段
  mergeColumn = 'merge-column', // 合并字段
  columnMergeColumns = 'columnMergeColumns', // 合并字段
  createColumn = 'createColumn', // 生成新列
  formulaColumn = 'formulaColumn', //  公式新增列
  aggreFunc = 'aggreFunc', //  聚合函数
  // 2021-7
  jsonParse = 'jsonParse', //  对象解析
  arrayParse = 'arrayParse', //  数组解析
  chimeraIntoJson = 'chimeraIntoJson', // 嵌合成对象
  chimeraIntoArray = 'chimeraIntoArray', // 嵌合成数组
  // 2021-7 end

  editOrder = 'editOrder', // 编辑有序顺序
  semanticTransitionCreateColumn = 'semanticTransitionCreateColumn', // 修改语义新增列
  semanticTransform = 'semanticTransform', // 语义转换
  handleNullOutliers = 'handleNullOutliers', // 异常值处理-Null值
  handleSpecificOutliers = 'handleSpecificOutliers', // 异常值处理-特定值
  handleWildcardOutliers = 'handleWildcardOutliers', // 异常值处理-通用符匹配
  removeRepeatCells = 'removeRepeatCells', // 移除重复行
}

/**
 * action中文名
 */
export enum ActionInCN {
  copyColumn = '复制字段', // 复制字段
  keepOnlyColumn = '仅保留字段', // 仅保留字段
  deleteColumn = '删除字段', // 删除字段
  trimSpace = '剪裁空格', // 剪裁空格
  typeTransform = '类型转换', // 类型转换
  dataFormatConversion = '格式转换', // 格式转换
  upperCase = '设为大写', // 设为大写
  lowerCase = '设为小写', // 设为小写
  removeLetter = '移除字母', // 移除字母
  removeNumber = '移除数字', // 移除数字
  removePunctuation = '移除标点符号', // 移除标点符号
  removeAllSpace = '移除所有空格', // 移除所有空格
  cleanFromVis = '可视化清洗', // 可视化清洗
  renameColumn = '重命名字段', // 重命名字段
  filterSelected = '特定值筛选', // 特定值筛选
  filterNumberRange = '数值范围筛选', // 数值范围筛选
  filterDateRange = '日期范围筛选', // 日期范围筛选
  filterWildcard = '通配符筛选', // 通配符筛选
  filterNull = 'null值筛选', // null值筛选
  filterGroup = '值分组', // 值分组
  splitColumn = '拆分字段', // 拆分字段
  mergeColumn = '合并字段', // 合并字段
  createColumn = '按示例新增列', // 生成新列
  formulaColumn = '按公式新增列', //  公式新增列
  aggreFunc = '聚合函数', //  聚合函数
  jsonParse = '对象解析', //  对象解析
  arrayParse = '数组解析', //  数组解析
  chimeraIntoJson = '嵌合成对象', // 嵌合成对象
  chimeraIntoArray = '嵌合成数组', // 嵌合成数组

  // --------------------------------其他操作---------------------------------------------------
  rowCommonDelete = '删除所选行',
  rowCommonKeep = '仅保留所选行',
  rowFilterSelectNullRows = '选出存在缺失值的行',
  rowFilterSelectNullRowsDelete = '删除所选行',
  rowFilterSelectNullRowsKeep = '仅保留所选行',
  columnFilterSelectNullCells = '选出该列存在缺失值的单元格',
  columnFilterSelectNullCellsReset = '为所选单元格重新赋值',
  columnFilterSelectNullCellsDeleteRows = '删除所选单元格存在的行',
  columnFilterSelectNullCellsKeepRows = '仅保留所选单元格存在的行',
  columnFilterContains = '选出值为xxx的单元格',
  columnFilterContainsReset = '所选单元格重新赋值',
  columnFilterContainsDeleteRows = '删除所选单元格所在的行',
  columnFilterContainsKeepRows = '仅保留所选单元格所在的行',
  columnFilterCompare = '比较运算符',
  columnFilterCompareReset = '重置单元格',
  columnFilterCompareDeleteRows = '删除单元格所在的行',
  columnFilterCompareKeepRows = '仅保留单元格所在的行',
  columnFilterRange = '范围筛选',
  columnFilterRangeReset = '重置单元格',
  columnFilterRangeDeleteRows = '删除单元格所在的行',
  columnFilterRangeKeepRows = '仅保留单元格所在的行',
  columnMergeColumns = '列合并',
  columnCommonDeleteSelectColumns = '删除当前选中的列',
  columnCommonDeleteOtherColumns = '删除未被选中的列',
  columnCommonDeleteRepeatCells = '去除所选列中的重复行',
  cellFilterSelectNullCells = '选出该列存在缺失值的单元格',
  cellFilterSelectNullCellsReset = '为所选单元格重新赋值',
  cellFilterSelectNullCellsDeleteRows = '删除所选单元格存在的行',
  cellFilterSelectNullCellsKeepRows = '仅保留所选单元格存在的行',
  cellFilterSelectContainsCell = '选出与选中单元格值相等的单元格',
  cellFilterSelectContainsCellReset = '为所选单元格重新赋值',
  cellFilterSelectContainsCellDeleteRows = '删除所选单元格存在的行',
  cellFilterSelectContainsCellKeepRows = '仅保留所选单元格存在的行',
  cellFilterSelectCompareCell = '选出大于某个数值的单元格',
  cellFilterSelectCompareCellReset = '为所选单元格重新赋值',
  cellFilterSelectCompareCellDeleteRows = '删除所选单元格存在的行',
  cellFilterSelectCompareCellKeepRows = '仅保留所选单元格存在的行',
  cellFilterSelectRangeCell = '选出与选中单元格值相等的单元格',
  cellFilterSelectRangeCellReset = '为所选单元格重新赋值',
  cellFilterSelectRangeCellDeleteRows = '删除单元格所在的行',
  cellFilterSelectRangeCellKeepRows = '仅保留单元格所在的行',
  cellResetSelectCellReset = '重新赋值为null',
  cellCommonDeleteSelectRows = '删除所选单元格所在的行',
  cellCommonKeepSelectRows = '仅保留所选单元格所在的行',

  // ----------------------------------------通用刷选 start-------------------------------------------
  selectionFilterSelectCellsByKeyword = '选出包含关键字的单元格',
  selectionFilterSelectCellsByKeywordReplaceKeyword = '替换当前所选单元格中刷选内容',
  selectionFilterSelectCellsByKeywordReplaceContent = '为所选单元格重新赋值',
  selectionFilterSelectCellsByKeywordDeleteRows = '删除单元格所在的行',
  selectionFilterSelectCellsByKeywordKeepRows = '仅保留单元格所在的行',
  selectionFilterSelectCellsByKeywordExtractColumn = '抽取所选内容单独形成一列',
  selectionFilterSelectCellsByKeywordSplitColumns = '根据所选内容将当前列拆分多列',

  // ----------------------------------------通用刷选 end-------------------------------------------

  // ----------------------------------------刷选内容为首部 start-------------------------------------------
  selectionFilterSelectCellsStartWithKeyword = '选出以关键字开头的单元格',
  selectionFilterSelectCellsStartWithKeywordReplaceKeyword = '替换选中的内容',
  selectionFilterSelectCellsStartWithKeywordReplaceContent = '替换刷选单元格的内容',
  selectionFilterSelectCellsStartWithKeywordExtractColumn = '抽取选中内容为一列',
  selectionFilterSelectCellsStartWithKeywordSplitColumns = '根据刷选内容将列拆分为多列',
  selectionFilterSelectCellsStartWithKeywordDeleteRows = '删除单元格所在的行',
  selectionFilterSelectCellsStartWithKeywordKeepRows = '仅保留单元格所在的行',

  // ----------------------------------------刷选内容为首部 end-------------------------------------------

  // ----------------------------------------刷选内容为尾部部 start-------------------------------------------
  selectionFilterSelectCellsEndWithKeyword = '选出以关键字结尾的单元格',
  selectionFilterSelectCellsEndWithKeywordReplaceKeyword = '替换选中的内容',
  selectionFilterSelectCellsEndWithKeywordReplaceContent = '替换刷选单元格的内容',
  selectionFilterSelectCellsEndWithKeywordExtractColumn = '抽取选中内容为一列',
  selectionFilterSelectCellsEndWithKeywordSplitColumns = '根据刷选内容将列拆分为多列',
  selectionFilterSelectCellsEndWithKeywordDeleteRows = '删除所选单元格',
  selectionFilterSelectCellsEndWithKeywordKeepRows = '仅保留所选单元格',
  selectionFilterSelectCellsKeywordSamePosition = '选出关键字位置相同的单元格',
  selectionFilterSelectCellsSamePosSelection = '选出与刷选内容位置一致的内容',
  selectionFilterSelectCellsSamePosSelectionReplaceKeyword = '替换刷选内容',
  selectionFilterSelectCellsSamePosSelectionReplaceContent = '替换所在单元格的内容',
  selectionFilterSelectCellsSamePosSelectionExtractColumn = '刷选内容抽取为单独一列',
  selectionFilterSelectCellsSamePosSelectionSplitColumns = '根据刷选内容拆分为多列',
  selectionFilterSelectCellsSamePosSelectionDeleteRows = '删除所选行',
  selectionFilterSelectCellsSamePosSelectionKeepRows = '仅保留所选行',
  selectionReplaceSelectionKeyword = '替换刷选内容',
  selectionCellsReset = '刷选单元格重新赋值',
  selectionFilterSelectCellsSamePosWord = '选出与刷选内容单词位置一致的内容',
  selectionFilterSelectCellsSamePosWordReplaceKeyword = '替换刷选内容',
  selectionFilterSelectCellsSamePosWordReplaceContent = '重置单元格内容',
  selectionFilterSelectCellsSamePosWordExtractColumn = '抽取刷选内容为一列',
  selectionFilterSelectCellsSamePosWordSplitColumns = '根据刷选内容拆分为多列',
  selectionFilterSelectCellsSamePosWordDeleteRows = '删除所选内容所在单元格的行',
  selectionFilterSelectCellsSamePosWordKeepRows = '仅保留所选内容所在单元格的行',
  selectionFilterSelectCellsStartWithSameWord = '选出前面为word的单元格',
  selectionFilterSelectCellsStartWithSameWordReplaceKeyword = '替换刷选内容',
  selectionFilterSelectCellsStartWithSameWordReplaceContent = '设置单元格内容',
  selectionFilterSelectCellsStartWithSameWordExtractColumn = '抽取刷选内容为一列',
  selectionFilterSelectCellsStartWithSameWordSplitColumns = '根据刷选内容拆分为多列',
  selectionFilterSelectCellsStartWithSameWordDeleteRows = '删除所选内容所在单元格的行',
  selectionFilterSelectCellsStartWithSameWordKeepRows = '保留所选内容所在单元格的行',
  selectionFilterSelectCellsEndWithSameWord = '包含xxx，且前面有单词的单元格刷选出该单词',
  selectionFilterSelectCellsEndWithSameWordReplaceKeyword = '替换关键字',
  selectionFilterSelectCellsEndWithSameWordReplaceContent = '单元格重新赋值',
  selectionFilterSelectCellsEndWithSameWordExtractColumn = '抽取刷选内容为一列',
  selectionFilterSelectCellsEndWithSameWordSplitColumns = '拆分成多列',
  selectionFilterSelectCellsEndWithSameWordKeepRows = '仅保留所选行',
  selectionFilterSelectCellsEndWithSameWordDeleteRows = '删除所选行',
  selectionFilterSelectCellsStartWithAndEndWithSameWord = '选出前面为xxx且后面为yyy的单元格',
  selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceKeyword = '替换关键字',
  selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceContent = '替换单元格',
  selectionFilterSelectCellsStartWithAndEndWithSameWordExtractColumn = '抽取出一列',
  selectionFilterSelectCellsStartWithAndEndWithSameWordSplitColumns = '拆分成多列',
  selectionFilterSelectCellsStartWithAndEndWithSameWordKeepRows = '仅保留所选行',
  selectionFilterSelectCellsStartWithAndEndWithSameWordDeleteRows = '删除所选行',
  mapDataBarSelectReset = '为特定值',
  removeRepeatCells = '移除重复行',
}

export enum CleanFromVisAction {
  cleanFromVis = 'cleanFromVis', // 可视化清洗
}

// 需要特殊屏蔽的action
const specialDisableAction = new Set([
  'selectionReplaceSelectionKeyword',
  'selectionCellsReset',
])
// 禁用编辑标识
const getVrDisableEditActions = () => {
  const tag = ['Replace', 'Extract', 'Split', 'Reset', 'columnMergeColumns']
  return Object.values(OptionPreViewAction).reduce((rec, item) => {
    const find = tag.find((tagString) => {
      /* eslint-disable */
      return item.indexOf(tagString) !== -1
    })
    if (!find || specialDisableAction.has(item)) {
      rec.push(item)
    }
    return rec
  }, [] as Array<string>)
}

/**
 * 不可再编辑的action
 */
export const DisableEditAction = [
  StatisticsAction.copyColumn,
  StatisticsAction.keepOnlyColumn,
  StatisticsAction.deleteColumn,
  StatisticsAction.trimSpace,
  StatisticsAction.typeTransform,
  StatisticsAction.dataFormatConversion,
  StatisticsAction.upperCase,
  StatisticsAction.lowerCase,
  StatisticsAction.removeLetter,
  StatisticsAction.removeNumber,
  StatisticsAction.removePunctuation,
  StatisticsAction.removeAllSpace,
  StatisticsAction.removeRepeatCells,
  StatisticsAction.cleanFromVis,
  StatisticsAction.createColumn,
  StatisticsAction.semanticTransform, // 语义修改不允许编辑
  OptionPreViewAction.mapDataBarSelectReset,
  CleanFromVisAction.cleanFromVis,
  ...getVrDisableEditActions(),
]

export default OptionPreViewAction

/**
 * 需要使用操作前、操作后模板的acion
 */
export const BeforeAfterModes = [
  OptionPreViewAction.columnFilterSelectNullCellsReset,
  OptionPreViewAction.cellFilterSelectNullCellsReset,
  OptionPreViewAction.columnMergeColumns,
  OptionPreViewAction.cellResetSelectCellReset,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordReplaceContent,
  OptionPreViewAction.selectionReplaceSelectionKeyword,
  OptionPreViewAction.selectionCellsReset,
  OptionPreViewAction.selectionFilterSelectCellsStartWithKeywordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsStartWithKeywordReplaceContent,
  OptionPreViewAction.columnCommonDeleteSelectColumns,
  OptionPreViewAction.columnCommonDeleteOtherColumns,
  OptionPreViewAction.cellFilterSelectContainsCellReset,
  OptionPreViewAction.cellFilterSelectCompareCellReset,
  OptionPreViewAction.cellFilterSelectRangeCellReset,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsStartWithKeywordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsEndWithKeywordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsEndWithKeywordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsEndWithKeywordReplaceContent,
  OptionPreViewAction.columnFilterContainsReset,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordSplitColumns,
  OptionPreViewAction.columnFilterCompareReset,
  OptionPreViewAction.columnFilterRangeReset,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordExtractColumn,
  OptionPreViewAction.mapDataBarSelectReset,

  OptionPreViewAction.columnsChimeraIntoJson,
  OptionPreViewAction.columnsChimeraIntoArray,
]

/**
 * 需要使用关键字模板的action
 */
export const KeyWordModes = [
  OptionPreViewAction.selectionReplaceSelectionKeyword,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsStartWithKeywordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsStartWithKeywordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsEndWithKeywordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsEndWithKeywordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsSamePosSelectionSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsEndWithKeywordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsStartWithSameWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsEndWithSameWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceKeyword,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordReplaceContent,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsStartWithAndEndWithSameWordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsByKeywordSplitColumns,
  OptionPreViewAction.selectionFilterSelectCellsStartWithKeywordExtractColumn,
  OptionPreViewAction.selectionFilterSelectCellsSamePosWordSplitColumns,
  OptionPreViewAction.selectionCellsReset,
]

/**
 * 清洗action对应的图标
 * 清洗历史列表 HistoryList.vue, pipeline 节点操作可用
 */
export const filterActionIconType = {
  ORIGINAL_DATA: 'iconshujubiao',
  [StatisticsAction.filterNull]: 'iconnullzhishaixuan',
  [StatisticsAction.filterWildcard]: 'icontongpeifushaixuan',
  [StatisticsAction.filterSelected]: 'icontedingzhishaixuan',
  [StatisticsAction.filterNumberRange]: 'iconshaixuanshuzhifanwei',
  [StatisticsAction.filterDateRange]: 'iconriqifanweishaixuan',
  [StatisticsAction.deleteColumn]: 'iconshanchuziduan',
  [StatisticsAction.copyColumn]: 'iconfuzhiziduan',
  [StatisticsAction.typeTransform]: 'icongenggaiziduanleixing',
  [StatisticsAction.partitionBy]: 'iconfenzu',
  [StatisticsAction.lowerCase]: 'icongaiweixiaoxie',
  [StatisticsAction.upperCase]: 'icongaiweidaxie',
  [StatisticsAction.splitColumn]: 'iconziduanchaifen',
  [StatisticsAction.mergeColumn]: 'iconhebing',
  [StatisticsAction.removeLetter]: 'iconyichuzimu',
  [StatisticsAction.removeNumber]: 'iconyichushuzi',
  [StatisticsAction.removePunctuation]: 'iconyichubiaodian',
  [StatisticsAction.renameColumn]: 'iconzhongmingming',
  [StatisticsAction.keepOnlyColumn]: 'iconjinbaoliugailie',
  [StatisticsAction.trimSpace]: 'iconcaijiankongge',
  [StatisticsAction.removeAllSpace]: 'iconyichusuoyoukongge',
  [StatisticsAction.cleanFromVis]: 'iconkeshihuaqingxi',
  [StatisticsAction.aggreFunc]: 'iconkeshihuaqingxi',
  // 修改语义
  [StatisticsAction.semanticTransform]: 'icona-duixiangjiexibeifen5', // 语义转换
  // menu 对象 数组 解析与嵌合
  [StatisticsAction.jsonParse]: 'icona-duixiangjiexibeifen4', // menu操作 对象解析
  [StatisticsAction.arrayParse]: 'icona-duixiangjiexibeifen3', // menu操作 数组解析
  [StatisticsAction.chimeraIntoJson]: 'icona-duixiangjiexibeifen2', // menu操作 嵌合成对象
  [StatisticsAction.chimeraIntoArray]: 'iconduixiangjiexibeifen', // menu操作 嵌合成数组
  [StatisticsAction.editOrder]: 'iconpaixu', // menu操作 排序
  // 智能推荐 对象 数组 解析与嵌合
  [OptionPreViewAction.selectionJsonParse]: 'icona-duixiangjiexibeifen4', // 智能推荐的对象解析
  [OptionPreViewAction.selectionArrayParse]: 'icona-duixiangjiexibeifen3', // 智能推荐的数组解析
  [OptionPreViewAction.columnsChimeraIntoJson]: 'icona-duixiangjiexibeifen2', // 智能推荐的嵌合成对象
  [OptionPreViewAction.columnsChimeraIntoArray]: 'iconduixiangjiexibeifen', // 智能推荐的嵌合成数组

  MORE_ICON: 'icongengduocaozuo',
}
