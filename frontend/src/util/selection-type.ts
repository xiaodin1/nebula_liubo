/**
 * 数据表操作类型 columns|rows|cells|selection|none
 */
enum SelectionType {
  /**
   * 选中一列或多列
   */
  'columns' = 'columns',
  /**
   * 选中一行或多行
   */
  'rows' = 'rows',
  /**
   * 选中一个或多个单元格
   */
  'cells' = 'cells',
  /**
   * 单元格文字刷选
   */
  'selection' = 'selection', 
  /**
   * 数据卡片单条bar被选中
   */
  'bar' = 'bar', 
  /**
   * 数据卡片多条bar被选中
   */
  'bars' = 'bars', 
  /**
   * 默认无选择状态
   */
  'none' = '',
}

export default SelectionType
