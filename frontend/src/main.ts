import Vue from 'vue'
import {
  Layout,
  Avatar,
  Menu,
  Icon,
  Modal,
  Dropdown,
  Breadcrumb,
  Button,
  Table,
  Divider,
  Select,
  Tree,
  Tooltip,
  Input,
  Form,
  FormModel,
  Radio,
  Checkbox,
  InputNumber,
  message,
  Upload,
  Spin,
  Tabs,
  Switch,
  Slider,
  Collapse,
  Col,
  Row,
  Popover,
  Transfer,
  DatePicker,
  Pagination,
  Progress,
  Cascader,
  Timeline,
  Popconfirm,
  List,
  Alert,
  Steps,
} from 'ant-design-vue'
import 'ant-design-vue/dist/antd.less'
// @ts-ignore
import io from 'socket.io-client'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(Layout)
Vue.use(Layout.Header)
Vue.use(Layout.Sider)
Vue.use(Avatar)
Vue.use(Menu)
Vue.use(Menu.Item)
Vue.use(Icon)
Vue.use(Modal)
Vue.use(Dropdown)
Vue.use(Breadcrumb)
Vue.use(Breadcrumb.Item)
Vue.use(Button)
Vue.use(Table)
Vue.use(Divider)
Vue.use(Select)
Vue.use(Select.Option)
Vue.use(Tree)
Vue.use(Tooltip)
Vue.use(Input)
Vue.use(Form)
Vue.use(Form.Item)
Vue.use(FormModel)
Vue.use(FormModel.Item)
Vue.use(Radio.Group)
Vue.use(Radio)
Vue.use(Checkbox)
Vue.use(InputNumber)
Vue.use(Upload)
Vue.use(Spin)
Vue.use(Input.TextArea)
Vue.use(Tabs)
Vue.use(Tabs.TabPane)
Vue.use(Switch)
Vue.use(Slider)
Vue.use(Collapse)
Vue.use(Col)
Vue.use(Row)
Vue.use(Popover)
Vue.use(Transfer)
Vue.use(DatePicker)
Vue.use(DatePicker.RangePicker)
Vue.use(Pagination)
Vue.use(Progress)
Vue.use(Cascader)
Vue.use(Timeline)
Vue.use(Popconfirm)
Vue.use(List)
Vue.use(Alert)
Vue.use(Steps)

let socket: any = null
const socketOptions = {
  transports: ['websocket'],
}
if (process.env.NODE_ENV === 'production') {
  socket = io(socketOptions) // prodection 环境用nginx转发
} else {
  // devserver没搞定websocket的转发，先临时这样
  socket = io('//10.5.24.17:10246', socketOptions)
}

message.config({
  top: '100px',
  duration: 2,
  maxCount: 2,
})
Vue.prototype.$message = message
Vue.prototype.$confirm = Modal.confirm
Vue.prototype.$socket = socket

Vue.component(
  'AIconFont',
  Icon.createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_1566179_qv3wd92tp4.js', // at.alicdn.com/t/font_1566179_1skd5tg1lja.js',
  })
)

Vue.config.productionTip = false
window.addEventListener('load', () => {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount('#app')
})

// router.beforeEach((to, from, next) => {
//   console.log(to,from,next)
//   alert('全局路由验证：'+store.getters.token)
//   // 没有登录时自动跳转,开发环境免登陆时注释
//   if ( (to.path !== '/auth' && to.path !== '/message' ) && ( store.getters.token === '' || store.getters.token === null )  ) {
//     //alert('登录失败');
//     next({ path: '/message' })

//   }else{
//    //alert('登录成功');
//   //   路由的next必须存在,否则无法进入下一页
//     next()
//   }
//   next()

// })
