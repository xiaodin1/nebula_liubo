/**
 * user store
 * manage all states of current user
 * @author Huihua Lu
 */
import {
  Module,
  Mutation,
  VuexModule,
  getModule,
  Action,
} from 'vuex-module-decorators'
import store from '@/store'
import {
  userLogin,
  userRegister,
  userLogout,
  getUserInfo,
  getUserInfoById,
} from '@/api/user'
import { getUserRole } from '@/api/pipeline'

// 用户信息
interface UserInfo {
  id: number
  name: string
  email?: string
  status?: number
  phone?: string
  remark?: string
  gmtCreator?: string
  gmtModifier?: string
  gmtCreate?: string
  gmtModify?: string
  role?: number
  nickName?: string
  realName?: string
  dateBirth?: string
  gender?: string
  profession?: string
  workOrg?: string
  address?: string
  researchField?: string
  picEncode?: string
  guideStatus?: number // 是否没有体验过用户引导
}

@Module({ dynamic: true, namespaced: true, name: 'UserStore', store })
class UserStore extends VuexModule {
  private _currentUser: UserInfo | null = null // 当前用户信息

  private _registerCode: number = 0
  public loginToken: string = ''

  public _userRole: any = null

  public get currentUser() {
    return this._currentUser
  }

  public get userRole() {
    return this._userRole
  }

  public get registerCode() {
    return this._registerCode
  }

  // 登录
  @Action({ commit: 'setCurrentUser' })
  public async login(passData: any) {
    const response = await userLogin({ data: passData })
    return response.data
  }

  // 注册
  @Action({ commit: 'setRegisterCode' })
  public async register(passData: any) {
    const response = await userRegister({
      data: passData,
      params: { code: passData.phoneCode },
    })
    return response.data.code
  }

  // 登出
  @Action({ commit: 'clearUserInfo' })
  public async logout() {
    await userLogout()
  }

  // 获取用户信息
  @Action({ commit: 'setCurrentUser' })
  public async queryUserInfo() {
    const response = await getUserInfo() // 获取用户id
    const result = await getUserInfoById({
      data: { id: response.data.result.user.id },
    }) // 使用query的方式获取用户信息
    return { user: result.data.result }
  }

  // 获取用户信息
  @Action({ commit: 'setCurrentUser' })
  public async queryUserInfoById(passData: any) {
    const result = await getUserInfoById(passData) // 使用query的方式获取用户信息
    return { user: result.data.result }
  }

  // 获取某个项目下用户的角色
  @Action({ commit: 'setUserRole' })
  public async getUserRole(parameters: any) {
    const result = await getUserRole({
      data: parameters,
    })
    let userRole: any = null
    if (result.data.code === 100) {
      userRole = result.data.result
    }
    return userRole
  }

  @Mutation
  public setUserRole(data: any) {
    this._userRole = data
  }

  @Mutation
  private setCurrentUser(loginInfo: any) {
    this._currentUser = loginInfo.user
    this.loginToken = loginInfo.token
  }

  @Mutation
  private setRegisterCode(code: number) {
    this._registerCode = code
  }

  @Mutation
  private clearUserInfo() {
    this._registerCode = 0
    this._currentUser = null
  }

  @Mutation
  public clearState() {
    this._currentUser = null
    this._registerCode = 0
  }
}

export default getModule(UserStore)
