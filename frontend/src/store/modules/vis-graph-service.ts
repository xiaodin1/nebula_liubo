/**
 * VisGraphStore
 * create by jacktan 2021/12/15
 */
import {
  Module,
  Mutation,
  VuexModule,
  getModule,
  Action,
} from 'vuex-module-decorators'
import store from '@/store'

const NullValues = new Set([null, undefined, ''])

/** visualization graph servie 用于辅助可视化缓存graph数据，提供字段筛选等功能 */
@Module({ dynamic: true, namespaced: true, name: 'VisGraphStore', store })
class VisGraphStore extends VuexModule {
  /** graph数据缓存 */
  private cacheStore = {} as any

  private pipelineNodeCacheStore = {} as any

  /** 从缓存中拿数据 */
  @Action
  public getCache(key: string): any {
    return this.cacheStore[key]
  }

  @Mutation
  public clearCache(): any {
    this.cacheStore = {} as any
    this.pipelineNodeCacheStore = {} as any
  }

  /** 保存数据到缓存中 */
  @Mutation
  save(paylod: { id: string; data: any }) {
    if (NullValues.has(paylod.id as any)) {
      console.error('save cache data use null key!!!')
    } else {
      this.cacheStore[paylod.id] = paylod.data
    }
  }

  /** 从缓存中拿数据 */
  @Action
  public getCachePipelineNode(key: string): any {
    return this.cacheStore[key]
  }

  @Mutation
  savePipeNodeInfo(paylod: { id: string; data: any }) {
    if (NullValues.has(paylod.id as any)) {
      console.error('save cache data use null key!!!')
    } else {
      this.pipelineNodeCacheStore[paylod.id] = paylod.data
    }
  }
}

export default getModule(VisGraphStore)
