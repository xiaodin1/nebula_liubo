/* eslint-disable unicorn/prevent-abbreviations */
import {
  Module,
  Mutation,
  VuexModule,
  getModule,
  Action,
} from 'vuex-module-decorators'
import store from '@/store'
import UserStore from '@/store/modules/user'
import {
  // queryDatalist, // dev
  importData,
  queryProjectDataset,
} from '@/api/data-list'
import { Compare } from '@/util/data-view-table-helper'
import {
  allNodeList,
  taskComplexLoad,
  taskQueryById,
  queryInputTables,
  taskQueryDataVis,
  simpleUpdate,
} from '@/api/task'
import {
  apiGetHistoryList,
  apiAddHistory,
  apiDelHistory,
  apiUpdateHistory,
  apiUpdateSemantic,
  apiGetTableData,
  checkFormula,
  apiMultiColumnSearch,
} from '@/api/data-clean'
import { queryModelList } from '@/api/model'
// import { getUserRole } from '@/api/pipeline'
import { message } from 'ant-design-vue'
import { cloneDeep, isEqual, isFunction, isEmpty } from 'lodash'
import { ComputeSpaceAfterNode } from '@/components/studio/data/util-pipeline'
import CompareInfoInterface from '@/views/studio/vr-operations/common/compare-info-interface'
import SplitInfo from '@/views/studio/vr-operations/common/split-info-interface'
import SelectionType from '@/util/selection-type'
import VROptionSetModeInteface from '@/util/vr-option-set-mode-interface'
import SelectionInfo from '@/util/selection-info-interface'
import { getUserConfig } from '@/api/user'

interface KeyValue {
  [key: string]: any
}

export enum EditActionArea {
  PIPELINE = 'pipeline', //  活动区域在pipeline
  OPERATOR = 'edit-operator', //  活动区域在editOperatorModal 【自定义算子】
  PANEL = 'edit-panel', //  活动区域在nodeEditPanel 【清洗panel】
  COMMENT = 'edit-comment', //  活动区域在nodeCapsule 【编辑注释】
}

// project 数据
export interface Data {
  type: string
  id: number
  name: string
}

export interface IKeyValueStore {
  [key: string]: any
}

export interface IColumn {
  name: string
  type: number
  desc?: string
  semantic?: string
  _stamp?: number
  width?: number
  numberFormat?: any
}

export interface NodeStatus {
  status: string
  logInfo: string
}

export interface NodeStatusMap {
  [id: string]: NodeStatus
}

/**
 * 单元格信息
 */
export interface CellInfo {
  rowIndex: number
  columnName: string
  _record_id_: string
  cellValue: string
}

export interface TableVueInstance {
  clearAllSelect(): void

  clearTableSlection: void

  checkHeight(): void
}

export interface SelectNodeTableInfo {
  nodeId: string
  tableName: string
}

export { default as OptionPreViewAction } from '@/util/data-selection-actions'
export { default as SelectionInfo } from '@/util/selection-info-interface'

@Module({ dynamic: true, namespaced: true, name: 'DataViewStore', store })
class DataViewStore extends VuexModule {
  private _projectData: any = [] // 当前项目数据源
  private _selectNode: any = null // 选择的node
  private visualWindowShow: boolean = false // 新建/编辑可视化窗口是否显示
  private datasetName: string = ''
  private pipelineId: number | null = null
  private tabName: string | null = null
  // private _userRole: any = null
  private _projectId: number | null = null
  private _lock: boolean | null = null // 项目是否锁定
  private currentData: any = []
  private _pipelineSnapshotId: number | null = null
  private favoriteStatus: boolean = false // 收藏开关，每次收藏将值取非，对应组件监听该变量改变来重新拉取可视化信息
  private historyStatus: boolean = false // 版本快照开关
  private overSaveVisible: boolean = false // 覆盖版本弹框显示
  private selectFunctionIndex: number | null = null // 右侧选中function btn index
  private oldItems: any = null // 已有配置项（可视化重编辑）
  private _historyList: IKeyValueStore[] = [] // 操作历史列表
  private _currentHistory: IKeyValueStore | null = null
  private _tableColumns: IColumn[] = [] // 表列字段
  private _tableData: IKeyValueStore[] = [] // 表明细数据
  private _tableCurPage: number = 1 //  表格当前页码
  private _tableHeight: number = 128 // 底部表格高度
  private _isEditHistory: boolean = false // 是否编辑操作历史
  private _executionCompletionTime: number = 0 // 链路执行完成
  private _nodeStatusMap: NodeStatusMap = {} // 节点状态
  private _isSideMenuShow: boolean = true // 选择数据视图和可视化视图的菜单是否显示
  private _isSidePanelShow: boolean = true // 数据源和算子的面板是否显示
  public selectNodeTableInfo: SelectNodeTableInfo | null = null // pipeline节点id和表名组成的唯一信息，便于监测节点或表的变化
  public _editNodeId: any = 0 //  编辑自定义算子节点的task_id
  private _selectModel: any = null // 当前选中的model

  // 2020/11/3 算子列表
  private _projectOperator: any[] = []

  private _columnFilters: any[] = [] // 列字段上的筛选条件
  private _selectedColumns: IColumn[] = [] // node-edit-panel中选中的字段卡片
  private _mergeColumnHotKey: boolean = false
  private _nodeEditPanelHeight: number = 400 // node-edit-panel 高度
  private _savingWidgetConfig: any = null // 正在保存的推荐可视化

  // table列表交互缓存相关数据
  private _tb_selectColumns: IColumn[] = [] // 选中列
  private _tb_selectRowNumbers: number[] = [] // 选中行号
  private _tb_selectRowDatas: any[] = [] // 选中行的数据
  private _tb_selectCellDatas: Array<CellInfo> = [] // 选中的单元格
  private _tb_selectionType: SelectionType = SelectionType.none // 当前表格选择模式
  private _tb_vue_instance: TableVueInstance | null = null
  private _tb_selectionInfo: SelectionInfo | null = null
  private _isShowOperatorParameter: boolean = true

  // 隐藏预览面板的计时器
  private _vr_timer: any = null
  private _vr_previewAction: string = ''
  // 需要替换的值
  public columnFilterSelectNullCellsResetValue: string = ''
  // 链接符号
  public columnMergeColumnsJoinString: string = '-'
  // 新字段名称
  public columnMergeColumnsNewColumnName: string = ''

  // 单元格重新赋值所填入的值
  public cellResetSelectCellResetValue: string = ''

  // 刷选需要替换的关键字
  public selectionReplaceKeyword: string = ''
  // 刷选需要替换为该值
  public selectionReplaceValue: string = ''

  // 当选中一列时当前列是否含有空值
  public columnHasNullCell: boolean = true
  // 行操作状态下，是否存在缺失值的行
  public tableHasNullRows: boolean = true
  // 匹配的值
  public tableValueContains: Array<string> | null = null

  // 范围推荐（比较模式）推荐用户填入信息
  public vrCompareInfo: CompareInfoInterface | null = null
  public vrRangeInfo: Array<number> | null = []
  // 刷选内容抽取出一列
  public tableExtractColumnName: string = ''
  // 推荐右侧预览窗口y定位
  public hoverYPos: number = -1
  // 推荐指令组点击确定后相关指令信息
  public vrCurrenSetMode: VROptionSetModeInteface | null = null
  // 推荐相关指令历史记录编辑，点击触发标识
  public vrEditTrigger: number = -1

  // 2021.07.22 刷选解析 Json Array (使用模块: 1.推荐结果预览 vr-operations/common/dataPreview.vue $)
  public _currentSelectionJsonKey: string | null = null // 刷选 json 时的 key
  public _currentSelectionArrayIndex: number | null = null // 刷选 array 时的 index

  public _chimeraJsonNewColumnName: string | null = null // 嵌合对象的新列名
  public _chimeraArrayNewColumnName: string | null = null // 嵌合数组的新列名

  public _fullAmountTask: boolean = false // 全量任务状态
  public _multiSelectNodeArray: any[] = [] // pipeline 中刷选或者 Ctrl 多选的节点

  // 节点表数据全局搜索
  public _globalSearchTime: number = 0 // 搜索时间戳
  public _globalSearchColumns: string[] = [] // 搜索选择的列
  public _isTableDataGlobalSearchMode: boolean = false // 全局搜索 模式
  public _tableDataGlobalSearchKey: string = '' // 全局搜索关键字 key
  public _tableDataGlobalSearchData: any[] = [] // 全局搜索结果

  // 添加经济算子时， 引入算子运行提示
  public _showTaskPrompt: boolean = true
  public _showTaskPromptText: string = ''

  // 智能推荐显示控制
  public _intelliRecommendationType: string = 'DataFiltering'

  public get intelliRecommendationType() {
    return this._intelliRecommendationType
  }

  @Mutation
  setIntelliRecommendationType(type: string) {
    this._intelliRecommendationType = type
  }

  // 用户名
  public get userName() {
    return UserStore.currentUser ? UserStore.currentUser.name : '未命名用户'
  }

  public get userRole() {
    return UserStore.userRole
  }

  // 访客模式
  public get isVisitor() {
    if (this.userRole) {
      return this.userRole.id === 4 || this.userName !== this.tabName
    }
    return false
  }

  // 定位节点
  public _locateNodes: any[] = []
  // 保存数据源
  public _saveToDmVisible: boolean = false

  /**
   * 可视化推荐面板展开收起状态
   */
  public isVRPanelShow: boolean = false

  /**
   * 是否启用智能推荐功能
   */
  public enableVRPanelRecommend: boolean = false

  /**
   * 是否仅显示formula card
   */
  public onlyShowFormulaCard: boolean = false

  /**
   * 可视化面板transfrom推荐重置触发标识
   */
  public vrResetTag: number = 0

  public tableDataCount: number = 0

  public totalPages: number = 0

  /**
   * 字段拆分结构
   */
  public tableSplitInfo: SplitInfo | null = null

  public autojoinTableName: string | null = null // autojoin时预览数据的表名

  /**
   * 按示例新增列 - 是否需要打开CreateNewColumn modal （在table 入口处需要设置 ）
   */

  public ifOpenCreateNewColumn: boolean = false

  /**
   * 按示例新增列 - CreateNewColumn 所依据的example 的列名 （在table 入口处需要设置 ）
   */
  public createColumnName: string = ''

  /**
   * 按示例新增列 - 新创建列的相关数据（用于在table里的hightlight）
   */
  public newColumnData: IColumn[] = []

  /**
   * 地图数据 - 获取清洗数据所需 对象
   */
  public _openVRPanelPara: object = {}

  /**
   * 地图数据 - 当前hover 的推荐值
   */
  public _mapDataReplaveValue: string = ''

  public lastHistoryCompleteId: string = '' // 新 添加/更新 成功的历史记录id，便于接口成功刷新historylist定位用
  public isTableDetailLoading: boolean = false // 数据清洗的统计区表格是否正在加载中

  /**
   * 交互式编程 - 自定义算子节点 所连接的数据节点list
   */
  public _linkedTableList: Array<string> = []
  /**
   * 交互式编程 - 当前选择的代码块 cell
   */
  public _selectedCell: string = ''
  /**
   * 交互式编程 - 当前复制的代码块 value
   */
  public _copiedCellValue: string = ''
  /**
   * 交互式编程 - MODAL 打开所需的参数
   */
  public _editOperatorParam: object = {}
  /**
   * 隔离 各组件之间的快捷键监听
   */
  public _actionArea: EditActionArea = EditActionArea.PIPELINE
  // 正在编辑的自定义算子
  private _editOperatorNode: any = null

  /**
   * 按公式新增列 - 是否进入编辑状态 【决定c2,c2...的显示隐藏】
   */
  public _showColumnIndex: boolean = false

  public _formulaColName: string = ''

  @Mutation
  setMapDataReplaveValue(data: any) {
    this._mapDataReplaveValue = data
  }

  @Mutation
  setOpenVRPanelPara(data: any) {
    this._openVRPanelPara = data
  }

  // table|推荐相关操作--------------------------------------start
  @Mutation
  setTableExtractColumnName(newColumName: string) {
    this.tableExtractColumnName = newColumName
  }

  @Mutation
  setTableValueContains(containsValue: Array<string> | null) {
    this.tableValueContains = containsValue
  }

  @Mutation
  setTableSplitInfo(info: SplitInfo) {
    this.tableSplitInfo = info
  }

  @Mutation
  public setVrCompareInfo(info: CompareInfoInterface | null) {
    this.vrCompareInfo = info
  }

  @Mutation
  public setVrRangeInfo(info: [number, number] | null) {
    this.vrRangeInfo = info
  }

  /**
   * 清空状态以及推荐填入信息
   */
  @Mutation
  public resetVrSettingValues() {
    this.columnFilterSelectNullCellsResetValue = ''
    this.columnMergeColumnsJoinString = '-'
    this.columnMergeColumnsNewColumnName = ''
    this.cellResetSelectCellResetValue = ''
    this.selectionReplaceKeyword = ''
    this.selectionReplaceValue = ''
    // 相关填入信息
    this.tableExtractColumnName = ''
    this.tableValueContains = null
    this.vrCompareInfo = null
    this.vrRangeInfo = null
    this.tableExtractColumnName = ''
    this.vrCurrenSetMode = null
    // 刷新transform重置的标识
    this.vrResetTag = Math.random()
    // // 重置历史记录编辑标识
    // this.vrEditTrigger = -1
  }

  @Mutation
  public setNewColumnOpen(newValue: boolean) {
    this.ifOpenCreateNewColumn = newValue
  }

  @Mutation
  public setPipelineSnapshotId(newValue: number) {
    this._pipelineSnapshotId = newValue
  }

  @Mutation
  public setTotalPages(count: number) {
    this.totalPages = count
  }

  @Mutation
  public setCreateColumnName(name: string) {
    this.createColumnName = name
  }

  @Mutation
  public setNewColumnData(data: IColumn[]) {
    this.newColumnData = data
  }

  @Mutation
  public setTableDataCount(count: number) {
    this.tableDataCount = count
  }

  @Mutation
  public setTableHasNullRows(hasNull: boolean) {
    this.tableHasNullRows = hasNull
  }

  @Mutation
  public setColumnHasNullCell(hasNull: boolean) {
    this.columnHasNullCell = hasNull
  }

  @Mutation
  public setSelectionReplaceKeyword(value: string = '') {
    this.selectionReplaceKeyword = value
  }

  @Mutation
  public setSelectionReplaceValue(value: string = '') {
    this.selectionReplaceValue = value
  }

  @Mutation
  public setCellResetSelectCellResetValue(value: string) {
    this.cellResetSelectCellResetValue = value
  }

  @Mutation
  public setColumnMergeColumnsJoinString(joinString: string) {
    this.columnMergeColumnsJoinString = joinString
  }

  @Mutation
  public setColumnMergeColumnsNewColumnName(columnName: string) {
    this.columnMergeColumnsNewColumnName = columnName
  }

  @Mutation
  public setColumnFilterSelectNullCellsResetValue(value: string) {
    this.columnFilterSelectNullCellsResetValue = value
  }

  @Mutation
  public setVRPanelShow(status: boolean) {
    this.isVRPanelShow = status
  }

  @Mutation
  public toggleEnableVRPanelRecommend(status: boolean) {
    this.enableVRPanelRecommend = status
  }

  @Mutation
  public toggleOnlyShowFormulaCard(status: boolean) {
    this.onlyShowFormulaCard = status
  }

  @Mutation
  public setPreviewAction(action: string) {
    this._vr_previewAction = action
  }

  @Mutation
  public setVrEditTrigger(data?: number) {
    this.vrEditTrigger = data !== undefined ? data : Math.random()
    if (this.vrEditTrigger > 0 && !this.isVRPanelShow) {
      // 如果推荐面板是关闭的，需要打开
      this.isVRPanelShow = true
    }
  }

  /**
   * 立即清除预览弹框
   */
  @Mutation
  public clearDataPreviewImmediately() {
    clearTimeout(this._vr_timer)
    this.hoverYPos = -1
  }

  @Mutation
  public setHoverYPos(yNumber: number) {
    clearTimeout(this._vr_timer)
    if (yNumber < 0) {
      this._vr_timer = setTimeout(() => {
        this.hoverYPos = yNumber
      }, 300)
    } else {
      this.hoverYPos = yNumber
    }
  }

  @Mutation
  public clearHoverTimer() {
    clearTimeout(this._vr_timer)
  }

  @Mutation
  public clearTableSelection() {
    if (this._tb_vue_instance) {
      this._tb_vue_instance.clearAllSelect()
    }
    this._tb_selectColumns = []
    this._tb_selectRowNumbers = []
    this._tb_selectRowDatas = []
    this._tb_selectCellDatas = []
    this._tb_selectionType = SelectionType.none
  }

  @Mutation
  public setTableSelectionInfo(selectionInfo: SelectionInfo | null) {
    this._tb_selectionInfo = selectionInfo
  }

  @Mutation
  public setDataTableInstance(instance: TableVueInstance) {
    if (instance && isFunction(instance.clearTableSlection)) {
      this._tb_vue_instance = instance
    }
  }

  @Mutation
  public setTableSelectColumns(columns: IColumn[]) {
    this._tb_selectColumns = columns
  }

  @Mutation
  public toggleTableSelectColumn(column: IColumn) {
    const index = this._tb_selectColumns.findIndex(
      (columnItem) => columnItem.name === column.name
    )
    if (index > -1) {
      this._tb_selectColumns.splice(index, 1)
    } else {
      this._tb_selectColumns = [...this._tb_selectColumns, column]
    }
  }

  @Mutation
  public setTableSelectRowDatas(data: any[]) {
    this._tb_selectRowDatas = data
  }

  @Mutation
  public setTableSelectRowNumbers(rows: number[]) {
    this._tb_selectRowNumbers = rows
  }

  @Mutation
  public toggleTableSelectRowNumber(rowIndex: number) {
    if (this._tb_selectRowNumbers.includes(rowIndex)) {
      this._tb_selectRowNumbers.splice(rowIndex, 1)
    } else {
      this._tb_selectRowNumbers.push(rowIndex)
    }
  }

  @Mutation
  public toggleTableSelectData(data: any) {
    let tag = false
    let dataindex = -1
    this._tb_selectRowDatas.forEach((dataItem, index) => {
      if (Compare(dataItem, data)) {
        tag = true
        dataindex = index
      }
    })
    if (tag) {
      this._tb_selectRowDatas.splice(dataindex, 1)
    } else {
      this._tb_selectRowDatas.push(data)
    }
  }

  @Mutation
  public setTableSelectCells(cells: Array<CellInfo>) {
    this._tb_selectCellDatas = cells
  }

  @Mutation
  public toggleTableSelectCell(cell: CellInfo) {
    // 判断新的cell是否在同一列
    const isSameColumn = this._tb_selectCellDatas.every((item) => {
      return item.columnName === cell.columnName
    })
    if (!isSameColumn) {
      this._tb_selectCellDatas = [cell]
    } else {
      const index = this._tb_selectCellDatas.findIndex((cellItem) => {
        return (
          cellItem.rowIndex === cell.rowIndex &&
          cellItem.columnName === cell.columnName
        )
      })
      if (index > -1) {
        this._tb_selectCellDatas.splice(index, 1)
      } else {
        this._tb_selectCellDatas.push(cell)
      }
    }
  }

  @Mutation
  public setTableSelectiontype(selectionType: SelectionType) {
    this._tb_selectionType = selectionType
    this.isVRPanelShow = ![SelectionType.none, SelectionType.bars].includes(
      selectionType
    )
  }

  @Mutation
  public setTableVrCurrenSetMode(optionInfo: VROptionSetModeInteface | null) {
    this.vrCurrenSetMode = optionInfo
  }

  @Mutation
  public setSelectedCell(paraId: string) {
    this._selectedCell = paraId
  }

  @Mutation
  public setCopiedCellValue(value: string) {
    this._copiedCellValue = value
  }

  @Mutation
  public setLinkedTableList(list: Array<string>) {
    this._linkedTableList = list
  }

  @Mutation
  public setEditOperatorParam(value: object) {
    this._editOperatorParam = value
  }

  @Mutation
  public setActionArea(value: EditActionArea) {
    this._actionArea = value
  }

  @Mutation
  public setEditOperatorNode(value: any) {
    this._editOperatorNode = value
  }

  @Mutation
  public setShowColumnIndex(value: boolean) {
    this._showColumnIndex = value
  }

  @Mutation
  public setEditedFormulaColName(value: string) {
    this._formulaColName = value
  }

  @Mutation
  public setCurrentSelectionJsonKey(value: any) {
    this._currentSelectionJsonKey = value
  }

  @Mutation
  public setCurrentSelectionArrayIndex(value: any) {
    this._currentSelectionArrayIndex = value
  }

  @Mutation
  public setChimeraJsonNewColumnName(value: any) {
    this._chimeraJsonNewColumnName = value
  }

  @Mutation
  public setChimeraArrayNewColumnName(value: any) {
    this._chimeraArrayNewColumnName = value
  }

  public get currentSelectionJsonKey() {
    return this._currentSelectionJsonKey
  }

  public get currentSelectionArrayIndex() {
    return this._currentSelectionArrayIndex
  }

  public get chimeraArrayNewColumnName() {
    return this._chimeraArrayNewColumnName
  }

  public get chimeraJsonNewColumnName() {
    return this._chimeraJsonNewColumnName
  }

  public get editOperatorNode() {
    return this._editOperatorNode
  }

  public get showColumnIndex() {
    return this._showColumnIndex
  }

  public get formulaColName() {
    return this._formulaColName
  }

  /**
   * 隔离 不同组件之间的监听事件
   */
  public get ActionArea() {
    return this._actionArea
  }

  /**
   * 交互式编程 - MODAL 所需参数
   */
  public get EditOperatorParam() {
    return this._editOperatorParam
  }

  /**
   * 交互式编程 - 自定义算子节点 所连接的数据节点list
   */
  public get LinkedTableList() {
    return this._linkedTableList
  }

  /**
   * 交互式编程 - 当前选择的代码块 cell
   */
  public get SelectedCell() {
    return this._selectedCell
  }

  /**
   * 交互式编程 - 当前复制的代码块 value
   */
  public get CopiedCellValue() {
    return this._copiedCellValue
  }

  /**
   * 地图数据 清洗的相关信息
   */
  public get MapDataReplaveValue() {
    return this._mapDataReplaveValue
  }

  public get OpenVRPanelPara() {
    return this._openVRPanelPara
  }

  /**
   * 推荐面板预览类型
   */
  public get VRPreviewAction() {
    return this._vr_previewAction
  }

  /**
   * DataTable实例,用于外部调用
   */
  public get dataTableInstance() {
    return this._tb_vue_instance
  }

  public get tableSelectCells() {
    return this._tb_selectCellDatas
  }

  public get tableSelectColumns() {
    return this._tb_selectColumns
  }

  public get tableSelectRowNumbers() {
    return this._tb_selectRowNumbers
  }

  public get tableSelectRowDatas() {
    return this._tb_selectRowDatas
  }

  public get tableSelectionType() {
    return this._tb_selectionType
  }

  /**
   * 获取数据表刷选信息
   */
  public get tableSelectionInfo() {
    return this._tb_selectionInfo
  }

  /**
   * 获取数据表刷选文本
   */
  public get tableSelctionText() {
    return this._tb_selectionInfo ? this._tb_selectionInfo.selection : ''
  }

  @Mutation
  public setOperatorList(operatorList: any[]) {
    this._projectOperator = operatorList
  }

  private _modelList: any[] = [] // 所有model
  public get modelList() {
    return this._modelList
  }
  @Mutation
  public setModelList(models: Array<any>) {
    this._modelList = models
  }
  //  所有模型
  @Action({ commit: 'setModelList' })
  public async queryAllModelList(projectId: any, type: any | null) {
    let modelList: any = null
    const response = await queryModelList({
      data: { projectId, status: type === 'all' ? null : type },
    })
    if (response.data.code === 100) {
      modelList = response.data.result
    }
    return modelList
  }

  // 算子源---页面算子列表
  public get projectOperator() {
    return this._projectOperator
  }

  // 可视化对比状态
  private _comparisonStatus: boolean = false

  @Mutation
  public setComparisonStatus(status: boolean) {
    this._comparisonStatus = status
  }

  public get comparisonStatus() {
    return this._comparisonStatus
  }

  // 可视化对比的节点 Ids
  private _comparisonNodes: any[] = []
  public get comparisonNodes() {
    return this._comparisonNodes
  }

  // pipeline 删除节点二次确认
  private _pipelineDeleteConfirm: boolean = true

  public get pipelineDeleteConfirm() {
    return this._pipelineDeleteConfirm
  }

  @Mutation
  public setPipelineDeleteConfirm(status: any) {
    this._pipelineDeleteConfirm = status
  }

  @Action({ commit: 'setPipelineDeleteConfirm' })
  public async getPipelineDeleteConfirm() {
    const {
      data: {
        result: { nodeDeletionSecondaryConfirmation },
      },
    } = await getUserConfig()
    return nodeDeletionSecondaryConfirmation === 1
  }

  public get comparisonNodeIds() {
    return this._comparisonNodes.map((node: any) => node.id)
  }

  @Mutation
  public setComparisonNodes(nodes: any[]) {
    this._comparisonNodes = nodes
  }

  // pipeline 中选择的节点对比状态
  @Action({ commit: 'setComparisonNodes' })
  public async changeComparisonNodes(node: any) {
    const nodes = cloneDeep(this._comparisonNodes)
    const nodeIds = nodes.map((item: any) => item.id)
    if (nodeIds.includes(node.id)) {
      nodes.splice(nodeIds.indexOf(node.id), 1)
    } else {
      nodes.push(node)
    }
    return nodes
  }

  // 保存数据源相关弹窗控制
  public get saveToDmVisible() {
    return this._saveToDmVisible
  }

  public get pipelineSnapshotId() {
    return this._pipelineSnapshotId
  }

  @Mutation
  public setSaveToDmVisible(value: boolean) {
    this._saveToDmVisible = value
  }

  public get getOldItems() {
    return this.oldItems
  }

  public get getFavoriteStatus() {
    return this.favoriteStatus
  }

  public get getHistoryStatus() {
    return this.historyStatus
  }

  public get getSelectFunctionIndex() {
    return this.selectFunctionIndex
  }

  public get getOverSaveVisible() {
    return this.overSaveVisible
  }

  public get getCurrentData() {
    return this.currentData
  }

  public get getDatasetName() {
    return this.datasetName
  }

  public get getVisualWindowShow() {
    return this.visualWindowShow
  }

  public get getPipelineId() {
    return this.pipelineId
  }

  public get getTabName() {
    return this.tabName
  }

  public get projectId() {
    return this._projectId
  }

  // 项目是否锁定
  public get getLock() {
    return this._lock
  }

  public get locateNodes() {
    return this._locateNodes
  }

  @Mutation
  public setOldItems(items: any) {
    this.oldItems = items
  }

  @Mutation
  public setFavoriteStatus(favoriteStatus: boolean) {
    this.favoriteStatus = favoriteStatus
  }

  @Mutation
  public setHistoryStatus(historyStatus: boolean) {
    this.historyStatus = historyStatus
  }

  @Mutation
  public setSelectFunctionIndex(selectFunctionIndex: number | null) {
    this.selectFunctionIndex = selectFunctionIndex
  }

  @Mutation
  public setOverSaveVisible(overSaveVisible: boolean) {
    this.overSaveVisible = overSaveVisible
  }

  @Mutation
  public setCurrentData(currentData: any) {
    this.currentData = currentData
  }

  @Mutation
  public setPipelineId(pipelineId: number | null) {
    this.pipelineId = pipelineId
  }

  @Mutation
  public setTabName(tabName: string | null) {
    this.tabName = tabName
  }

  // @Mutation
  // public setUserRole(userRole: string | null) {
  //   this._userRole = userRole
  // }

  @Mutation
  public setProjectId(projectId: number | null) {
    this._projectId = projectId
  }

  @Mutation
  public setLock(lock: boolean) {
    this._lock = lock
  }

  @Mutation
  public setLocateNodes(array: any[]) {
    this._locateNodes = array
  }

  @Mutation
  public setDatasetName(name: string) {
    this.datasetName = name
  }

  @Mutation
  public setVisualWindowShow(status: boolean) {
    this.visualWindowShow = status
  }

  @Mutation
  public setCurrentHistory(history: IKeyValueStore | null) {
    this._currentHistory = history
  }

  @Mutation
  public setIsEditHistory(flag: boolean) {
    this._isEditHistory = flag
  }

  @Mutation
  public setEditNodeId(id: number) {
    this._editNodeId = id
  }

  // 数据源---页面获取数据源
  public get projectData() {
    return this._projectData
  }

  public get editNodeId() {
    return this._editNodeId
  }

  public get selectNode() {
    return this._selectNode
  }

  public get selectModel() {
    return this._selectModel
  }

  public get historyList() {
    return this._historyList
  }

  public get currentHistory() {
    return this._currentHistory
  }

  public get tableColumns() {
    return this._tableColumns
  }

  public get tableData() {
    return this._tableData
  }

  public get tableCurPage() {
    return this._tableCurPage
  }

  public get tableHeight() {
    return this._tableHeight
  }

  public get isEditHistory() {
    return this._isEditHistory
  }

  // pipeline 中选择的节点
  @Action({ commit: 'setSelectNode' })
  public async changeSelectNode(node: any) {
    return node
  }

  // 数据源--query data list
  @Action({ commit: 'setProjectData' })
  public async loadProjectData(projectId: number) {
    if (!UserStore.currentUser) {
      await UserStore.queryUserInfo()
    }
    const postData = {
      projectId,
    }
    // dev
    // const response = await queryDatalist({data: postData})
    const response = await queryProjectDataset({ data: postData })
    response.data.result.forEach((element: { userName: string }) => {
      if (element.userName === this.userName) {
        element.userName = '我'
      }
    })
    // console.log(response.data.result,this.userName)
    return response.data.result
  }

  // 数据源--加载数据
  @Action({ commit: 'setProjectData' })
  public async importData(parameters: any) {
    const postData = {
      datasetIds: parameters.checkDataIds,
      projectId: parameters.projectId,
    }
    const result = await importData({ data: postData })
    if (result.data.code === 100) {
      message.success(result.data.message)
    } else {
      message.error(result.data.message)
    }
    // dev
    // const response = await queryDatalist({data: {projectId: postData.projectId}})
    const response = await queryProjectDataset({
      data: { projectId: postData.projectId },
    })
    response.data.result.forEach((element: { userName: string }) => {
      if (element.userName === this.userName) {
        element.userName = '我'
      }
    })
    return response.data.result
  }

  // 当删除节点的时候, 选择的节点是否更新（ 包含）
  @Action({ commit: 'setSelectNode' })
  public deleteNodeUpdateSelectNode(nodes: any[]) {
    if (
      this.selectNode &&
      nodes.some((item: any) => item.id === this.selectNode.id)
    ) {
      this.setVRPanelShow(false) // 关闭可视化智能推荐
      return null
    }
    // 清空 操作 nodes 为 []
    if (nodes.length === 0) {
      this.setVRPanelShow(false)
      return null
    }
    return this.selectNode
  }

  // @Action({ commit: 'setUserRole' })
  // public async getUserRole(parameters: any) {
  //   const result = await getUserRole({
  //     data: { projectId: parameters.projectId },
  //   })
  //   let userRole: any = null
  //   if (result.data.code === 100) {
  //     userRole = result.data.result
  //   } else {
  //     message.error(result.data.message)
  //   }
  //   return userRole
  // }

  @Mutation
  // 选择的model
  public setSelectModel(model: any) {
    this._selectModel = model
  }

  // 数据源
  @Mutation
  private setProjectData(data: Array<Data>) {
    this._projectData = data
  }

  // 选择的node
  @Mutation
  public setSelectNode(node: any) {
    if (!node) {
      this.selectNodeTableInfo = null
    } else {
      const { id, data = {}, type } = node
      const output = (data.output || [])[0]
      let tableName = output ? output.tableName : ''
      if (tableName?.endsWith?.('_')) {
        // 算子节点表名以下划线结尾，需要加上时间戳
        tableName += data?.lastTimeStamp ?? ''
      }
      if (!(+type === 5 && id === this.selectNodeTableInfo?.nodeId)) {
        // 清洗节点且节点id未变 时抛弃表名的修改，这时候表名以清洗历史列表的表名为准
        this.selectNodeTableInfo = {
          nodeId: id,
          tableName,
        }
      }
    }
    this._selectNode = node
  }

  @Mutation
  public setSelectNodeTableInfo(info: SelectNodeTableInfo | null) {
    this.selectNodeTableInfo = info
  }

  @Mutation
  public setHistoryList(data: any) {
    this._historyList = data.historyList
    this._currentHistory = data.currentHistory
    if (data.currentHistory) {
      this.selectNodeTableInfo = {
        nodeId: this.selectNodeTableInfo?.nodeId || '',
        tableName: data.currentHistory.data.table,
      }
    }
  }

  @Mutation
  public setTableData(data: any) {
    if (data.mode !== 'detail') {
      // detail: 只是table detail 发生变化(适用于表格滚动加载或者head不变的情况下进行数据筛选), all: header和detail都变化
      const stamp = +new Date()
      data.head.forEach((item: IKeyValueStore) => {
        item._stamp = stamp
      })
    }
    this._tableColumns = data.head.map((column: any) => {
      if (column.semantic === 'category') {
        return {
          ...column,
          semantic: null,
        }
      }
      return column
    })
    this._tableData = data.data
  }

  @Mutation
  public setTableCurPage(data: number) {
    this._tableCurPage = data
  }

  @Mutation
  public setTableHeight(height: number) {
    this._tableHeight = height
  }

  @Action({ commit: 'setHistoryList' })
  public async getHistoryList(params?: any) {
    const response = await apiGetHistoryList({
      data: {
        taskId: this._selectNode.id,
        ...params,
      },
    })
    const data = response.data.result
    const errorIndex = data.findIndex(
      (item: any) => item.data.status !== 'SUCCESS'
    )
    let currentHistory: IKeyValueStore = {}
    if (this.lastHistoryCompleteId) {
      // 存在要定位的list
      const activeIndex = data.findIndex(
        (item: any) => item.data.id === this.lastHistoryCompleteId
      )
      if (activeIndex > -1 && errorIndex > -1) {
        // 都存在
        currentHistory =
          activeIndex >= errorIndex ? data[errorIndex - 1] : data[activeIndex]
      } else if (activeIndex === errorIndex) {
        // 均不存在
        ;[currentHistory] = data.slice(-1)
      } else if (activeIndex > -1) {
        // errorIndex: -1, activeIndex > -1
        currentHistory = data[activeIndex]
      } else {
        // errorIndex > -1, activeIndex: -1
        currentHistory = data[errorIndex - 1]
      }
    } else {
      currentHistory =
        errorIndex > -1 ? data[errorIndex - 1] : data.slice(-1)[0]
    }

    return {
      historyList: data,
      currentHistory,
    }
  }

  /**
   * 添加操作历史记录
   */
  @Action
  public async addHistory(params?: any) {
    const response = await apiAddHistory({
      data: {
        projectId: this._selectNode.projectId,
        taskId: this._selectNode.id,
        taskName: this._selectNode.name,
        ...params,
      },
    })
    return response.data
  }

  /**
   * 检测formula 的值是否能正常运行
   */
  @Action
  public async checkFormula(params?: any) {
    const response = await checkFormula({
      data: {
        projectId: this._selectNode.projectId,
        taskId: this._selectNode.id,
        taskName: this._selectNode.name,
        ...params,
      },
    })
    return response.data
  }

  /**
   * 检测formula 的值是否能正常运行
   */
  @Action({ commit: 'setLinkedTableList' })
  public async getTableList() {
    const response = await queryInputTables({
      data: {
        id: this._selectNode.id,
      },
    })

    if (response.data.code === 100) {
      return response.data.result
    }
    return []
  }

  /**
   * 更新操作历史记录
   */
  @Action
  public async updateHistory(params?: any) {
    const response = await apiUpdateHistory({
      data: {
        projectId: this._selectNode.projectId,
        taskId: this._selectNode.id,
        taskName: this._selectNode.name,
        ...params,
      },
    })
    return response.data
  }

  /**
   * 删除操作历史记录
   * @param params id: xxx 要删除的历史记录id
   */
  @Action
  public async delHistory(params: any) {
    const response = await apiDelHistory({
      data: {
        projectId: this._selectNode.projectId,
        taskId: this._selectNode.id,
        ...params,
      },
    })
    return response.data
  }

  public get columnFilters() {
    return this._columnFilters
  }

  @Mutation
  public setColumnFilters(columnFilters: any[]) {
    if (!isEqual(this._columnFilters, columnFilters)) {
      this._columnFilters = columnFilters
    }
  }

  @Mutation
  public setDataTableDetailLoading(status: boolean) {
    this.isTableDetailLoading = status
  }

  // 2020/11/11
  private _pipelineNodes: any[] = [] // pipeline 所有节点
  public get pipelineNodes() {
    return this._pipelineNodes
  }

  // pipeline 所有节点
  @Mutation
  private setPipelineNodes(nodes: Array<any>) {
    this._pipelineNodes = nodes
  }

  //  所有节点
  @Action({ commit: 'setPipelineNodes' })
  public async queryPipelineNodes(
    parameters: any = { id: this._projectId, pipelineId: this.pipelineId }
  ) {
    let nodeList: any = null
    const response = await allNodeList({ data: parameters })
    if (response.data.code === 100) {
      nodeList = response.data.result
      // if(this._selectNode) {
      //   this.setSelectNode(nodeList.filter((node: any) => node.id === this._selectNode.id)[0])
      // }
    }
    return nodeList
  }

  // 清空 pipeline nodes
  @Action({ commit: 'setPipelineNodes' })
  public async initPipelineNodes() {
    return []
  }

  // 节点确认编辑节点配置时间戳
  private _confirmEditTime: number = 0
  public get confirmEditTime() {
    return this._confirmEditTime
  }

  @Mutation
  public setConfirmEditTime(time: number) {
    this._confirmEditTime = time
  }

  // 执行日志
  private _runLogs: any[] = []
  public get runLogs() {
    return this._runLogs
  }

  @Mutation
  public setRunLogs(logs: any[]) {
    this._runLogs = logs
  }

  //  刷新单个节点 更改 selectNode
  @Action({ commit: 'setSelectNode' })
  public async refreshTaskQueryById(id: number) {
    const response = await taskQueryById({
      data: { id, projectId: this.projectId },
    })
    let node: any = null
    if (response.data.code === 100) {
      node = response.data.result
      // 更新pipeline
      const parameters = {
        id: node.projectId,
        pipelineId: node.pipelineId,
      }
      const allNodes: any[] = await this.queryPipelineNodes(parameters)
      ;[node] = allNodes.filter((n) => n.id === node.id)
    }
    return node
  }

  // 当前节点添加清洗
  @Action({ commit: 'setSelectNode' })
  public async selectNodeAddCleanNode() {
    const { addCol, addRow } = ComputeSpaceAfterNode(this._pipelineNodes, {
      nodePosition: this._selectNode.data.position,
    })
    let node: any = null
    const parameters = {
      name: '清洗',
      projectId: this._selectNode.projectId,
      pipelineId: this._selectNode.pipelineId,
      parentId: this._selectNode.id,
      childId: null,
      type: 5, // 类型: 3--表示数据节点， 1--算子， 2--ETL算子
      data: {
        algName: '清洗',
        algType: 1,
        position: {
          row: addRow,
          col: addCol,
        },
      },
    }
    const response = await taskComplexLoad({ data: parameters })
    if (response.data.code === 100) {
      node = response.data.result
      const parameters2 = {
        id: node.projectId,
        pipelineId: node.pipelineId,
      }
      await this.queryPipelineNodes(parameters2)
    }
    return node
  }

  // StudioSideMenu collapsed 状态 (数据视图 & 可视化视图)
  private _collapsed: boolean = false

  @Mutation
  public setCollapsed(collapsed: boolean) {
    this._collapsed = collapsed
  }

  public get collapsed() {
    return this._collapsed
  }

  // StudioSideMenu 视图菜单是否显示
  @Mutation
  public setSideMenu(isShow: boolean) {
    this._isSideMenuShow = isShow
  }

  public get isSideMenuShow() {
    return this._isSideMenuShow
  }

  // Studio SidePanel 数据源算子面板是否显示
  @Mutation
  public setSidePanel(isShow: boolean) {
    this._isSidePanelShow = isShow
  }

  public get isSidePanelShow() {
    return this._isSidePanelShow
  }

  // Edit panel 左侧算子配置面板是否显示
  @Mutation
  public setIsShowOperatorParameter(isShow: boolean) {
    this._isShowOperatorParameter = isShow
  }

  public get isShowOperatorParameter() {
    return this._isShowOperatorParameter
  }

  private _nodeEditPanelIsFullScreen: boolean = false

  @Mutation
  public setNodeEditPanelIsFullScreen(isFullScreen: boolean) {
    this._nodeEditPanelIsFullScreen = isFullScreen
  }

  public get nodeEditPanelIsFullScreen() {
    return this._nodeEditPanelIsFullScreen
  }

  // 节点编辑面板数据透视是否展开
  private _nodeEditPanelIsShowVisualization: boolean = false

  @Mutation
  public setNodeEditPanelIsShowVisualization(isShow: boolean) {
    this._nodeEditPanelIsShowVisualization = isShow
  }

  public get nodeEditPanelIsShowVisualization() {
    return this._nodeEditPanelIsShowVisualization
  }

  @Mutation
  public toggleSelectedColumn(column: IColumn) {
    const columns = cloneDeep(this._selectedColumns)
    const index = columns.findIndex((c) => c.name === column.name)
    if (index !== -1) {
      columns.splice(index, 1)
    } else {
      columns.push(column)
    }
    this._selectedColumns = columns
  }

  public get selectedColumns() {
    return this._selectedColumns
  }

  @Mutation
  public setSelectedColumns(selectedColumn: IColumn[]) {
    this._selectedColumns = selectedColumn
  }

  public get mergeColumnHotKey() {
    return this._mergeColumnHotKey
  }

  @Mutation
  public setMergeColumnHotKey(status: boolean) {
    this._mergeColumnHotKey = status
  }

  @Mutation
  public clearSelectedColumns() {
    if (this._selectedColumns.length > 0) {
      this._selectedColumns = []
    }
  }

  public get nodeEditPanelHeight() {
    return this._nodeEditPanelHeight
  }

  @Mutation
  public setNodeEditPanelHeight(height: number) {
    this._nodeEditPanelHeight = height
  }

  // 执行完成时间
  public get executionCompletionTime() {
    return this._executionCompletionTime
  }

  @Mutation
  public setExecutionCompletionTime(time: number) {
    this._executionCompletionTime = time
  }

  public _executionCompletionType: boolean = false

  // 执行完成时间 状态
  public get executionCompletionType() {
    return this._executionCompletionType
  }

  @Mutation
  public setExecutionCompletionType(type: boolean) {
    this._executionCompletionType = type
  }

  public _executionStart: any = false

  // 执行完成时间 状态
  public get executionStart() {
    return this._executionStart
  }

  @Mutation
  public setExecutionStart(type: any) {
    this._executionStart = type
  }

  // 节点状态 map
  public get nodeStatusMap() {
    return this._nodeStatusMap
  }

  @Mutation
  public saveNodeStatusMap(statusMap: NodeStatusMap) {
    this._nodeStatusMap = statusMap
  }

  // 日志刷新的时候
  @Action
  public setNodeStatusMap(statusMap: NodeStatusMap) {
    this.context.commit('saveNodeStatusMap', statusMap)
    // 经济算子提示
    if (
      this._selectNode &&
      this._selectNode.type === 8 &&
      this._nodeStatusMap[this._selectNode.id]
    ) {
      const nodeMessage: any = JSON.parse(
        this._nodeStatusMap[this._selectNode.id].logInfo
      ).message
      const messageIsEqual: boolean = isEqual(
        nodeMessage,
        this._showTaskPromptText
      )
      if (!messageIsEqual && nodeMessage) {
        this.context.commit('setShowTaskPromptText', nodeMessage)
      }
      if (nodeMessage) {
        this.context.commit('setShowTaskPrompt', true)
      } else {
        this.context.commit('setShowTaskPrompt', false)
      }
    }
  }

  // 切换节点， 更新节点提示
  @Action
  public dealNodePrompt() {
    // 经济算子提示
    if (
      this._selectNode &&
      this._selectNode.type === 8 &&
      this._nodeStatusMap[this._selectNode.id]
    ) {
      const nodeMessage: any = JSON.parse(
        this._nodeStatusMap[this._selectNode.id].logInfo
      ).message
      if (nodeMessage) {
        this.context.commit('setShowTaskPromptText', nodeMessage)
        this.context.commit('setShowTaskPrompt', true)
      } else {
        this.context.commit('setShowTaskPromptText', '')
        this.context.commit('setShowTaskPrompt', false)
      }
    } else {
      this.context.commit('setShowTaskPromptText', '')
      this.context.commit('setShowTaskPrompt', false)
    }
  }

  public get fullAmountTask() {
    return this._fullAmountTask
  }

  @Mutation
  public setFullAmountTask(status: boolean) {
    this._fullAmountTask = status
  }

  public get savingWidgetConfig() {
    return this._savingWidgetConfig
  }

  @Mutation
  public setSavingWidgetConfig(widgetConfig: any) {
    this._savingWidgetConfig = widgetConfig
  }

  @Action({ commit: 'setSavingWidgetConfig' })
  public saveRecommendVisualization(widgetConfig: any) {
    return widgetConfig
  }

  // 多选的节点id
  public get multiSelectNodeArray() {
    return this._multiSelectNodeArray.map((node: any) => {
      return { id: node.id, name: node.name }
    })
  }

  // 多选的节点id + selectNodes
  public get multiWithSelectNodes() {
    const multiNodeIds = cloneDeep(this.multiSelectNodeArray)
    if (!isEmpty(this.selectNode)) {
      const isExists = multiNodeIds.some(
        (item: any) => item.id === this.selectNode.id
      )
      if (!isExists) {
        multiNodeIds.push({
          id: this.selectNode.id,
          name: this.selectNode.name,
        })
      }
    }
    return multiNodeIds
  }

  // 被定位的节点

  @Mutation
  public setMultiSelectNodeArray(nodes: any[]) {
    this._multiSelectNodeArray = nodes
  }

  @Mutation
  public addMultiSelectNodeArray(node: any) {
    // TODO 如果存在
    const isExists = this._multiSelectNodeArray.findIndex((item: any) => {
      return item.id === node.id
    })
    if (isExists > -1) {
      this._multiSelectNodeArray.splice(isExists, 1)
    } else {
      this._multiSelectNodeArray.push(node)
    }
  }

  @Mutation
  public clearMultiSelectNodeArray() {
    this._multiSelectNodeArray = []
  }

  @Mutation
  public clearLocateNodeArray() {
    this._locateNodes = []
  }

  @Mutation
  public resetTableColumn() {
    // 重置数据表格、列统计的信息
    this._tableColumns = []
    this._tableData = []
  }

  @Mutation
  public setAutojoinPreview(tableName: string | null) {
    this.autojoinTableName = tableName
  }

  /**
   * 重置数据清洗操作的状态
   */
  @Mutation
  public resetDataCleanAction() {
    this._isEditHistory = false // 关闭编辑状态
    this._mergeColumnHotKey = false // 关闭合并字段快捷键
    this._selectedColumns = [] // 关闭选中的字段
    this.ifOpenCreateNewColumn = false
  }

  /**
   * 切换字段语义
   * @param result {column:目标列, semantic:目标语义 }
   */
  @Mutation
  public async changeSemanticMutation(result: any) {
    const { response, semantic, column } = result
    if (response.data.success === true) {
      const index = this._tableColumns.findIndex((c) => c.name === column.name)
      this._tableColumns.splice(index, 1, {
        ...this._tableColumns[index],
        semantic,
      })
    }
  }

  @Action({ commit: 'changeSemanticMutation' })
  public async changeSemantic({
    column,
    semantic,
  }: {
    column: IColumn
    semantic: string
  }) {
    const response: any = await apiUpdateSemantic({
      data: {
        projectId: this._selectNode.projectId,
        taskId: this._selectNode.id,
        data: {
          col: column.name,
          semantic,
        },
      },
    })
    return { response, semantic, column }
  }

  @Mutation
  public setLastHistoryCompleteId(id: string) {
    this.lastHistoryCompleteId = id
  }

  /**
   * 获取表数据详情
   * @param params
   */
  @Action({ commit: 'setTableData' })
  public async queryTableData(params: any) {
    const response = await apiGetTableData({
      data: {
        curPage: 1,
        name: '_record_id_', // 如果没有字段，那就固定传 _record_id_
        pageSize: 50,
        filter: [],
        ...params,
      },
    })
    const result = response.data.result || {}

    return {
      head: result.head || [],
      data: result.data || [],
    }
  }

  @Action
  public async updateComment(params: any) {
    const response = await simpleUpdate({ data: params })

    return response
  }

  @Mutation
  public setGlobalSearchInfo(info: any) {
    this._globalSearchTime = info.time
    this._globalSearchColumns = info.columns
    this._tableDataGlobalSearchKey = info.key
  }

  @Mutation
  public setGlobalSearchTime(time: number) {
    this._globalSearchTime = time
  }

  public get globalSearchTime() {
    return this._globalSearchTime
  }

  @Mutation
  public setGlobalSearchColumns(columns: string[]) {
    this._globalSearchColumns = columns
  }

  public get globalSearchColumns() {
    return this._globalSearchColumns
  }

  @Mutation
  public setIsTableDataGlobalSearchMode(mode: boolean) {
    this._isTableDataGlobalSearchMode = mode
  }

  public get isTableDataGlobalSearchMode() {
    return this._isTableDataGlobalSearchMode
  }

  @Mutation
  public setTableDataGlobalSearchKey(key: string) {
    this._tableDataGlobalSearchKey = key
  }

  public get tableDataGlobalSearchKey() {
    return this._tableDataGlobalSearchKey
  }

  @Mutation
  public setTableDataGlobalSearchData(data: any[]) {
    if (!this._isTableDataGlobalSearchMode) {
      this._isTableDataGlobalSearchMode = true
    }
    this._tableDataGlobalSearchData = data
  }

  public get tableDataGlobalSearchData() {
    return this._tableDataGlobalSearchData
  }

  // 关闭搜索模式
  @Mutation
  public closeGlobalSearch() {
    this._isTableDataGlobalSearchMode = false
    this._tableDataGlobalSearchKey = ''
    this._tableDataGlobalSearchData = []
  }

  // 清空搜索关键字
  @Mutation
  public globalSearchEmpty() {
    this._tableDataGlobalSearchKey = ''
    this._tableDataGlobalSearchData = []
  }

  /**
   * 表数据详情 全局搜索
   */
  @Action
  public async tableDataGlobalSearch({ curPage, pageSize, table }: any) {
    const response = await apiMultiColumnSearch({
      data: {
        columns: this._globalSearchColumns,
        curPage,
        pageSize,
        search: this._tableDataGlobalSearchKey,
        table,
        taskId: this._selectNode.id,
      },
    })
    return response
  }

  public _nodeDataVis: any = {}

  @Mutation
  public setQueryDataVis(result: any) {
    this._nodeDataVis = result
  }

  public get getQueryDataVis() {
    return this._nodeDataVis
  }

  // 经济算子节点可视化
  @Action({ commit: 'setQueryDataVis' })
  public async taskQueryDataVis() {
    const response = await taskQueryDataVis({
      data: {
        id: this.selectNode.id,
        projectId: this.projectId,
      },
    })
    if (response.data.code === 100) {
      if (
        response.data.result &&
        Object.keys(response.data.result).length > 0
      ) {
        return response.data.result
      }
      return {}
    }
    return {}
  }

  public get showTaskPrompt() {
    return this._showTaskPrompt
  }

  @Mutation
  public setShowTaskPrompt(value: boolean) {
    this._showTaskPrompt = value
  }

  // 当前节点提示信息
  public get showTaskPromptText() {
    return this._showTaskPromptText
  }

  @Mutation
  public setShowTaskPromptText(value: string) {
    this._showTaskPromptText = value
  }
}

export default getModule(DataViewStore)
