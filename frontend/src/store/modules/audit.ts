/**
 * audit store
 * manage all states of audit
 */
import {
  Module,
  Mutation,
  VuexModule,
  getModule,
  Action,
} from 'vuex-module-decorators'
import store from '@/store'
import {
  queryAllUserInfo,
  queryDataInfoById,
  searchDatasetNames,
  queryAllDatasetInfo,
  adminAudit,
  queryTaskType,
  queryTaskInstanceInfo,
  queryUserFeedback,
  queryFeedbackById,
  sendNotice,
} from '@/api/audit'

// 用户信息
interface UserInfo {
  id: number
  name: string
  gmtCreator?: string
  gmtModifier?: string
  gmtCreate?: string
  gmtModify?: string
  datasetCnt?: number
  datasetSize?: string
  projectNum?: number
}

@Module({ dynamic: true, namespaced: true, name: 'AuditStore', store })
class AuditStore extends VuexModule {
  private _selectedUser: UserInfo | null = null // 当前用户信息
  private _selectedUserDatasetInfo: any = {} // 当前用户数据表信息

  public get selectedUser() {
    return this._selectedUser
  }
  public get selectedUserDatasetInfo() {
    return this._selectedUserDatasetInfo
  }

  @Mutation
  public setSelectedUser(value: any) {
    this._selectedUser = value
  }

  @Mutation
  public setSelectedUserDatasetInfo(value: any) {
    this._selectedUserDatasetInfo = value
  }

  @Mutation
  private clearUserInfo() {
    this._selectedUser = null
  }

  @Mutation
  public clearState() {
    this._selectedUser = null
  }

  // 获取用户列表
  @Action
  public async queryAllUserInfo(passData: any) {
    const response = await queryAllUserInfo({ data: passData })
    return response
  }

  // 获取用户数据表信息
  @Action({ commit: 'setSelectedUserDatasetInfo' })
  public async queryUserDatasetInfo(id: any) {
    const passData = {
      userId: id,
    }
    const response = await queryDataInfoById({ params: passData })
    if (response.data.code !== 100) {
      return {
        datasetsInfoDTOList: [],
        tableCnt: 0,
        totalSize: '0 bytes',
      }
    }
    return response.data.result
  }

  // 获取所有数据集信息
  @Action
  public async queryDatasetInfo(passData: any) {
    const response = await queryAllDatasetInfo({ data: passData })
    return response
  }

  // 模糊查询数据集
  @Action
  public async getDatasetNames(dataset: string) {
    const passData = {
      datasetName: dataset,
    }

    const response = await searchDatasetNames({ params: passData })

    return response
  }

  // 查询访问历史
  @Action
  public async getVisitHistory(passData: any) {
    const response = await adminAudit({ data: passData })
    return response
  }

  // 查询操作类型
  @Action
  public async getTaskType() {
    const response = await queryTaskType()
    return response
  }

  // 查询任务监测列表
  @Action
  public async getTaskList(passData: any) {
    const response = await queryTaskInstanceInfo({ data: passData })
    return response
  }

  // 查询用户反馈列表
  @Action
  public async getUserFeedback(passData: any) {
    const response = await queryUserFeedback({ data: passData })
    return response
  }

  // 根据id查询反馈内容
  @Action
  public async getFeedbackContent(passData: any) {
    const response = await queryFeedbackById({ params: passData })
    return response
  }

  // 发送私信
  @Action
  public async sendMessege(passData: any) {
    const response = await sendNotice({ data: passData })
    return response
  }
}

export default getModule(AuditStore)
