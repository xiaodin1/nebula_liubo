/**
 * data store
 * manage all states of datasets
 * @author Huihua Lu
 */
import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule,
} from 'vuex-module-decorators'
import store from '@/store'
import {
  cancelDatabaseImport,
  checkFileMd5,
  checkProgress,
  checkSchedule,
  createNewCategory,
  deleteAllSqlCategory,
  deleteDatasetById,
  deleteDatasetCategory,
  deleteGraphDataset,
  deleteSqlCategory,
  exportToCsv,
  exportToXlsx,
  generateMask,
  getDatabaseTables,
  getDataTypeImg,
  httpUpdateDataStatus,
  importDatabaseData,
  listDataLevel,
  listGraphDataset,
  listMaskingType,
  parseError,
  queryCurrentUserDatasetCategory,
  queryDataBySql,
  queryDatasetById,
  queryDataUploadProgress,
  querySqlCategory,
  queryTableData,
  queryUnread,
  renameCategory,
  repreviewData,
  selectActionByDataId,
  selectDatasets,
  testDatabaseConnection,
  updateConfig,
  updateDataset,
  updateGraphDataset,
  uploadDataset,
  uploadGraphDataset,
  writeDataset,
  writeGraphDataset,
} from '@/api/data'
import { message } from 'ant-design-vue'

// 数据集
export interface Dataset {
  id: number
  name: string
  type: string
}

// 数据集分类
export interface DatasetCategory {
  categoryId: number
  categoryName: string
  dataset: Array<Dataset>
}

export interface MessageModalOption {
  visible: boolean
  message: string
  tips: string
}

// 数据库链接测试状态
export enum DatabaseTestState {
  BEFORE_TEST = 'BEFORE_TEST',
  TESTING = 'TESTING',
  TEST_SUCCEED = 'TEST_SUCCEED',
  TEST_FAILED = 'TEST_FAILED',
}

@Module({ dynamic: true, namespaced: true, name: 'DataStore', store })
class DataStore extends VuexModule {
  private _datasetCategory: Array<DatasetCategory> = [] // 当前用户的所有数据集分类
  private _datasetCodingType: Array<string> = [
    'utf-8',
    'ASCII',
    'GB18030',
    'Unicode',
  ] // 导入数据编码方式

  // 查看数据
  private _selectedDatasetId: number = -1 // 选择查看的数据集id
  private _preSelectedDataset: Array<Dataset> = []

  private _selectedDatasetInfo: any = null // 数据集基本信息

  // 上传数据
  private _isUploading: boolean = false // 当前是否在导入数据界面
  private _dataIsUploading: boolean = false // 是否展示进度加载页面
  private _beginDatabaseUpload: boolean = false // 是否开始数据库的导入
  private _previewedDataset: any = null // 上传文件后的预览数据 （21.08.26 修改每次修改编码 是否首行做字段名称都上传源文件 不在使用）
  private _writeDatasetSucceed: boolean = false // 是否成功创建数据集

  // 导入数据库
  private _datasetEditModalVisible: boolean = false // 是否打开数据配置
  private _testState: DatabaseTestState = DatabaseTestState.BEFORE_TEST // 数据库链接测试状态
  private _databaseOptions: Array<string> = [] // 所有可选的数据库名
  private _databaseTables: Array<string> = [] // 所有可选的数据库表
  private _databaseConfig: any = null // 数据库的一些参数，如server,port等
  private _selectedTables: Array<string> = [] // 选中的数据表
  private _selectedTableData: any = null // 选择预览的数据库表数据

  private _messageModalOption: MessageModalOption = {
    visible: false,
    message: '',
    tips: '',
  }

  // 导入数据部分
  private _selectedImportType: string = 'table'

  // 2021 01 25 脱敏
  private _listDataLevel: any[] = [] // 脱敏类型
  private _listMaskingType: any[] = [] // 脱敏方式

  // 脱敏和定时合并配置
  private _datasetConfigModalVisible: boolean = false // 是否打开数据配置
  private _isConfigSubmit: boolean = false // 脱敏/定时配置点击提交
  private _isSaveDesensitization: boolean = false // 脱敏配置保存

  //  选中的dataset and category
  private _datasetId: string = ''
  private _categoryId: string = ''
  // 导入外部http数据弹框
  private _httpModalVisible: boolean = false

  // 外部数据导入定时设置弹框
  private _timingSettingModalVisible: boolean = false

  // csv提交失败弹框
  private _submitFailModalVisible: boolean = false

  // sql查询
  private _isSqlSearch: boolean = false // 当前是否在sql查询界面

  // 图文件列表
  private _graphFileList: Array<string> = []
  public selectedGraphFileName: string = ''

  // 元数据信息列表
  private _datasetsList: any[] = []

  private _datasetEditPanelVisible: boolean = false // 元数据管理中的编辑面板

  private _dataTabIndex: string = '1' // 数据管理tab标签

  private _isEditSubmit: boolean = false

  private _isClearForm: boolean = false

  private _scheduleList: any[] = []

  private _unreadList: any[] = []

  public get datasetId() {
    return this._datasetId
  }

  public get categoryId() {
    return this._categoryId
  }

  public get datasetCodingType() {
    return this._datasetCodingType
  }

  public get datasetCategory() {
    return this._datasetCategory
  }

  public get selectedDatasetId() {
    return this._selectedDatasetId
  }

  public get preSelectedDataset() {
    return this._preSelectedDataset
  }

  public get selectedDatasetInfo() {
    return this._selectedDatasetInfo
  }

  public get isUploading() {
    return this._isUploading
  }

  public get dataIsUploading() {
    return this._dataIsUploading
  }

  public get beginDatabaseUpload() {
    return this._beginDatabaseUpload
  }

  public get writeDatasetSucceed() {
    return this._writeDatasetSucceed
  }

  // abandoned
  public get previewedDataset() {
    return this._previewedDataset
  }

  public get testState() {
    return this._testState
  }

  public get databaseOptions() {
    return this._databaseOptions
  }

  public get databaseTables() {
    return this._databaseTables
  }

  public get datasetEditModalVisible() {
    return this._datasetEditModalVisible
  }

  public get isConfigSubmit() {
    return this._isConfigSubmit
  }

  public get isSaveDesensitization() {
    return this._isSaveDesensitization
  }

  public get datasetConfigModalVisible() {
    return this._datasetConfigModalVisible
  }

  public get databaseConfig() {
    return this._databaseConfig
  }

  public get selectedTables() {
    return this._selectedTables
  }

  public get selectedTableData() {
    return this._selectedTableData
  }

  public get messageModalOption() {
    return this._messageModalOption
  }

  public get httpModalVisible() {
    return this._httpModalVisible
  }

  public get timingSettingModalVisible() {
    return this._timingSettingModalVisible
  }

  public get submitFailModalVisible() {
    return this._submitFailModalVisible
  }

  public get isSqlSearch() {
    return this._isSqlSearch
  }

  public get graphFileList() {
    return this._graphFileList
  }

  public get datasetsList() {
    return this._datasetsList
  }

  public get datasetEditPanelVisible() {
    return this._datasetEditPanelVisible
  }

  public get dataTabIndex() {
    return this._dataTabIndex
  }

  public get isEditSubmit() {
    return this._isEditSubmit
  }

  public get isClearForm() {
    return this._isClearForm
  }

  public get scheduleList() {
    return this._scheduleList
  }

  public get unreadList() {
    return this._unreadList
  }

  public get selectedImportType() {
    return this._selectedImportType
  }

  // 获取当前用户未读信息
  @Action({ commit: 'setUnreadList' })
  public async loadUnreadList() {
    const response = await queryUnread()
    if (response.data.success) {
      return response.data.result
    }
    return []
  }

  @Action({ commit: 'setUnreadList' })
  public async updateUnreadList(id: number) {
    const resultList = this._unreadList
    resultList.forEach((item) => {
      if (item.datasetId === id) {
        item.unreadNum = 0
      }
    })
    return resultList
  }

  // 获取当前用户所有数据集及分类
  @Action({ commit: 'setDatasetCategory' })
  public async loadDatasetCategory() {
    const response = await queryCurrentUserDatasetCategory()
    return response.data.result
  }

  // 删除分类
  @Action({ commit: 'setDatasetCategory' })
  public async deleteCategory(categoryId: number) {
    const passData = {
      id: categoryId,
    }
    const deleteResponse = await deleteDatasetCategory({ data: passData })

    const response = await queryCurrentUserDatasetCategory()

    if (deleteResponse.data.code === 100) {
      message.success('删除成功！')
    } else {
      message.error(
        `${deleteResponse.data.message} ${deleteResponse.data.tips}`
      )
    }

    return response.data.result
  }

  // 重命名数据集目录
  @Action({ commit: 'setDatasetCategory' })
  public async renameDatasetCategory(passData: any) {
    await renameCategory({ data: passData })

    const response = await queryCurrentUserDatasetCategory()
    return response.data.result
  }

  // 重命名数据集 移动数据集目录
  @Action
  public async updateDataset(passData: any) {
    const updateResponse = await updateDataset({ data: passData })

    const response = await queryCurrentUserDatasetCategory()
    this.context.commit('setDatasetCategory', response.data.result)
    return updateResponse
  }

  // 重命名图文件数据集
  @Action
  public async updateGraphDataset(passData: any) {
    const updateResponse = await updateGraphDataset({ data: passData })

    const response = await listGraphDataset()
    this.context.commit('setGraphFileList', response.data.result)
    return updateResponse
  }

  // 创建数据集目录
  @Action
  public async submitNewDatasetCategory(newName: string) {
    const response = await createNewCategory({ data: { name: newName } })
    return response.data.result
  }

  // 创建数据集分类成功后再次刷新分组信息
  @Action({ commit: 'setDatasetCategory' })
  public async updateCurrentUserDatasetCategory() {
    const response = await queryCurrentUserDatasetCategory()
    return response.data.result
  }

  // 获取指定数据集数据
  @Action({ commit: 'setSelectedDatasetInfo' })
  public async queryDataset(datasetId: number) {
    const passData = { id: datasetId }
    const response = await queryDatasetById({ data: passData })

    const { dataConfig } = response.data.result
    // 如果不为null和undefined
    if (dataConfig && typeof dataConfig === 'object') {
      // 重设数据配置
      const {
        databaseName,
        databaseType,
        password,
        port,
        server,
        user,
      } = dataConfig
      this.setDatabaseConfig({
        databaseName,
        databaseType,
        password,
        port,
        server,
        user,
      })
    } else {
      this.setDatabaseConfig(dataConfig)
    }
    return response.data.result
  }

  // 删除数据集
  @Action
  public async deleteDataset(passData: any) {
    const deleteResponse = await deleteDatasetById({ data: passData })

    const response = await queryCurrentUserDatasetCategory()
    this.context.commit('setDatasetCategory', response.data.result)

    if (deleteResponse.data.code === 500) {
      message.error(deleteResponse.data.tips)
    } else if (deleteResponse.data.code === 100) {
      message.success('删除成功！')
    }

    return deleteResponse
  }

  // 校验文件表格数据的md5值
  @Action
  public async checkFileMd5(passData: any) {
    try {
      const response = await checkFileMd5({ data: passData })
      return response
    } catch (error) {
      throw new Error(error)
    }
  }

  // 上传表格型数据集
  @Action
  public async uploadDatasetFile(passData: any) {
    try {
      const response = await uploadDataset({ data: passData })
      this.context.commit('setPreviewedDataset', response.data.result)
      return response
    } catch (error) {
      throw new Error(error)
    }
  }

  // 选择编码后重新获取预览数据 abandoned， 重新编码也上传源文件 使用 uploadDatasetFile
  @Action
  public async reUploadDatasetFile(passData: any) {
    try {
      const response = await repreviewData({ data: passData })
      this.context.commit('setPreviewedDataset', response.data.result)
      return response
    } catch (error) {
      throw new Error(error)
    }
  }

  // 获取已经上传文件块的处理进度
  @Action
  public async checkProgress(passData: any) {
    try {
      return await checkProgress({ data: passData })
    } catch (error) {
      throw new Error(error)
    }
  }

  // 提交表格型数据集
  @Action
  public async writeFinalDataset(passData: any) {
    this.context.commit('setWriteDatasetSucceed', false)
    const writeResponse = await writeDataset({ data: passData })
    if (writeResponse.data.code === 100) {
      this.context.commit('setWriteDatasetSucceed', true)
      const response = await queryCurrentUserDatasetCategory()
      this.context.commit('setDatasetCategory', response.data.result)
      this.context.commit(
        'setSelectedDatasetId',
        writeResponse.data.result.datasetId
      )
    }
    return writeResponse
  }

  // 提交带错误的表格型数据集
  @Action
  public async writeErrorFinalDataset(passData: any) {
    this.context.commit('setWriteDatasetSucceed', false)
    const writeResponse = await writeDataset({ data: passData })
    if (writeResponse.data.success) {
      this.context.commit('setWriteDatasetSucceed', true)
      const response = await queryCurrentUserDatasetCategory()
      this.context.commit('setDatasetCategory', response.data.result)
      this.context.commit(
        'setSelectedDatasetId',
        writeResponse.data.result.datasetId
      )
    }
    return writeResponse
  }

  // 列图数据文件
  @Action({ commit: 'setGraphFileList' })
  public async listGraphFile() {
    try {
      const response = await listGraphDataset()
      return response.data.result
    } catch (error) {
      throw new Error(error)
    }
  }

  // 删除图数据
  @Action
  public async deleteGraphFile(passData: any) {
    try {
      const writeResponse = await deleteGraphDataset({ data: passData })
      const response = await listGraphDataset()
      this.context.commit('setGraphFileList', response.data.result)
      return writeResponse
    } catch (error) {
      throw new Error(error)
    }
  }

  // 上传图数据
  @Action
  public async uploadGraphFile(passData: any) {
    try {
      const response = await uploadGraphDataset({ data: passData })
      this.context.commit('setPreviewedDataset', response.data.result)
      return response
    } catch (error) {
      throw new Error(error)
    }
  }

  // 提交图数据
  @Action
  public async writeGraphDataset(passData: any) {
    this.context.commit('setWriteDatasetSucceed', false)
    const writeResponse = await writeGraphDataset({ data: passData })
    if (writeResponse.data.code === 100) {
      this.context.commit('setWriteDatasetSucceed', true)
      const response = await listGraphDataset()
      this.context.commit('setGraphFileList', response.data.result)
      // this.context.commit('setDatasetCategory',response.data.result)
      // this.context.commit('setSelectedDatasetId',writeResponse.data.result)
    }
    return writeResponse
  }

  // 提交候选中对应的http表
  @Action
  public async updateHttpDataSet(dataId: string) {
    const response = await queryCurrentUserDatasetCategory()
    this.context.commit('setDatasetCategory', response.data.result)
    this.context.commit('setSelectedDatasetId', dataId)
    return response
  }

  // 数据库链接测试，并获取所有数据库
  @Action
  public async testConnection(passData: any) {
    this.context.commit('setTestState', DatabaseTestState.TESTING)
    /** 解决await无法处理接口reject的情况 */
    try {
      const response = await testDatabaseConnection({ data: passData })
      if (response.data.code === 100) {
        this.context.commit('setTestState', DatabaseTestState.TEST_SUCCEED)
      } else {
        this.context.commit('setTestState', DatabaseTestState.TEST_FAILED)
      }
      this.context.commit('setDatabaseOptions', response.data.result)
    } catch (error) {
      console.log('testConnection error:', error)
      this.context.commit('setTestState', DatabaseTestState.TEST_FAILED)
    }
  }

  // 获取指定数据库的所有表
  @Action({ commit: 'setDatabaseTables' })
  public async queryDatabaseTables(passData: any) {
    const response = await getDatabaseTables({ data: passData })
    return response.data.code === 100 ? response.data.result : []
  }

  // 获取指定数据表的数据
  @Action({ commit: 'setSelectedTableData' })
  public async getTablePreviewData(passData: any) {
    try {
      const response = await queryTableData({ data: passData })
      return response.data.result
    } catch (error) {
      console.error('getTableData error', error)
      throw new Error('getTableData error')
    }
  }

  // 提交数据库数据
  @Action
  public async uploadDatabase(passData: any) {
    await importDatabaseData({ data: passData })

    const response = await queryCurrentUserDatasetCategory()
    return response.data.result
  }

  // 获取当前导入数据的进度
  @Action
  public async getDatabaseImportProgress() {
    return queryDataUploadProgress()
  }

  // 取消数据库导入
  @Action
  public async cancelDatabaseImport() {
    return cancelDatabaseImport()
  }

  // 获取脱敏类型
  @Action({ commit: 'setListDataLevel' })
  public async queryListDataLevel() {
    const response = await listDataLevel({})
    return response.data.result
  }

  // 更新url状态  http
  @Action
  public async updateHttpDataStatus(passData: any) {
    const response = await httpUpdateDataStatus({ data: passData })
    return response.data.result
  }

  // 获取数据表元信息
  @Action({ commit: 'setDatasetsList' })
  public async queryDatasetsList(passData: any) {
    const response = await selectDatasets({ data: passData })
    if (response.data.code === 100) {
      return response.data.result
    }
    return []
  }

  // 获取元数据历史
  @Action
  public async queryActionByDataId(passData: any) {
    const response = await selectActionByDataId({ data: passData })
    return response.data.result
  }

  // sql 查询
  @Action
  public async queryDataBySql(passData: any) {
    const response = await queryDataBySql({ data: passData })
    return response.data
  }

  @Action
  public async querySqlCategory(passData: any) {
    const response = await querySqlCategory({ data: passData })
    return response.data.result
  }

  @Action
  public async deleteSqlCategory(passData: any) {
    const response = await deleteSqlCategory({ data: passData })
    setTimeout(() => {
      if (response.data.result.length === 0) {
        message.success('删除成功！')
      } else {
        const { result } = response.data
        let failedRecord: string = ''
        result.forEach((element: any, index: number) => {
          failedRecord = `${failedRecord}${index ? ',' : ''}${element}`
        })
        message.error(`未成功删除记录：${failedRecord}`)
      }
    }, 100)

    return response.data.result
  }

  @Action
  public async deleteAllSqlCategory(passData: any) {
    const response = await deleteAllSqlCategory({ data: passData })
    if (response.data.code === 500) {
      message.error(response.data.tips)
    } else if (response.data.code === 100) {
      message.success('删除成功！')
    }
    return response.data.result
  }

  @Action
  public async exportToCsv(passData: any) {
    const response = await exportToCsv({ data: passData })
    return response.data
  }

  @Action
  public async exportToXlsx(passData: any) {
    const response = await exportToXlsx({ data: passData })
    return response.data
  }

  @Action
  public async updateDatabaseConfig(passData: any) {
    const response = await updateConfig({ data: passData })
    return response.data.result
  }

  @Action
  public async parseErrorFile(passData: any) {
    const response = await parseError({ data: passData })
    return response
  }

  @Action
  public async getDatabaseTypeImg(passData: any) {
    const response = await getDataTypeImg({ data: passData })
    return response.data
  }

  @Action({ commit: 'setScheduleList' })
  public async checkDatasetSchedule(passData: any) {
    const response = await checkSchedule({ data: passData })
    return response.data.result
  }

  @Mutation
  public setDatasetId(id: string) {
    this._datasetId = id
  }

  @Mutation
  public setCategoryId(id: string) {
    this._categoryId = id
  }

  @Mutation
  public setListDataLevel(level: any[]) {
    this._listDataLevel = level
  }

  // 脱敏类型
  public get getListDataLevel() {
    return this._listDataLevel
  }

  // 获取所有脱敏方式
  @Action({ commit: 'setListMaskingType' })
  public async queryListMaskingType() {
    const response = await listMaskingType({})
    return response.data.result
  }

  @Mutation
  public setListMaskingType(type: any[]) {
    this._listMaskingType = type
  }

  // 脱敏方式
  public get getListMaskingType() {
    return this._listMaskingType
  }

  @Action
  public async generateMask(postData: any) {
    const response = await generateMask({ data: postData })
    return response
  }

  // 删除分类/数据集，从历史记录中删除对应的项
  @Mutation
  public deleteFromPreSelectedDataset(id: number) {
    for (let i = 0; i < this._preSelectedDataset.length; i += 1) {
      if (this._preSelectedDataset[i].id === id) {
        this._preSelectedDataset.splice(i, 1)
        break
      }
    }
  }

  @Mutation
  private setDatasetCategory(category: Array<DatasetCategory>) {
    this._datasetCategory = category
  }

  @Mutation
  public addNewDatasetCategory() {
    this._datasetCategory.push({
      categoryId: -1,
      categoryName: '',
      dataset: [],
    })
  }

  @Mutation
  public cancelAddCategory() {
    this._datasetCategory.pop()
  }

  @Mutation
  public setSelectedDatasetId(id: number) {
    this._selectedDatasetId = id
  }

  @Mutation
  public setSelectedGraphFileName(name: string) {
    this.selectedGraphFileName = name
  }

  @Mutation
  public pushPreSelectedDataset(value: { id: number; name: string; type: '' }) {
    this._preSelectedDataset.push(value)
  }

  @Mutation
  public popPreSelectedDataset() {
    this._preSelectedDataset.pop()
  }

  @Mutation
  public setSelectedDatasetInfo(info: any) {
    this._selectedDatasetInfo = info
  }

  @Mutation
  public setIsUploading(value: boolean) {
    this._isUploading = value
  }

  @Mutation
  public setDataIsUploading(value: boolean) {
    this._dataIsUploading = value
  }

  @Mutation
  public setBeginDatabaseUpload(value: boolean) {
    this._beginDatabaseUpload = value
  }

  @Mutation
  public setWriteDatasetSucceed(value: boolean) {
    this._writeDatasetSucceed = value
  }

  @Mutation
  public setPreviewedDataset(dataset: any) {
    this._previewedDataset = dataset
  }

  @Mutation
  public setDatasetEditModalVisible(value: boolean) {
    this._datasetEditModalVisible = value
  }

  /* 脱敏/定时配置提交 */
  @Mutation
  public setIsConfigSubmit(value: boolean) {
    this._isConfigSubmit = value
  }

  @Mutation
  public setIsSaveDesensitization(value: boolean) {
    this._isSaveDesensitization = value
  }

  /* 脱敏/定时配置面板 */
  @Mutation
  public setDatasetConfigModalVisible(value: boolean) {
    this._datasetConfigModalVisible = value
  }

  @Mutation
  public setTestState(value: DatabaseTestState) {
    this._testState = value
  }

  @Mutation
  public setDatabaseOptions(options: Array<string>) {
    this._databaseOptions = options
  }

  @Mutation
  public setDatabaseTables(tables: Array<string>) {
    this._databaseTables = tables
  }

  @Mutation
  public setDatabaseConfig(config: any) {
    this._databaseConfig = config
  }

  @Mutation
  public setSelectedTables(tables: Array<string>) {
    this._selectedTables = tables
  }

  @Mutation
  public setSelectedTableData(data: any) {
    this._selectedTableData = data
  }

  @Mutation
  public setMessageModalOption(messageModalOption: MessageModalOption) {
    this._messageModalOption = messageModalOption
  }

  @Mutation
  public setHttpModalVisible(visible: boolean) {
    this._httpModalVisible = visible
  }

  @Mutation
  public setTimingSettingModalVisible(visible: boolean) {
    this._timingSettingModalVisible = visible
  }

  @Mutation
  public setSubmitFailModalVisible(visible: boolean) {
    this._submitFailModalVisible = visible
  }

  @Mutation
  public setIsSqlSearch(value: boolean) {
    this._isSqlSearch = value
  }

  @Mutation
  public setGraphFileList(value: Array<string>) {
    this._graphFileList = value
  }

  @Mutation
  public setDatasetsList(value: any[]) {
    this._datasetsList = value
  }

  @Mutation
  public setDataTabIndex(value: string) {
    this._dataTabIndex = value
  }

  @Mutation
  public setDatasetEditPanelVisible(value: boolean) {
    this._datasetEditPanelVisible = value
  }

  @Mutation
  public setIsEditSubmit(value: boolean) {
    this._isEditSubmit = value
  }

  @Mutation
  public setIsClearForm(value: boolean) {
    this._isClearForm = value
  }

  @Mutation
  public setScheduleList(value: any[]) {
    this._scheduleList = value
  }

  @Mutation
  public setUnreadList(value: any[]) {
    this._unreadList = value
  }

  @Mutation
  public setSelectedImportType(value: string) {
    this._selectedImportType = value
  }

  @Mutation
  public cancelImport() {
    this._isUploading = false
    this._dataIsUploading = false
    this._beginDatabaseUpload = false
    this._previewedDataset = null
    this._writeDatasetSucceed = false

    this._testState = DatabaseTestState.BEFORE_TEST
    this._databaseOptions = []
    this._databaseTables = []
    this._databaseConfig = null
    this._selectedTables = []
    this._selectedTableData = null
    this._messageModalOption = { visible: false, message: '', tips: '' }
  }

  @Mutation
  public clearState() {
    this._datasetCategory = []
    this._selectedDatasetId = -1
    this._preSelectedDataset = []
    this._selectedDatasetInfo = null
    this._isUploading = false
    this._dataIsUploading = false
    this._beginDatabaseUpload = false
    this._previewedDataset = null
    this._writeDatasetSucceed = false

    this._testState = DatabaseTestState.BEFORE_TEST
    this._databaseOptions = []
    this._databaseTables = []
    this._databaseConfig = null
    this._selectedTables = []
    this._selectedTableData = null
    this._messageModalOption = { visible: false, message: '', tips: '' }

    this._isSqlSearch = false
    this._graphFileList = []
  }
}

export default getModule(DataStore)
