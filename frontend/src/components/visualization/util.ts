export const gridRowHeight = 30
// eslint-disable-next-line unicorn/prevent-abbreviations
export const gridColNum = 36
export const defaultGridWidth = 8
export const defaultGridHeight = 5
export const chartHeight = 190
export const girdMargin = [10, 10]
export const minBackgroundBoardWidth: number = 400 // 可视化画布最小宽度
export const minBackgroundBoardHeight: number = 200 // 可视化画布最小高度
export const maxBackgroundBoardWidth: number = 1920 // 可视化画布最小宽度
export const maxBackgroundBoardHeight: number = 9999 // 可视化画布最小高度
/** dom rectangle type */
export type DomReact = {
  x: number
  y: number
  width: number
  height: number
  [key: string]: any
}

/** 筛选列 */
export type FilterColumnType = {
  col: string
  desc: string
  enabled?: boolean
  producer: 'filterForm' | undefined
  producerId: string | number
  filterType: '[a,b]' | 'in' | undefined
  values: Array<string | number>
}

/** filterForm */
export type HoverFilterFormInfo = {
  i: string
  widgetId: string | number
  // 不在这里存储是因为关联图表是动态的随时会更新
  // associationChart: any[]
}
