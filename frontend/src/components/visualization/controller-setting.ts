import { cloneDeep, isArray } from 'lodash'
import {
  componentTypeEnum,
  KeyValueStore,
  controlTypeEnum,
  radioOrCheckboxEnum,
  timeAccuracyEnum,
} from '../vueGridLayout/interfate/grid-Item-data-inteface'

interface InfoMap {
  [key: string]: Array<string>
}

interface LockMap {
  [key: string]: {
    [key: string]: Array<string | number>
  }
}

// 组件类型 与 需要显示的配置项的名称集合之间的映射
export const showItemsInfoMap: InfoMap = {
  [`${componentTypeEnum.checkbox}`]: ['controlType', 'name', 'radioOrCheckbox'],
  [`${componentTypeEnum.radio}`]: ['controlType', 'name', 'radioOrCheckbox'],
  [`${componentTypeEnum.rangeDay}`]: [
    'controlType',
    'name',
    'timeAccuracy',
    'timeRangeDate',
  ],
  [`${componentTypeEnum.rangeMonth}`]: [
    'controlType',
    'name',
    'timeAccuracy',
    'timeRangeMonth',
  ],
  [`${componentTypeEnum.rangeYear}`]: [
    'controlType',
    'name',
    'timeAccuracy',
    'timeRangeYear',
  ],
  [`${componentTypeEnum.select}`]: ['controlType', 'name'],
  [`${componentTypeEnum.remoteSelect}`]: ['controlType', 'name', 'isRemote'],
}

// 部分选项锁定
export const componentLockOptionMap: LockMap = {
  [`${componentTypeEnum.checkbox}`]: {
    controlType: ['timePicker'],
  },
  [`${componentTypeEnum.radio}`]: {
    controlType: ['timePicker'],
  },
  [`${componentTypeEnum.select}`]: {
    controlType: ['timePicker'],
  },
  [`${componentTypeEnum.rangeDay}`]: {
    controlType: ['radioOrCheckbox', 'select'],
  },
  [`${componentTypeEnum.rangeMonth}`]: {
    controlType: ['radioOrCheckbox', 'select'],
  },
  [`${componentTypeEnum.rangeYear}`]: {
    controlType: ['radioOrCheckbox', 'select'],
  },
}

const formConfig = {
  formData: {
    // 绑定控件id
    componentId: '',
    controlType: 'radioOrCheckbox',
    name: '',
    radioOrCheckbox: 'radio',
    timeAccuracy: 'd',
    isRemote: '0',
    options: [],
    defaultValue: [],
    timeRangeYear: [],
    timeRangeMonth: [],
    timeRangeDate: [],
    associationChart: [],
  },
  items: [
    {
      label: '控件类型',
      name: 'controlType',
      type: 'select',
      value: 'select',
      isHide: false,
      props: {
        options: [
          {
            text: '选项选择',
            value: 'radioOrCheckbox',
          },
          {
            text: '下拉选择',
            value: 'select',
          },
          {
            text: '时间选择',
            value: 'timePicker',
          },
        ],
      },
    },
    {
      label: '控件名称',
      name: 'name',
      type: 'input',
      bind: {
        allowClear: true,
        placeholder: '请输入控件名称',
      },
    },
    {
      label: '查询方式',
      name: 'radioOrCheckbox',
      type: 'radioGroup',
      isHide: false,
      bind: {
        options: [
          {
            label: '单选',
            value: 'radio',
          },
          {
            label: '多选',
            value: 'checkbox',
          },
        ],
      },
      props: {},
    },
    {
      label: '远程搜索',
      name: 'isRemote',
      type: 'radioGroup',
      isHide: false,
      bind: {
        options: [
          {
            label: '是',
            value: '1',
          },
          {
            label: '否',
            value: '0',
          },
        ],
      },
      props: {},
    },
    {
      label: '时间粒度',
      name: 'timeAccuracy',
      type: 'radioGroup',
      isHide: true,
      bind: {
        options: [
          {
            label: '年',
            value: 'y',
          },
          {
            label: '月',
            value: 'm',
          },
          {
            label: '日',
            value: 'd',
          },
        ],
      },
      props: {},
    },
    {
      type: 'br',
    },
    {
      label: '日期选择',
      name: 'timeRangeYear',
      type: 'rangeYear',
      isHide: true,
      bind: {
        style: {
          width: '300px',
          height: '24px',
          transform: 'translateY(8px)',
        },
      },
    },
    {
      label: '日期选择',
      name: 'timeRangeMonth',
      type: 'rangeMonth',
      bind: {
        style: {
          width: '300px',
          height: '24px',
          transform: 'translateY(8px)',
        },
      },
      isHide: true,
    },
    {
      label: '日期选择',
      name: 'timeRangeDate',
      type: 'rangeDay',
      isHide: true,
      bind: {
        mode: ['time', 'time'],
        format: 'yyyy-MM-DD',
        placeholder: ['开始时间', '结束时间'],
      },
    },
  ],
}

export function getFormSettingByComponent(
  componentName: componentTypeEnum
): KeyValueStore {
  const result = cloneDeep(formConfig)
  const showInfo = showItemsInfoMap[componentName] || {}
  const lockInfo = componentLockOptionMap[componentName] || {}
  const lockKeys = Object.keys(lockInfo)
  result.items = result.items.map((item) => {
    item.isHide = !(item.name != null && showInfo.includes(item.name))
    if (item.name && lockKeys.includes(item.name)) {
      if (item.bind && item.bind.options && isArray(item.bind.options)) {
        item.bind.options.forEach((option: KeyValueStore) => {
          option.disable = lockInfo[item.name].includes(option.value)
        })
      } else if (
        item.props &&
        item.props.options &&
        isArray(item.props.options)
      ) {
        item.props.options.forEach((option: KeyValueStore) => {
          option.disabled = lockInfo[item.name].includes(option.value)
        })
      }
    }
    return item
  })
  return result
}

export function getDefaultValue(
  componentRender: componentTypeEnum,
  fieldInfo: KeyValueStore
): any {
  let result: any
  switch (componentRender) {
    case componentTypeEnum.radio:
    case componentTypeEnum.select:
      result = fieldInfo.formConfig.formData.defaultValue[0] as string
      break

    case componentTypeEnum.checkbox:
      result = fieldInfo.formConfig.formData.defaultValue as Array<
        string | number
      >
      break

    case componentTypeEnum.rangeDay:
      result = fieldInfo.formConfig.formData.timeRangeDate as Array<
        string | number
      >
      break

    case componentTypeEnum.rangeMonth:
      result = fieldInfo.formConfig.formData.timeRangeMonth as Array<
        string | number
      >
      break

    case componentTypeEnum.rangeYear:
      result = fieldInfo.formConfig.formData.timeRangeYear as Array<
        string | number
      >
      break

    default:
      break
  }
  return result
}

export function getDefaultValueFromDashBoardData(
  componentRender: componentTypeEnum,
  fieldInfo: KeyValueStore
): any {
  let result: any
  switch (componentRender) {
    case componentTypeEnum.radio:
    case componentTypeEnum.select:
      result = fieldInfo.defaultValue[0] as string
      break

    case componentTypeEnum.checkbox:
      result = fieldInfo.defaultValue as Array<string | number>
      break

    case componentTypeEnum.rangeDay:
      result = fieldInfo.timeRangeDate as Array<string | number>
      break

    case componentTypeEnum.rangeMonth:
      result = fieldInfo.timeRangeMonth as Array<string | number>
      break

    case componentTypeEnum.rangeYear:
      result = fieldInfo.timeRangeYear as Array<string | number>
      break

    default:
      break
  }
  return result
}

/**
 * 渲染控件默认配置项，用于自动填入
 */
export const compMapConfig: KeyValueStore = {
  [`${componentTypeEnum.radio}`]: {
    controlType: controlTypeEnum.radioOrCheckbox,
    radioOrCheckbox: radioOrCheckboxEnum.radio,
  },
  [`${componentTypeEnum.checkbox}`]: {
    controlType: controlTypeEnum.radioOrCheckbox,
    radioOrCheckbox: radioOrCheckboxEnum.checkbox,
  },
  [`${componentTypeEnum.select}`]: {
    controlType: controlTypeEnum.select,
  },
  [`${componentTypeEnum.remoteSelect}`]: {
    controlType: controlTypeEnum.select,
    isRemote: '1',
  },
  [`${componentTypeEnum.rangeDay}`]: {
    controlType: controlTypeEnum.timePicker,
    timeAccuracy: timeAccuracyEnum.date,
  },

  [`${componentTypeEnum.rangeMonth}`]: {
    controlType: controlTypeEnum.timePicker,
    timeAccuracy: timeAccuracyEnum.month,
  },

  [`${componentTypeEnum.rangeYear}`]: {
    controlType: controlTypeEnum.timePicker,
    timeAccuracy: timeAccuracyEnum.year,
  },
}

/**
 * 获取实际渲染组件类型
 */
export function getControllerRenderByFormData(formData: KeyValueStore): string {
  const ctrols = Object.keys(compMapConfig)
  let control: string = ''
  Object.values(compMapConfig).forEach((item, i) => {
    const tag = Object.keys(item).every((property: string) => {
      return item[property] === formData[property]
    })
    if (tag) {
      control = ctrols[i]
    }
  })
  return control
}

export default formConfig
