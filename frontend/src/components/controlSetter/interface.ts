
export interface IPanelConfig {
  type: string,
  label: string,
  children?: []
}

export interface IComponentConfig {
  type: string,
  label: string,
  tip?: string,
  name: string,
  value?: any,
  required?: boolean,
  props?: object,
  onDataChange?: Function,
  domClass?:string,
  domStyle?:string,
}
