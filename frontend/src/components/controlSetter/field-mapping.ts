import SelectField from '@/components/controlSetter/material/SelectField.vue'

const componentLibrary = {
  SelectField,
}

export default {
  inputNumber: {
    component: 'a-input-number',
    props: {}
  },
  text: {
    component: 'a-input',
    props: {}
  },

  select: {
    component: 'select-field',
    props: {
      defaultChecked: false,
      disabled: false
    }
  }, 

  switch: {
    component: 'a-switch',
    props: {
      checked: false
    }
  }, 
}

export { componentLibrary }
