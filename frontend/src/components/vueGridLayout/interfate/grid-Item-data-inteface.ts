export interface KeyValueStore {
  [key: string]: any
}
/**
 * 弹框筛选类型配置
 */
export enum controlTypeEnum {
  // 单选或者多选
  radioOrCheckbox = 'radioOrCheckbox',
  // 下拉选择
  select = 'select',
  // 时间选择器
  timePicker = 'timePicker',
}

/**
 * 单选或多选模式时查询模式
 */
export enum radioOrCheckboxEnum {
  radio = 'radio',
  checkbox = 'checkbox',
}

/**
 * 时间选择器,时间精度
 */
export enum timeAccuracyEnum {
  year = 'y',
  month = 'm',
  date = 'd',
}

export interface ColumnInterface {
  tableId: string
  label: string
  value: string
  type?: string
  isDate: boolean
  isNumber: boolean
}
/**
 * 选项
 */
export interface OptionInterface {
  label: string
  value: string | number
}
/**
 * 单个字段的formData 配置项
 */
export interface ControlConfigurationInterface {
  // 真正渲染的控件
  controlRender: componentTypeEnum

  column: ColumnInterface

  columnType?: string

  // 使用控件时绑定的值
  value: Array<string | number> | string | number

  // 默认值
  defaultValue: any

  // 绑定图表的id
  componentId: string

  // 字段名称
  fieldName: string

  // 控件展示种类
  controlType: controlTypeEnum

  // 单选或多选模式时查询模式
  radioOrCheckbox: radioOrCheckboxEnum

  // 时间精度
  timeAccuracy: timeAccuracyEnum

  // 单选或多选可用选项
  options: OptionInterface

  // 时间段(精度为'年')
  timeRangeYear?: [number | string, number | string]

  // 时间段(精度为'月')
  timeRangeMonth?: [number | string, number | string]

  // 时间段(精度为'日')
  timeRangeDate?: [number | string, number | string]

  // 关联图表(暂定为widgetId)
  associationChart: Array<string>
}

/**
 * 渲染控件类型
 */
export enum componentTypeEnum {
  radio = 'radio',
  checkbox = 'checkbox',
  rangeDay = 'rangeDay',
  rangeMonth = 'rangeMonth',
  rangeYear = 'rangeYear',
  select = 'select',
  remoteSelect = 'remoteSelect',
}

/**
 * 控件水平或垂直布局
 */
export enum controlLayoutEnum {
  horizontal = 'horizontal',
  vertical = 'vertical',
}

export const compTypeDefaultValueMap: KeyValueStore = {
  radio: '',
  checkbox: [],
  rangeDay: [],
  rangeMonth: [],
  rangeYear: [],
}

export interface GridItemDataInterface {
  chartOptions: KeyValueStore
  chartType: Array<string> | string
  widgetType?: 'system' | 'pipeline' // 先简单定义数据节点或者pipelien节点下两种，后面视情况加
  x: string | number
  y: string | number
  w: string | number
  h: string | number
  i: string | number
  dataQuery?: {
    [key: string]: any
  }
  [key: string]: any
}

export interface gridItemFilterInterface extends GridItemDataInterface {
  chartType: 'filter'
  tableName: string
  dataId: string
  layout: controlLayoutEnum
  fieldConfig: Array<ControlConfigurationInterface>
}

export const textPropertyPanelList: KeyValueStore = [
  {
    children: [
      {
        label: '标题',
        name: 'title',
        type: 'input',
        value: '',
        props: {
          maxLength: 50,
          placeholder: '请填写标题内容',
        },
      },
      {
        defaultValue: true,
        label: `标题栏`,
        name: 'showTitle',
        type: 'switch',
        tooltip: '发布后生效',
      },
      {
        defaultValue: true,
        label: '文本边框',
        name: 'showBorder',
        type: 'switch',
      },
    ],
  },
]
