/**
 *  pipeline 节点 icon 及相关背景色
 * @author houjinhui
 */

interface KeyValue {
  [key: string]: any
}

// 算子列表 图标  graph 画布算子节点图标
export const AlgTypeNodeIconEnum: KeyValue = {
  1: { icon: 'iconjuleiDBSCAN', color: '#aed495', opacity: 1 }, // DBSCAN
  2: { icon: 'iconjuleiK-Means', color: '#aed495', opacity: 1 }, // KMEANS
  3: { icon: 'iconxianxinghuigui1', color: '#f5a4ac', opacity: 1 }, // 线性回归
  4: { icon: 'iconjiangweipac', color: '#f5a4ac', opacity: 1 }, //  降维 PCA
  5: { icon: 'iconT-SNE', color: '#f5a4ac', opacity: 1 }, // 降维 TSNE
  6: { icon: 'iconLLE3beifen', color: '#f5a4ac', opacity: 1 }, // 降维 LLE
  7: { icon: 'iconstat-anomaly2beifen3', color: '#fbaf85', opacity: 1 }, // 异常检测 LT
  8: { icon: 'iconiForest2', color: '#fbaf85', opacity: 1 }, // 独立森林
  9: { icon: 'iconluojihuigui1', color: '#f5a4ac', opacity: 1 }, // LOGREGRE
  10: { icon: 'iconpinfanFP', color: '#aed495', opacity: 1 }, // 频繁挖掘 FP_GROWTH
  11: { icon: 'iconPrefixSpan', color: '#aed495', opacity: 1 }, // 频繁挖掘 PREFIX_SPAN
}
// ETL 节点图标
export const ETLAlgTypeNodeIconEnum: KeyValue = {
  1: { icon: 'iconFilterbeifen1', color: '#aed495', opacity: 1 }, // FILTER
  2: { icon: 'iconinterJion', color: '#aed495', opacity: 1 }, // JOIN
  3: { icon: 'iconSample', color: '#f5a4ac', opacity: 1 }, // SAMPLE
  4: { icon: 'iconUnion', color: '#f5a4ac', opacity: 1 }, //  UNION
  6: { icon: 'iconsql2', color: '#f5a4ac', opacity: 1 }, // SQL
  7: { icon: 'iconshujutoushi', color: '#f5a4ac', opacity: 1 }, // PIVOT TABLE
}
// JOIN 节点图标
export const JOINModeIconEnum: KeyValue = {
  1: { icon: 'iconJionbeifen-copy', color: '#aed495', opacity: 1 }, // LEFT JOIN
  2: { icon: 'iconJionbeifen', color: '#aed495', opacity: 1 }, // RIGHT JOIN
  3: { icon: 'iconinterJion', color: '#aed495', opacity: 1 }, // INNER JOIN
  4: { icon: 'iconJionbeifen3', color: '#aed495', opacity: 1 }, // FULL OUTER JOIN
  5: { icon: 'icona-Jionbeifen8', color: '#aed495', opacity: 1 }, // LEFT JOIN EXCLUDING INNER JOIN
  6: { icon: 'icona-Jionbeifen4', color: '#aed495', opacity: 1 }, // RIGHT JOIN EXCLUDING INNER JOIN
  7: { icon: 'icona-Jionbeifen5', color: '#aed495', opacity: 1 }, // FULL OUTER JOIN EXCLUDING INNER JOIN
}

export const EconomicOperatorIconEnum: KeyValue = {
  1: { icon: 'icontongjiyichangjiance', color: '#aed495', opacity: 1 }, // 异常值检测-统计
  2: { icon: 'iconjinlinfenxi', color: '#aed495', opacity: 1 }, // 异常值检测-近邻
  3: { icon: 'icontongyongchabu', color: '#aed495', opacity: 1 }, // 缺失值插补-统计
  4: { icon: 'iconduozhongchabu', color: '#aed495', opacity: 1 }, // 缺失值插补-多重
  5: { icon: 'iconbiaozhunhua', color: '#aed495', opacity: 1 }, // 标准化
  6: { icon: 'iconshijianxuliefenjie', color: '#aed495', opacity: 1 }, // 季节性调整
  7: { icon: 'iconshujupinghua', color: '#aed495', opacity: 1 }, // 数据平滑
  8: { icon: 'iconmonituiyan', color: '#aed495', opacity: 1 }, // 模拟推演
  9: { icon: 'iconmonituiyan', color: '#aed495', opacity: 1 }, // 模拟推演-多参数
  10: { icon: 'iconshijianxulieweiyi', color: '#aed495', opacity: 1 }, // 时间序列位移
  11: { icon: 'iconxuanzhuan', color: '#aed495', opacity: 1 }, // 反向模拟推演
}

// 数据 图标
export const DataIcon = {
  icon: 'iconshujubeifen',
  color: '#a4abfb',
  opacity: 1,
}
// ETL 图标
export const EtlIcon = { icon: 'iconETL', color: '#74a0fa', opacity: 0.8 }
// 清洗节点 icon
export const ClearNodeIcon = {
  icon: 'iconqingxibeifen2',
  color: '#74a0fa',
  opacity: 0.8,
}
// 图网络构建 icon
export const TugoujianIcon = {
  icon: 'icontugoujian1',
  color: '#9AA2FF',
  opacity: 1,
}
// 模型 icon
export const ModelIcon = {
  icon: 'iconmoxing',
  color: '#9AA2FF',
  opacity: 1,
}
// 自定义 icon
export const SelfDefinedIcon = {
  icon: 'iconzidingyibeifen',
  color: '#a2dced',
  opacity: 1,
}

/**
 * 根据 type algType 返回节点图标相关信息
 * @param type 节点类型：1-系统算子节点，2-ETL节点, 3-数据节点，4-自定义算子节点，5-清新节点， 6-图网络节点， 7-模型节点， 8-py算子节点
 * @param algType
 */
export function nodeIconWithBackGround(type: number, algType: number) {
  let icon: KeyValue
  switch (type) {
    case 0:
      // pipeline join 节点的不同 join 类型 《组件 NodeCapsule 使用 nodeIconWithBackGround(0, mod)》
      icon = JOINModeIconEnum[Number(algType)]
      break
    case 1:
      icon = AlgTypeNodeIconEnum[Number(algType)]
      break
    case 2:
      icon = ETLAlgTypeNodeIconEnum[Number(algType)]
      break
    case 3:
      icon = DataIcon
      break
    case 4:
      icon = SelfDefinedIcon
      break
    case 5:
      icon = ClearNodeIcon
      break
    case 6:
      icon = TugoujianIcon
      break
    case 7:
      icon = ModelIcon
      break
    case 8:
      icon = EconomicOperatorIconEnum[Number(algType)]
      break
    case 9:
      icon = SelfDefinedIcon
      break
    default:
      // 通用的 算子图标 （用自定义算子图标）
      icon = SelfDefinedIcon
      break
  }
  return icon
}

/**
 * 新版 节点图标
 */
export const newIcon: KeyValue = {
  3: 'iconshujubeifen', // 数据节点
  4: 'iconzidingyibeifen', // 自定义
  5: 'iconFilterbeifen1', //  清洗节点
  6: 'icontugoujian1', // 图网络构建
  7: 'iconzidingyi', // 自定义
  8: 'iconqingxi-copy', // 清洗
  9: 'icontugoujian', // 图网络构建
}

// 算子列表分组图标
export const OperatorGroupIconEnum: KeyValue = {
  0: 'iconETL', // ETL 操作
  1: 'iconjulei', // 聚类
  2: 'iconhuigui', // 回归
  3: 'iconjiangwei', // 降维
  4: 'iconyichangjiance', // 异常检测
  5: 'iconpinfanmoshiwajue', //  频繁模式挖掘
  6: 'iconshoucang', // 收藏
  7: 'iconzidingyi', // 自定义
  8: 'iconqingxi-copy', // 清洗
  9: 'icontugoujian', // 图网络构建
}

// 算子列表分组图标 背景色
export const OperatorGroupIconBackGround: KeyValue = {
  0: { color: '#74a0fa', opacity: 0.8 }, // ETL 操作
  1: { color: '#aed495', opacity: 1 }, // 聚类
  2: { color: '#f5a4ac', opacity: 1 }, // 回归
  3: { color: '#f5a4ac', opacity: 1 }, // 降维
  4: { color: '#fbaf85', opacity: 1 }, // 异常检测
  5: { color: '#aed495', opacity: 1 }, //  频繁模式挖掘
  7: { color: '#a2dced', opacity: 1 }, // 自定义
  8: { color: '#74a0fa', opacity: 0.8 }, // 清洗
}
