/**
 *  pipeline 节点运行状态
 * @author houjinhui
 */

interface KeyValue{
  [key: string] : any
}
// 节点运行状态
export const NodeStatusEnum: KeyValue =  {
  CREATE: '',
  RUNNING: '',
  SUCCESS: '',
  FAIL: 'iconhuaban',
  STOP: 'iconjinggaodanchuang',
  KILLED: 'iconwuquanxian',
}

export const NodeStatus = 'iconyouquanxianlianjiechenggong'
