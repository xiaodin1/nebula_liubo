type ColumnValue = number | string

interface StatRangeCount {
  count: number // 数量
  highLight?: number // 筛选后的部分，如果筛选条件为空，不返回
  anomaly?: number // 异常数量，如果为空，不返回
  imputation?: number // 插补数量，如果为空，不返回
  low: number // 下界数据index
  up: number // 上界数据index
}

interface StatValueCount {
  column: ColumnValue // 值
  count: number // 条数
  highLight?: number // 筛选后的部分，如果筛选条件为空，不返回
  anomaly?: number // 异常数量，如果为空，不返回
  imputation?: number // 插补数量，如果为空，不返回
  isMatching?: boolean
}

export interface ColumnStat {
  table: string // 表名
  name: string // 字段名
  type: number // 字段类型
  total: number // 总记录数
  distinctTotal: number // 唯一值数目
  max: ColumnValue // 最大值
  min: ColumnValue // 最小值
  topColumn: {
    // 最多记录的列
    column: ColumnValue // 值
    count: number // 记录条数
    highLight?: number | null // highlight记录条数
  }
  rangeCount: StatRangeCount[]
  pageCount?: {
    // 分页数据
    totalPages: number // 总页数
    pageSize: number // 每页条数
    curPage: number // 当前页码
    totalElements: number // 唯一值数目 // cjh: ==外层的"distinctTotal"？
    data: StatValueCount[]
  }
}

export class EventMessage {
  data: any
  name? = ''
  emitor?: any
  constructor(data: any, name?: string, emitor?: any) {
    this.data = data
    this.name = name
    this.emitor = emitor
  }
}

export enum ColumnDataType {
  VARCHAR = 'varchar',
  DATE = 'date',
  INT = 'int',
  DECIMAL = 'decimal',
  TEXT = 'text',
  JSON = 'json',
  ARRAY = 'array',
}

export interface TableHead {
  name: string
  type: number
  desc: ColumnDataType
}

interface KeyValue {
  [key: string]: any
}

/**
 * 语义 menu
 */
export interface ISemantic {
  index: number
  name: string
  icon?: string
  text: string
  haveSub: boolean
  detail?: KeyValue[]
}

export interface IChangeSemanticInfo {
  value: any[]
  toSemantic: string
}

/**
 * 修改语义，DataColumn.vue 向 DataColumns.vue   $emit 参数
 */
export interface IChangeSemanticData {
  action: string // eg：修改语义 'SEMANTIC_TRANSFORM'
  col: string // 列名
  toSemantic: string // 修改的语义
  description: string
  actionType: string // 修改语义 'semanticTransform', 与{ StatisticsAction } from '@/util/data-selection-actions' 中对应
  order?: any[] // 若修改为有序语义，其各值的顺序
  update?: KeyValue // 若有不符合语义规则的 修改映射
  newColumnName?: string // 新增列的列名（转换为无语义不新增列）
}
