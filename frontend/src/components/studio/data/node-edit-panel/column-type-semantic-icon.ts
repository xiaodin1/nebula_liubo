/**
 * 统一数据类型的图标
 * 2021.07.27
 * Hou Jinhui
 */
import { ColumnDataType } from '@/components/studio/data/node-edit-panel/interface'

interface KeyValue {
  [key: string]: any
}

/**
 * 数据类型图标
 * 使用文件:
 *  components/studio/data/node-edit-panel/DataColumn.vue
 *  components/visualization/VisualDataColumn.vue
 *  components/data/DatasetPreviewTable.vue
 * 注：
 *  这里的 icon 没有放在 /node-edit-panel/column-type-semantic.ts 数据类型 menu 中， 单独出来， 是为了与表头的数据类型显示 公用
 */
export const columnTypeIcon: { [key in ColumnDataType]: string } = {
  [ColumnDataType.VARCHAR]: 'iconshujuleixing-zifu', // 字符串
  [ColumnDataType.DATE]: 'iconshujuleixing-riqi', // 日期
  [ColumnDataType.INT]: 'iconzhengshu1', // 整数
  [ColumnDataType.DECIMAL]: 'iconxiaoshu1', // 小数
  [ColumnDataType.TEXT]: 'iconshujuleixing-zifu', // 文本
  [ColumnDataType.JSON]: 'iconobject1', // JSON
  [ColumnDataType.ARRAY]: 'iconarray1', // 数组
}

/**
 * 语义分类图标
 * 与 ./column-type-semantic.ts 中的 columnSemanticMenu 一级分类 对应
 */
export const columnSemanticClassificationIcon: KeyValue = {
  geography: 'icondili1', // 地理
  network: 'iconwangluo2', // 网络
  information: 'iconxinxi1', // 信息
  category: 'iconleibie1', // 类别
}

/**
 * 语义类型图标
 * 需求：https://cf.zjvis.org/pages/viewpage.action?pageId=29567573
 *    4. 每一个数据类型都要涉及一个icon（语义数据类型最好给每个小类都设计相应的icon，实在不行给大类设计也可以）
 */
export const columnSemanticIcon: KeyValue = {
  // 地理
  longitude: '', // 经度
  latitude: '', // 纬度
  coordinate: '', // 经纬度
  country: '', // 国家
  province: '', // 省份
  city: '', // 城市
  postcode: '', // 邮编
  // 网络
  ip: '', // IP
  url: '', // URL
  httpstatus: '', // HTTP状态码
  // 信息
  email: '', // 邮件地址
  telephone: '', // 电话
  id: '', // 身份证
  passport: '', // 护照
  // 类别
  disorder: '', // 无序
  order: '', // 有序
  // 无
  null: '', // 无
}
