/**
 * 语义转换规则
 * 2021.09.14
 * Hou Jinhui
 */

/**
  语义转化规则
 */
const semanticTransformationRules: any = {
  // 地理
  longitude: [
    {
      key: '0',
      dataType: '数字',
      format: '数据范围在[-180,180]',
      sample: '120, 111.1',
    },
    {
      key: '1',
      dataType: '字符串',
      format:
        'ddd.ddddd ° 【度 . 度 格式】, ddd°mm.mmm’  【度 . 分 . 分 格式】, ddd°mm’ss’’ 【度 . 分 . 秒 格式】, 转换为第一种后数据范围在[-180,180]，若后面带E/W则在[0,180]',
      sample: '120.109°, 116°20.12’, 118°20’43”',
    },
  ], // 经度
  latitude: [
    {
      key: '0',
      dataType: '数字',
      format: '数据范围在[-90,90]',
      sample: '39.12',
    },
    {
      key: '1',
      dataType: '字符串',
      format:
        '同经度的三种类型, 转换为第一种后数据范围在[-90,90]，若后面带N/S则在[0,90]',
      sample: '30.123°, 39°12.34’, 60°20’37”',
    },
  ], // 纬度
  coordinate: [
    {
      key: '0',
      dataType: '字符串',
      format:
        '经度在前，纬度在后，英文逗号分隔, 经度和纬度只能是上面的数字类型，不带符号',
      sample: '116.407394,39.904211',
    },
  ], // 经纬度
  country: [
    {
      key: '0',
      dataType: '整数',
      format: '国际电话区号',
      sample: '86（中国）',
    },
    {
      key: '1',
      dataType: ' 字符串',
      format:
        '中文全称, 中文简称, 英文全称, 英文简称, 英文缩写, 国家代码（三位）, 国家代码（二位）',
      sample:
        '中华人名共和国, 中国, the People\'s Republic of China, China, PRC、CHN、CN, CHN, CN',
    },
  ], // 国家
  province: [
    {
      key: '0',
      dataType: '整数',
      format: '行政区划代码',
      sample: '330000（浙江省）',
    },
    {
      key: '1',
      dataType: ' 字符串',
      format: '中文全称, 中文简称, 英文全称, 英文简称',
      sample: '杭州[市], Zhejiang[ Province], zj',
    },
  ], // 省份
  city: [
    {
      key: '0',
      dataType: '整数',
      format: '行政区划代码',
      sample: '330100 (杭州市)',
    },
    {
      key: '1',
      dataType: '字符串',
      format: '中文全称, 中文简称, 英文名',
      sample: '杭州[市], 杭, Hangzhou',
    },
  ], // 城市
  postcode: [
    {
      key: '0',
      dataType: '整数',
      format:
        '采用四级六位编码制,前两位表示省(直辖市、自治区),第三位代表邮区,第四位代表县(市),最后两位为投递区的位置',
      sample: '310012（杭州市余杭区）',
    },
  ], // 邮编
  // 网络
  ip: [
    {
      key: '0',
      dataType: '字符串',
      format:
        'IPv4: 点分十进制表示法，每一段的范围为[0,255], IPv6: 128位，用":"分成8段，用16进制表示, 简写的IPv6: 每段中前面的0可以省略，连续的0可省略为"::"，但只能出现一次',
      sample:
        '202.101.105.66, 2001:0db8:3c4d:0015:0000:0000:1a2f:1a2b, FF01::101 (FF01:0:0:0:0:0:0:101), ::1 (0:0:0:0:0:0:0:1)',
    },
  ], // IP
  url: [
    {
      key: '0',
      dataType: '字符串',
      format:
        'protocol://hostname[:port][/path][;parameters][?query][#fragment], 带方括号[]的为可选项',
      sample: 'https://baidu.com',
    },
  ], // URL
  httpstatus: [
    {
      key: '0',
      dataType: '整数',
      format:
        '三位数的整数，取值范围：[100, 600], HTTP状态码（HTTP Status Code）',
      sample: '200, 404',
    },
  ], // HTTP状态码
  // 信息
  email: [
    {
      key: '0',
      dataType: '字符串',
      format:
        '^[A-Za-z0-9_-\u4E00-\u9FA5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$',
      sample: 'zhangsan-001@gmail.com',
    },
  ], // 邮件地址
  telephone: [
    {
      key: '0',
      dataType: '整数/字符串',
      format:
        '手机号码：11位, 座机号码一般 7-8 位, 前面可以加上国际区号，区域号（可以只有一个区号）',
      sample:
        '座机的四种格式, 12345678, 0086-10-12345678, +86-10-12345678, 86-10-12345678, 中间的连接符号可以是空格，或者没有连接符，如：10 12345678, 1012345678, 手机号码同上',
    },
  ], // 电话
  id: [
    {
      key: '0',
      dataType: '整数/字符串',
      format:
        '18位，前17位一定是数字，最后一位是数字或字母 x/X, /^([1-6][1-9]|50)\\d{4}(18|19|20)\\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$/',
      sample: '123456200010101234, 12345620001010123x',
    },
  ], // 身份证
  passport: [
    {
      key: '0',
      dataType: '字符串',
      format:
        '共9位：, 普通电子护照：E+8位数字, 公务电子护照：两个字母（如SE/DE/PE）+7位数字, 澳门特别行政区护照：MA+7位数字, 香港特别行政区护照：K+8位数字',
      sample: 'E12345678, SE1234567',
    },
  ], // 护照
  // 类别
  disorder: [
    {
      key: '0',
      dataType: '整数',
      format:
        '数据种类<=12，同时可作为数值类型（在推荐时既具有类别语义，同时也是数值类型）',
      sample: '[0,2,1,1,0]',
    },
    {
      key: '1',
      dataType: '字符串',
      format: '数据种类<=12',
      sample: '[数学，语文，英语，英语，地理，数学] ',
    },
  ], // 无序
  order: [
    {
      key: '0',
      dataType: '整数/字符串',
      format: '数据种类<=12',
      sample: '空气质量：健康，不太健康，有毒, 成绩等级：A，B，C，D',
    },
  ], // 有序
}

export default semanticTransformationRules
