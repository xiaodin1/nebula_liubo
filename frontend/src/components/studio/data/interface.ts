// ------------  Interface  ---------

// 定位
export interface Position {
  x: number;
  y: number;
}

// 行列定位
export interface PositionWithColRow {
  col: number;
  row: number
}

// 尺寸
export interface Size {
  width: number;
  height: number;
}

export interface NodeGridSize {
  rowCount: number,
  colCount: number
}

// 节点 模型
export interface Node {
  id: number,
  name: string,
  projectId: number,
  pipelineId: number,
  parentId: string | null,
  childId: string | null,
  userId: number,
  data?: any,
  type?: number
}

// 连接线信息
export interface Edge {
  startNodeId: number,  // 开始节点id
  endNodeId: number,  // 结束节点id
  startEndPos: any,
  x: number,
  y: number,
}
