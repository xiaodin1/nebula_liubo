// ------------ Auto join Interface  ---------
// 结果概览
export interface OverviewResult {
  leftMatchView: string
  leftNotMatch: number
  leftNotMatchView: string
  leftMatch: number
  rightMatch: number
  rightMatchView: string
  rightNotMatch: number
  rightNotMatchView: string
}

// 推荐的子句
export interface RecommendClause {
  score: number
  headerScore: number
  leftHeaderName: string
  valueScore: number
  rightHeaderName: string
  leftTableName: string
  rightTableName: string
  id?: number | null
}

// 联接子句
export interface JoinClause extends RecommendClause {
  operator: string
}
