import { message } from 'ant-design-vue'

export default function noPermissionPrompt() {
  message.warning('无权限进行该操作！')
}
