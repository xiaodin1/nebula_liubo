/**
 * 图表的全部配置描述
 */
import { protoConfig as BarChartProto } from '@/components/common/VisualizationComp/ChartConfig/barchart-config'
import { protoConfig as GroupBarChartProto } from '@/components/common/VisualizationComp/ChartConfig/groupbarchart-config'
import { protoConfig as PieChartProto } from '@/components/common/VisualizationComp/ChartConfig/piechart-config'
import { protoConfig as DonutChartProto } from '@/components/common/VisualizationComp/ChartConfig/donutchart-config'
import { protoConfig as LineChartProto } from '@/components/common/VisualizationComp/ChartConfig/linechart-config'
import { protoConfig as AreaRangeChartProto } from '@/components/common/VisualizationComp/ChartConfig/arearangechart-config'
import { protoConfig as AreaChartProto } from '@/components/common/VisualizationComp/ChartConfig/areachart-config'
import { protoConfig as ScatterPlotProto } from '@/components/common/VisualizationComp/ChartConfig/scatterplot-config'
import { protoConfig as heatmapMatrixProto } from '@/components/common/VisualizationComp/ChartConfig/heatmapmatrix-config'
import { protoConfig as DagreProto } from '@/components/common/VisualizationComp/ChartConfig/dagre-config'
import { protoConfig as graphLineChartProto } from '@/components/common/VisualizationComp/ChartConfig/graph-linechart-config'
import { protoConfig as DualAxesChartProto } from '@/components/common/VisualizationComp/ChartConfig/dualaxeschart-config'
import FilterFormConfig from '@/components/common/VisualizationComp/ChartConfig/filter-form-config'
import { filterFormTypeName } from '@/config/contant'

export default {
  barChart: BarChartProto,
  pieChart: PieChartProto,
  donutChart: DonutChartProto,
  groupBarChart: GroupBarChartProto,
  stackBarChart: GroupBarChartProto,
  dualAxesChart: DualAxesChartProto,
  lineChart: LineChartProto,
  areaRangeChart: AreaRangeChartProto,
  areaChart: AreaChartProto,
  scatterplot: ScatterPlotProto,
  heatmapMatrix: heatmapMatrixProto,
  dagre: DagreProto,
  graphLineChart: graphLineChartProto,
  diffBarChart: BarChartProto,
  [filterFormTypeName]: FilterFormConfig,
} as {
  [key: string]: any
}
