/** filter from 配置 */
const protoConfig = [
  {
    label: '',
    type: 'property',
    group: 'filterData',
    children: [
      {
        type: 'GroupSelect',
        label: '起始节点',
        value: '',
        defaultValue: '',
        name: 'startNode',
        optionwithGroup: true,
        props: {
          placeholder: '请选择起始节点',
          maxLength: 50,
        },
        bind: {
          style: {
            // width: '150px'
          },
        },
      },
      {
        type: 'fleterColumnSelector',
        label: '筛选字段',
        value: [],
        props: {},
        defaultValue: [{ name: '', type: 'string', step: 1 }],
        name: 'filterColumns',
        bind: {
          style: {
            // width: '150px'
          },
        },
      },
      {
        type: 'switch',
        label: '显示标签',
        name: 'showLabel',
        defaultValue: false,
      },
      {
        type: 'AssociatedChartSelection',
        label: '关联图表',
        value: [],
        layout: 'as-fix-box',
        defaultValue: [],
        name: 'associationChart',
        showFunc: (formData: any, formItems: any[]) => {
          const find = formItems.find(
            (item) => item.name === 'associationChart'
          )
          return find?.props?.options?.length > 0
        },
        props: {
          disabled: true,
          class: 'as-charts',
          options: [
            // 示例数据
            // {
            //   label: '图表1',
            //   disabled: true,
            //   showHander: false,
            //   value: 'chart1'
            // }
          ],
        },
        bind: {
          style: {
            // width: '150px'
          },
        },
      },
    ],
  },
  {
    label: '',
    type: 'property',
    group: 'property',
    children: [
      {
        type: 'input',
        label: '标题',
        value: '',
        name: 'title',
        props: {
          placeholder: '请填写标题内容',
          maxLength: 50,
        },
        bind: {
          style: {
            // width: '150px'
          },
        },
      },
    ],
  },
]

export default protoConfig
