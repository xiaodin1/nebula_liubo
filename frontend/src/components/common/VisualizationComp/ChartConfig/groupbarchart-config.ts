/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'
import { barProtoConfig } from '@/components/common/VisualizationComp/ChartConfig/barchart-config'

const {
  titleConfig,
  rightLegendConfig,
  tooltipConfig,
  xAxis,
  yAxis,
  layoutConfig,
  themeColorDiscreteConfig,
} = commonProtoConfig

const { labelConfig } = barProtoConfig

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      ...xAxis,
      ...yAxis,
      ...labelConfig,
      ...rightLegendConfig,
      ...tooltipConfig,
      ...layoutConfig,
      ...themeColorDiscreteConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}
