/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'

const { titleConfig } = commonProtoConfig

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      {
        type: 'select',
        label: '节点样式',
        name: 'nodeType',
        defaultValue: 'rect',
        props: {
          options: [
            {
              label: '矩形',
              value: 'rect',
            },
            {
              label: '圆形',
              value: 'circle',
            },
            {
              label: '椭圆',
              value: 'ellipse',
            },
          ],
        },
      },
      // {
      //   type: 'select',
      //   label: '边类型',
      //   name: 'edgeType',
      //   defaultValue: 'polyline',
      //   props: {
      //     options: [
      //       {
      //         label: '折线',
      //         value: 'polyline',
      //       },
      //       {
      //         label: '直线',
      //         value: 'line',
      //       },
      //       // { // 连线效果有问题，先屏蔽
      //       //   label: '弧线',
      //       //   value: 'quadratic',
      //       // },
      //       // {
      //       //   label: '三次贝塞尔',
      //       //   value: 'cubic',
      //       // },
      //     ],
      //   },
      // },
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}
