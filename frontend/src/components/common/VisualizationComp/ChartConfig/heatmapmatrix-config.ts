/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'
import { cloneDeep } from 'lodash'

const {
  titleConfig,
  legendConfig,
  tooltipConfig,
  xAxis,
  yAxis,
  // layoutConfig,
  legendBasicColorConfig,
} = commonProtoConfig

const disableProtoConfig = new Set(['yAxisMax', 'yAxisMin'])
const copyYAxis = cloneDeep(yAxis)
copyYAxis.forEach((item) => {
  item.children = item.children.filter(
    (child) => !disableProtoConfig.has(child.name)
  )
})

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      ...xAxis,
      ...copyYAxis,
      ...legendConfig,
      ...tooltipConfig,
      // ...layoutConfig,
      ...legendBasicColorConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}
