/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'

const {
  titleConfig,
  labelConfig,
  rightLegendConfig,
  tooltipConfig,
  themeColorDiscreteConfig,
} = commonProtoConfig

const pieLabelConfig = [
  {
    ...labelConfig[0],
    children: [
      {
        type: 'select',
        label: '标签位置',
        name: 'labelType',
        defaultValue: 'outer',
        props: {
          options: [
            {
              label: '内部',
              value: 'inner',
            },
            {
              label: '外部',
              value: 'outer',
            },
            {
              label: '蜘蛛',
              value: 'spider',
            },
          ],
        },
      },
      ...labelConfig[0].children,
    ],
  },
]

export const pieProtoConfig = {
  labelConfig: pieLabelConfig,
}

export const protoConfig = [
  {
    type: 'fetch', // fetch 属于和获取数据挂钩的属性
    children: [
      {
        type: 'inputNumber',
        label: 'topN',
        name: 'topN',
        defaultValue: 50,
      },
    ],
  },
  {
    label: '',
    type: 'property',
    children: [
      {
        type: 'inputNumber',
        label: '最多块数',
        name: 'visibleItemsNumber',
        defaultValue: 16,
      },
      ...titleConfig,
      ...pieLabelConfig,
      ...rightLegendConfig,
      ...tooltipConfig,
      ...themeColorDiscreteConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}
