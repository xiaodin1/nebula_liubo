/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'

const { titleConfig } = commonProtoConfig

const {
  themeColorDiscreteConfig,
  lineSmoothConfig,
  // scaleConfig,
} = commonProtoConfig

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      {
        type: 'group',
        label: '网络图',
        children: [
          {
            type: 'select',
            label: '边类型',
            name: 'edgeType',
            defaultValue: 'line',
            props: {
              options: [
                {
                  label: '直线',
                  value: 'line',
                },
                {
                  label: '折线',
                  value: 'polyline',
                },
                {
                  label: '弧线',
                  value: 'quadratic',
                },
                {
                  label: '三次贝塞尔',
                  value: 'cubic',
                },
              ],
            },
          },
          {
            type: 'switch',
            label: '边方向',
            name: 'edgeDirected',
            defaultValue: false,
          },
          {
            type: 'inputNumberRange',
            label: '边阈值',
            name: 'edgeWeightRange',
            defaultValue: [0, 0],
          },
        ],
      },
      {
        type: 'group',
        label: '折线图',
        children: [
          {
            type: 'slider',
            label: '大小',
            name: 'subWidthSize',
            defaultValue: 200,
            props: {
              min: 100,
              max: 600,
              step: 50,
            },
          },
          ...lineSmoothConfig,
          // ...scaleConfig,
          ...themeColorDiscreteConfig,
        ],
      },
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}
