/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'

const {
  titleConfig,
  tooltipConfig,
  lineSmoothConfig,
  scaleConfig,
  xAxis,
  yAxis,
  labelConfig,
  themeColorDiscreteConfig,
} = commonProtoConfig

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      ...xAxis,
      ...yAxis,
      ...labelConfig,
      ...lineSmoothConfig,
      ...scaleConfig,
      ...tooltipConfig,
      ...themeColorDiscreteConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}
