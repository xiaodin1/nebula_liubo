export interface IKeyValue {
  [key: string]: any
}

export interface IDatasetList {
  dataset: Array<{
    id: number
    name: string
    tableName: string
  }>
  pipeline: Array<{
    id: number
    name: string
    tableName: string
  }>
}

interface IProtoConfig {
  type: string
  label: string
  value: any
  name: string
  props?: IKeyValue
  bind?: IKeyValue
  [key: string]: any
}

export interface IPanel {
  label: string
  type: 'property' | 'fetch'
  children: IProtoConfig[]
}

export interface IColumn {
  name: string
  desc: string
}

export interface IWidgetJson {
  isAggr: boolean
  config: {
    topN: number
    filters: Array<any>
    keys: Array<{
      col: string
      filterType: string
      sort: string
      values: string
      desc?: string
    }>
    values: Array<{
      aggregate_type: string
      col: string
      sort: string
      topN: number
    }>
    groups?: Array<{ col: string; values: any[] }>
    [key: string]: any
  }
}

export interface IWidgetInfo {
  id: string
  type: string
  widgetJson?: IWidgetJson
  tableJson?: {
    curPage: number
    pageSize: number
    name: string
    column?: IColumn[]
  }
}
