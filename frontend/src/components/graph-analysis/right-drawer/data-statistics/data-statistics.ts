/**
 *  graph analysis
 * @author zhangjing
 */

export const statisticsParameters: any = {
  1: {
    name: '平均度',
    result: ['图的平均度'],
    description: '节点度分布图',
    detail: '',
    reportIcon: true,
  },
  2: {
    name: '平均加权度',
    result: ['图的平均加权度'],
    description: '节点加权度分布图',
    detail: '',
    reportIcon: true,
  },
  3: {
    name: '图密度',
    result: [],
    description:
      '度量网络完整性，一个完全图具有所有可能连接的边，即任意两节点之间有边连接，其密度为1',
    detail: '',
    reportIcon: false,
  },
  4: {
    name: '连通分量',
    result: ['图中连通分量数目', '图中弱连通分量数目', '图中强连通分量数目'],
    description:
      '确定网络中连通分量的个数。无向图G的极大连通子图称为G的连通分量。有向图将探测强和弱连通分量，无向图将只探测弱连通分量',
    detail: '',
    reportIcon: true,
  },
  5: {
    name: '网络直径',
    result: ['图的直径'],
    description: '节点分布图',
    detail:
      '<br/>Closeness Centrality：从一个给定起始节点到其他节点的平均距离。<br/><br/> Betweenness Centrality：度量一个节点出现在网络中最短路径上的频率。<br/><br/> Eccentricity：从一个给定起始节点到距其最远节点的距离。',
    reportIcon: true,
  },
  6: {
    name: '聚类系数',
    result: ['平均聚类系数', '三角形数目'],
    description:
      '衡量节点聚集的程度，表明节点如何嵌入其邻居当中。平均聚类系数给出关于节点聚类或抱团的总体迹象',
    detail: '',
    reportIcon: true,
  },
  7: {
    name: '特征向量中心度',
    result: [],
    description: '节点特征向量中心度分布图',
    detail: '',
    reportIcon: true,
  },
  8: {
    name: '平均路径长度',
    result: ['图的平均路径长度'],
    description: '节点分布图',
    detail:
      '<br/>Closeness Centrality：从一个给定起始节点到其他节点的平均距离。<br/><br/> Betweenness Centrality：度量一个节点出现在网络中最短路径上的频率。<br/><br/> Eccentricity：从一个给定起始节点到距其最远节点的距离。',
    reportIcon: true,
  },
  9: {
    name: '社团检测',
    result: ['社团数目'],
    description: '社团分布图',
    detail: '',
    reportIcon: true,
  },
  10: {
    name: 'PageRank',
    result: [],
    description: '节点得分分布图',
    detail: '',
    reportIcon: true,
  },
}

// 页面展示顺序
export const statisticsPageOrder: any = [9, 10, 1, 2, 3, 4, 5, 6, 7, 8]

export const NetTop: any = {
  1: {
    index: 1,
    name: '平均度',
    description: '整个网络所有节点度的平均值',
    metricTag: 'MeanDegree',
    attributeName: 'Degree',
    reportIcon: true,
  },
  2: {
    index: 2,
    name: '平均加权度',
    description: '整个网络所有节点加权度（权指边的权重）的平均值',
    metricTag: 'MeanWeightedDegree',
    attributeName: 'WeightedDegree',
    reportIcon: true,
  },
  3: {
    index: 3,
    name: '图密度',
    description:
      '度量网络完整性，一个完全图具有所有可能连接的边，即任意两节点之间有边连接，其密度为1',
    metricTag: '',
    attributeName: '',
    reportIcon: false,
  },
  4: {
    index: 4,
    name: '连通分量',
    description:
      '确定网络中连通分量的个数。无向图G的极大连通子图称为G的连通分量。有向图将探测强和弱连通分量，无向图将只探测弱连通分量',
    metricTag: 'ConnectedComponents',
    attributeName: 'ConnectedID、StronglyConnectedID、WeaklyConnectedID',
    reportIcon: true,
  },
  5: {
    index: 5,
    name: '网络直径',
    description: '网络直径为图中距离最远的两个节点之间的距离',
    metricTag: 'Diameter',
    attributeName: 'BetweennessCentrality、ClosenessCentrality、Eccentricity',
    reportIcon: true,
  },
}

export const NodeCal: any = {
  1: {
    index: 6,
    name: '聚类系数',
    description:
      '衡量节点聚集的程度，表明节点如何嵌入其邻居当中。平均聚类系数给出关于节点聚类或抱团的总体迹象',
    metricTag: 'ClusteringCoefficient',
    attributeName: 'ClusteringCoefficient、NumberOfTriangles',
    reportIcon: true,
  },
  2: {
    index: 7,
    name: '特征向量中心度',
    description: '基于节点连接来衡量节点重要性的指标',
    metricTag: 'EigenvectorCentrality',
    attributeName: 'EigenvectorCentrality',
    reportIcon: true,
  },
}

export const EdgeCal: any = {
  1: {
    index: 8,
    name: '平均路径长度',
    description: '所有节点之间的平均图距离，互相连接的节点的图距离为1',
    metricTag: 'AvgDist',
    attributeName: 'BetweennessCentrality、ClosenessCentrality、Eccentricity',
    reportIcon: true,
  },
}

export const NetAlgrithm: any = {
  1: {
    index: 9,
    name: '社团检测',
    description:
      '在网络中找出拓扑关系密切的点的集合的算法，这里具体实现为标签传播（LPA）算法',
    metricTag: 'ModularityClass',
    attributeName: 'ModularityClass',
    reportIcon: true,
  },
  2: {
    index: 10,
    name: 'PageRank',
    description: '对网络中节点的重要性排序的算法',
    metricTag: 'PageRankScore',
    attributeName: 'PageRankScore',
    reportIcon: true,
  },
}
