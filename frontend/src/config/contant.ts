import { KeyValueStore } from '@/components/vueGridLayout/interfate/grid-Item-data-inteface'
import { isArray } from 'lodash'
/**
 * 可用组件列表
 */
export enum ChartDetailsEnum {
  pieChart = 'pieChart',
  donutChart = 'donutChart',
  barChart = 'barChart',
  groupBarChart = 'groupBarChart',
  stackBarChart = 'stackBarChart',
  lineChart = 'lineChart',
  areaRangeChart = 'areaRangeChart',
  scatterplot = 'scatterplot',
  areaChart = 'areaChart',
  heatmapMatrix = 'heatmapMatrix',
  parallelCoordinates = 'parallelCoordinates',
  geographicMap = 'geographicMap',
  densityDataBarChart = 'densityDataBarChart',
  densityDataAreaChart = 'densityDataAreaChart',
  dualAxesChart = 'dualAxesChart',
}
export const chart2ArrayChartType: KeyValueStore = {
  pieChart: ['pieChart', 'pieChart'],
  donutChart: ['pieChart', 'donutChart'],
  barChart: ['barChart', 'barChart'],
  groupBarChart: ['barChart', 'groupBarChart'],
  stackBarChart: ['barChart', 'stackBarChart'],
  lineChart: ['lineChart', 'lineChart'],
  areaRangeChart: ['lineChart', 'areaRangeChart'],
  scatterplot: ['scatterplot', 'scatterplot'],
  areaChart: ['areaChart', 'areaChart'],
  heatmapMatrix: ['heatmapMatrix', 'heatmapMatrix'],
  parallelCoordinates: ['parallelCoordinates', 'parallelCoordinates'],
  geographicMap: ['geographicMap', 'geographicMap'],
  dualAxesChart: ['barChart', 'dualAxesChart'],
}

export enum ChartDataTypeEnum {
  system = 'system',
  myComponent = 'myComponent',
  recommend = 'recommend',
}
/**
 * 组件对应的class 名称
 */
export const chartDetailToVisClassMaps: KeyValueStore = {
  pieChart: 'PieChart',
  donutChart: 'pieChart',
  barChart: 'barChart',
  groupBarChart: 'groupBarChart',
  stackBarChart: 'groupBarChart',
  lineChart: 'lineChart',
  areaRangeChart: 'areaRangeChart',
  scatterplot: 'scatterplot',
  areaChart: 'areaChart',
  heatmapMatrix: 'heatmapMatrix',
  parallelCoordinates: 'parallelCoordinates',
  geographicMap: 'geographicMap',
  table: 'table',
  graph: 'graph',
}

export const chartMaps = {
  text: [
    {
      name: 'text',
      title: '文本',
      chartType: 'text',
      imgSrc: '/icon/components/text.svg',
    },
  ],
  table: [
    {
      name: 'table',
      title: '表格',
      chartType: 'table',
      imgSrc: '/icon/components/table.svg',
    },
  ],
  configuration: [
    {
      name: 'configuration',
      title: '控件',
      chartType: 'configuration',
      imgSrc: '/icon/components/configuration.svg',
    },
  ],
  pieChart: [
    {
      name: 'pieChart',
      title: '饼图',
      chartType: 'pieChart',
      imgSrc: '/icon/components/pieChart.png',
      iconSrc: 'iconkeshihuatuijianbeifen',
    },

    {
      name: 'donutChart',
      title: '圆环图',
      chartType: 'pieChart',
      imgSrc: '/icon/components/donutChart.png',
      iconSrc: 'iconfsux_tubiao_huantu',
    },
  ],
  barChart: [
    {
      name: 'barChart',
      title: '柱状图',
      chartType: 'barChart',
      imgSrc: '/icon/components/barChart.png',
      iconSrc: 'icontiaoxing2',
    },
    {
      name: 'groupBarChart',
      title: '分组柱状图',
      chartType: 'barChart',
      imgSrc: '/icon/components/groupBarChart.png',
      iconSrc: 'icona-2',
    },
    {
      name: 'stackBarChart',
      title: '堆叠柱状图',
      chartType: 'barChart',
      imgSrc: '/icon/components/stackBarChart.png',
      iconSrc: 'icona-3',
    },
    {
      name: 'dualAxesChart',
      title: '柱线混合图',
      chartType: 'barChart',
      imgSrc: '/icon/components/dualAxesChart.png',
      iconSrc: 'icona-zuhetubeifen2',
    },
  ],
  lineChart: [
    {
      name: 'lineChart',
      title: '折线图',
      chartType: 'lineChart',
      imgSrc: '/icon/components/lineChart.png',
      iconSrc: 'iconzhexiantu1',
    },
    {
      name: 'areaRangeChart',
      title: '面积折线图',
      chartType: 'lineChart',
      imgSrc: '/icon/components/areaRangeChart.png',
      iconSrc: 'iconzuhetu',
    },
  ],
  scatterplot: [
    {
      name: 'scatterplot',
      title: '散点图',
      chartType: 'scatterplot',
      imgSrc: '/icon/components/scatterplot.png',
      iconSrc: 'icondianzhuang1',
    },
  ],
  areaChart: [
    {
      name: 'areaChart',
      title: '面积图',
      chartType: 'areaChart',
      imgSrc: '/icon/components/areaChart.png',
      iconSrc: 'iconmianjitu1',
    },
  ],

  parallelCoordinates: [
    {
      name: 'parallelCoordinates',
      title: '平行坐标轴',
      chartType: 'parallelCoordinates',
      imgSrc: '/icon/components/parallelCoordinates.png',
    },
  ],
  heatmapMatrix: [
    {
      name: 'heatmapMatrix',
      title: '热力图',
      chartType: 'heatmapMatrix',
      imgSrc: '/icon/components/heatmapMatrix.png',
      iconSrc: 'icona-4',
    },
  ],
  geographicMap: [
    {
      name: 'geographicMap',
      title: '地图',
      chartType: 'geographicMap',
      imgSrc: '/icon/components/map.png',
    },
  ],
  graph: [
    {
      name: 'dagre',
      title: '流程图',
      chartType: 'graph',
      imgSrc: '/icon/components/dagre.png',
    },
    {
      name: 'graphLineChart',
      title: 'graph折线图',
      chartType: 'graph',
      imgSrc: '/icon/components/graphLineChart.png',
    },
  ],
  filterForm: [
    {
      name: 'filterForm',
      title: '筛选控件',
      chartType: 'filterForm',
      imgSrc: '/icon/components/filter.png',
    },
  ],
}

export function getChartIconImage(chartType: Array<string> | string): string {
  let chartTypeUsed = chartType
  if (isArray(chartType)) {
    ;[, chartTypeUsed] = chartType
  } else if (/\|/.test(chartType)) {
    ;[, chartTypeUsed] = chartType.split('|')
  }

  const temporaryMaps = Object.values(chartMaps)
  let find: KeyValueStore = {}
  temporaryMaps.forEach((item, i) => {
    temporaryMaps[i].forEach((chartItem: any) => {
      if (chartItem.name === chartTypeUsed) {
        find = chartItem
      }
    })
  })
  return find && find.imgSrc ? find.imgSrc : ''
}

export const filterTypeName: string = 'filter'
export const tableTypeName: string = 'table'
/** 筛选器组件 */
export const filterFormTypeName: string = 'filterForm'

export const chartTypes: any = {
  barChart: {
    title: '柱状图',
    iconCharCode: '/icon/BarChartbeifen',
    iconClass: 'icon-barchart',
    active: true,
  },
  lineChart: {
    title: '折线图',
    iconCharCode: '/icon/LineChartbeifen',
    iconClass: 'icon-linechart',
    active: false,
  },
  scatterplot: {
    title: '散点图',
    iconCharCode: '/icon/Scatterplotbeifen',
    iconClass: 'icon-scatterplot',
    active: false,
  },
  areaChart: {
    title: '面积图',
    iconCharCode: '/icon/mianjitu',
    iconClass: 'icon-areabarchart',
    active: false,
  },
  // 暂时屏蔽掉
  // parallelCoordinates: {
  //   title: '平行坐标轴',
  //   iconCharCode: '/icon/parallel',
  //   iconClass: 'icon-parallelcoordinates',
  //   active: false
  // },
  heatmapMatrix: {
    title: '热力图',
    iconCharCode: '/icon/MatrixCopybeifen',
    iconClass: 'icon-heatmapmatrix',
    active: false,
  },
  pieChart: {
    title: '饼图',
    iconCharCode: '/icon/bingtu',
    iconClass: 'icon-piechart',
    active: false,
  },
  // geographicMap: {
  //   title: '地图',
  //   iconCharCode: '/icon/map',
  //   iconClass: 'icon-map',
  //   active: false,
  // },
  table: {
    title: '表格',
    iconCharCode: '/icon/table',
    iconClass: 'icon-table',
    active: false,
  },
  text: {
    title: '文本',
    iconCharCode: '/icon/text',
    iconClass: 'icon-text',
    active: false,
  },
  graph: {
    title: '图',
    iconCharCode: '/icon/graph',
    iconClass: 'icon-graph',
    active: false,
  },
  filterForm: {
    title: '筛选器',
    iconCharCode: '/icon/filter',
    iconClass: 'icon-filter',
    active: false,
  },
}

export const chartTypesSub = {
  groupBarChart: {
    title: '分组柱状图',
  },
  stackBarChart: {
    title: '堆叠柱状图',
  },
  areaRangeChart: {
    title: '面积折线图',
  },
  donutChart: {
    title: '圆环图',
  },
  table: {
    title: '表格',
  },
}

/**
 * 以下为占位
 */
export function getArrayCharts(): Array<any> {
  const result: Array<any> = []
  Object.values(chartMaps).forEach((charts: Array<any>) => {
    result.push(...charts)
  })
  return result
}

// 语义中文描述  (2021.7 整理后， 统一放在  @/components/studio/data/node-edit-panel/column-type-semantic.ts)
export const semanticMapping = {
  category: '类别',
  coordinate: '经纬度',
  time: '时间',
  city: '城市',
  ip: 'IP',
  country: '国家',
  latitude: '纬度',
  longitude: '经度',
  province: '省份',
  postcode: '邮编',
  url: 'URL',
  httpstatus: 'HTTP状态码',
  email: '邮件地址',
  telephone: '电话',
  id: '身份证',
  passport: '护照',
  disorder: '无序',
  order: '有序',
  null: '无',
}

/**
 * 获取graph配置信息
 * @param data 绑定数据 参数参照vis-chart-plus { categories: Array<any>, edges: Array<any>, links: Array<any>, nodes: Array<any>
 * @param size 尺寸 [number, number]
 * @param dataSetId 数据集名称，多个graph出现时请确保唯一
 */
export const getDefaultGraphConfig = (
  data: any,
  size: [number, number],
  dataSetId: string,
  nodeLabelValue = '',
  nodeEncodingValue = ''
) => {
  return {
    // 传入graph的配置参数，除了标注有“必需”的其余均可没有
    view: {
      size, // 必需,netV的canvas最小宽高是750，小于750可能会显示不全
      chartBackground: '#FFFFFF',
    },
    label: {
      labelIsShow: true,
      // labelTextMaxlength: 12,
      labelFontSize: 12,
      labelFontFamily: 'Arial',
      labelFontColor: 'rgb(55,59,82)',
    },
    title: {
      title: null,
      titleBackground: '#ffffff',
      titleFontColor: '#000000',
      titleFontFamily: 'Arial',
      titleFontSize: 16,
      titleIsShow: false,
      titlePosition: 'top',
      titleTextPosition: 'center',
      titleTextMaxLength: 20,
    },
    tooltip: {
      tooltipIsShow: false,
      tooltipPaddingTop: 4,
      tooltipPaddingRight: 4,
      tooltipPaddingBottom: 4,
      tooltipPaddingLeft: 4,
      tooltipBorder: '1px solid',
      tooltipBorderRadius: 0,
      tooltipBackground: '#ccc',
      tooltipFontSize: 12,
      tooltipFontFamily: 'Arial',
      tooltipFontColor: '#fff',
    },
    legend: {
      legendIsShow: false,
      legendPosition: 'bottomRight',
      legendAlign: 'vertical',
      legendFontSize: 10,
      legendFontFamily: 'Arial',
      legendFontColor: '#000',
      legendTextMaxLength: 6,
      legendIconWidth: 14,
      legendIconHeight: 10,
    },
    graph: {
      nodeColor: '#ff0000',
      nodeTheme: 'default',
      nodeEncodingValue,
      nodeLabelValue,
      nodeRadius: 7,
      linkColor: '#ccc',
      linkColorFrom: 'none',
      linkEncodingValue: '',
      linkWidth: 1.5,
      linkCurveness: 0,
      linkArrow: true,
      dataset: dataSetId, // 必需（可以随便起个名字，用于区分不同的数据）
      value: data, // 必需
      selectedNodes: [],
      selectedLinks: [],
      selectedPath: [], // 最短路径的node id list
      lassoEnable: false, // 套索
    },
  }
}

export const GRAPH_ARRAY: string[] = ['dagre', 'graphLineChart'] // graph 类型的chartType
export const CONFIGURATION_NAME = 'configuration'

export const GEOMAP_NAME = 'geographicMap'

// 不能进行清洗的图表类型
export const disableCleanActionChart = [
  ChartDetailsEnum.geographicMap,
  ChartDetailsEnum.densityDataBarChart,
  ChartDetailsEnum.densityDataAreaChart,
]
export const GeoMapDefaultConfig = {
  title: '', // 标题
  titleHeight: 44,
  // size: [400, 300],                  // 尺寸
  mapTheme: 'normal', // 主题
  mapCenter: [116.397428, 39.90923], // 地图中心点
  mapZoom: 10, // 地图缩放比例
  dragEnable: true, // 允许拖拽
  zoomEnable: true, // 允许缩放
  glyphLayers: [], // 各层参数，具体格式见addNewLayer
  geoLayer: [], // 各层参数，用于外部传入
  activeLayerIndex: 0, // 当前用户正在操作的layer在glyphLayers数组中的索引
  geoCombineField: [],
  labelKey: '',
  valueKey: [{ value: '', sort: 'asc', func: 'sum' }],
  value: [],
  status: {
    dragEnable: true,
    zoomEnable: true,
  },
  // isVisualization: true,
}

/** geo glyph point */
export const GeoMapPointProperty = {
  mappingType: {
    type: 'enum',
    title: '映射方式',
    default: 'none',
    values: ['none', 'country', 'china', 'zhejiang'],
  },
  encodingType: {
    type: 'enum',
    title: '编码方式',
    default: 'none',
    values: ['none', 'color', 'area'],
  },
  pointType: {
    type: 'enum',
    title: '点类型',
    default: 'RoundPoint',
    values: ['RoundPoint', 'ScatterPoint', 'PointCloud'],
  },
  pointColor: {
    type: 'color',
    title: '点颜色',
    default: '#ff0000',
  },
  pointRadius: {
    type: 'number',
    title: '点半径',
    default: 5,
    values: [1, 10000],
  },
}

export const GeoMapHeatmapProperty = {
  heatmapRadius: {
    type: 'number',
    title: '热力半径',
    default: 10,
    values: [1, 100],
  },
}

/** geo text property */
export const GeoMapTextProperty = {
  mappingType: {
    type: 'enum',
    title: '映射方式',
    default: 'none',
    values: ['none', 'country', 'china', 'zhejiang'],
  },
  textColor: {
    type: 'color',
    title: '文字颜色',
    default: '#ff0000',
  },
  textFontFamily: {
    type: 'enum',
    title: '文字字体',
    default: 'Arial',
    values: ['Arial', 'sans-serif'],
  },
  textFontSize: {
    type: 'number',
    title: '文字字号',
    default: 12,
    values: [2, 20],
  },
}
/** 可用geoMap的glyph类型 */
export enum GeoMapEnableGlyphTypes {
  point = 'point',
  heatmap = 'heatmap',
  line = 'line',
  polygon = 'polygon',
  world = 'world',
}

/** geoMap glyph配置映射 */
export const GeoMapGlyphSettingMap = {
  point: GeoMapPointProperty,
  heatmap: GeoMapHeatmapProperty,
  text: GeoMapTextProperty,
}

/** geoMap关联字段 */
export const GeoMapAllRelationKeys = [
  'mappingType',
  'encodingType',
  'pointType',
  'pointColor',
  'pointRadius',
  'valueKey',
  'labelKey',
  'polygonColor',
  'borderColor',
  'borderWidth',
  'heatmapRadius',
  'valueSeparator',
]
export const GeoMapHeatmapKeys = ['heatmapRadius', 'labelKey']
export const GeoMapPointKeys = [
  'mappingType',
  'encodingType',
  'pointType',
  'pointColor',
  'pointRadius',
  'valueKey',
  'labelKey',
]
export const GeoMapPolygonKeys = ['polygonColor', 'borderColor', 'borderWidth']
export const GeoMapWorldKeys = ['borderColor', 'valueSeparator']
export const GeoMapLineKeys = ['borderColor', 'borderWidth']
