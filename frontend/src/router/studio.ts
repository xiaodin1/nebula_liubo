/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { RouteConfig } from 'vue-router'

// 在 studioSideMenu 设置侧边栏的显示

export const DATA_PATH = 'data'
export const MODEL_PATH = 'model'
export const VISUALIZATION_PATH = 'visualization'
export const PREVIEW_PATH = 'preview'
export const GRAPH_PATH = 'graph/:taskid'
export const TRAINNING_PATH = 'trainning'
export const GRAPH_ANALYSIS_PATH = 'graph-analysis'
export const MODELTRAINNING_PATH = 'create'
export const DATASOURCE_PATH = 'source'
export const MODELCONFIG_PATH = 'config'
export const MODELACCESS_PATH = 'report'

export const CHILD_ROUTE_METADATA: {
  [path: string]: {
    displayedAsViewButton: boolean
    icon: string
  }
} = {
  [DATA_PATH]: {
    displayedAsViewButton: true,
    icon: 'iconmoxingfenxi',
  },
  [VISUALIZATION_PATH]: {
    displayedAsViewButton: true,
    icon: 'iconkeshifenxi',
  },
  [GRAPH_ANALYSIS_PATH]: {
    displayedAsViewButton: true,
    icon: 'icontufenxi',
  },
  [PREVIEW_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
  [GRAPH_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
  [TRAINNING_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
  [MODELTRAINNING_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
  [DATASOURCE_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
  [MODELCONFIG_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
  [MODELACCESS_PATH]: {
    displayedAsViewButton: false,
    icon: 'iconicon-chakan-copy',
  },
}

const STUDIO_ROUTE_CONFIG: RouteConfig = {
  path: '/project/:id',
  redirect: '/project/:id/data',
  name: 'Studio',
  component: () =>
    import(
      /* webpackChunkName: "studio-view" */ '../views/studio/StudioView.vue'
    ),
  children: [
    {
      path: DATA_PATH,
      name: '数据视图',
      component: () =>
        import(
          /* webpackChunkName: "data-view" */ '../views/studio/DataView.vue'
        ),
    },
    {
      path: VISUALIZATION_PATH,
      name: '系统构建',
      component: () =>
        import(
          /* webpackChunkName: "visualization-view" */ '../views/studio/VisualizationView.vue'
        ),
    },
    {
      path: GRAPH_ANALYSIS_PATH,
      name: '图分析视图',
      component: () =>
        import(
          /* webpackChunkName: "graphAnalysis-view" */ '../views/studio/GraphAnalysisView.vue'
        ),
    },
    // {
    //   path: PREVIEW_PATH,
    //   name: '预览',
    //   component: () =>
    //     import(
    //       /* webpackChunkName: "preview-view" */ '../views/studio/PreviewView.vue'
    //     ),
    // },
    {
      path: GRAPH_PATH,
      name: '图构建',
      component: () =>
        import(
          /* webpackChunkName: "graph-view" */ '../views/studio/GraphView.vue'
        ),
    },
    {
      path: TRAINNING_PATH,
      name: '模型训练',
      // meta: {
      //   keepAlive: true
      // },
      component: () =>
        import(
          /* webpackChunkName: "trainning-view" */ '../views/studio/TrainningView.vue'
        ),
    },
    {
      path: MODELTRAINNING_PATH,
      name: '模型训练-创建模型',
      component: () =>
        import(
          /* webpackChunkName: "trainning-view" */ '../components/studio/data/model/ModelTrainning.vue'
        ),
    },
    {
      path: DATASOURCE_PATH,
      name: '模型训练-数据表',
      component: () =>
        import(
          /* webpackChunkName: "trainning-view" */ '../components/studio/data/model/DataSource.vue'
        ),
    },
    {
      path: MODELCONFIG_PATH,
      name: '模型训练-查看配置',
      component: () =>
        import(
          /* webpackChunkName: "trainning-view" */ '../components/studio/data/model/ModelConfig.vue'
        ),
    },
    {
      path: MODELACCESS_PATH,
      name: '模型训练-评估报告',
      component: () =>
        import(
          /* webpackChunkName: "trainning-view" */ '../components/studio/data/model/ModelReport.vue'
        ),
    },
  ],
}

export default STUDIO_ROUTE_CONFIG
