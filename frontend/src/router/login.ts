import { RouteConfig } from 'vue-router'

const LOGIN_ROUTE_CONFIG: RouteConfig = {
  path: '/',
  redirect: '/management',
  name: 'Login',
  component: () =>
    import('../views/login/LoginView.vue'),
  children: [
    {
      path: '/login',
      name: '登录',
      component: () => import('../components/login/LoginForm.vue'),
    },
    {
      path: '/register',
      name: '注册',
      component: () => import('../components/login/RegisterForm.vue')
    }
  ]
}

export default LOGIN_ROUTE_CONFIG