import { RouteConfig } from 'vue-router'

const USER_ROUTE_CONFIG: RouteConfig = {
  path: '/user',
  redirect: '/user/infos',
  name: 'User',
  component: () => import('../views/user/UserView.vue'),
  children: [
    {
      path: 'infos',
      name: '用户信息',
      component: () => import('../components/account/PersonalInformation.vue'),
    },
    {
      path: 'security-settings',
      name: '账号安全',
      component: () => import('../components/account/SecuritySettings.vue'),
    },
    {
      path: 'settings',
      name: '账号设置',
      component: () => import('../components/account/SecuritySettings.vue'),
    },
    {
      path: 'messages',
      name: '消息中心',
      component: () => import('../components/message/MessageCenter.vue'),
    },
    {
      path: 'messages-settings',
      name: '消息偏好设置',
      component: () => import('../components/account/MessageSettings.vue'),
    },
  ],
}

export default USER_ROUTE_CONFIG
