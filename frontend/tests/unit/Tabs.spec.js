import { shallowMount, createLocalVue } from '@vue/test-utils'
import StyleLayout from '../../src/components/graph-analysis/Tabs.vue'

import { Vue } from 'vue-property-decorator'
import {
  Input,
  Tooltip,
  Icon,
  Popover,
} from 'ant-design-vue'

Vue.component(
  'AIconFont',
  Icon.createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_1566179_nlydkel94un.js',
  })
)

const localVue = createLocalVue()

localVue.use(Icon)
localVue.use(Tooltip)
localVue.use(Input)
localVue.use(Popover)

const tabData =  [
  {id: 1693, name: "节点二分类"},
  {id: 1694, name: "单类节点2"},
  {id: 1695, name: "演员"},
]

// 默认进入 激活第一个 tab
const defaultProps = {
  value: tabData[0].id, // 当前 激活 tab, 默认第一个
  tabs: tabData, // 所有 tabs
  minWidth: 40, // tab 最小宽度
  maxWidth: 235, // tab 最大宽度
  gap: 7, // 暂时未知
  insertToAfter: false, // 添加 tab 到当前 激活tab 后,
  allowedEdit: true, // 可编辑
  projectId: 0 // 当前项目Id
}

describe('StyleLayout.vue', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = shallowMount(StyleLayout, {
      propsData: {
        ...defaultProps
      },

      data() {
        return {
          allowedDelete: true, // 可删除
          tabWidth: 0, // tab 宽度 需要实时计算
          timer: null, // 定时器
          editPipelineId: null, // 添加编辑功能
          activeEditPipelineName: '' // 编辑的 tab name
        }
      },
      localVue,
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('渲染所有 tab:', async () => {
    const tabs = wrapper.findAll('span.tabs-label-text')
    expect(tabs.length).toBe(tabData.length)
    for (let index = 0; index < tabs.length; index++) {
      const tab = tabs.at(index)
      expect(tab.text()).toBe(tabData[index].name)
    }
  })

  it('默认激活第一个 tab:', async () => {
    const defaultActiveTab = wrapper.find('.tabs-content .active .tabs-label-text')
    expect(defaultActiveTab.text()).toBe(tabData[0].name)
  })


  it('点击默认之外的 tab:', async () => {
    const tabs = wrapper.findAll('span.tabs-label-text')
    for (let index = 1; index < tabs.length; index++) {
      await wrapper.setProps({ value: tabData[index].id })
      const activeTab = wrapper.find('.tabs-content .active .tabs-label-text')
      expect(activeTab.text()).toBe(tabData[index].name)
    }
  })

  it('编辑激活的 tab:', async () => {
    const activeTab = wrapper.find('.tabs-content .active .tabs-label-text')
    await activeTab.trigger('click')
    const afterActiveTab = wrapper.find('.tabs-content .active .tabs-label-text')
    expect(afterActiveTab.exists()).toBe(false)
    // 编辑状态的 tab
    const editActiveTab = wrapper.find('.tabs-content .active .tabs-label-edit')
    expect(editActiveTab.exists()).toBe(true)
  })

})
