import { htmlEncode, htmlDecode } from '../../src/util/util.ts'

const testData = `<a> test & 'test for htmlEnCode' & 'decode'
  "second line" for 
</a>`

const testDataEncode = "&lt;a&gt;&nbsp;test&nbsp;&amp;&nbsp;&#39;test&nbsp;for&nbsp;htmlEnCode&#39;&nbsp;&amp;&nbsp;&#39;decode&#39;<br/>&nbsp;&nbsp;&quot;second&nbsp;line&quot;&nbsp;for&nbsp;<br/>&lt;/a&gt;"

describe('test htmlEncode', () => {
  it('test htmlEncode', () => {
    expect(htmlEncode(testData)).toBe(testDataEncode)
  })
  
  it('test htmlDecode', () => {
    expect(htmlDecode(testDataEncode)).toBe(testData)
  })
})