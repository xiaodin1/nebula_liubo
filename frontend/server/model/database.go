package model

// Database denotes a database registered at Nebula platform
type Database struct {
	ID   uint
	Name string
}
