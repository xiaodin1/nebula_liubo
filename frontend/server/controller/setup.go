package controller

import (
	"git.zjvis.org/nebula-platform/nebula/server/controller/data"
	"github.com/gin-gonic/gin"
)

// Setup installs routes and returns a gin server
func Setup() *gin.Engine {
	r := gin.Default()
	data.Mount(r.Group("/api"))
	return r
}
