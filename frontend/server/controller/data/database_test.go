package data_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"git.zjvis.org/nebula-platform/nebula/server/controller"
	"git.zjvis.org/nebula-platform/nebula/server/model"
)

type DatabaseList struct {
	Data struct {
		Items []model.Database `json:"items"`
	} `json:"data"`
}

func TestGetAllDatabases(t *testing.T) {
	r := controller.Setup()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/data/databases", nil)
	r.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)

	var dbList DatabaseList
	if err := json.Unmarshal(w.Body.Bytes(), &dbList); err != nil {
		t.Fatalf("unable to parse the result: %s", err)
	}
}
