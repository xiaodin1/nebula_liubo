package data

import (
	"github.com/gin-gonic/gin"
)

// Mount adds routes in package data to the given router group
func Mount(router *gin.RouterGroup) {
	data := router.Group("/data")
	data.GET("/databases", GetAllDatabases)
}
